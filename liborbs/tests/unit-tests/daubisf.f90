program test_daubisf
  use yaml_output
  use f_unittests
  use wrapper_MPI

  implicit none

  type(mpi_environment) :: env
  
  call mpiinit()

  env = mpi_environment_comm()  

  if (env%iproc == 0) then
     call f_lib_initialize()

     call yaml_sequence_open("isf_to_daub <-> daub_to_isf")
     call run(periodicBC_nohybrid)
     call run(periodicBC_hybrid)
     call run(surfaceBC)
     call run(wireBC)
     call run(freeBC)
     call yaml_sequence_close()

     call yaml_sequence_open("isf_to_daub <-> daub_to_isf_locham")
     call run(locham_periodicBC_nohybrid)
     call run(locham_periodicBC_hybrid)
     call run(locham_surfaceBC)
     call run(locham_wireBC)
     call run(locham_freeBC)
     call yaml_sequence_close()

     call yaml_sequence_open("isf_to_daub and kinetic operator")
     call run(kinetic_periodicBC_nohybrid)
     call run(kinetic_periodicBC_hybrid)
     call run(kinetic_surfaceBC)
     call run(kinetic_wireBC)
     call run(kinetic_freeBC)
     call yaml_sequence_close()

     call f_lib_finalize()
  end if

  call release_mpi_environment(env)
  call mpifinalize()

contains

  subroutine check(lr, psiref)
    use f_precisions, only: f_long
    use liborbs_precisions
    use locregs
    use liborbs_workarrays
    use dynamic_memory
    use at_domain
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    real(wp), dimension(:,:,:), intent(in) :: psiref

    type(workarr_sumrho) :: w
    real(wp), dimension(:), allocatable :: psi
    real(wp), dimension(:,:,:), allocatable :: psiref_copy
    real(wp), dimension(:,:,:), allocatable :: psir
    real(wp) :: ref
    integer(f_long) :: memwork

    call initialize_work_arrays_sumrho(lr, w)
    call memspace_work_arrays_sumrho(lr, memwork)
    select case(domain_geocode(lr%mesh_coarse%dom))
    case('F')
       call compare(memwork, int(182782, f_long), "memory")
    case('W')
       call compare(memwork, int(246796, f_long), "memory")
    case('S')
       call compare(memwork, int(139040, f_long), "memory")
    case('P')
       if (lr%hybrid_on) then
          call compare(memwork, int(76675, f_long), "memory")
       else
          call compare(memwork, int(84480, f_long), "memory")
       end if
    end select

    psi = f_malloc0(array_dim(lr), id = "psi")

    ! Run the Daubechie transformation once to
    ! filter out non representable frequencies.
    psiref_copy = f_malloc(psiref, id = "psiref_copy")
    call isf_to_daub(lr, w, psiref_copy, psi)
    call f_free(psiref_copy)
    ref = sum(psi ** 2)
    call compare(ref, 1._wp, "norm in Daubechie", tol = 3.d-5)
    
    psir = f_malloc0(lr%mesh%ndims, id = "psir")
    call daub_to_isf(lr, w, psi, psir)
    call compare(sum(psir ** 2), ref, "norm in ISF", tol = 8.d-11)
    call compare(psir, psiref, "point to point from input", tol = 3.d-4)

    psi = 0._gp
    call isf_to_daub(lr, w, psir, psi)
    call compare(sum(psi ** 2), ref, "norm in Daubechie roundtrip", tol = 2.d-10)

    call f_free(psir)
    call f_free(psi)
    call deallocate_work_arrays_sumrho(w)
  end subroutine check

  subroutine check_locham(lr, psiref)
    use f_precisions, only: f_long
    use liborbs_precisions
    use locregs
    use locreg_operations
    use liborbs_workarrays
    use dynamic_memory
    use at_domain
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    real(wp), dimension(:,:,:), intent(in) :: psiref

    type(workarr_locham) :: w
    type(workarr_sumrho) :: ws
    real(wp), dimension(:,:,:), allocatable :: psiref_copy
    real(wp), dimension(:), allocatable :: psi
    real(wp), dimension(:), allocatable :: psir
    real(wp) :: ref
    integer, parameter :: nspinor = 2
    integer :: i
    integer(f_long) :: memwork

    call initialize_work_arrays_locham(lr, int(nspinor), w)
    call memspace_work_arrays_locham(lr, memwork)
    select case(domain_geocode(lr%mesh_coarse%dom))
    case('F')
       call compare(memwork, int(196107, f_long), "memory")
    case('W')
       call compare(memwork, int(493592, f_long), "memory")
    case('S')
       call compare(memwork, int(278080, f_long), "memory")
    case('P')
       if (lr%hybrid_on) then
          call compare(memwork, int(88485, f_long), "memory")
       else
          call compare(memwork, int(168960, f_long), "memory")
       end if
    end select

    psi = f_malloc0(array_dim(lr) * nspinor, id = "psi")
    psir = f_malloc(lr%mesh%ndim * nspinor, id = "psir")

    ! Run the Daubechie transformation once to
    ! filter out non representable frequencies.
    do i = 1, nspinor
       psiref_copy = f_malloc(psiref, id = "psiref_copy")
       call initialize_work_arrays_sumrho(lr, ws)
       call isf_to_daub(lr, ws, psiref_copy, psi(1 + (i-1) * array_dim(lr)))
       call deallocate_work_arrays_sumrho(ws)
       call f_free(psiref_copy)
    end do
    ref = sum(psi ** 2) / nspinor
    call compare(ref, 1._wp, "norm in Daubechie", tol = 3.d-5)

    call daub_to_isf_locham(nspinor, lr, w, psi, psir)
    call compare(sum(psir ** 2) / nspinor, ref, "norm in ISF", tol = 8.d-11)
    !call compare(psir, psiref, "point to point from input", tol = 7.d-2)

    call f_free(psir)
    call f_free(psi)
    call deallocate_work_arrays_locham(w)
  end subroutine check_locham

  subroutine periodicBC_nohybrid(label)
    use liborbs_precisions
    use locregs
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Periodic BC, no hybrid"

    call setup_periodicBC(data, .false.)
    call setup_psir(data)
    call check(data%lr, data%psir)
    call setup_clean(data)
  end subroutine periodicBC_nohybrid

  subroutine periodicBC_hybrid(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Periodic BC, hybrid"

    call setup_periodicBC(data, .true.)
    call setup_psir(data)
    call check(data%lr, data%psir)
    call setup_clean(data)
  end subroutine periodicBC_hybrid

  subroutine locham_periodicBC_nohybrid(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Periodic BC, no hybrid"

    call setup_periodicBC(data, .false.)
    call setup_psir(data)
    call check_locham(data%lr, data%psir)
    call setup_clean(data)
  end subroutine locham_periodicBC_nohybrid

  subroutine locham_periodicBC_hybrid(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Periodic BC, hybrid"

    call setup_periodicBC(data, .true.)
    call setup_psir(data)
    call check_locham(data%lr, data%psir)
    call setup_clean(data)
  end subroutine locham_periodicBC_hybrid

  subroutine surfaceBC(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Surface BC"

    call setup_surfaceBC(data)
    call setup_psir(data)
    call check(data%lr, data%psir)
    call setup_clean(data)
  end subroutine surfaceBC

  subroutine locham_surfaceBC(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Surface BC"

    call setup_surfaceBC(data)
    call setup_psir(data)
    call check_locham(data%lr, data%psir)
    call setup_clean(data)
  end subroutine locham_surfaceBC

  subroutine wireBC(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Wire BC"

    call setup_wireBC(data)
    call setup_psir(data)
    call check(data%lr, data%psir)
    call setup_clean(data)
  end subroutine wireBC

  subroutine locham_wireBC(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Wire BC"

    call setup_wireBC(data)
    call setup_psir(data)
    call check_locham(data%lr, data%psir)
    call setup_clean(data)
  end subroutine locham_wireBC

  subroutine freeBC(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Free BC"

    call setup_freeBC(data)
    call setup_psir(data)
    call check(data%lr, data%psir)
    call setup_clean(data)
  end subroutine freeBC

  subroutine locham_freeBC(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Free BC"

    call setup_freeBC(data)
    call setup_psir(data)
    call check_locham(data%lr, data%psir)
    call setup_clean(data)
  end subroutine locham_freeBC

  subroutine check_kinetic(lr, psiref, nspinor)
    use f_precisions, only: f_long
    use liborbs_precisions
    use locregs
    use locreg_operations
    use liborbs_workarrays
    use dynamic_memory
    use at_domain
    use f_utils
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    real(wp), dimension(:), intent(in) :: psiref
    integer, intent(in), optional :: nspinor

    type(workarr_locham) :: w
    real(wp), dimension(:,:), allocatable :: hpsi
    real(wp), dimension(:), allocatable :: psi, psir
    real(gp) :: ekin
    real(wp), dimension(6) :: k_strten, strtenref
    integer :: nspinor_

    nspinor_ = 1
    if (present(nspinor)) nspinor_ = nspinor
    hpsi = f_malloc0((/ array_dim(lr), nspinor_ /), id = "hpsi")
    psi = f_malloc(psiref, id = "psi")
    psir = f_malloc(lr%mesh%ndim * nspinor_, id = "psir")

    if (nspinor_  == 2) then
       call initialize_work_arrays_locham(lr, nspinor_, w)
       call daub_to_isf_locham(nspinor_, lr, w, psi, psir) ! Must be called before
       ! to initialise w%x_c
       call isf_to_daub_kinetic(lr, 0.1_gp, 0.2_gp, 0.3_gp, nspinor_, w, psir, hpsi, ekin, k_strten)
       select case(domain_geocode(lr%mesh_coarse%dom))
       case('P')
          call compare(sum(hpsi**2), 8.281441277094981_gp, "at k non null", tol = 1.d-11)
          call compare(ekin, 1.56954653080123_gp, "kinetic energy at k", tol = 1.d-11)
       case('S')
          call compare(sum(hpsi**2), 8.280122032757886_gp, "at k non null", tol = 1.d-11)
          call compare(ekin, 1.569576189059522_gp, "kinetic energy at k", tol = 1.d-11)
       case('W')
          call compare(sum(hpsi**2), 8.25326546694563_gp, "at k non null", tol = 1.d-11)
          call compare(ekin, 1.570659096688507_gp, "kinetic energy at k", tol = 1.d-11)
       end select
       if (domain_geocode(lr%mesh%dom) == 'P') then
          strtenref = (/ 0.50463077843791382_gp, 0.51998794907550039_gp, 0.54492780328782264_gp, &
               0._gp, 0._gp, 0._gp /)
          call compare(k_strten, strtenref, "stress tensor at k", tol = 1.d-11)
       end if
       call deallocate_work_arrays_locham(w)

       psi = psi * sqrt(real(nspinor_))
       nspinor_ = 1
       call f_zero(hpsi)
    end if

    call initialize_work_arrays_locham(lr, nspinor_, w)
    call daub_to_isf_locham(nspinor_, lr, w, psi, psir) ! Must be called before
    ! to initialise w%x_c
    call isf_to_daub_kinetic(lr, 0._gp, 0._gp, 0._gp, nspinor_, w, psir, hpsi, ekin, k_strten)
    select case(domain_geocode(lr%mesh_coarse%dom))
    case('P')
       call compare(sum(hpsi(:,1)**2), 7.786630248700818_gp, "at gamma", tol = 3.d-7)
       call compare(ekin, 1.49954650689085_gp, "kinetic energy at gamma", tol = 6.d-8)
    case('S')
       call compare(sum(hpsi(:,1)**2), 7.785305215938352_gp, "at gamma", tol = 1.d-11)
       call compare(ekin, 1.499576188511163_gp, "kinetic energy at gamma", tol = 1.d-11)
    case('W')
       call compare(sum(hpsi(:,1)**2), 7.758287281410901_gp, "at gamma", tol = 1.d-11)
       call compare(ekin, 1.500660285737144_gp, "kinetic energy at gamma", tol = 1.d-11)
    case('F')
       call compare(sum(hpsi(:,1)**2), 7.751614036578002_gp, "at gamma", tol = 1.d-11)
       call compare(ekin, 1.500854753589619_gp, "kinetic energy at gamma", tol = 1.d-11)
    end select
    if (domain_geocode(lr%mesh%dom) == 'P') then
       strtenref = (/ 0.49963076329462819_gp, 0.49998793979504047_gp, 0.49992780380118096_gp, &
            0._gp, 0._gp, 0._gp /)
       call compare(k_strten, strtenref, "stress tensor at gamma", tol = 2.d-8)
    end if
    call deallocate_work_arrays_locham(w)

    call f_free(psi)
    call f_free(psir)
    call f_free(hpsi)
  end subroutine check_kinetic

  subroutine kinetic_periodicBC_nohybrid(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Periodic BC, no hybrid"

    call setup_periodicBC(data, .false.)
    call setup_psi(data, 2)
    call check_kinetic(data%lr, data%psi, 2)
    call setup_clean(data)
  end subroutine kinetic_periodicBC_nohybrid

  subroutine kinetic_periodicBC_hybrid(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Periodic BC, hybrid"

    call setup_periodicBC(data, .true.)
    call setup_psi(data, 1)
    call check_kinetic(data%lr, data%psi, 1)
    call setup_clean(data)
  end subroutine kinetic_periodicBC_hybrid

  subroutine kinetic_surfaceBC(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Surface BC"

    call setup_surfaceBC(data)
    call setup_psi(data, 2)
    call check_kinetic(data%lr, data%psi, 2)
    call setup_clean(data)
  end subroutine kinetic_surfaceBC

  subroutine kinetic_wireBC(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Wire BC"

    call setup_wireBC(data)
    call setup_psi(data, 2)
    call check_kinetic(data%lr, data%psi, 2)
    call setup_clean(data)
  end subroutine kinetic_wireBC

  subroutine kinetic_freeBC(label)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Free BC"

    call setup_freeBC(data)
    call setup_psi(data, 1)
    call check_kinetic(data%lr, data%psi, 1)
    call setup_clean(data)
  end subroutine kinetic_freeBC

end program test_daubisf
