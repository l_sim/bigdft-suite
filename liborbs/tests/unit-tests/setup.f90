module setup
  use liborbs_precisions
  use liborbs_potentials
  use locregs
  use locreg_operations

  implicit none

  private

  public :: setup_periodicBC
  public :: setup_surfaceBC
  public :: setup_wireBC
  public :: setup_freeBC
  public :: setup_fromFile

  public :: setup_llr

  public :: setup_psir
  public :: setup_psi
  public :: setup_tmb

  public :: setup_confdata

  public :: setup_clean

  type, public :: setup_data
     type(locreg_descriptors) :: lr
     type(locreg_descriptors), dimension(:), pointer :: llr => null()
     real(wp), dimension(:,:,:), pointer :: psir => null()
     real(wp), dimension(:), pointer :: psi => null()
     real(wp), dimension(:), pointer :: tmb => null()
     type(confpot_data) :: confdata
  end type setup_data

contains

  subroutine setup_clean(data)
    use dynamic_memory
    implicit none
    type(setup_data), intent(inout) :: data

    integer :: i

    call deallocate_locreg_descriptors(data%lr)
    if (associated(data%llr)) then
       do i = 1, size(data%llr)
          call deallocate_locreg_descriptors(data%llr(i))
       end do
       deallocate(data%llr)
    end if
    call f_free_ptr(data%psir)
    call f_free_ptr(data%psi)
    call f_free_ptr(data%tmb)
  end subroutine setup_clean

  subroutine setup_periodicBC(data, hybrid, acell, ndims)
    use liborbs_precisions
    use at_domain
    implicit none
    type(setup_data), intent(out) :: data
    logical, intent(in) :: hybrid
    real(gp), dimension(3), intent(in), optional :: acell
    integer, dimension(3), intent(in), optional :: ndims

    call setup_lr(data, (/ PERIODIC_BC, PERIODIC_BC, PERIODIC_BC /), acell, ndims, hybrid)
  end subroutine setup_periodicBC

  subroutine setup_surfaceBC(data, acell, ndims)
    use liborbs_precisions
    use at_domain
    implicit none
    type(setup_data), intent(out) :: data
    real(gp), dimension(3), intent(in), optional :: acell
    integer, dimension(3), intent(in), optional :: ndims

    call setup_lr(data, (/ PERIODIC_BC, FREE_BC, PERIODIC_BC /), acell, ndims)
  end subroutine setup_surfaceBC

  subroutine setup_wireBC(data, acell, ndims)
    use liborbs_precisions
    use at_domain
    implicit none
    type(setup_data), intent(out) :: data
    real(gp), dimension(3), intent(in), optional :: acell
    integer, dimension(3), intent(in), optional :: ndims

    call setup_lr(data, (/ FREE_BC, FREE_BC, PERIODIC_BC /), acell, ndims)
  end subroutine setup_wireBC

  subroutine setup_freeBC(data, acell, ndims)
    use liborbs_precisions
    use at_domain
    implicit none
    type(setup_data), intent(out) :: data
    real(gp), dimension(3), intent(in), optional :: acell
    integer, dimension(3), intent(in), optional :: ndims

    call setup_lr(data, (/ FREE_BC, FREE_BC, FREE_BC /), acell, ndims)
  end subroutine setup_freeBC

  subroutine setup_lr(data, peri, acell, ndims, hybrid)
    use liborbs_precisions
    use f_enums
    use f_precisions, only: f_byte
    use at_domain
    use locregs
    use dynamic_memory
    use f_unittests
    implicit none
    type(setup_data), intent(inout) :: data
    type(f_enumerator), dimension(3), intent(in) :: peri
    logical, intent(in), optional :: hybrid
    real(gp), dimension(3), intent(in), optional :: acell
    integer, dimension(3), intent(in), optional :: ndims

    type(domain) :: dom
    integer, dimension(3) :: ndims_
    real(gp), dimension(3) :: hgridsh
    integer, dimension(3), parameter :: ndims0 = (/ 20, 24, 22 /)
    real(gp), dimension(3), parameter :: acell0 = (/ 4.1_gp, 4.9_gp, 4.5_gp /)
    integer, dimension(2, 3) :: nboxc, nboxf
    logical(f_byte), dimension(:,:,:), allocatable :: logrid_c,logrid_f
    integer :: i, j, k

    if (present(acell)) then
       dom = domain_new(ATOMIC_UNITS, peri, acell = acell)
    else
       dom = domain_new(ATOMIC_UNITS, peri, acell = acell0)
    end if
    if (present(ndims)) then
       ndims_ = ndims
    else
       ndims_ = ndims0
    end if
    nboxc(1, :) = 0
    where (peri == FREE_BC)
       nboxc(2, :) = ndims_
    elsewhere
       nboxc(2, :) = ndims_ - 1
    end where
    nboxf(1, :) = nboxc(2,:) / 2 - 2
    nboxf(2, :) = nboxc(2,:) / 2 + 2
    if (present(acell)) then
       hgridsh = acell / 2. / ndims_
    else
       hgridsh = acell0 / 2. / ndims_
    end if

    data%lr = locreg_null()
    if (present(hybrid)) then
       call init_lr(data%lr, dom, hgridsh, nboxc, nboxf, hybrid)
       call compare(data%lr%hybrid_on, hybrid, "hybrid is as desired")
    else
       call init_lr(data%lr, dom, hgridsh, nboxc, nboxf, .false.)
    end if

    logrid_c = f_malloc(data%lr%mesh_coarse%ndims, id = "logrid_c")
    logrid_f = f_malloc(data%lr%mesh_coarse%ndims, id = "logrid_f")
    do k = 1, data%lr%mesh_coarse%ndims(3)
       do j = 1, data%lr%mesh_coarse%ndims(2)
          do i = 1, data%lr%mesh_coarse%ndims(1)
             logrid_c(i, j, k) = .true.
             logrid_f(i, j, k) = &
                  i-1 >= nboxf(1, 1) .and. i-1 < nboxf(2, 1) .and. &
                  j-1 >= nboxf(1, 2) .and. j-1 < nboxf(2, 2) .and. &
                  k-1 >= nboxf(1, 3) .and. k-1 < nboxf(2, 3)
          end do
       end do
    end do
    call init_glr_from_grids(data%lr, logrid_c, logrid_f, .true., warn = .true.)
    call f_free(logrid_c)
    call f_free(logrid_f)
  end subroutine setup_lr

  subroutine setup_llr(data, env)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    use wrapper_MPI
    implicit none
    type(setup_data), intent(inout) :: data
    type(mpi_environment), intent(in), optional :: env

    integer :: iproc, nproc
    integer, parameter :: nlr = 3
    logical, dimension(nlr) :: bounds
    integer :: i, j, ip
    integer, dimension(:,:), allocatable :: lr_par
    integer, dimension(:), allocatable :: lr_ovr

    iproc = 0
    if (present(env)) iproc = env%iproc
    nproc = 1
    if (present(env)) nproc = env%nproc

    allocate(data%llr(nlr))
    lr_par = f_malloc0((/ nlr / nproc + 1, nproc /), id = "lr_par")
    lr_ovr = f_malloc0(0, id = "lr_ovr")
    do i = 1, nlr
       bounds(i) = (modulo(i - iproc - 1, nproc) == 0)
       if (bounds(i)) then
          ip = modulo(i - 1, nproc) + 1
          do j = 1, size(lr_par, 1)
             if (lr_par(j, ip) == 0) then
                lr_par(j, ip) = i
                exit
             end if
          end do
       end if
       data%llr(i)%locrad = data%lr%mesh_coarse%hgrids(1) * data%lr%mesh_coarse%ndims(1) &
            * 0.75_gp / nlr
       data%llr(i)%locregCenter = data%lr%mesh_coarse%hgrids * data%lr%mesh_coarse%ndims &
            * (i - 0.5_gp) / nlr
    end do
    
    call init_llr_from_spheres(iproc, nproc, data%lr, nlr, data%llr,&
         0, bounds, size(lr_par, 1), lr_par, lr_ovr, env%mpi_comm)
    call f_free(lr_ovr)
    call f_free(lr_par)
  end subroutine setup_llr

  subroutine setup_psir(data, ilr)
    use liborbs_precisions
    use dynamic_memory
    use at_domain
    implicit none
    type(setup_data), intent(inout), target :: data
    integer, intent(in), optional :: ilr

    integer :: i, j, k
    real(gp), dimension(3) :: r, c
    type(locreg_descriptors), pointer :: lr

    lr => data%lr
    if (present(ilr)) lr => data%llr(ilr)

    c = real(lr%mesh%ndims / 2) * lr%mesh%hgrids
    data%psir = f_malloc_ptr(lr%mesh%ndims, id = "psir")
    do k = 1, lr%mesh%ndims(3)
       do j = 1, lr%mesh%ndims(2)
          do i = 1, lr%mesh%ndims(1)
             r = real((/ i - 1, j - 1, k - 1 /)) * lr%mesh%hgrids
             data%psir(i, j, k) = exp(-distance(lr%mesh%dom, r, c) ** 2)
          end do
       end do
    end do
    data%psir = data%psir / sqrt(sum(data%psir ** 2))
  end subroutine setup_psir

  subroutine setup_psi(data, ncplx)
    use liborbs_precisions
    use dynamic_memory
    use liborbs_workarrays
    implicit none
    type(setup_data), intent(inout) :: data
    integer, intent(in), optional :: ncplx

    type(workarr_sumrho) :: w
    real(wp), dimension(:,:,:), allocatable :: psiref_copy
    integer :: i, ncplx_

    ncplx_ = 1
    if (present(ncplx)) ncplx_ = ncplx

    if (.not. associated(data%psir)) call setup_psir(data)

    call initialize_work_arrays_sumrho(data%lr, w)
    data%psi = f_malloc0_ptr(ncplx_ * array_dim(data%lr), id = "psi")
    do i = 1, ncplx_
       psiref_copy = f_malloc(data%psir, id = "psiref_copy")
       psiref_copy = psiref_copy / sqrt(real(ncplx_, wp))
       call isf_to_daub(data%lr, w, psiref_copy, data%psi(1 + (i-1) * array_dim(data%lr)))
       call f_free(psiref_copy)
    end do
    call deallocate_work_arrays_sumrho(w)
  end subroutine setup_psi

  subroutine setup_tmb(data, ilr, ncplx)
    use liborbs_precisions
    use dynamic_memory
    use liborbs_workarrays
    implicit none
    type(setup_data), intent(inout) :: data
    integer, intent(in) :: ilr
    integer, intent(in), optional :: ncplx

    type(workarr_sumrho) :: w
    real(wp), dimension(:,:,:), allocatable :: psiref_copy
    integer :: i, ncplx_

    ncplx_ = 1
    if (present(ncplx)) ncplx_ = ncplx

    if (.not. associated(data%psir)) call setup_psir(data, ilr)

    call initialize_work_arrays_sumrho(data%llr(ilr), w)
    data%tmb = f_malloc0_ptr(ncplx_ * array_dim(data%llr(ilr)), id = "psi")
    do i = 1, ncplx_
       psiref_copy = f_malloc(data%psir, id = "psiref_copy")
       psiref_copy = psiref_copy / sqrt(real(ncplx_, wp))
       call isf_to_daub(data%llr(ilr), w, psiref_copy, data%tmb(1 + (i-1) * array_dim(data%llr(ilr))))
       call f_free(psiref_copy)
    end do
    call deallocate_work_arrays_sumrho(w)
  end subroutine setup_tmb

  subroutine setup_confdata(data, prefac, order)
    implicit none
    type(setup_data), intent(inout) :: data
    real(gp), intent(in) :: prefac
    integer, intent(in) :: order

    call nullify_confpot_data(data%confdata)
    data%confdata%prefac = prefac
    data%confdata%potorder = order
  end subroutine setup_confdata

  subroutine setup_fromFile(data, filename, binary)
    use f_utils
    use dynamic_memory
    use f_unittests
    implicit none
    type(setup_data), intent(out) :: data
    character(len = *), intent(in) :: filename
    logical, intent(in) :: binary

    integer :: rd, stat, nat, iat
    
    rd = 456
    call f_open_file(rd, file = filename, binary = binary)
    read(rd, *, iostat = stat)
    call verify(stat == 0, "iorb, eval")
    read(rd, *, iostat = stat)
    call verify(stat == 0, "hgrids")
    read(rd, *, iostat = stat)
    call verify(stat == 0, "ndims")
    read(rd, *, iostat = stat)
    call verify(stat == 0, "nboxc start")
    read(rd, *, iostat = stat)
    call verify(stat == 0, "locreg centre and other linear stuff")
    read(rd, *, iostat = stat) nat
    call verify(stat == 0, "nat")
    do iat = 1, nat
       read(rd, *, iostat = stat)
       call verify(stat == 0, "rxyz")
    end do
    data%lr = locreg_null()
    call read_lr_txt(rd, data%lr)
    data%psi = f_malloc_ptr(array_dim(data%lr), id = "psi")
    call read_array_txt(data%lr, rd, data%psi)
    call f_close(rd)
  end subroutine setup_fromFile

end module setup
