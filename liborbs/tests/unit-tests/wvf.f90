program test_wvf
  use yaml_output
  use f_unittests
  use time_profiling
  use wrapper_MPI

  implicit none

  type(mpi_environment) :: env
  
  call mpiinit()

  env = mpi_environment_comm()  

  if (env%iproc == 0) then
     ! Serial test only
     call f_lib_initialize()

     call f_timing_category_group("Convolutions", "")

     call yaml_sequence_open("wvf managers")
     call run(null_manager)
     call run(simple_manager)
     call run(multi_manager)
     call yaml_sequence_close()

     call yaml_sequence_open("daub views")
     call run(null_daub)
     call run(factor_daub)
     call run(create_daub)
     call run(create_daub_on_sub)
     call run(create_daub_by_copy)
     call run(daub_to_mesh)
     call run(daub_norm)
     call run(daub_addition)
     call run(daub_dotp)
     call yaml_sequence_close()

     call yaml_sequence_open("mesh views")
     call run(null_mesh)
     call run(factor_mesh)
     call run(create_mesh)
     call run(create_mesh_on_sub)
     call run(mesh_to_daub)
     call run(mesh_norm)
     call run(mesh_addition)
     call yaml_sequence_close()

     call yaml_sequence_open("operators")
     call run(pot_op)
     call run(kin_op)
     call run(kin_by_dir)
     call run(prec_op)
     call run(nl_op)
     call yaml_sequence_close()

     call f_lib_finalize()
  end if

  call release_mpi_environment(env)
  call mpifinalize()

contains
  
  subroutine null_manager(label)
    use liborbs_precisions
    use liborbs_functions
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_manager) :: manager

    label = "null object"

    call compare(int(manager%max_daub), 0, "max number of coefficients in daub")
    call compare(int(manager%max_mesh), 0, "max number of coefficients in isf")
    call verify(.not. associated(manager%w_sumrho), "sumrho workarrays not associated")
    call verify(.not. associated(manager%w_locham), "locham workarrays not associated")
    call verify(.not. associated(manager%w_precond), "precond workarrays not associated")
    call verify(.not. associated(manager%w_quartic), "quartic workarrays not associated")
    call verify(.not. associated(manager%w_tensp), "tensor product workarrays not associated")
    call verify(.not. associated(manager%w_daub_1%mem) .and. &
         .not. associated(manager%w_daub_2%mem) .and. &
         .not. associated(manager%w_daub_3%mem) .and. &
         .not. associated(manager%w_daub_4%mem) .and. &
         .not. associated(manager%w_daub_5%mem), "daub workarrays not associated")
    call verify(.not. associated(manager%w_mesh_1%mem) .and. &
         .not. associated(manager%w_mesh_2%mem) .and. &
         .not. associated(manager%w_mesh_3%mem) .and. &
         .not. associated(manager%w_mesh_4%mem) .and. &
         .not. associated(manager%w_mesh_5%mem), "mesh workarrays not associated")
  end subroutine null_manager
  
  subroutine simple_manager(label)
    use liborbs_precisions
    use liborbs_functions
    use setup
    use locregs
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_manager) :: manager
    type(wvf_daub_view) :: daub
    type(setup_data), target :: data1
    type(setup_data), target :: data2

    label = "simple object"

    manager = wvf_manager_new(4)
    call compare(manager%nspinor, 4, "nspinor")
    call compare(int(manager%max_daub), 0, "max number of coefficients in daub")
    call compare(int(manager%max_mesh), 0, "max number of coefficients in isf")
    call verify(.not. associated(manager%w_sumrho), "sumrho workarrays not associated")
    call verify(.not. associated(manager%w_locham), "locham workarrays not associated")
    call verify(.not. associated(manager%w_precond), "precond workarrays not associated")
    call verify(.not. associated(manager%w_quartic), "quartic workarrays not associated")
    call verify(.not. associated(manager%w_tensp), "tensor product workarrays not associated")
    call verify(.not. associated(manager%w_daub_1%mem) .and. &
         .not. associated(manager%w_daub_2%mem) .and. &
         .not. associated(manager%w_daub_3%mem) .and. &
         .not. associated(manager%w_daub_4%mem) .and. &
         .not. associated(manager%w_daub_5%mem), "daub workarrays not associated")
    call verify(.not. associated(manager%w_mesh_1%mem) .and. &
         .not. associated(manager%w_mesh_2%mem) .and. &
         .not. associated(manager%w_mesh_3%mem) .and. &
         .not. associated(manager%w_mesh_4%mem) .and. &
         .not. associated(manager%w_mesh_5%mem), "mesh workarrays not associated")

    call setup_freeBC(data1, ndims = (/ 4, 4, 3 /)) ! smallest
    call setup_freeBC(data2, ndims = (/ 3, 5, 4 /)) ! largest
    daub = wvf_view_on_daub(manager, data1%lr)
    call compare(manager%i_daub, 1, "buffer index")
    call verify(associated(manager%w_daub_1%mem), "daub workarray 1")
    call compare(size(manager%w_daub_1%mem, 1), int(array_dim(data1%lr)), "daub 1 size")

    call wvf_manager_release(manager, daub)
    call compare(manager%i_daub, 0, "buffer index")
    call verify(associated(manager%w_daub_1%mem), "memory association")

    daub = wvf_view_on_daub(manager, data2%lr)
    call compare(manager%i_daub, 1, "buffer index")
    call verify(associated(manager%w_daub_1%mem), "reallocating daub workarray 1")
    call compare(size(manager%w_daub_1%mem, 1), int(array_dim(data2%lr)), "daub 1 resize")
    call wvf_deallocate_manager(manager)

    call setup_clean(data1)
    call setup_clean(data2)
  end subroutine simple_manager
  
  subroutine multi_manager(label)
    use liborbs_precisions
    use liborbs_functions
    use setup
    use locregs
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_manager) :: manager
    type(wvf_daub_view) :: daub
    type(setup_data), target :: data1
    type(setup_data), target :: data2
    type(locreg_descriptors), dimension(3) :: llr

    label = "multi-lr object"

    call setup_freeBC(data1, ndims = (/ 4, 4, 3 /)) ! smallest
    call setup_freeBC(data2, ndims = (/ 3, 5, 4 /)) ! largest
    llr(1) = data1%lr
    llr(2) = data2%lr

    manager = wvf_manager_new(2, llr, 2)
    call compare(manager%nspinor, 2, "nspinor")
    call compare(int(manager%max_daub), max(array_dim(data1%lr), array_dim(data2%lr)), &
         "max number of coefficients in daub")
    call compare(manager%max_mesh, max(data1%lr%mesh%ndim, data2%lr%mesh%ndim), &
         "max number of coefficients in isf")
    call verify(associated(manager%w_sumrho), "sumrho workarrays not associated")
    call verify(associated(manager%w_locham), "locham workarrays not associated")
    call verify(associated(manager%w_precond), "precond workarrays not associated")
    call verify(associated(manager%w_quartic), "quartic workarrays not associated")
    call verify(associated(manager%w_tensp), "tensor product workarrays not associated")
    call verify(.not. associated(manager%w_daub_1%mem) .and. &
         .not. associated(manager%w_daub_2%mem) .and. &
         .not. associated(manager%w_daub_3%mem) .and. &
         .not. associated(manager%w_daub_4%mem) .and. &
         .not. associated(manager%w_daub_5%mem), "daub workarrays not associated")
    call verify(.not. associated(manager%w_mesh_1%mem) .and. &
         .not. associated(manager%w_mesh_2%mem) .and. &
         .not. associated(manager%w_mesh_3%mem) .and. &
         .not. associated(manager%w_mesh_4%mem) .and. &
         .not. associated(manager%w_mesh_5%mem), "mesh workarrays not associated")

    daub = wvf_view_on_daub(manager, data1%lr)
    call compare(manager%i_daub, 1, "buffer index")
    call verify(associated(manager%w_daub_1%mem), "daub workarray 1")
    call verify(size(manager%w_daub_1%mem, 1) >= array_dim(data1%lr), "daub 1 size")

    call wvf_manager_release(manager, daub)
    call compare(manager%i_daub, 0, "buffer index")
    call verify(associated(manager%w_daub_1%mem), "memory association")

    daub = wvf_view_on_daub(manager, data2%lr)
    call compare(manager%i_daub, 1, "buffer index")
    call verify(associated(manager%w_daub_1%mem), "reallocating daub workarray 1")
    call verify(size(manager%w_daub_1%mem, 1) >= array_dim(data2%lr), "daub 1 resize")
    call wvf_deallocate_manager(manager)

    call setup_clean(data1)
    call setup_clean(data2)
  end subroutine multi_manager
  
  subroutine null_daub(label)
    use liborbs_precisions
    use liborbs_functions
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_daub_view) :: daub

    label = "null object"

    call verify(.not. associated(daub%manager), "manager not associated")
    call compare(daub%factor, f_scalar(1., 0.), "unity factor")
    call verify(.not. associated(daub%lr), "locreg not associated")
    call verify(.not. associated(daub%c%mem), "memory not associated")
    call verify(.not. associated(daub%c%gpu), "GPU memory not associated")
  end subroutine null_daub

  subroutine factor_daub(label)
    use liborbs_precisions
    use liborbs_functions
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_daub_view) :: daub, tmp

    label = "factor with a scalar"

    tmp = 5._wp * daub
    call compare(daub%factor, f_scalar(1., 0.), "unity factor")
    call compare(tmp%factor, f_scalar(5., 0.), "real factor")
    tmp = f_scalar(4., 2.) * daub
    call compare(daub%factor, f_scalar(1., 0.), "unity factor")
    call compare(tmp%factor, f_scalar(4., 2.), "imaginary factor")
    tmp = - daub
    call compare(daub%factor, f_scalar(1., 0.), "unity factor")
    call compare(tmp%factor, f_scalar(-1., 0.), "opposite factor")
  end subroutine factor_daub

  subroutine create_daub(label)
    use liborbs_precisions
    use liborbs_functions
    use setup
    use locregs
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_manager), target :: manager
    type(wvf_daub_view) :: daub
    type(setup_data), target :: data

    label = "create object"

    call setup_freeBC(data)
    manager = wvf_manager_new()

    daub = wvf_view_on_daub(manager, data%lr)
    
    call verify(associated(daub%manager, target = manager), "manager associated")
    call compare(daub%factor, f_scalar(1., 0.), "unity factor")
    call verify(associated(daub%lr, target = data%lr), "locreg associated")
    call verify(associated(daub%c%mem, target = manager%w_daub_1%mem), "memory associated")
    call compare(size(daub%c%mem), array_dim(data%lr), "memory size")
    call verify(.not. associated(daub%c%gpu), "GPU memory not associated")

    call wvf_deallocate_manager(manager)
    call setup_clean(data)
  end subroutine create_daub

  subroutine create_daub_on_sub(label)
    use liborbs_precisions
    use liborbs_functions
    use setup
    use locregs
    use dynamic_memory
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_manager), target :: manager
    type(wvf_daub_view) :: daub
    type(setup_data), target :: data
    real(wp), dimension(:), allocatable :: psi
    real(wp), dimension(:), pointer :: sub

    label = "create object on allocated memory"

    call setup_freeBC(data)
    psi = f_malloc(array_dim(data%lr) * 2, id = "psi")
    
    manager = wvf_manager_new()

    sub => f_subptr(psi, from = array_dim(data%lr) + 1, size = array_dim(data%lr))
    daub = wvf_view_on_daub(manager, data%lr, sub)
    
    call verify(associated(daub%manager, target = manager), "manager associated")
    call compare(daub%factor, f_scalar(1., 0.), "unity factor")
    call verify(associated(daub%lr, target = data%lr), "locreg associated")
    call verify(associated(daub%c%mem), "memory associated")
    call verify(.not. associated(daub%c%mem, target = manager%w_daub_1%mem), "public memory")
    call compare(size(daub%c%mem), array_dim(data%lr), "memory size")
    call verify(.not. associated(daub%c%gpu), "GPU memory not associated")

    call wvf_deallocate_manager(manager)
    call setup_clean(data)

    call f_free(psi)
  end subroutine create_daub_on_sub

  subroutine create_daub_by_copy(label)
    use liborbs_precisions
    use liborbs_functions
    use setup
    use locregs
    use dynamic_memory
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_manager), target :: manager
    type(wvf_daub_view) :: daub
    type(setup_data), target :: data
    real(wp), dimension(:), allocatable :: psi
    real(wp), dimension(:), pointer :: sub
    integer :: i

    label = "create object by copy"

    call setup_freeBC(data)
    psi = f_malloc(array_dim(data%lr) * 2, id = "psi")
    do i = 1, size(psi)
       psi(i) = exp(-real(i*i))
    end do
    
    manager = wvf_manager_new()

    sub => f_subptr(psi, from = array_dim(data%lr) + 1, size = array_dim(data%lr))
    daub = wvf_view_on_daub(manager, data%lr, psisrc = sub)
    
    call verify(associated(daub%manager, target = manager), "manager associated")
    call compare(daub%factor, f_scalar(1., 0.), "unity factor")
    call verify(associated(daub%lr, target = data%lr), "locreg associated")
    call verify(associated(daub%c%mem, target = manager%w_daub_1%mem), "memory associated")
    call compare(size(daub%c%mem), array_dim(data%lr), "memory size")
    call compare(sum(sub), sum(daub%c%mem), "by copy")
    call verify(.not. associated(daub%c%gpu), "GPU memory not associated")

    call wvf_deallocate_manager(manager)
    call setup_clean(data)

    call f_free(psi)
  end subroutine create_daub_by_copy

  subroutine mesh_check(mesh, manager, lr, psir)
    use liborbs_functions
    use locregs
    implicit none
    type(wvf_mesh_view), intent(in) :: mesh
    type(wvf_manager), intent(in), target :: manager
    type(locreg_descriptors), intent(in), target :: lr
    real(wp), dimension(lr%mesh%ndim, manager%nspinor), intent(in), target, optional :: psir
    logical :: asso

    call verify(associated(mesh%manager, target = manager), "manager associated")
    call compare(mesh%factor, f_scalar(1., 0.), "unity factor")
    call verify(associated(mesh%lr, target = lr), "locreg associated")
    if (present(psir)) then
       call verify(associated(mesh%c%mem, target = psir), "memory associated")
    else
       asso = .false.
       asso = asso .or. associated(mesh%c%mem, target = manager%w_mesh_1%mem)
       asso = asso .or. associated(mesh%c%mem, target = manager%w_mesh_2%mem)
       asso = asso .or. associated(mesh%c%mem, target = manager%w_mesh_3%mem)
       asso = asso .or. associated(mesh%c%mem, target = manager%w_mesh_4%mem)
       asso = asso .or. associated(mesh%c%mem, target = manager%w_mesh_5%mem)
       call verify(asso, "memory associated")
    end if
    call compare(size(mesh%c%mem), int(lr%mesh%ndim * manager%nspinor), "memory size")
    call verify(.not. associated(mesh%c%gpu), "GPU memory not associated")
  end subroutine mesh_check

  subroutine check_daub_to_mesh(lr)
    use liborbs_functions
    use locregs
    use f_utils
    use f_arrays
    use dynamic_memory
    implicit none
    type(locreg_descriptors), intent(in), target :: lr

    type(wvf_manager), target :: manager
    type(wvf_daub_view) :: daub
    type(wvf_mesh_view) :: mesh, mesh2
    type(f_scalar) :: sigma
    real(wp), dimension(:), allocatable :: psir

    manager = wvf_manager_new()

    daub = wvf_view_on_daub(manager, lr)
    call f_zero(daub%c%mem)
    sigma = 0.1_gp
    call wvf_accumulate_gaussian(daub, sigma, (/ 2._gp, 2._gp, 2._gp /))
    call wvf_finish_accumulate_gaussians(daub)

    mesh = daub
    call mesh_check(mesh, manager, lr)
    call compare(wvf_nrm(mesh), wvf_nrm(daub), "norm", tol = 4.d-6)
    call wvf_manager_release(manager, mesh)

    mesh = wvf_view_to_mesh(daub, store = .true.)
    call mesh_check(mesh, manager, lr)
    call compare(wvf_nrm(mesh), wvf_nrm(daub), "norm", tol = 4.d-6)
    call wvf_manager_release(manager, mesh)

    psir = f_malloc(2 * lr%mesh%ndim, id = "psir")
    mesh = wvf_view_to_mesh(daub, psir = psir)
    call mesh_check(mesh, manager, lr, psir)
    call compare(wvf_nrm(mesh), wvf_nrm(daub), "norm", tol = 4.d-6)

    mesh2 = wvf_view_to_mesh(daub, psir(lr%mesh%ndim + 1))
    call mesh_check(mesh2, manager, lr, psir(lr%mesh%ndim + 1))
    call compare(wvf_nrm(mesh2), wvf_nrm(mesh), "norm")
    call f_free(psir)

    call wvf_deallocate_manager(manager)
  end subroutine check_daub_to_mesh

  subroutine daub_to_mesh(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data

    label = "convert daub view to mesh view"

    call setup_freeBC(data)
    call check_daub_to_mesh(data%lr)
    call setup_clean(data)

    call setup_wireBC(data)
    call check_daub_to_mesh(data%lr)
    call setup_clean(data)

    call setup_surfaceBC(data)
    call check_daub_to_mesh(data%lr)
    call setup_clean(data)

    call setup_periodicBC(data, .true.)
    call check_daub_to_mesh(data%lr)
    call setup_clean(data)

    call setup_periodicBC(data, .false.)
    call check_daub_to_mesh(data%lr)
    call setup_clean(data)
  end subroutine daub_to_mesh

  subroutine daub_norm(label)
    use liborbs_precisions
    use liborbs_functions
    use f_utils
    use f_arrays
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data
    type(wvf_manager), target :: manager
    type(wvf_daub_view) :: daub
    type(f_scalar) :: sigma
    real(wp) :: nrm, oldnrm

    label = "norm calculation / normalisation"

    call setup_freeBC(data)
    manager = wvf_manager_new()

    daub = wvf_view_on_daub(manager, data%lr)
    call f_zero(daub%c%mem)
    sigma = 0.1_gp
    call wvf_accumulate_gaussian(daub, sigma, (/ 2._gp, 2._gp, 2._gp /))
    call wvf_finish_accumulate_gaussians(daub)

    nrm = wvf_nrm(daub)
    call compare(nrm, 7.37896d-002, "norm", tol = 2.d-8)

    call wvf_normalize(daub, oldnrm = oldnrm)
    call compare(wvf_nrm(daub), 1._wp, "unity norm", tol = 1e-12_wp)
    call compare(oldnrm, nrm, "old norm")

    call wvf_deallocate_manager(manager)
    call setup_clean(data)
  end subroutine daub_norm

  subroutine daub_addition(label)
    use liborbs_precisions
    use liborbs_functions
    use f_utils
    use f_arrays
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data
    type(wvf_manager), target :: manager
    type(wvf_daub_view) :: daub, out
    type(f_scalar) :: sigma

    label = "inplace add"

    call setup_freeBC(data)
    manager = wvf_manager_new()

    daub = wvf_view_on_daub(manager, data%lr)
    call f_zero(daub%c%mem)
    sigma = 0.1_gp
    call wvf_accumulate_gaussian(daub, sigma, (/ 2._gp, 2._gp, 2._gp /))
    call wvf_finish_accumulate_gaussians(daub)
    call wvf_normalize(daub)

    out = wvf_view_on_daub(manager, data%lr)
    out = 3._wp * out
    call f_zero(out%c%mem)
    call wvf_add(out, 2._wp * daub)
    call compare(wvf_nrm(out), 2._wp, "out norm")

    call wvf_deallocate_manager(manager)

    manager = wvf_manager_new(4)
    daub = wvf_view_on_daub(manager, data%lr)
    call f_zero(daub%c%mem)
    sigma = 0.1_gp
    call wvf_accumulate_gaussian(daub, sigma, (/ 2._gp, 2._gp, 2._gp /))
    call wvf_finish_accumulate_gaussians(daub)
    call wvf_normalize(daub)

    out = wvf_view_on_daub(manager, data%lr)
    out = 3._wp * out
    call f_zero(out%c%mem)
    call wvf_add(out, 0.5_wp * f_scalar(1._wp, sqrt(3._wp)) * daub, ncplx = 2)
    call compare(wvf_nrm(out), 1._wp, "out norm (complex case)", tol = 1.e-12_wp)
    call wvf_deallocate_manager(manager)
    
    call setup_clean(data)
  end subroutine daub_addition

  subroutine daub_dotp(label)
    use liborbs_precisions
    use liborbs_functions
    use f_utils
    use f_arrays
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data
    type(wvf_manager), target :: manager
    type(wvf_daub_view) :: daub1, daub2
    type(f_scalar) :: sigma

    label = "dot product (same locreg)"

    call setup_freeBC(data)
    manager = wvf_manager_new()

    daub1 = wvf_view_on_daub(manager, data%lr)
    call f_zero(daub1%c%mem)
    sigma = 0.5_gp
    call wvf_accumulate_gaussian(daub1, sigma, (/ 2._gp, 2._gp, 2._gp /))
    call wvf_finish_accumulate_gaussians(daub1)
    call wvf_normalize(daub1)

    daub2 = wvf_view_on_daub(manager, data%lr)
    call f_zero(daub2%c%mem)
    sigma = 1.0_gp
    call wvf_accumulate_gaussian(daub2, sigma, (/ 3._gp, 1._gp, 4._gp /))
    call wvf_finish_accumulate_gaussians(daub2)
    call wvf_normalize(daub2)

    call compare(daub1 .dot. daub2, (/ 7.5811e-2_wp /), "nspinor = 1", tol = 2d-7)

    call wvf_deallocate_manager(manager)

    ! Complex case
    manager = wvf_manager_new(2)

    daub1 = wvf_view_on_daub(manager, data%lr)
    call f_zero(daub1%c%mem)
    sigma = 0.5_gp
    call wvf_accumulate_gaussian(daub1, sigma, (/ 2._gp, 2._gp, 2._gp /), &
         (/ 0.5_wp * f_scalar(1._wp, sqrt(3._wp)) /), kpt = (/ 1._gp, 0._gp, 0._gp /))
    call wvf_finish_accumulate_gaussians(daub1)
    call wvf_normalize(daub1)

    daub2 = wvf_view_on_daub(manager, data%lr)
    call f_zero(daub2%c%mem)
    sigma = 1.0_gp
    call wvf_accumulate_gaussian(daub2, sigma, (/ 3._gp, 1._gp, 4._gp /), &
         (/ 0.5_wp * f_scalar(sqrt(2._wp), sqrt(2._wp)) /), kpt = (/ 1._gp, 1._gp, 1._gp /))
    call wvf_finish_accumulate_gaussians(daub2)
    call wvf_normalize(daub2)

    call compare(daub1 .dot. daub2, (/ -2.8243e-2_wp, 3.0559e-2_wp /), "nspinor = 2", tol = 2d-7)

    call wvf_deallocate_manager(manager)

    ! Sipnorial case
    manager = wvf_manager_new(4)

    daub1 = wvf_view_on_daub(manager, data%lr)
    call f_zero(daub1%c%mem)
    sigma = 0.5_gp
    call wvf_accumulate_gaussian(daub1, sigma, (/ 2._gp, 2._gp, 2._gp /), &
         (/ 0.5_wp * f_scalar(1._wp, sqrt(3._wp)), 0.5_wp * f_scalar(-sqrt(3._wp), 1._wp) /), &
         kpt = (/ 1._gp, 0._gp, 0._gp /))
    call wvf_finish_accumulate_gaussians(daub1)
    call wvf_normalize(daub1)

    daub2 = wvf_view_on_daub(manager, data%lr)
    call f_zero(daub2%c%mem)
    sigma = 1.0_gp
    call wvf_accumulate_gaussian(daub2, sigma, (/ 3._gp, 1._gp, 4._gp /), &
         (/ 0.5_wp * f_scalar(sqrt(2._wp), sqrt(2._wp)), 0.5_wp * f_scalar(sqrt(2._wp), -sqrt(2._wp)) /), &
         kpt = (/ 1._gp, 1._gp, 1._gp /))
    call wvf_finish_accumulate_gaussians(daub2)
    call wvf_normalize(daub2)

    call compare(daub1 .dot. daub2, (/ -9.9854e-3_wp, 1.0804e-2_wp, &
         1.7295e-2_wp, -1.8713e-2_wp /), "nspinor = 4", tol = 4d-7)

    call wvf_deallocate_manager(manager)

    call setup_clean(data)
  end subroutine daub_dotp

  subroutine null_mesh(label)
    use liborbs_precisions
    use liborbs_functions
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_mesh_view) :: mesh

    label = "null object"

    call verify(.not. associated(mesh%manager), "manager not associated")
    call compare(mesh%factor, f_scalar(1., 0.), "unity factor")
    call verify(.not. associated(mesh%lr), "locreg not associated")
    call verify(.not. associated(mesh%c%mem), "memory not associated")
    call verify(.not. associated(mesh%c%gpu), "GPU memory not associated")
  end subroutine null_mesh

  subroutine factor_mesh(label)
    use liborbs_precisions
    use liborbs_functions
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_mesh_view) :: mesh, tmp

    label = "factor with a scalar"

    tmp = 5._wp * mesh
    call compare(mesh%factor, f_scalar(1., 0.), "unity factor")
    call compare(tmp%factor, f_scalar(5., 0.), "real factor")
    tmp = f_scalar(4., 2.) * mesh
    call compare(mesh%factor, f_scalar(1., 0.), "unity factor")
    call compare(tmp%factor, f_scalar(4., 2.), "imaginary factor")
    tmp = - mesh
    call compare(mesh%factor, f_scalar(1., 0.), "unity factor")
    call compare(tmp%factor, f_scalar(-1., 0.), "opposite factor")
  end subroutine factor_mesh

  subroutine create_mesh(label)
    use liborbs_precisions
    use liborbs_functions
    use setup
    use locregs
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_manager), target :: manager
    type(wvf_mesh_view) :: mesh
    type(setup_data), target :: data

    label = "create object"

    call setup_freeBC(data)
    manager = wvf_manager_new()

    mesh = wvf_view_on_mesh(manager, data%lr)
    
    call verify(associated(mesh%manager, target = manager), "manager associated")
    call compare(mesh%factor, f_scalar(1., 0.), "unity factor")
    call verify(associated(mesh%lr, target = data%lr), "locreg associated")
    call verify(associated(mesh%c%mem, target = manager%w_mesh_1%mem), "memory associated")
    call compare(int(size(mesh%c%mem), f_long), data%lr%mesh%ndim, "memory size")
    call verify(.not. associated(mesh%c%gpu), "GPU memory not associated")

    call wvf_deallocate_manager(manager)
    call setup_clean(data)
  end subroutine create_mesh

  subroutine create_mesh_on_sub(label)
    use liborbs_precisions
    use liborbs_functions
    use setup
    use locregs
    use dynamic_memory
    implicit none
    character(len = *), intent(out) :: label

    type(wvf_manager), target :: manager
    type(wvf_mesh_view) :: mesh
    type(setup_data), target :: data
    real(wp), dimension(:), allocatable :: psi
    real(wp), dimension(:), pointer :: sub

    label = "create object on allocated memory"

    call setup_freeBC(data)
    psi = f_malloc(array_dim(data%lr) * 2, id = "psi")
    
    manager = wvf_manager_new()

    sub => f_subptr(psi, from = array_dim(data%lr) + 1, size = array_dim(data%lr))
    mesh = wvf_view_on_mesh(manager, data%lr, sub)
    
    call verify(associated(mesh%manager, target = manager), "manager associated")
    call compare(mesh%factor, f_scalar(1., 0.), "unity factor")
    call verify(associated(mesh%lr, target = data%lr), "locreg associated")
    call verify(associated(mesh%c%mem), "memory associated")
    call verify(.not. associated(mesh%c%mem, target = manager%w_mesh_1%mem), "public memory")
    call compare(int(size(mesh%c%mem), f_long), data%lr%mesh%ndim, "memory size")
    call verify(.not. associated(mesh%c%gpu), "GPU memory not associated")

    call wvf_deallocate_manager(manager)
    call setup_clean(data)

    call f_free(psi)
  end subroutine create_mesh_on_sub

  subroutine daub_check(daub, manager, lr, psi)
    use liborbs_functions
    use locregs
    implicit none
    type(wvf_daub_view), intent(in) :: daub
    type(wvf_manager), intent(in), target :: manager
    type(locreg_descriptors), intent(in), target :: lr
    real(wp), dimension(array_dim(lr), manager%nspinor), intent(in), target, optional :: psi
    logical :: asso

    call verify(associated(daub%manager, target = manager), "manager associated")
    call compare(daub%factor, f_scalar(1., 0.), "unity factor")
    call verify(associated(daub%lr, target = lr), "locreg associated")
    if (present(psi)) then
       call verify(associated(daub%c%mem, target = psi), "memory associated")
    else
       asso = .false.
       asso = asso .or. associated(daub%c%mem, target = manager%w_daub_1%mem)
       asso = asso .or. associated(daub%c%mem, target = manager%w_daub_2%mem)
       asso = asso .or. associated(daub%c%mem, target = manager%w_daub_3%mem)
       asso = asso .or. associated(daub%c%mem, target = manager%w_daub_4%mem)
       asso = asso .or. associated(daub%c%mem, target = manager%w_daub_5%mem)
       call verify(asso, "memory associated")
    end if
    call compare(size(daub%c%mem), array_dim(lr) * manager%nspinor, "memory size")
    call verify(.not. associated(daub%c%gpu), "GPU memory not associated")
  end subroutine daub_check

  subroutine check_mesh_to_daub(lr)
    use liborbs_functions
    use locregs
    use f_utils
    use f_arrays
    use dynamic_memory
    implicit none
    type(locreg_descriptors), intent(in), target :: lr

    type(wvf_manager), target :: manager
    type(wvf_daub_view) :: daub, ref
    type(wvf_mesh_view) :: mesh, tmp
    type(f_scalar) :: sigma
    real(wp), dimension(:), allocatable :: psi
    type(f_scalar), dimension(1, 1) :: factor
    integer, dimension(3, 1) :: lxyz

    manager = wvf_manager_new()

    sigma = 0.1_gp

    ref = wvf_view_on_daub(manager, lr)
    call f_zero(ref%c%mem)
    call wvf_accumulate_gaussian(ref, sigma, (/ 2._gp, 2._gp, 2._gp /))
    call wvf_finish_accumulate_gaussians(ref)    

    mesh = wvf_view_on_mesh(manager, lr)
    call f_zero(mesh%c%mem)
    factor = 1._gp
    lxyz = 0
    call wvf_accumulate_gaussian(mesh, sigma, (/ 2._gp, 2._gp, 2._gp /), &
         1, factor, lxyz)

    call compare(wvf_nrm(ref), wvf_nrm(mesh), "ref", tol = 9.d-4)

    daub = mesh
    call daub_check(daub, manager, lr)
    call compare(wvf_nrm(daub), wvf_nrm(mesh), "norm", tol = 9.d-4)
    call compare(wvf_nrm(daub), wvf_nrm(ref), "after", tol = 3.d-5)
    call wvf_manager_release(manager, daub)

    tmp = wvf_view_to_mesh(mesh)
    psi = f_malloc0(array_dim(lr), id = "psi")
    daub = wvf_view_to_daub(tmp, psi = psi)
    call daub_check(daub, manager, lr, psi)
    call compare(wvf_nrm(daub), wvf_nrm(mesh), "norm", tol = 9.d-4)
    call f_free(psi)

    call wvf_deallocate_manager(manager)
  end subroutine check_mesh_to_daub

  subroutine mesh_to_daub(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data

    label = "convert mesh view to daub view"

    call setup_freeBC(data)
    call check_mesh_to_daub(data%lr)
    call setup_clean(data)

    call setup_wireBC(data)
    call check_mesh_to_daub(data%lr)
    call setup_clean(data)

    call setup_surfaceBC(data)
    call check_mesh_to_daub(data%lr)
    call setup_clean(data)

    call setup_periodicBC(data, .true.)
    call check_mesh_to_daub(data%lr)
    call setup_clean(data)

    call setup_periodicBC(data, .false.)
    call check_mesh_to_daub(data%lr)
    call setup_clean(data)
  end subroutine mesh_to_daub

  subroutine mesh_norm(label)
    use liborbs_precisions
    use liborbs_functions
    use f_utils
    use f_arrays
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data
    type(wvf_manager), target :: manager
    type(wvf_mesh_view) :: mesh
    type(f_scalar) :: sigma
    real(wp) :: nrm, oldnrm
    type(f_scalar), dimension(1, 1) :: factor
    integer, dimension(3, 1) :: lxyz

    label = "norm calculation / normalisation"

    call setup_freeBC(data)
    manager = wvf_manager_new()

    mesh = wvf_view_on_mesh(manager, data%lr)
    call f_zero(mesh%c%mem)
    sigma = 0.1_gp
    factor = 1._gp
    lxyz = 0
    call wvf_accumulate_gaussian(mesh, sigma, (/ 2._gp, 2._gp, 2._gp /), &
         1, factor, lxyz)

    nrm = wvf_nrm(mesh)
    call compare(nrm, 7.46046d-002, "norm", tol = 3.d-8)

    call wvf_normalize(mesh, oldnrm = oldnrm)
    call compare(wvf_nrm(mesh), 1._wp, "unity norm", tol = 1.e-12_wp)
    call compare(oldnrm, nrm, "old norm")

    call wvf_deallocate_manager(manager)
    call setup_clean(data)
  end subroutine mesh_norm

  subroutine mesh_addition(label)
    use liborbs_precisions
    use liborbs_functions
    use f_utils
    use f_arrays
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data
    type(wvf_manager), target :: manager
    type(wvf_mesh_view) :: mesh, out
    type(f_scalar) :: sigma
    type(f_scalar), dimension(1, 1) :: factor
    integer, dimension(3, 1) :: lxyz

    label = "inplace add"

    call setup_freeBC(data)
    manager = wvf_manager_new()

    mesh = wvf_view_on_mesh(manager, data%lr)
    call f_zero(mesh%c%mem)
    sigma = 0.1_gp
    factor = 1._gp
    lxyz = 0
    call mesh_accumulate_gaussian(mesh, sigma, (/ 2._gp, 2._gp, 2._gp /), &
         1, factor, lxyz)
    call wvf_normalize(mesh)

    out = wvf_view_on_mesh(manager, data%lr)
    out = 3._wp * out
    call f_zero(out%c%mem)
    call wvf_add(out, 2._wp * mesh)
    call compare(wvf_nrm(out), 2._wp, "out norm", tol = 1.e-12_wp)

    call wvf_deallocate_manager(manager)

    manager = wvf_manager_new(4)
    mesh = wvf_view_on_mesh(manager, data%lr)
    call f_zero(mesh%c%mem)
    sigma = 0.1_gp
    call wvf_accumulate_gaussian(mesh, sigma, (/ 2._gp, 2._gp, 2._gp /), &
         1, factor, lxyz)
    call wvf_normalize(mesh)

    out = wvf_view_on_mesh(manager, data%lr)
    out = 3._wp * out
    call f_zero(out%c%mem)
    call wvf_add(out, 0.5_wp * f_scalar(1._wp, sqrt(3._wp)) * mesh, ncplx = 2)
    call compare(wvf_nrm(out), 1._wp, "out norm (complex case)", tol = 1.d-12)
    call wvf_deallocate_manager(manager)
    
    call setup_clean(data)
  end subroutine mesh_addition

  subroutine pot_op(label)
    use liborbs_precisions
    use liborbs_functions
    use setup
    use dynamic_memory
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data
    type(wvf_manager), target :: manager
    type(wvf_mesh_view) :: view, out
    integer :: npot
    real(wp), dimension(:), allocatable :: pot
    real(gp) :: epot

    label = "potential operator"

    manager = wvf_manager_new(2)
    call setup_freeBC(data)

    view = wvf_view_on_mesh(manager, data%lr)
    view%c%mem = 1._wp

    npot = 2
    pot = f_malloc(data%lr%mesh%ndim * int(2, f_long), id = "pot")
    pot = 2._wp

    out = wvf_potential_operator(view, pot, npot, epot)

    call mesh_check(out, manager, data%lr)
    call verify(all(out%c%mem == 2._wp), "vpsi")
    call compare(epot, 2 * 2._gp * data%lr%mesh%ndim, "epot")

    call wvf_potential_inplace(view, pot, npot, epot)
    call verify(all(view%c%mem == 2._wp), "vpsi inplace")
    call compare(epot, 2 * 2._gp * data%lr%mesh%ndim, "epot inplace")

    call f_free(pot)

    call setup_clean(data)
    call wvf_deallocate_manager(manager)
  end subroutine pot_op

  subroutine kin_op(label)
    use liborbs_precisions
    use liborbs_functions
    use locregs
    use setup
    use dynamic_memory
    use f_utils
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data
    type(wvf_manager), target :: manager
    type(wvf_mesh_view) :: view
    type(wvf_daub_view) :: in, out, out1d, outadd, direct
    real(gp) :: ekin
    real(gp), dimension(6) :: stress
    real(gp), dimension(3) :: kpt
    real(wp), dimension(:), allocatable :: tpsi

    label = "kinetic operator"

    manager = wvf_manager_new(2)
    call setup_periodicBC(data, .false.)

    in = wvf_view_on_daub(manager, data%lr)
    in%c%mem = 1._wp
    view = wvf_view_to_mesh(in, store = .true.)

    kpt = (/ 0._gp, 0.5_gp, 0.25_gp /)
    out = wvf_kinetic_operator(view, kpt = kpt, ekin = ekin, stress = stress)

    call daub_check(out, manager, data%lr)
    call compare(wvf_nrm(out), 24091.864042_wp, "norm tpsi", tol = 1.e-6_wp)
    out%c%mem = out%c%mem**2
    call compare(wvf_nrm(out), 24406268.524816_wp, "norm tpsi^2", tol = 5.e-6_wp)
    call compare(ekin, 672109.66279217_gp, "ekin", tol = 1.e-6_gp)
    call compare(stress, (/ 221955.506133_gp, 226523.085533_gp, 223631.071125_gp, &
         0._gp, 0._gp, 0._gp /), "stress", tol = 1.e-6_gp)

    tpsi = f_malloc0(array_dim(data%lr) * 2, id = 'tpsi')

    view = wvf_view_to_mesh(in, store = .true.)
    out1d = wvf_kinetic_operator(view, tpsi, kpt = kpt)
    call daub_check(out1d, manager, data%lr, tpsi)
    call compare(wvf_nrm(out1d), 24091.864042_wp, "norm tpsi 1D", tol = 1.e-6_wp)
    out1d%c%mem = out1d%c%mem**2
    call compare(wvf_nrm(out1d), 24406268.524816_wp, "norm tpsi^2 1D", tol = 5.e-6_wp)

    call f_zero(tpsi)
    view = wvf_view_to_mesh(in, store = .true.)
    outadd = wvf_kinetic_operator(view, tpsi(1), kpt = kpt)
    call daub_check(outadd, manager, data%lr, tpsi)
    call compare(wvf_nrm(outadd), 24091.864042_wp, "norm tpsi by address", tol = 1.e-6_wp)
    outadd%c%mem = outadd%c%mem**2
    call compare(wvf_nrm(outadd), 24406268.524816_wp, "norm tpsi^2 by address", tol = 5.e-6_wp)

    direct = wvf_kinetic_operator(in, kpt = kpt, ekin = ekin, stress = stress)
    call daub_check(direct, manager, data%lr)
    ! Should be the same as for the mesh case
    call compare(wvf_nrm(direct), 24064.322414_wp, "norm tpsi direct", tol = 1.e-6_wp)
    direct%c%mem = direct%c%mem**2
    call compare(wvf_nrm(direct), 24361756.524391_wp, "norm tpsi^2 direct", tol = 1.e-6_wp)
    call compare(ekin, 672109.66279217_gp, "ekin direct", tol = 1.e-6_gp)
    call compare(stress, (/ 221955.506133_gp, 226523.085533_gp, 223631.071125_gp, &
         0._gp, 0._gp, 0._gp /), "stress direct", tol = 1.e-6_gp)
    
    call f_zero(tpsi)
    direct = wvf_kinetic_operator(in, tpsi, kpt = kpt)
    call daub_check(direct, manager, data%lr, tpsi)
    ! Should be the same as for the mesh case
    call compare(wvf_nrm(direct), 24064.322414_wp, "norm tpsi direct 1D", tol = 1.e-6_wp)
    direct%c%mem = direct%c%mem**2
    call compare(wvf_nrm(direct), 24361756.524391_wp, "norm tpsi^2 direct 1D", tol = 1.e-6_wp)
    
    call f_free(tpsi)

    call setup_clean(data)
    call wvf_deallocate_manager(manager)
  end subroutine kin_op

  subroutine kin_by_dir(label)
    use liborbs_precisions
    use liborbs_functions
    use locregs
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data
    type(wvf_manager), target :: manager
    type(wvf_daub_view) :: in
    type(wvf_daub_view), dimension(3) :: out

    label = "kinetic along directions"

    manager = wvf_manager_new(2)
    call setup_freeBC(data)

    in = wvf_view_on_daub(manager, data%lr)
    in%c%mem = 1._wp

    out = wvf_kinetic_operator_dir(in)

    call daub_check(out(1), manager, data%lr)
    call compare(wvf_nrm(out(1)), 9750.733877_wp, "norm along x", tol = 1.e-6_wp)
    out(1)%c%mem = out(1)%c%mem**2
    call compare(wvf_nrm(out(1)), 4216314.409473_wp, "norm^2 along x", tol = 1.e-6_wp)

    call daub_check(out(2), manager, data%lr)
    call compare(wvf_nrm(out(2)), 9821.365929_wp, "norm along y", tol = 1.e-6_wp)
    out(2)%c%mem = out(2)%c%mem**2
    call compare(wvf_nrm(out(2)), 4285565.895270_wp, "norm^2 along y", tol = 1.e-6_wp)

    call daub_check(out(3), manager, data%lr)
    call compare(wvf_nrm(out(3)), 9789.175695_wp, "norm along z", tol = 1.e-6_wp)
    out(3)%c%mem = out(3)%c%mem**2
    call compare(wvf_nrm(out(3)), 4253913.062570_wp, "norm^2 along z", tol = 1.e-6_wp)

    call setup_clean(data)
    call wvf_deallocate_manager(manager)
  end subroutine kin_by_dir

  subroutine prec_op(label)
    use liborbs_precisions
    use liborbs_functions
    use liborbs_potentials
    use locregs
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data
    type(wvf_manager), target :: manager
    type(wvf_daub_view) :: view
    type(confpot_data) :: confpot
    real(wp) :: scpr2
    real(gp), dimension(3) :: kpt

    label = "preconditioning"

    manager = wvf_manager_new(2)
    call setup_freeBC(data)

    view = wvf_view_on_daub(manager, data%lr)
    view%c%mem = 1._wp

    call wvf_add_noise(view)
    call wvf_normalize(view)

    kpt = (/ 0._gp, 0.5_gp, 0.25_gp /)
    call wvf_precondition(view, 10, 1._gp, 1._gp, scpr2, kpt, 2)

    call daub_check(view, manager, data%lr)
    call compare(wvf_nrm(view), 0.85988078_wp, "norm", tol = 1.e-8_wp)
    view%c%mem = view%c%mem**2
    call compare(wvf_nrm(view), 6.60887988e-3_wp, "norm^2", tol = 1.e-10_wp)
    call compare(scpr2, 1._wp, "scpr^2", tol = 1.e-6_wp)

    confpot = confinement_data(4, 1._gp, data%lr, (/ 1.5_gp, 1.2_gp, 0.8_gp /), data%lr%mesh)
    call wvf_precondition(view, 10, 1._gp, 1._gp, scpr2, kpt, 2, confpot)

    call daub_check(view, manager, data%lr)
    call compare(wvf_nrm(view), 4.11675851e-4_wp, "norm with confinement", tol = 1.e-8_wp)
    view%c%mem = view%c%mem**2
    call compare(wvf_nrm(view), 3.84068548e-9_wp, "norm^2 with confinement", tol = 1.e-10_wp)
    call compare(scpr2, 4.3677293e-5_wp, "scpr^2", tol = 1.e-6_wp)

    call setup_clean(data)
    call wvf_deallocate_manager(manager)
  end subroutine prec_op

  subroutine nl_op(label)
    use liborbs_precisions
    use liborbs_functions
    use locregs
    use setup
    use f_arrays
    use dynamic_memory
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data), target :: data
    type(wvf_manager), target :: manager, manager_p
    type(wvf_daub_view) :: view
    type(workarrays_tolr) :: w
    type(interacting_locreg) :: Plr
    integer, parameter :: mproj = 5
    real(wp), dimension(:,:), allocatable :: proj
    integer :: iproj, ig
    type(f_scalar) :: sigma
    real(wp), dimension(2, mproj) :: scpr, ref

    label = "non local operator"

    call setup_surfaceBC(data)

    call nullify_interacting_locreg(plr)
    plr%plr = data%lr

    call create_workarrays_tolr(w, data%lr)
    call workarrays_tolr_add_plr(w, plr%plr)
    call workarrays_tolr_allocate(w)
    call set_lr_to_lr(plr, data%lr, w)
    call deallocate_workarrays_tolr(w)

    manager_p = wvf_manager_new(1)
    proj = f_malloc0((/ array_dim(data%lr), mproj /), id = 'proj')
    do iproj = 1, mproj
       view = wvf_view_on_daub(manager_p, plr%plr, psi = proj(1, iproj))
       do ig = 1, 4
          sigma = 0.5_gp / ig
          call wvf_accumulate_gaussian(view, sigma, (/ 1.5_gp, 1.2_gp, 0.8_gp /))
       end do
       call wvf_finish_accumulate_gaussians(view)
       call wvf_normalize(view)
    end do
    call wvf_deallocate_manager(manager_p)

    manager = wvf_manager_new(2)
    view = wvf_view_on_daub(manager, data%lr)
    view%c%mem = 1._wp
    call wvf_add_noise(view)
    call wvf_normalize(view)

    scpr = wvf_scal_proj(view, mproj, plr%plr, proj, &
         get_lr_to_lr(plr, 1, data%lr, data%lr), manager)

    ref = 0.1340881154_wp
    call compare(scpr, ref, "scpr", tol = 1e-10_wp)

    call wvf_non_local_inplace(view, scpr, mproj, plr%plr, proj, &
         get_lr_to_lr(plr, 1, data%lr, data%lr), manager)
    call compare(wvf_nrm(view), 1.50285514_wp, "norm", tol = 1e-8_wp)
    view%c%mem = view%c%mem ** 2
    call compare(wvf_nrm(view), 9.48229472e-2_wp, "norm^2", tol = 1e-8_wp)

    call f_free(proj)
    call setup_clean(data)
    call wvf_deallocate_manager(manager)
    plr%plr = locreg_null()
    call deallocate_interacting_locreg(plr)
  end subroutine nl_op

end program test_wvf
