#ifndef PRECONDITIONER_H
#define PRECONDITIONER_H

#include "liborbs_ocl.h"

/** Preconditions the data for further hamiltonian minimizing, in periodic boundary conditions.
 *  @param command_queue used to process the data.
 *  @param dimensions of the input data, vector of 3 values.
 *  @param h hgrid of the system, vector of 3 values.
 *  @param c scaling factor.
 *  @param ncong number of iterations of the preconditioner.
 *  @param nseg_c number of segment of coarse data.
 *  @param nvctr_c number of point of coarse data.
 *  @param keyg_c array of size 2 * nseg_c * sizeof(uint), representing the beginning and end of coarse segments in the compressed data.
 *  @param keyv_c array of size nseg_c * sizeof(uint), representing the beginning of coarse segments in the uncompressed data.
 *  @param nseg_f number of segment of fine data.
 *  @param nvctr_f number of point of fine data.
 *  @param keyg_f array of size 2 * nseg_f * sizeof(uint), representing the beginning and end of fine segments in the compressed data.
 *  @param keyv_f array of size nseg_f * sizeof(uint), representing the beginning of fine segments in the uncompressed data.
 *  @param psi_c array of size nvctr_c * sizeof(double), containing coarse input and output data.
 *  @param psi_f array of size nvctr_f * 7 * sizeof(double), containing fine input and output data.
 *  @param psi_c_r temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_r temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param psi_c_b temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_b temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param psi_c_d temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_d temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param work1 temporary buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double).
 *  @param work2 temporary buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double).
 *  @param work3 temporary buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double).
 *  @param work4 temporary buffer of size (2 * dimensions[0]) * (2 * dimensions[1]) * (2 * dimensions[2]) * sizeof(double).
 */
void ocl_preconditioner(liborbs_command_queue *command_queue,
                        const cl_uint dimensions[3], const double h[3],
                        double c, cl_uint ncong,
                        cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c,
                        cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                        cl_mem psi_c, cl_mem psi_f,
                        cl_mem psi_c_r, cl_mem psi_f_r,
                        cl_mem psi_c_b, cl_mem psi_f_b,
                        cl_mem psi_c_d, cl_mem psi_f_d,
                        cl_mem work1, cl_mem work2, cl_mem work3, cl_mem work4);

/** Preconditions the data for further hamiltonian minimizing.
 *  @param command_queue used to process the data.
 *  @param dimensions of the input data, vector of 3 values.
 *  @param periodic periodicity of the convolution. Vector of three value, one for each dimension. Non zero means periodic.
 *  @param h hgrid of the system, vector of 3 values.
 *  @param c scaling factor.
 *  @param ncong number of iterations of the preconditioner.
 *  @param nseg_c number of segment of coarse data.
 *  @param nvctr_c number of point of coarse data.
 *  @param keyg_c array of size 2 * nseg_c * sizeof(uint), representing the beginning and end of coarse segments in the compressed data.
 *  @param keyv_c array of size nseg_c * sizeof(uint), representing the beginning of coarse segments in the uncompressed data.
 *  @param nseg_f number of segment of fine data.
 *  @param nvctr_f number of point of fine data.
 *  @param keyg_f array of size 2 * nseg_f * sizeof(uint), representing the beginning and end of fine segments in the compressed data.
 *  @param keyv_f array of size nseg_f * sizeof(uint), representing the beginning of fine segments in the uncompressed data.
 *  @param psi_c array of size nvctr_c * sizeof(double), containing coarse input and output data.
 *  @param psi_f array of size nvctr_f * 7 * sizeof(double), containing fine input and output data.
 *  @param psi_c_r temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_r temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param psi_c_b temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_b temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param psi_c_d temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_d temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param work1 temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work2 temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work3 temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work4 temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 */
void ocl_preconditioner_generic(liborbs_command_queue *command_queue,
                                const cl_uint dimensions[3], const cl_uint periodic[3],
                                const double h[3], double c, cl_uint ncong,
                                cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                                cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                                cl_mem psi_c, cl_mem psi_f,
                                cl_mem psi_c_r, cl_mem psi_f_r,
                                cl_mem psi_c_b, cl_mem psi_f_b,
                                cl_mem psi_c_d, cl_mem psi_f_d,
                                cl_mem work1, cl_mem work2, cl_mem work3, cl_mem work4);

/** Preconditions the data for further hamiltonian minimizing.
 *  @param command_queue used to process the data.
 *  @param dimensions of the input data, vector of 3 values.
 *  @param periodic periodicity of the convolution. Vector of three value, one for each dimension. Non zero means periodic.
 *  @param h hgrid of the system, vector of 3 values.
 *  @param k k-points values in the reduced Brillouin zone.
 *  @param c scaling factor.
 *  @param ncong number of iterations of the preconditioner.
 *  @param nseg_c number of segment of coarse data.
 *  @param nvctr_c number of point of coarse data.
 *  @param keyg_c array of size 2 * nseg_c * sizeof(uint), representing the beginning and end of coarse segments in the compressed data.
 *  @param keyv_c array of size nseg_c * sizeof(uint), representing the beginning of coarse segments in the uncompressed data.
 *  @param nseg_f number of segment of fine data.
 *  @param nvctr_f number of point of fine data.
 *  @param keyg_f array of size 2 * nseg_f * sizeof(uint), representing the beginning and end of fine segments in the compressed data.
 *  @param keyv_f array of size nseg_f * sizeof(uint), representing the beginning of fine segments in the uncompressed data.
 *  @param psi_c_r array of size nvctr_c * sizeof(double), containing coarse input and output data.
 *  @param psi_f_r array of size nvctr_f * 7 * sizeof(double), containing fine input and output data.
 *  @param psi_c_i array of size nvctr_c * sizeof(double), containing coarse input and output data.
 *  @param psi_f_i array of size nvctr_f * 7 * sizeof(double), containing fine input and output data.
 *  @param psi_c_r_r temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_r_r temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param psi_c_r_i temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_r_i temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param psi_c_b_r temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_b_r temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param psi_c_b_i temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_b_i temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param psi_c_d_r temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_d_r temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param psi_c_d_i temporary buffer of size nvctr_c * sizeof(double).
 *  @param psi_f_d_i temporary buffer of size nvctr_f * 7 * sizeof(double).
 *  @param work1_r temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work2_r temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work3_r temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work4_r temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work1_i temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work2_i temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work3_i temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param work4_i temporary buffer of size (2 * dimensions[0] + (periodic[0]?0:14+15)) * (2 * dimensions[1] + (periodic[1]?0:14+15)) * (2 * dimensions[2] + (periodic[2]?0:14+15)) * sizeof(double).
 *  @param nspinor identifies the number of internal components used in the computation (1: real only, 2: both, 4: unsupported).
 */
void ocl_preconditioner_generic_k(liborbs_command_queue *command_queue,
                                  const cl_uint dimensions[3],
                                  const cl_uint periodic[3],
                                  const double h[3], const double k[3],
                                  double c, cl_uint ncong,
                                  cl_uint nseg_c, cl_uint nvctr_c, cl_mem keyg_c, cl_mem keyv_c, 
                                  cl_uint nseg_f, cl_uint nvctr_f, cl_mem keyg_f, cl_mem keyv_f,
                                  cl_mem psi_c_r, cl_mem psi_f_r,
                                  cl_mem psi_c_i, cl_mem psi_f_i,
                                  cl_mem psi_c_r_r, cl_mem psi_f_r_r,
                                  cl_mem psi_c_r_i, cl_mem psi_f_r_i,
                                  cl_mem psi_c_b_r, cl_mem psi_f_b_r,
                                  cl_mem psi_c_b_i, cl_mem psi_f_b_i,
                                  cl_mem psi_c_d_r, cl_mem psi_f_d_r,
                                  cl_mem psi_c_d_i, cl_mem psi_f_d_i,
                                  cl_mem work1_r, cl_mem work2_r, cl_mem work3_r, cl_mem work4_r,
                                  cl_mem work1_i, cl_mem work2_i, cl_mem work3_i, cl_mem work4_i,
                                  cl_uint nspinor, double norm_sq_cf[2]);

#endif
