module liborbs_errors
  use dictionaries, only: f_err_raise, f_err_throw, f_err_check, f_err_pop, f_err_open_try, f_err_close_try
  use yaml_strings, only: yaml_toa, operator(//), operator(+)
  use f_utils, only: f_assert

  implicit none

  public

  integer, private, save :: COMPRESSION_ERROR = 0
  integer, private, save :: LOCREG_ERROR = 0
  integer, private, save :: OPERATION_ERROR = 0
  integer, private, save :: IO_FORMAT_ERROR = 0

  public :: LIBORBS_COMPRESSION_ERROR
  public :: LIBORBS_LOCREG_ERROR
  public :: LIBORBS_OPERATION_ERROR
  public :: LIBORBS_IO_FORMAT_ERROR
  
contains

  function LIBORBS_COMPRESSION_ERROR()
    use dictionaries
    implicit none
    integer :: LIBORBS_COMPRESSION_ERROR

    if (COMPRESSION_ERROR == 0) then
       call f_err_define('LIBORBS_COMPRESSION_ERROR',&
            'data compression for real space grids',&
            COMPRESSION_ERROR)
    end if
    LIBORBS_COMPRESSION_ERROR = COMPRESSION_ERROR
  end function LIBORBS_COMPRESSION_ERROR

  function LIBORBS_LOCREG_ERROR()
    use dictionaries
    implicit none
    integer :: LIBORBS_LOCREG_ERROR

    if (LOCREG_ERROR == 0) then
       call f_err_define('LIBORBS_LOCREG_ERROR',&
            'localisation region',&
            LOCREG_ERROR)
    end if
    LIBORBS_LOCREG_ERROR = LOCREG_ERROR
  end function LIBORBS_LOCREG_ERROR

  function LIBORBS_OPERATION_ERROR()
    use dictionaries
    implicit none
    integer :: LIBORBS_OPERATION_ERROR

    if (OPERATION_ERROR == 0) then
       call f_err_define('LIBORBS_OPERATION_ERROR',&
            'localisation region operation',&
            OPERATION_ERROR)
    end if
    LIBORBS_OPERATION_ERROR = OPERATION_ERROR
  end function LIBORBS_OPERATION_ERROR

  function LIBORBS_IO_FORMAT_ERROR()
    use dictionaries
    implicit none
    integer :: LIBORBS_IO_FORMAT_ERROR

    if (IO_FORMAT_ERROR == 0) then
       call f_err_define('LIBORBS_IO_FORMAT_ERROR',&
            'localisation region operation',&
            IO_FORMAT_ERROR)
    end if
    LIBORBS_IO_FORMAT_ERROR = IO_FORMAT_ERROR
  end function LIBORBS_IO_FORMAT_ERROR

end module liborbs_errors
