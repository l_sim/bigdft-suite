!> @file
!! Datatypes and associated methods relative to the localization regions (mesh grid)
!! @author
!!    Copyright (C) 2007-2015 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS
!> Datatypes for localization regions descriptors
module locregs
  use f_precisions, only: f_address
  use liborbs_precisions
  use dynamic_memory
  use box, only: cell,box_iterator
  use compression, wfd_read_array_bin => read_array_bin, wfd_read_array_txt => read_array_txt, &
       wfd_dump_array_bin => dump_array_bin, wfd_dump_array_txt => dump_array_txt
  use bounds, only: convolutions_bounds
  implicit none

  private

  integer, parameter :: SIZE_=1,LR_=2

  !> Contains the information needed for describing completely a wavefunction localisation region
  type, public :: locreg_descriptors
!!$     character(len=1) :: geocode            !< @copydoc poisson_solver::doc::geocode
     logical :: hybrid_on = .false.                  !< Interesting for global, periodic, localisation regions
     integer :: Localnorb = 0                  !< Number of orbitals contained in locreg
     integer, dimension(3) :: outofzone = 0     !< Vector of points outside of the zone outside Glr for periodic systems
     real(gp), dimension(3) :: locregCenter = 0.0_gp !< Center of the locreg
     real(gp) :: locrad = 0.0_gp                    !< Cutoff radius of the localization region
     real(gp) :: locrad_kernel = 0.0_gp             !< Cutoff radius of the localization region (kernel)
     real(gp) :: locrad_mult = 0.0_gp               !< Cutoff radius of the localization region for the sparse matrix multiplications
     integer, dimension(2, 3) :: nboxc !< Bounding box of the coarse grid
     integer, dimension(2, 3) :: nboxi !< Bounding box of the interpolating grid
     integer, dimension(2, 3) :: nboxf !< Bounding box of the fine grid
     type(wavefunctions_descriptors) :: wfd
     integer(f_address) :: gpu_context = 0_f_address, gpu_queue = 0_f_address
     type(ocl_wavefunctions_descriptors) :: ocl_wfd
     type(convolutions_bounds) :: bounds
     type(cell) :: mesh !<defines the cell of the system
                        !! (should replace the other geometrical informations)
     !> grid in the fine scaling functions box
     type(cell) :: mesh_fine
     type(cell) :: mesh_coarse !<discretization of the coarse egrees of freedom
     !>iterator over the mesh degrees of freedom
     type(box_iterator) :: bit
  end type locreg_descriptors

  !>storage of localization regions, to be used for
  !!communicating the descriptors
  type, public :: locregs_ptr
     type(locreg_descriptors), pointer :: lr=>null()
     logical :: owner = .false.
  end type locregs_ptr

  type, public :: locreg_storage
     type(locregs_ptr), dimension(:), pointer  :: lrs_ptr=>null()
     integer, dimension(:), pointer :: encode_buffer => null()
     integer, dimension(:,:), pointer :: lr_full_sizes => null()
     integer :: lr_size = 0
  end type locreg_storage

  interface assignment(=)
     module procedure allocate_locregs_ptr
  end interface assignment(=)

  type, public :: interacting_locreg
     type(locreg_descriptors) :: plr !< localization region descriptor
     integer :: nlr !< total no. localization regions potentially interacting with the plr
     type(wfd_to_wfd), dimension(:), pointer :: tolr !<maskings for the locregs, dimension noverlap
     integer,dimension(:),pointer :: lut_tolr !< lookup table for tolr, dimension noverlap
     integer :: noverlap !< number of locregs which overlap with the projectors of the given atom
  end type interacting_locreg

  ! Used outside liborbs:
  public :: locreg_null,nullify_locreg_descriptors
  public :: init_lr, init_llr_from_spheres, init_llr_from_centers
  public :: init_glr_from_grids, init_llr_from_subgrids, init_sized
  public :: lr_attach_gpu
  public :: copy_locreg_descriptors
  public :: deallocate_locreg_descriptors
  public :: draw_locregs
  public :: check_overlap
  public :: get_isf_offset,ensure_locreg_bounds, lr_broadcast_wfd
  public :: array_dim, grid_dim
  public :: lr_merge_to_dict, lr_set_from_dict
  public :: print_lr_compression
  public :: read_lr_txt, read_lr_bin
  public :: read_array_txt, read_array_bin
  public :: dump_array_txt, dump_array_bin
  public :: lr_reformat, lr_do_reformat, lr_rewrap
  public :: nullify_interacting_locreg, deallocate_interacting_locreg
  public :: lr_resize

  public :: workarrays_tolr, workarrays_tolr_allocate, deallocate_workarrays_tolr
  public :: workarrays_tolr_add_plr, create_workarrays_tolr
  interface create_workarrays_tolr
     module procedure create_workarrays_tolr_1d, create_workarrays_tolr_0d
  end interface create_workarrays_tolr
  public :: set_lr_to_lr, get_lr_to_lr

  interface read_array_txt
     module procedure read_array_plain_txt, read_array_compress_txt
  end interface read_array_txt
  interface read_array_bin
     module procedure read_array_plain_bin, read_array_compress_bin
  end interface read_array_bin

  interface lr_reformat
     module procedure lr_reformat_grid, lr_reformat_array
  end interface lr_reformat

  interface lr_rewrap
     module procedure lr_rewrap_grid, lr_rewrap_array
  end interface lr_rewrap

  public :: lr_invert, lr_bucket, lr_count_bucketing, lr_reduce_bucketing

  public :: extract_lr,gather_locreg_storage
  public :: lr_storage_free,lr_storage_init
  public :: steal_lr

  public :: lr_loc_weights, lr_n_c_points, lr_n_f_points
  
  ! Used only in liborbs:
  public :: store_lr
  public :: lr_is_stored, lr_is_stolen
  public :: lr_accumulate

  interface lr_accumulate
     module procedure acc_from_tensprod, acc_from_tensprod_cossin
  end interface lr_accumulate


  character(len = *), parameter :: KEY_HGRIDS = "hgrids"
  character(len = *), parameter :: KEY_NDIMS = "ndims"
  character(len = *), parameter :: KEY_OFFSET = "offset"
  character(len = *), parameter :: KEY_NDIMS_FINE = "ndims (fine)"
  character(len = *), parameter :: KEY_NS_FINE = "offset (fine)"
  character(len = *), parameter :: KEY_RADIUS = "radius"
  character(len = *), parameter :: KEY_CENTER = "center"
  character(len = *), parameter :: KEY_KEYS = "keys"
  character(len = *), parameter :: KEY_BINKEYS = "keys (binary)"

contains


  pure function locreg_null() result(lr)
    implicit none
    type(locreg_descriptors) :: lr
    call nullify_locreg_descriptors(lr)
  end function locreg_null


  pure subroutine nullify_locreg_descriptors(lr)
    use box
    use bounds, only: nullify_convolutions_bounds
    implicit none
    type(locreg_descriptors), intent(out) :: lr
    lr%hybrid_on=.false.
    lr%Localnorb=0
    lr%outofzone=(/0,0,0/)
    lr%nboxc = 0
    lr%nboxi = 0
    lr%nboxf = 0
    call nullify_wfd(lr%wfd)
    call nullify_convolutions_bounds(lr%bounds)
    lr%locregCenter=(/0.0_gp,0.0_gp,0.0_gp/)
    lr%locrad_kernel = 0.0_gp
    lr%locrad_mult = 0.0_gp
    lr%locrad=0.0_gp
    lr%mesh=cell_null()
    lr%mesh_fine=cell_null()
    lr%mesh_coarse=cell_null()
    call nullify_box_iterator(lr%bit)
  end subroutine nullify_locreg_descriptors

  pure subroutine nullify_lr_storage(lr_storage)
    implicit none
    type(locreg_storage), intent(out) :: lr_storage
  end subroutine nullify_lr_storage

  !> Destructors
  subroutine deallocate_locreg_descriptors(lr)
    use bounds
    implicit none
    ! Calling arguments
    type(locreg_descriptors),intent(inout):: lr

    call deallocate_wfd(lr%wfd)
    call deallocate_ocl_wfd(lr%ocl_wfd)
    call deallocate_convolutions_bounds(lr%bounds)

  end subroutine deallocate_locreg_descriptors

  pure subroutine nullify_interacting_locreg(tolr)
    implicit none
    type(interacting_locreg), intent(out) :: tolr
    tolr%nlr=0
    call nullify_locreg_descriptors(tolr%plr)
    nullify(tolr%tolr)
    nullify(tolr%lut_tolr)
    tolr%noverlap=0
  end subroutine nullify_interacting_locreg

  subroutine deallocate_interacting_locreg(tolr)
    use dynamic_memory
    implicit none
    type(interacting_locreg), intent(inout) :: tolr
    !local variables
    integer :: ilr
    if (associated(tolr%tolr)) then
       do ilr=lbound(tolr%tolr,1),ubound(tolr%tolr,1)
          call deallocate_wfd_to_wfd(tolr%tolr(ilr))
       end do
       deallocate(tolr%tolr)
       nullify(tolr%tolr)
    end if
    call f_free_ptr(tolr%lut_tolr)
    call deallocate_locreg_descriptors(tolr%plr)
  end subroutine deallocate_interacting_locreg

  subroutine nullify_lr_pointers(lr)
    use bounds
    use compression
    implicit none
    type(locreg_descriptors), intent(inout) :: lr

    !nullify pointers internal to the structure to avoid fake deallocation
    call nullify_wfd_pointers(lr%wfd)
    call nullify_convolutions_bounds(lr%bounds)
  end subroutine nullify_lr_pointers

  function lr_ptr_sizeof(array)
    use f_precisions, only: f_loc
    implicit none
    type(locregs_ptr), dimension(:), intent(in) :: array
    integer :: lr_ptr_sizeof
    !local variables
    type(locregs_ptr), dimension(2) :: lrs_ptr

    if (size(array) > 1) then
       lr_ptr_sizeof=int(f_loc(array(2))-f_loc(array(1)))
    else
       lr_ptr_sizeof=int(f_loc(lrs_ptr(2))-f_loc(lrs_ptr(1)))
    end if
  end function lr_ptr_sizeof

  subroutine allocate_locregs_ptr(array,m)
    use dynamic_memory
    use f_precisions, only: f_long, f_loc
    implicit none
    type(locregs_ptr), dimension(:), pointer, intent(inout) :: array
    type(malloc_information_ptr), intent(in) :: m
    !local variables
    integer :: ierror

    call f_timer_interrupt(TCAT_ARRAY_ALLOCATIONS)

    allocate(array(m%lbounds(1):m%ubounds(1)),stat=ierror)

    if (.not. malloc_validate(ierror,size(shape(array)),m)) return

    !here the database for the allocation might be updated
    call update_allocation_database(f_loc(array),&
         product(int(m%shape(1:m%rank),f_long)),lr_ptr_sizeof(array),m)

    call f_timer_resume()!TCAT_ARRAY_ALLOCATIONS

  end subroutine allocate_locregs_ptr

  subroutine locregs_ptr_free(array)
    use f_precisions, only: f_long, f_loc
    implicit none
    type(locregs_ptr), dimension(:), pointer, intent(inout) :: array
    !local variables
    integer :: ierror, i

    ! Deallocate the locregs we are owner of.
    do i = 1, size(array)
       if (array(i)%owner) then
          call deallocate_locreg_descriptors(array(i)%lr)
          deallocate(array(i)%lr)
          nullify(array(i)%lr)
       end if
    end do

    ! Deallocate container.
    call f_timer_interrupt(TCAT_ARRAY_ALLOCATIONS)

    call f_purge_database(product(int(shape(array),f_long)),lr_ptr_sizeof(array),&
         f_loc(array))

    deallocate(array,stat=ierror)

    if (.not. free_validate(ierror)) return
    nullify(array)
    call f_timer_resume()!TCAT_ARRAY_ALLOCATIONS
  end subroutine locregs_ptr_free

  subroutine lr_storage_init(lr_storage,nlr)
    implicit none
    type(locreg_storage), intent(out) :: lr_storage
    integer, intent(in) :: nlr

    lr_storage%lrs_ptr=f_malloc_ptr(nlr,id='lrs_ptr')
    lr_storage%lr_size=get_locreg_encode_size()
  end subroutine lr_storage_init

  subroutine lr_storage_free(lr_storage)
    implicit none
    type(locreg_storage), intent(inout) :: lr_storage
    call locregs_ptr_free(lr_storage%lrs_ptr)
    call f_free_ptr(lr_storage%encode_buffer)
    call f_free_ptr(lr_storage%lr_full_sizes)
    call nullify_lr_storage(lr_storage)
  end subroutine lr_storage_free

  !might be used in favour of copying of the datatypes
  function get_locreg_encode_size() result(s)
    implicit none
    integer :: s
    !local variable
    type(locreg_descriptors) :: lr
    integer, dimension(3) :: dummy_char_mold

    s=size(transfer(lr,dummy_char_mold))

  end function get_locreg_encode_size

  function get_locreg_full_encode_size(lr) result(s)
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    integer :: s

    s=get_locreg_encode_size()
    if (associated(lr%wfd%buffer)) s=s+size(lr%wfd%buffer)

  end function get_locreg_full_encode_size

  subroutine locreg_encode(lr,lr_size,dest)
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    integer, intent(in) :: lr_size !<obtained from locreg_encode_size
    !array of dimension at least equal to locreg_encode_size
    integer, dimension(lr_size), intent(out) :: dest

    dest=transfer(lr,dest)
  end subroutine locreg_encode

  subroutine locreg_full_encode(lr,lr_size,lr_full_size,dest)
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    integer, intent(in) :: lr_size,lr_full_size !<obtained from locreg_encode_size
    !array of dimension at least equal to locreg_full_encode_size
    integer, dimension(lr_full_size), intent(out) :: dest

    call locreg_encode(lr,lr_size,dest)
    if (lr_full_size-lr_size > 0) &
         & call f_memcpy(n=lr_full_size-lr_size,src=lr%wfd%buffer,dest=dest(lr_size+1))
  end subroutine locreg_full_encode

  subroutine locregs_encode(llr,nlr,lr_size,dest_arr,mask)
    implicit none
    integer, intent(in) :: lr_size,nlr
    type(locreg_descriptors), dimension(nlr), intent(in) :: llr
    !>array of minimal second dimension count(mask(nlr)), if present
    integer, dimension(lr_size,*), intent(out) :: dest_arr
    !> array of logical telling the locregs to mask
    logical, dimension(nlr), intent(in), optional :: mask
    !local variables
    integer :: ilr,iilr

    iilr=0
    do ilr=1,nlr
       iilr=iilr+1
       if (present(mask)) then
          if (.not. mask(ilr)) then
             iilr=iilr-1
             cycle
          end if
       end if
       call locreg_encode(llr(ilr),lr_size,dest_arr(1,iilr))
    end do

  end subroutine locregs_encode


  !> Decode a locreg from a src array
  !! warning, the status of the pointers of lr is nullified after communication
  subroutine locreg_decode(src,lr_size,lr)
    implicit none
    integer, intent(in) :: lr_size
    type(locreg_descriptors), target, intent(out) :: lr
    integer, dimension(lr_size), intent(in) :: src

    lr=transfer(src,lr)
    call nullify_lr_pointers(lr)
    lr%bit%mesh => lr%mesh
  end subroutine locreg_decode


  !> Decode a group of locregs given a src array
  subroutine locreg_full_decode(src,lr_size,lr_full_size,lr,bounds)
    use compression
    use f_utils
    implicit none
    integer, intent(in) :: lr_full_size,lr_size
    type(locreg_descriptors), intent(out) :: lr
    integer, dimension(lr_full_size), intent(in) :: src
    logical, intent(in), optional :: bounds
    !local variables
    integer, dimension(:), pointer :: buffer

    call locreg_decode(src,lr_size,lr)
    if (lr_size == lr_full_size) return
    buffer => f_subptr(src,from=lr_size+1,size=lr_full_size-lr_size)
    call wfd_keys_from_buffer(lr%wfd,buffer)

    if (f_get_option(default=.true.,opt=bounds)) call ensure_locreg_bounds(lr)

  end subroutine locreg_full_decode

  !> Locreg communication
  subroutine communicate_locreg_descriptors_basics(iproc, nproc, nlr, rootarr, calculateBounds, llr, mpi_comm)
    use wrapper_MPI
    implicit none

    ! Calling arguments
    integer,intent(in) :: iproc, nproc, nlr
    integer,dimension(nlr),intent(in) :: rootarr
    type(locreg_descriptors), dimension(nlr), intent(inout) :: llr
    logical, dimension(nlr), intent(in) :: calculateBounds
    integer, intent(in) :: mpi_comm

    ! Local variables
    character(len=*),parameter :: subname='communicate_locreg_descriptors_basics'
    integer :: ilr,iilr,jlr,jproc,lr_size,nlrp
    !type(locreg_descriptors) :: lr_tmp
    logical, dimension(:), allocatable :: mask
    integer, dimension(:), allocatable :: ipiv,recvcounts
    integer, dimension(:,:), allocatable :: encoded_send,encoded_recv

    call f_routine(id=subname)

    !first encode and communicate
    mask=f_malloc(nlr,id='mask')
    recvcounts=f_malloc0(0.to.nproc-1,id='recvcounts')

    do ilr=1,nlr
       !count order the locregs per process
       jproc=rootarr(ilr)
       recvcounts(jproc)=recvcounts(jproc)+1
       !mask the number of locreg that are associated to the
       !present mpi process
       mask(ilr) = iproc == jproc
    end do

    nlrp=recvcounts(iproc)

    lr_size=get_locreg_encode_size()
    encoded_send=f_malloc([lr_size,nlrp],id='encoded_send')

!!$    iilr=0
!!$    do ilr=1,nlr
!!$       if (mask(ilr)) then
!!$          iilr=iilr+1
!!$          print *,'ilr,iproc',iproc,ilr,llr(ilr)%wfd%nseg_c
!!$          call locreg_encode(llr(ilr),lr_size,encoded_send(1,iilr))
!!$          call locreg_decode(encoded_send(1,iilr),lr_size,lr_tmp)
!!$          print *,'ilr,iproc,after',iproc,ilr,lr_tmp%wfd%nseg_c
!!$       end if
!!$    end do

    call locregs_encode(llr,nlr,lr_size,encoded_send,mask)

    encoded_recv=f_malloc([lr_size,nlr],id='encoded_recv')

    recvcounts=lr_size*recvcounts

    call fmpi_allgather(sendbuf=encoded_send,&
         recvbuf=encoded_recv,&
         recvcounts=recvcounts,comm=mpi_comm)

    call f_free(mask)
    call f_free(recvcounts)
    call f_free(encoded_send)

    !then decode
    ipiv=f_malloc0(nlr,id='ipiv')
    iilr=0
    do jproc=0,nproc-1
       do ilr=1,nlr
          jlr=ilr
          if (jproc == iproc) jlr=0 !do not decode locregs already present on the process
          if (rootarr(ilr) == jproc) then
             iilr=iilr+1
             ipiv(iilr)=jlr
          end if
       end do
    end do
    do ilr=1,nlr
       !only decode locregs which were not present already
       if (ipiv(ilr) /= 0) then
          if (.not. calculateBounds(ipiv(ilr))) &
               call locreg_decode(encoded_recv(1,ilr),lr_size,llr(ipiv(ilr)))
       end if
    end do
    call f_free(ipiv)
    call f_free(encoded_recv)

    call f_release_routine()

  end subroutine communicate_locreg_descriptors_basics
  
  subroutine communicate_locreg_descriptors_keys(iproc, nproc, nlr, glr, llr, &
       rootarr, llr_on_all_mpi, nlr_max, lr_par, lr_ovr, comm)
    use dynamic_memory
    use wrapper_mpi
    use liborbs_errors
    implicit none

    ! Calling arguments
    integer,intent(in):: iproc, nproc, nlr, nlr_max, comm
    type(locreg_descriptors),intent(in) :: glr
    type(locreg_descriptors),dimension(nlr),intent(inout) :: llr
    integer,dimension(nlr),intent(in) :: rootarr
    integer,dimension(nlr_max, 0:nproc-1), intent(in) :: lr_par
    integer,dimension(:), intent(in) :: lr_ovr
    integer, intent(in) :: llr_on_all_mpi

    ! Local variables
    integer :: ierr, ilr, jlr, root, max_sim_comms
    !integer :: icomm, ilr_old, jtask, nalloc, nrecv
    integer :: maxrecvdim, maxsenddim, ioffset, ist_dest, ist_source, ithread, nthread, mthread
    integer :: ncount, iiorb, size_of_int, jproc, ii, ncover, isproc, nprocp
    logical :: isoverlap
    character(len=*), parameter:: subname='communicate_locreg_descriptors_keys'
    !integer,dimension(:),allocatable :: requests
    !integer,dimension(:,:),allocatable :: worksend_int, workrecv_int
    integer,dimension(:),allocatable :: worksend, workrecv, ncomm, nrecvcounts, types, derived_types!, tmparr_int, istarr
    integer, dimension(:), pointer :: buffer
    integer,dimension(:),allocatable :: cover_id, ncount_par, iscount_par
    integer,dimension(:,:),allocatable :: blocklengths
    integer(kind=fmpi_address),dimension(:,:),allocatable :: displacements
    logical,dimension(:),allocatable :: covered
    logical,dimension(:,:),allocatable :: mask
    type(fmpi_win) :: window
    !$ integer :: omp_get_thread_num, omp_get_max_threads

    call f_routine(id=subname)

    ithread = 0
    nthread = 1
    !$ nthread = omp_get_max_threads()

    ! should be 1D later...
    covered = f_malloc(nlr,id='covered')
    ! In principle bad practice to allocate with nthread, but as norb_max is usually small, it should be ok.
    mask = f_malloc((/1.to.nlr_max,0.to.nthread/),id='mask')
    ncount_par = f_malloc0(0.to.nproc-1,id='ncount_par')
    iscount_par = f_malloc0(0.to.nproc-1,id='iscount_par')

    ! Determine which locregs process iproc should get.
    ncover = 0
    !$omp parallel default(none) &
    !$omp shared(ncover, nlr, rootarr, covered, lr_par, lr_ovr, glr, llr, iproc, llr_on_all_mpi, nlr_max) &
    !$omp private(ilr, jlr, isoverlap)
    !$omp do reduction(+: ncover)
    do ilr=1,nlr
       covered(ilr) = .false.
       if (iproc == rootarr(ilr) .or. ilr==llr_on_all_mpi) cycle
       do jlr = 1, nlr_max
          if (lr_par(jlr, iproc) < 1) exit
          call check_overlap_cubic_periodic(glr,llr(ilr),llr(lr_par(jlr, iproc)),isoverlap)
          if (isoverlap) then
             covered(ilr)=.true.
             exit
          end if
       end do
       do jlr = 1, size(lr_ovr)
          call check_overlap_cubic_periodic(glr,llr(ilr),llr(lr_ovr(jlr)),isoverlap)
          if (isoverlap) then
             covered(ilr)=.true.
             exit
          end if
       end do
       if (covered(ilr)) then
          ncover = ncover + 1
       end if
    end do
    !$omp end do
    !$omp end parallel

    cover_id = f_malloc(ncover,id='cover_id')
    ii = 0
    do ilr=1,nlr
       if (covered(ilr)) then
          ii = ii + 1
          cover_id(ii) = ilr
       end if
    end do
    if (ii/=ncover) call f_err_throw('ii/=ncover')

    call f_free(covered)


    ! Each process makes its data available in a contiguous workarray.
    maxsenddim=0
    do ilr=1,nlr_max
       if (lr_par(ilr, iproc) > 0) then
          maxsenddim = maxsenddim + 6*(llr(lr_par(ilr, iproc))%wfd%nseg_c+llr(lr_par(ilr, iproc))%wfd%nseg_f)
       end if
    end do
    worksend = f_malloc(max(maxsenddim,1),id='worksend_int')

    ! Copy the keys to a contiguous array, ordered by the locreg ID (which
    ! is not necessarily identical to the orbital ID order).
    mask(:, 0) = lr_par(:, iproc) > 0
    ioffset=0
    !do iilr=1,nlr
    do while (any(mask(:, 0)))
       !iiorb=orbs%isorb+iorb
       !ilr=orbs%inwhichlocreg(iiorb)
       iiorb = minloc(lr_par(:, iproc), 1, mask(:, 0))
       ilr = lr_par(iiorb, iproc)
       mask(iiorb,0) = .false.
       !if (ilr==iilr) then
       !write(*,*) 'COPY: iproc, ilr', iproc, ilr
       ncount=llr(ilr)%wfd%nseg_c+llr(ilr)%wfd%nseg_f
       call f_memcpy(n=ncount,src=llr(ilr)%wfd%keyvloc(1),dest=worksend(ioffset+1))
       call f_memcpy(n=ncount,src=llr(ilr)%wfd%keyvglob(1),dest=worksend(ioffset+ncount+1))
       call f_memcpy(n=2*ncount,src= llr(ilr)%wfd%keyglob(1,1),dest=worksend(ioffset+2*ncount+1))
       call f_memcpy(n=2*ncount,src=llr(ilr)%wfd%keygloc(1,1),dest=worksend(ioffset+4*ncount+1))
       ioffset=ioffset+6*ncount
       !end if
    end do
    !end do

    ! Initialize the MPI window
    !!call mpi_type_size(mpi_integer, size_of_int, ierr)
    !!call mpi_info_create(info, ierr)
    !!call mpi_info_set(info, "no_locks", "true", ierr)
    !!call mpi_win_create(worksend(1), int(maxsenddim*size_of_int,kind=mpi_address_kind), size_of_int, &
    !!     info, bigdft_mpi%mpi_comm, window, ierr)
    !!call mpi_info_free(info, ierr)
    !!call mpi_win_fence(mpi_mode_noprecede, window, ierr)
    call mpi_type_size(mpitype(worksend), size_of_int, ierr)
    !window =  mpiwindow(maxsenddim, worksend(1),  bigdft_mpi%mpi_comm)
    call fmpi_win_create(window,worksend(1),maxsenddim,comm)
    call fmpi_win_fence(window,FMPI_WIN_OPEN)

    ! Allocate the receive buffer
    maxrecvdim=0
    !do ilr=1,nlr
    do ii=1,ncover
       ilr = cover_id(ii)
       !if (covered(ilr,iproc)) then
       ncount=6*(llr(ilr)%wfd%nseg_c+llr(ilr)%wfd%nseg_f)
       maxrecvdim=maxrecvdim+ncount
       !end if
    end do
    workrecv = f_malloc(maxrecvdim,id='workrecv')

    ! Do the communication. Communicate only once for each pair of MPI tasks using a derived datatype.
    ncomm = f_malloc0(0.to.nproc-1,id='ncomm')
    nrecvcounts = f_malloc0(0.to.nproc-1,id='nrecvcounts')
    blocklengths = f_malloc0((/1.to.nlr_max,0.to.nproc-1/),id='blocklengths')
    displacements = f_malloc0((/1.to.nlr_max,0.to.nproc-1/),id='displacements')

    nthread = 1
    ithread = 0
    !$ nthread = omp_get_max_threads()
    nprocp = nproc/nthread
    mthread = nproc-nthread*nprocp
    !$omp parallel default(none) &
    !$omp shared(mthread, ncover, cover_id, rootarr, ncomm, llr, blocklengths, nlr, mask) &
    !$omp shared(displacements, nrecvcounts, ncount_par, size_of_int, lr_par) &
    !$omp private(isproc, ilr, root, ii, ncount, ist_source, iiorb, jlr) &
    !$omp firstprivate(ithread, nprocp)
    !$ ithread = omp_get_thread_num()
    isproc = ithread*nprocp + min(mthread,ithread)
    if (ithread<mthread) nprocp = nprocp + 1
    do ii=1,ncover
       ilr = cover_id(ii)
       root=rootarr(ilr)
       if (root>=isproc .and. root<isproc+nprocp) then
          ncomm(root) = ncomm(root) + 1
          ncount=6*(llr(ilr)%wfd%nseg_c+llr(ilr)%wfd%nseg_f)
          ncount_par(root) = ncount_par(root) + ncount
          mask(:,ithread) = lr_par(:, root) > 0
          ist_source = 0
          do while(any(mask(:,ithread)))
             iiorb = minloc(lr_par(:, root), 1, mask(:, ithread))
             jlr=lr_par(iiorb, root)
             mask(iiorb, ithread) = .false.
             if (ilr==jlr) exit !locreg found
             ist_source=ist_source+6*(llr(jlr)%wfd%nseg_c+llr(jlr)%wfd%nseg_f)
          end do
          blocklengths(ncomm(root),root) = ncount
          displacements(ncomm(root),root) = int(ist_source*size_of_int,kind=fmpi_address)
          nrecvcounts(root) = nrecvcounts(root) + ncount
       end if
    end do
    !$omp end parallel


    iscount_par(0) = 0
    do jproc=1,nproc-1
       iscount_par(jproc) = iscount_par(jproc-1) + ncount_par(jproc-1)
    end do

!!! The values in displacements must be sorted...
!!!tmparr_long = f_malloc(maxval(ncomm(:)),id='tmparr')
!!!tmparr_int = f_malloc(maxval(ncomm(:)),id='tmparr')
!!!istarr = f_malloc(maxval(ncomm(:)),id='istarr')
    !!do jproc=0,nproc-1
    !!    !tmparr_long = displacements(1:ncomm(jproc),jproc)
    !!    !tmparr_int = blocklengths(1:ncomm(jproc),jproc)
    !!    ii = 0
    !!    do icomm=1,ncomm(jproc)
    !!        iloc = minloc(tmparr_long(1:ncomm(jproc),1)
    !!        displacements(icomm,jproc) = tmparr_long(iloc)
    !!        blocklengths(icomm,jproc) = tmparr_int(iloc)
    !!        !istarr(icomm) = ii
    !!        ii = ii + tmparr_int(iloc)
    !!    end do
    !!end do

    derived_types = f_malloc(0.to.nproc-1,id='derived_types')
    types = f_malloc(maxval(ncomm),id='types')
    types(:) = mpitype(workrecv)
    ist_dest=1
    do jproc=0,nproc-1
       !write(*,'(a,11i8)') 'iproc, jproc, ist_dest, blocklengths(1:ncomm(jproc)), displacements(1:ncomm(jproc))', iproc, jproc, ist_dest, blocklengths(1:ncomm(jproc),jproc), displacements(1:ncomm(jproc),jproc)
       call mpi_type_create_struct(ncomm(jproc), blocklengths(1,jproc), displacements(1,jproc), &
            types, derived_types(jproc), ierr)
       call mpi_type_commit(derived_types(jproc), ierr)
       !call mpi_type_size(types(1), ii, ierr)
       !write(*,*) 'size types', ii
       call mpi_type_size(derived_types(jproc), ii, ierr)
       if (ii/=nrecvcounts(jproc)*size_of_int) then
          !write(*,'(a,5i8)') 'iproc, jproc, ncomm(jproc), ii, nrecvcounts(jproc)*size_of_int', iproc, jproc, ncomm(jproc), ii, nrecvcounts(jproc)*size_of_int
          !write(*,'(a,10i8)') 'iproc, jproc, blocklengths(1:ncomm(jproc)), displacements(1:ncomm(jproc))', iproc, jproc, blocklengths(1:ncomm(jproc),jproc), displacements(1:ncomm(jproc),jproc)
          call f_err_throw('wrong size of derived_types(jproc)',err_id=LIBORBS_LOCREG_ERROR())
       end if
       if (nrecvcounts(jproc)>0) then
          !@todo this will have to be wrapped
          call mpi_get(workrecv(ist_dest), nrecvcounts(jproc), mpitype(workrecv), jproc, &
               int(0,kind=fmpi_address), 1, derived_types(jproc), window%handle, ierr)
       end if
       ist_dest = ist_dest + nrecvcounts(jproc)
    end do

    !    call mpi_get(workrecv(ist_dest), ncount, mpi_integer, root, &
    !         int(ist_source,kind=mpi_address_kind), ncount, mpi_integer, window, ierr)
    !    ist_dest=ist_dest+ncount
    !end if
    !end do

    ! Synchronize the communication
    !call mpi_win_fence(mpi_mode_nosucceed, window, ierr)
    call fmpi_win_fence(window,FMPI_WIN_CLOSE)
    do jproc=0,nproc-1
       call mpi_type_free(derived_types(jproc), ierr)
    end do
    call fmpi_win_free(window)
    call f_free(derived_types)

    call f_free(ncomm)
    call f_free(nrecvcounts)
    call f_free(blocklengths)
    call f_free(displacements)
    call f_free(types)

    !do ii=1,size(workrecv)
    !    write(100+iproc,*) ii, workrecv(ii)
    !end do


    ! Copy the date from the workarrays to the correct locations
    call f_free(worksend)
    !ist_dest=0
    !do jproc=0,nproc-1
    !do ilr=1,nlr
    !    if (.not.covered(ilr,iproc)) cycle
    do ii=1,ncover
       ilr = cover_id(ii)
       root = rootarr(ilr)
       ist_dest = iscount_par(root)
       !ist_dest = 0
       !do jproc=0,root-1
       !    ist_dest = ist_dest + ncount_par(jproc)
       !end do
       !do jorb=1,orbs%norb_par(root,0)
       !    jjorb = orbs%isorb_par(jproc) + jorb
       !    jlr = orbs%inwhichlocreg(jjorb)
       !    if (ilr==jlr) then
       ncount = llr(ilr)%wfd%nseg_c + llr(ilr)%wfd%nseg_f
       buffer => f_subptr(workrecv, from = ist_dest + 1, size = 6 * ncount)
       call wfd_keys_from_buffer(llr(ilr)%wfd, buffer)
       !    end if
       !end do
       iscount_par(root) = iscount_par(root) + 6*ncount
    end do
    !end do
    call f_free(workrecv)
    !call f_free(covered)
    call f_free(cover_id)
    call f_free(ncount_par)
    call f_free(iscount_par)

    call f_free(mask)

    call f_release_routine()

  contains

    pure function itag(ilr,recv)
      implicit none
      integer, intent(in) :: ilr,recv
      integer :: itag

      !itag=ilr+recv*nlr
      !itag=ilr+recv*max_sim_comms
      itag = mod(ilr-1,max_sim_comms)*nproc + recv + 1

    end function itag


  END SUBROUTINE communicate_locreg_descriptors_keys

  !put the localization region lr in the lrs_ptr array
  subroutine store_lr(lr_storage,ilr,lr)
    implicit none
    type(locreg_storage), intent(inout) :: lr_storage
    integer, intent(in) :: ilr
    type(locreg_descriptors), intent(in), target :: lr

    lr_storage%lrs_ptr(ilr)%lr => lr
    lr_storage%lrs_ptr(ilr)%owner = .false. ! Caller is responsible to free lr later
  end subroutine store_lr

  !take ownership of the localization region lr in the lrs_ptr array
  subroutine steal_lr(lr_storage,ilr,lr)
    implicit none
    type(locreg_storage), intent(inout) :: lr_storage
    integer, intent(in) :: ilr
    type(locreg_descriptors), intent(in) :: lr

    allocate(lr_storage%lrs_ptr(ilr)%lr)
    lr_storage%lrs_ptr(ilr)%lr = lr
    !call copy_locreg_descriptors(lr,lr_storage%lrs_ptr(ilr)%lr)
    lr_storage%lrs_ptr(ilr)%owner = .true. ! Caller should not touch lr anymore
  end subroutine steal_lr

  pure function lr_is_stored(lr_storage,ilr) result(ok)
    implicit none
    type(locreg_storage), intent(in) :: lr_storage
    integer, intent(in) :: ilr
    logical :: ok
    ok=associated(lr_storage%lrs_ptr(ilr)%lr) .and. .not. lr_storage%lrs_ptr(ilr)%owner
  end function lr_is_stored

  pure function lr_is_stolen(lr_storage,ilr) result(ok)
    implicit none
    type(locreg_storage), intent(in) :: lr_storage
    integer, intent(in) :: ilr
    logical :: ok
    ok=associated(lr_storage%lrs_ptr(ilr)%lr) .and. lr_storage%lrs_ptr(ilr)%owner
  end function lr_is_stolen


  !> Communicate the total locreg quantities given in a pointer of localisation regions
  !! WARNING: we assume that the association of the storage is performed in a mutually exclusive way, that if a locreg is associated on one proc it is not in the others.
  subroutine gather_locreg_storage(lr_storage, nproc, mpi_comm)
    use wrapper_MPI
    implicit none
    type(locreg_storage), intent(inout) :: lr_storage
    integer, intent(in) :: nproc, mpi_comm
    !local variables
    integer :: ilr,iilr,encoding_buffer_idx,nlr
    integer :: lr_full_size,encoding_buffer_size
    integer(f_long) :: full_encoding_buffer_size
    type(fmpi_win) :: win_counts
    integer, dimension(:), allocatable :: encoding_buffer
    integer, dimension(:,:), pointer :: lr_sizes


    nlr=size(lr_storage%lrs_ptr)
    lr_storage%lr_full_sizes=f_malloc0_ptr([2,nlr],id='lr_full_sizes')
    encoding_buffer_size=0
    iilr=0
    do ilr=1,nlr
       if ( .not. associated(lr_storage%lrs_ptr(ilr)%lr)) cycle
       iilr=iilr+1
       lr_full_size=get_locreg_full_encode_size(lr_storage%lrs_ptr(ilr)%lr)
       lr_storage%lr_full_sizes(SIZE_,iilr)=lr_full_size
       lr_storage%lr_full_sizes(LR_,iilr)=ilr
       encoding_buffer_size=encoding_buffer_size+lr_full_size
    end do

    if (nproc > 1) then
       lr_sizes=f_malloc_ptr([2,iilr],id='lr_sizes')
       if (iilr > 0) &
            & call f_memcpy(n=2*iilr,src=lr_storage%lr_full_sizes,dest=lr_sizes(1,1))
       call fmpi_allgather(sendbuf=lr_sizes,recvbuf=lr_storage%lr_full_sizes,&
            comm=mpi_comm,win=win_counts)
    else
       lr_sizes => lr_storage%lr_full_sizes
    end if

    encoding_buffer=f_malloc(encoding_buffer_size,id='encoding_buffer')
    !redo the loop to fill the encoding buffer
    encoding_buffer_idx=1
    iilr=0
    do ilr=1,nlr
       if ( .not. associated(lr_storage%lrs_ptr(ilr)%lr)) cycle
       iilr=iilr+1
       lr_full_size=lr_sizes(SIZE_,iilr) !storage%lr_full_sizes(SIZE_,ilr)
       call locreg_full_encode(lr_storage%lrs_ptr(ilr)%lr,lr_storage%lr_size, &
            & lr_full_size,encoding_buffer(encoding_buffer_idx))
       encoding_buffer_idx=encoding_buffer_idx+lr_full_size
    end do

    !close the previous communication window
    if (nproc > 1) then
       call fmpi_win_shut(win_counts)
       call f_free_ptr(lr_sizes)
    else
       nullify(lr_sizes)
    end if

    !allocate the full sized array
    full_encoding_buffer_size=0
    do ilr=1,nlr
       full_encoding_buffer_size=full_encoding_buffer_size+lr_storage%lr_full_sizes(SIZE_,ilr)
    end do
    lr_storage%encode_buffer=f_malloc_ptr(full_encoding_buffer_size,id='lr_storage%encode_buffer')

    !gather the array in the full encoding buffer
    if (nproc > 1) then
       call fmpi_allgather(sendbuf=encoding_buffer,recvbuf=lr_storage%encode_buffer,comm=mpi_comm)
    else
       call f_memcpy(src = encoding_buffer, dest = lr_storage%encode_buffer)
    end if

    call f_free(encoding_buffer)

  end subroutine gather_locreg_storage

  subroutine extract_lr(lr_storage,ilr,lr,bounds)
    use dictionaries, only: f_err_throw
    use f_precisions, only: f_long
    implicit none
    type(locreg_storage), intent(in) :: lr_storage
    integer, intent(in) :: ilr
    type(locreg_descriptors), intent(out) :: lr
    logical, intent(in), optional :: bounds
    !local variables
    integer :: iilr,nlr
    integer(f_long) :: encoding_buffer_idx
    integer, dimension(:), pointer :: src_buf

    nlr=size(lr_storage%lr_full_sizes,dim=2)
    !assume that lr_storage is ready for extraction
    encoding_buffer_idx=1
    do iilr=1,nlr
       if (lr_storage%lr_full_sizes(LR_,iilr) == ilr) exit
       encoding_buffer_idx=encoding_buffer_idx+lr_storage%lr_full_sizes(SIZE_,iilr)
    end do
    if (iilr == nlr+1) call f_err_throw('The locreg has not been found in the lr_storage',&
         err_name='BIGDFT_RUNTIME_ERROR')

    !we should generalize the API for the from optional variable (it should accept also f_long)
    src_buf => f_subptr(lr_storage%encode_buffer,&
         from=int(encoding_buffer_idx),size=lr_storage%lr_full_sizes(SIZE_,iilr))

    call locreg_full_decode(src_buf,&
         lr_storage%lr_size,lr_storage%lr_full_sizes(SIZE_,iilr),lr,bounds)

  end subroutine extract_lr

  !> Methods for copying the structures, can be needed to avoid recalculating them
  !! should be better by defining a f_malloc inheriting the shapes and the structure from other array
  !! of the type dest=f_malloc(src=source,id='dest')
  subroutine copy_locreg_descriptors(glrin, glrout)
    use bounds
    implicit none
    ! Calling arguments
    type(locreg_descriptors), intent(in) :: glrin !<input locreg. Unchanged on exit.
    type(locreg_descriptors), intent(out), target :: glrout !<output locreg. Must be freed on input.

    !we should here use encode and decode for safety

!!$    glrout%geocode = glrin%geocode
    glrout%hybrid_on = glrin%hybrid_on
    glrout%Localnorb = glrin%Localnorb
    glrout%locrad=glrin%locrad
    glrout%locrad_kernel=glrin%locrad_kernel
    glrout%locrad_mult=glrin%locrad_mult
    glrout%locregCenter=glrin%locregCenter
    glrout%outofzone= glrin%outofzone

    glrout%nboxc = glrin%nboxc
    glrout%nboxi = glrin%nboxi
    glrout%nboxf = glrin%nboxf
    call copy_wfd(glrin%wfd, glrout%wfd)
    glrout%gpu_context = glrin%gpu_context
    glrout%gpu_queue = glrin%gpu_queue
    if (glrout%gpu_context /= 0_f_address .and. glrout%gpu_queue /= 0_f_address) then
       call create_ocl_wfd(glrout%ocl_wfd, glrout%gpu_context, glrout%gpu_queue, glrout%wfd)
    end if
    call copy_convolutions_bounds(glrin%bounds, glrout%bounds)

    glrout%mesh=glrin%mesh
    glrout%mesh_fine=glrin%mesh_fine
    glrout%mesh_coarse=glrin%mesh_coarse
    glrout%bit=glrin%bit
    glrout%bit%mesh => glrout%mesh
  end subroutine copy_locreg_descriptors


  subroutine ensure_locreg_bounds(lr)
    use dynamic_memory
    use f_precisions, only: f_byte
    use compression
    use bounds, only: locreg_bounds_from_grids
    implicit none
    type(locreg_descriptors), intent(inout) :: lr
    logical(f_byte), dimension(:,:,:), allocatable :: logrid_c,logrid_f

    !take this as exemple of already associated bounds
    if (associated(lr%bounds%kb%ibyz_c)) return

    call f_routine(id='ensure_locreg_bounds')

    !define logrids
    logrid_c = f_malloc(lr%mesh_coarse%ndims, id='logrid_c')
    logrid_f = f_malloc(lr%mesh_coarse%ndims, id='logrid_f')

    call wfd_to_logrids(lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(2), &
         lr%mesh_coarse%ndims(3), lr%wfd, logrid_c, logrid_f)

    call locreg_bounds_from_grids(lr%mesh_coarse%ndims(1)-1, &
         lr%mesh_coarse%ndims(2)-1, lr%mesh_coarse%ndims(3)-1, &
         lr%nboxf, logrid_c, logrid_f, lr%bounds)

    call f_free(logrid_c)
    call f_free(logrid_f)

    call f_release_routine()
  end subroutine ensure_locreg_bounds

  pure function array_dim(lr)
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    integer :: array_dim

    array_dim = lr%wfd%nvctr_c + 7 * lr%wfd%nvctr_f
  end function array_dim

  pure function grid_dim(lr)
    use dynamic_memory
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    type(array_bounds), dimension(6) :: grid_dim

    grid_dim(1) = 0 .to. lr%mesh_coarse%ndims(1)-1
    grid_dim(2) = 1 .to. 2
    grid_dim(3) = 0 .to. lr%mesh_coarse%ndims(2)-1
    grid_dim(4) = 1 .to. 2
    grid_dim(5) = 0 .to. lr%mesh_coarse%ndims(3)-1
    grid_dim(6) = 1 .to. 2
  end function grid_dim

  !> Almost degenerate with get_number_of_overlap_region
  !! should merge the two... prefering this one since argument list is better
  subroutine check_overlap_cubic_periodic(Glr,Ilr,Jlr,isoverlap)
    use bounds, only: check_whether_bounds_overlap
    implicit none
    type(locreg_descriptors), intent(in) :: Glr
    type(locreg_descriptors), intent(in) :: Ilr
    type(locreg_descriptors), intent(in) :: Jlr
    logical, intent(out) :: isoverlap
    !Local variables
    integer :: is1, ie1, is2, ie2, is3, ie3, js1, je1, js2, je2, js3, je3
    logical :: overlap1, overlap2, overlap3
  !!  integer :: azones,bzones,ii,izones,jzones !, i_stat, i_all
  !!  logical :: go1, go2, go3
  !!  integer,dimension(3,8) :: astart,bstart,aend,bend

  !!  azones = 1
  !!  bzones = 1
  !!! Calculate the number of regions to cut alr and blr
  !!  do ii=1,3
  !!     if(Ilr%outofzone(ii) > 0) azones = azones * 2
  !!     if(Jlr%outofzone(ii) > 0) bzones = bzones * 2
  !!  end do
  !!
  !!!FRACTURE THE FIRST LOCALIZATION REGION
  !!  call fracture_periodic_zone(azones,Glr,Ilr,Ilr%outofzone,astart,aend)
  !!
  !!!FRACTURE SECOND LOCREG
  !!  call fracture_periodic_zone(bzones,Glr,Jlr,Jlr%outofzone,bstart,bend)
  !!
  !!! Now check if they overlap
  !!  isoverlap = .false.
  !!  loop_izones: do izones=1,azones
  !!    do jzones=1,bzones
  !!      go1 = (bstart(1,jzones) .le. aend(1,izones) .and. bend(1,jzones) .ge. astart(1,izones))
  !!      go2 = (bstart(2,jzones) .le. aend(2,izones) .and. bend(2,jzones) .ge. astart(2,izones))
  !!      go3 = (bstart(3,jzones) .le. aend(3,izones) .and. bend(3,jzones) .ge. astart(3,izones))
  !!      if(go1 .and. go2 .and. go3) then
  !!        isoverlap = .true.
  !!        exit loop_izones
  !!      end if
  !!    end do
  !!  end do loop_izones


    !@ NEW VERSION #########################################
    ! Shift all the indices into the periodic cell. This can result is starting
    ! indices being larger than ending indices
    isoverlap = .false.
    is3 = modulo(ilr%nboxc(1,3),glr%mesh_coarse%ndims(3))
    ie3 = modulo(ilr%nboxc(2,3),glr%mesh_coarse%ndims(3))
    js3 = modulo(jlr%nboxc(1,3),glr%mesh_coarse%ndims(3))
    je3 = modulo(jlr%nboxc(2,3),glr%mesh_coarse%ndims(3))
    overlap3 = check_whether_bounds_overlap(is3, ie3, js3, je3)
    if (overlap3) then
        is2 = modulo(ilr%nboxc(1,2),glr%mesh_coarse%ndims(2))
        ie2 = modulo(ilr%nboxc(2,2),glr%mesh_coarse%ndims(2))
        js2 = modulo(jlr%nboxc(1,2),glr%mesh_coarse%ndims(2))
        je2 = modulo(jlr%nboxc(2,2),glr%mesh_coarse%ndims(2))
        overlap2 = check_whether_bounds_overlap(is2, ie2, js2, je2)
        if (overlap2) then
            is1 = modulo(ilr%nboxc(1,1),glr%mesh_coarse%ndims(1))
            ie1 = modulo(ilr%nboxc(2,1),glr%mesh_coarse%ndims(1))
            js1 = modulo(jlr%nboxc(1,1),glr%mesh_coarse%ndims(1))
            je1 = modulo(jlr%nboxc(2,1),glr%mesh_coarse%ndims(1))
            overlap1 = check_whether_bounds_overlap(is1, ie1, js1, je1)
            if (overlap1) then
                ! If we are here, all three overlaps are true
                isoverlap = .true.
            end if
        end if
    end if

    !!if (overlap1 .and. overlap2 .and. overlap3) then
    !!    isoverlap = .true.
    !!else
    !!    isoverlap = .false.
    !!end if

    !@ END NEW VERSION #####################################

    !!!debug
    !!isoverlap=.true.

  end subroutine check_overlap_cubic_periodic

    subroutine check_overlap(Llr_i, Llr_j, Glr, overlap)
      implicit none

      ! Calling arguments
      type(locreg_descriptors),intent(in) :: Llr_i, Llr_j, Glr
      logical, intent(out) :: overlap

      ! Local variables
      integer :: onseg

      call check_overlap_cubic_periodic(Glr,Llr_i,Llr_j,overlap)
      if(overlap) then
         call check_overlap_from_descriptors_periodic(Llr_i%wfd%nseg_c, Llr_j%wfd%nseg_c,&
              Llr_i%wfd%keyglob, Llr_j%wfd%keyglob, overlap, onseg)
      end if
    end subroutine check_overlap

    ! check if Llrs overlap from there descriptors
    ! The periodicity is hidden in the fact that we are using the keyglobs
    ! which are correctly defined.
    subroutine check_overlap_from_descriptors_periodic(nseg_i, nseg_j, keyg_i, keyg_j,  &
         isoverlap, onseg)
      implicit none
      ! Calling arguments
      integer :: nseg_i, nseg_j
      integer,dimension(2,nseg_i),intent(in) :: keyg_i
      integer,dimension(2,nseg_j),intent(in) :: keyg_j
      logical,intent(out) :: isoverlap
      integer, intent(out) :: onseg
      ! Local variables
      integer :: iseg, jseg, istart, jstart, kstartg
      integer :: iend, jend, kendg, nseg_k


      ! Initialize some counters
      iseg=1
      jseg=1
      nseg_k=0
      isoverlap = .false.
      onseg = 0  ! in case they don't overlap
      ! Check whether all segments of both localization regions have been processed.
      if ((iseg>=nseg_i .and. jseg>=nseg_j) .or. nseg_i==0 .or. nseg_j==0) return

      segment_loop: do

         ! Starting point already in global coordinates
         istart=keyg_i(1,iseg)
         jstart=keyg_j(1,jseg)

         ! Ending point already in global coordinates
         iend=keyg_i(2,iseg)
         jend=keyg_j(2,jseg)
         ! Determine starting and ending point of the common segment in global coordinates.
         kstartg=max(istart,jstart)
         kendg=min(iend,jend)

         ! Check whether this common segment has a non-zero length
         if(kendg-kstartg+1>0) then
            isoverlap = .true.
            nseg_k=nseg_k+1
         end if

         ! Check whether all segments of both localization regions have been processed.
         if(iseg>=nseg_i .and. jseg>=nseg_j) exit segment_loop

         ! Increase the segment index
         if((iend<=jend .and. iseg<nseg_i) .or. jseg==nseg_j) then
            iseg=iseg+1
         else if(jseg<nseg_j) then
            jseg=jseg+1
         end if

      end do segment_loop

      if(isoverlap) then
         onseg = nseg_k
      end if

    end subroutine check_overlap_from_descriptors_periodic
    
    subroutine lr_attach_gpu(lr, context, queue)
      implicit none
      type(locreg_descriptors), intent(inout) :: lr
      integer(f_address), intent(in) :: context
      integer(f_address), intent(in), optional :: queue

      lr%gpu_context = context
      if (present(queue)) lr%gpu_queue = queue
    end subroutine lr_attach_gpu

    !> Create the localisation region information for cubic code
    !subroutine init_lr(lr,geocode,hgridsh,n1,n2,n3,nfl1,nfl2,nfl3,nfu1,nfu2,nfu3,&
    subroutine init_lr(lr,dom,hgridsh,nbox,nboxf,hybrid_flag,global_dom,boxit)
      use compression
      use bounds
      use box
      use at_domain
      implicit none
      logical, intent(in) :: hybrid_flag
      !character(len=1), intent(in) :: geocode !< @copydoc poisson_solver::doc::geocode
      type(domain), intent(in) :: dom !<data type for the simulation domain
      integer, dimension(2, 3), intent(in) :: nbox, nboxf
      real(gp), dimension(3), intent(in) :: hgridsh
      type(domain), intent(in), optional :: global_dom
      type(locreg_descriptors), intent(inout) :: lr
      logical, intent(in), optional :: boxit
      !local variables
      integer, parameter :: S0_GROW_BUFFER=14
      integer :: Gnbl1,Gnbl2,Gnbl3,Gnbr1,Gnbr2,Gnbr3
      integer :: Lnbl1,Lnbl2,Lnbl3,Lnbr1,Lnbr2,Lnbr3
      logical, dimension(3) :: peri,peri_glob
      integer, dimension(3) :: ndims
      real(gp), dimension(3) :: oxyz,hgrids
      type(domain) :: dom_tmp

      call f_routine(id='init_lr')

      ! Coarse cell
      lr%nboxc=nbox
      ndims(1)=lr%nboxc(2,1)-lr%nboxc(1,1)+1
      ndims(2)=lr%nboxc(2,2)-lr%nboxc(1,2)+1
      ndims(3)=lr%nboxc(2,3)-lr%nboxc(1,3)+1
      hgrids=2.0_gp*hgridsh
      dom_tmp=dom
      dom_tmp%acell=ndims*hgrids
      lr%mesh_coarse=cell_new(dom_tmp,ndims,hgrids)

      !dimensions of the fine grid inside the localisation region
      lr%nboxf(1,1)=max(nbox(1,1),nboxf(1,1))-nbox(1,1)
      lr%nboxf(1,2)=max(nbox(1,2),nboxf(1,2))-nbox(1,2)
      lr%nboxf(1,3)=max(nbox(1,3),nboxf(1,3))-nbox(1,3)
      !NOTE: This will not work with symmetries (must change it)
      lr%nboxf(2,1)=min(nbox(2,1),nboxf(2,1))-nbox(1,1)
      lr%nboxf(2,2)=min(nbox(2,2),nboxf(2,2))-nbox(1,2)
      lr%nboxf(2,3)=min(nbox(2,3),nboxf(2,3))-nbox(1,3)
      if (lr%nboxf(2,1) < lr%nboxf(1,1)) lr%nboxf(:,1) = ndims(1) / 2
      if (lr%nboxf(2,2) < lr%nboxf(1,2)) lr%nboxf(:,2) = ndims(2) / 2
      if (lr%nboxf(2,3) < lr%nboxf(1,3)) lr%nboxf(:,3) = ndims(3) / 2

      lr%nboxi(1,1)=0
      lr%nboxi(1,2)=0
      lr%nboxi(1,3)=0
      peri=domain_periodic_dims(dom)
      call ext_buffers(peri(1),Lnbl1,Lnbr1)
      call ext_buffers(peri(2),Lnbl2,Lnbr2)
      call ext_buffers(peri(3),Lnbl3,Lnbr3)
      if (present(global_dom)) then
         peri_glob=domain_periodic_dims(global_dom)
         call ext_buffers(peri_glob(1),Gnbl1,Gnbr1)
         call ext_buffers(peri_glob(2),Gnbl2,Gnbr2)
         call ext_buffers(peri_glob(3),Gnbl3,Gnbr3)
         lr%nboxi(1,1)= 2 * lr%nboxc(1,1) - (Lnbl1 - Gnbl1)
         lr%nboxi(1,2)= 2 * lr%nboxc(1,2) - (Lnbl2 - Gnbl2)
         lr%nboxi(1,3)= 2 * lr%nboxc(1,3) - (Lnbl3 - Gnbl3)
      end if
      lr%nboxi(2,1) = 2 * lr%mesh_coarse%ndims(1) + Lnbl1 + Lnbr1 + lr%nboxi(1,1)
      lr%nboxi(2,2) = 2 * lr%mesh_coarse%ndims(2) + Lnbl2 + Lnbr2 + lr%nboxi(1,2)
      lr%nboxi(2,3) = 2 * lr%mesh_coarse%ndims(3) + Lnbl3 + Lnbr3 + lr%nboxi(1,3)

      !this is the mesh in real space (ISF basis)
      ndims(1) = lr%nboxi(2,1) - lr%nboxi(1,1)
      ndims(2) = lr%nboxi(2,2) - lr%nboxi(1,2)
      ndims(3) = lr%nboxi(2,3) - lr%nboxi(1,3)
      dom_tmp=dom
      dom_tmp%acell=ndims*hgridsh
      lr%mesh=cell_new(dom_tmp,ndims,hgridsh)

      !this is the mesh in the fine scaling function
      call ext_buffers_coarse(peri(1),Lnbl1)
      call ext_buffers_coarse(peri(2),Lnbl2)
      call ext_buffers_coarse(peri(3),Lnbl3)
      ndims(1)=2*(lr%mesh_coarse%ndims(1)+Lnbl1)
      ndims(2)=2*(lr%mesh_coarse%ndims(2)+Lnbl2)
      ndims(3)=2*(lr%mesh_coarse%ndims(3)+Lnbl3)
      dom_tmp=dom
      dom_tmp%acell=ndims*hgridsh
      lr%mesh_fine=cell_new(dom_tmp,ndims,hgridsh)

      lr%hybrid_on = hybrid_flag
      lr%hybrid_on=lr%hybrid_on .and. (nboxf(2,1)-nboxf(1,1)+S0_GROW_BUFFER < nbox(2,1)+1)
      lr%hybrid_on=lr%hybrid_on .and. (nboxf(2,2)-nboxf(1,2)+S0_GROW_BUFFER < nbox(2,2)+1)
      lr%hybrid_on=lr%hybrid_on .and. (nboxf(2,3)-nboxf(1,3)+S0_GROW_BUFFER < nbox(2,3)+1)

      !here we have to put the modifications of the origin for the
      !iterator of the lr. get_isf_offset should be used as
      !soon as global_geocode is replaced
      oxyz=locreg_mesh_origin(lr%mesh)
      lr%bit=box_iter(lr%mesh,origin=oxyz)
      if (present(boxit)) then
         if (boxit) call box_iter_validate(lr%bit)
      else
         call box_iter_validate(lr%bit)
      end if

      call f_release_routine()

    END SUBROUTINE init_lr

    subroutine init_glr_from_grids(Glr, logrid_c, logrid_f, calculate_bounds, warn, pin)
      use f_precisions, only: f_byte
      use compression
      use bounds, only: locreg_bounds_from_grids, make_bounds_per, make_all_ib_per
      use at_domain, only: domain_geocode
      use liborbs_errors
      use yaml_output
      implicit none
      !Arguments
      type(locreg_descriptors), intent(inout) :: Glr
      logical, intent(in) :: calculate_bounds
      logical(f_byte), dimension(Glr%mesh_coarse%ndims(1),&
           Glr%mesh_coarse%ndims(2),Glr%mesh_coarse%ndims(3)), intent(in) :: logrid_c,logrid_f
      logical, intent(in), optional :: warn
      logical, intent(in), optional :: pin
      !local variables
      integer :: n1,n2,n3

      !assign the dimensions to improve (a little) readability
      n1=Glr%mesh_coarse%ndims(1)-1
      n2=Glr%mesh_coarse%ndims(2)-1
      n3=Glr%mesh_coarse%ndims(3)-1

      call init_wfd_from_full_grids(Glr%wfd, n1, n2, n3, logrid_c, logrid_f)
      if (Glr%wfd%nseg_c == 0) then
         call f_err_throw('There are no coarse grid points (nseg_c=0)! '//&
              'Control if the atomic positions have been specified.', &
              err_id=LIBORBS_LOCREG_ERROR())
      end if
      if (Glr%gpu_queue /= 0_f_address .and. Glr%gpu_context /= 0_f_address) then
         call create_ocl_wfd(Glr%ocl_wfd, Glr%gpu_context, Glr%gpu_queue, Glr%wfd, pin)
      end if

      !for free BC admits the bounds arrays
      if (calculate_bounds .and. domain_geocode(Glr%mesh%dom) == 'F' ) then
         call locreg_bounds_from_grids(n1,n2,n3,Glr%nboxf, &
              logrid_c,logrid_f,Glr%bounds)
      end if

      if (calculate_bounds .and. domain_geocode(Glr%mesh%dom) == 'P' .and. Glr%hybrid_on) then
         call make_bounds_per(n1,n2,n3,Glr%nboxf,Glr%bounds,Glr%wfd)
         call make_all_ib_per(n1,n2,n3,Glr%nboxf,&
              &   Glr%bounds%kb%ibxy_f,Glr%bounds%sb%ibxy_ff,Glr%bounds%sb%ibzzx_f,Glr%bounds%sb%ibyyzz_f,&
              &   Glr%bounds%kb%ibyz_f,Glr%bounds%gb%ibyz_ff,Glr%bounds%gb%ibzxx_f,Glr%bounds%gb%ibxxyy_f)
      endif

      if (present(warn) .and. domain_geocode(Glr%mesh%dom) == 'P' .and. .not. Glr%hybrid_on &
           .and. Glr%wfd%nvctr_c /= int(Glr%mesh_coarse%ndim) ) then
         if (warn) then
            call yaml_warning('The coarse grid does not fill the entire periodic box. '// &
                 & 'Errors due to translational invariance breaking may occur')
         end if
      end if

    END SUBROUTINE init_glr_from_grids

    subroutine init_llr_from_subgrids(lr, gdims, logrid_c, nboxc, logrid_f, nboxf, full , nbox, pin)
      use f_precisions, only: f_byte
      use compression
      implicit none
      type(locreg_descriptors), intent(inout) :: lr
      integer, dimension(3), intent(in) :: gdims
      logical(f_byte), dimension(gdims(1),&
           gdims(2),gdims(3)), intent(in) :: logrid_c,logrid_f
      integer, dimension(2,3), intent(in) :: nboxc, nboxf, nbox
      logical, intent(in) :: full
      logical, intent(in), optional :: pin

      call init_wfd_from_sub_grids(lr%wfd, gdims(1)-1, gdims(2)-1, gdims(3)-1, &
           logrid_c, nboxc, logrid_f, nboxf, full, nbox)
      if (lr%gpu_queue /= 0_f_address .and. lr%gpu_context /= 0_f_address) then
         call create_ocl_wfd(lr%ocl_wfd, lr%gpu_context, lr%gpu_queue, lr%wfd, pin)
      end if
    end subroutine init_llr_from_subgrids

    subroutine init_sized(lr, nvctr_c, nvctr_f)
      implicit none
      type(locreg_descriptors), intent(inout) :: lr
      integer, intent(in) :: nvctr_c, nvctr_f

      lr%wfd%nvctr_c = nvctr_c
      lr%wfd%nvctr_f = nvctr_f
    end subroutine init_sized

    subroutine init_llr_from_spheres(iproc, nproc, glr, nlr, llr, &
         llr_on_all_mpi, calculateBounds, nlr_max, lr_par, lr_ovr, comm)
      use at_domain, only: domain_geocode,domain_periodic_dims
      use liborbs_precisions
      use liborbs_profiling
      use wrapper_MPI
      use at_domain
      implicit none
      type(locreg_descriptors), intent(in) :: Glr
      integer, intent(in) :: nlr, iproc, nproc, nlr_max
      type(locreg_descriptors), dimension(nlr), intent(inout) :: Llr
      integer, intent(in) :: llr_on_all_mpi
      logical, dimension(nlr), intent(in) :: calculateBounds
      integer, dimension(nlr_max, nproc), intent(in) :: lr_par
      integer, dimension(:), intent(in) :: lr_ovr
      integer, intent(in) :: comm
      !local variables
      integer, parameter :: X_=1,Y_=2,Z_=3
      logical, dimension(3) :: peri
      !logical :: Gperx,Gpery,Gperz
      logical :: warningx,warningy,warningz
      integer :: ilr
      integer, dimension(2,3) :: nbox 
      integer, dimension(:), allocatable :: rootarr

      !determine the limits of the different localisation regions
      rootarr = f_malloc(nlr, id = 'rootarr')
      rootarr=1000000000

      ! Periodicity in the three directions
!!$      Gperx=(Glr%geocode /= 'F')
!!$      Gpery=(Glr%geocode == 'P')
!!$      Gperz=(Glr%geocode /= 'F')
      peri=domain_periodic_dims(Glr%mesh%dom)
      ! Gperx=peri(1) 
      ! Gpery=peri(2)
      ! Gperz=peri(3)

      call f_timing(WFD_CREATION_CAT(),'ON')  
      do ilr=1,nlr
         !initialize out of zone and logicals
         !outofzone (:) = 0     
         warningx = .false.
         warningy = .false.
         warningz = .false. 
!!$         xperiodic = .false.
!!$         yperiodic = .false.
!!$         zperiodic = .false. 

         if(calculateBounds(ilr) .or. ilr==llr_on_all_mpi) then 
            ! This makes sure that each locreg is only handled once by one specific processor.

            ! Determine the extrema of this localization regions (using only the coarse part, since this is always larger or equal than the fine part).
            call determine_boxbounds_sphere(peri(X_), peri(Y_), peri(Z_), glr%nboxc, &
                 glr%mesh_coarse%hgrids(1), glr%mesh_coarse%hgrids(2), glr%mesh_coarse%hgrids(3), &
                 llr(ilr)%locrad, llr(ilr)%locregCenter, &
                 glr%wfd%nseg_c, glr%wfd%keygloc, &
                 nbox(1,1),nbox(1,2),nbox(1,3),nbox(2,1),nbox(2,2),nbox(2,3))
!!!>isx, isy, isz, iex, iey, iez)
            !write(*,'(a,3i7)') 'ilr, isx, iex', ilr, isx, iex
            !hgrids = [hx,hy,hz]
            call lr_box(llr(ilr),Glr,nbox,.false.)
            ! construct the wavefunction descriptors (wfd)
            if (calculateBounds(ilr)) rootarr(ilr)=iproc
            call determine_wfdSphere(Glr,Llr(ilr))
            if (glr%gpu_queue /= 0_f_address .and. glr%gpu_context /= 0_f_address) then
               Llr(ilr)%gpu_context = Glr%gpu_context
               Llr(ilr)%gpu_queue = Glr%gpu_queue
               call create_ocl_wfd(Llr(ilr)%ocl_wfd, Llr(ilr)%gpu_context, Llr(ilr)%gpu_queue, Llr(ilr)%wfd)
            end if
         end if
      end do !on ilr
      call f_timing(WFD_CREATION_CAT(),'OF')

      if (nproc > 1) then
         call f_timing(WFD_COMM_CAT(), 'ON')
         call fmpi_allreduce(rootarr, op=FMPI_MIN, comm=comm)
         ! Communicate those parts of the locregs that all processes need.
         call communicate_locreg_descriptors_basics(iproc, nproc, nlr, rootarr, calculateBounds, llr, comm)
         call communicate_locreg_descriptors_keys(iproc, nproc, nlr, glr, llr, &
              rootarr, llr_on_all_mpi, nlr_max, lr_par, lr_ovr, comm)
         call f_timing(WFD_COMM_CAT(), 'OF')
      end if

      call f_free(rootarr)

      !create the bound arrays for the locregs we need on the MPI tasks
      call f_timing(BOUNDS_CAT(), 'ON') 
      do ilr = 1, nlr
         if (domain_geocode(Llr(ilr)%mesh%dom) =='F' .and. &
              (calculateBounds(ilr)  .or. ilr==llr_on_all_mpi) ) &
              call ensure_locreg_bounds(Llr(ilr))
      end do
      call f_timing(BOUNDS_CAT(), 'OF') 
    end subroutine init_llr_from_spheres
    
    !> Determine a set of localisation regions from the centers and the radii.
    !! cut in cubes the global reference system
    subroutine init_llr_from_centers(nlr,cxyz,locrad,Glr,Llr,calculateLlr)
      use liborbs_precisions
      use box
      use at_domain, only: domain_geocode
      implicit none
      integer, intent(in) :: nlr
      type(locreg_descriptors), intent(in) :: Glr
      real(gp), dimension(nlr), intent(in) :: locrad
      real(gp), dimension(3,nlr), intent(in) :: cxyz
      type(locreg_descriptors), dimension(nlr), intent(out) :: Llr
      logical,dimension(nlr),intent(in) :: calculateLlr
      !local variables
      logical :: warningx,warningy,warningz
      integer :: ilr
      integer, dimension(2,3) :: nbox 
      real(gp) :: rx,ry,rz,cutoff
      !!  integer :: iilr,ierr
      !!  integer,dimension(0:nproc-1) :: nlr_par,islr_par

      !!if (iproc == 0) then
      !!   write(*,*)'Inside determine_locreg_periodic:'
      !!end if

      !  call parallel_repartition_locreg(iproc,nproc,nlr,nlr_par,islr_par)

      !initialize out of zone and logicals
!!$      outofzone (:) = 0
      warningx = .false.
      warningy = .false.
      warningz = .false.
      !determine the limits of the different localisation regions
      do ilr=1,nlr
         call nullify_locreg_descriptors(Llr(ilr))
         if (.not. calculateLlr(ilr)) cycle         !calculate only for the locreg on this processor, without repeating for same locreg

         rx=cxyz(1,ilr)
         ry=cxyz(2,ilr)
         rz=cxyz(3,ilr)

         cutoff=locrad(ilr)

         nbox=box_nbox_from_cutoff(Glr%mesh_coarse,cxyz(1,ilr),locrad(ilr),inner=.false.)

!!$         nbox(1,1)=floor((rx-cutoff)/hx)
!!$         nbox(1,2)=floor((ry-cutoff)/hy)
!!$         nbox(1,3)=floor((rz-cutoff)/hz)
!!$    
!!$         nbox(2,1)=ceiling((rx+cutoff)/hx)
!!$         nbox(2,2)=ceiling((ry+cutoff)/hy)
!!$         nbox(2,3)=ceiling((rz+cutoff)/hz)

         call lr_box(Llr(ilr),Glr,nbox,.true.)

         ! construct the wavefunction descriptors (wfd)
         call determine_wfd_periodicity(Glr,Llr(ilr))
         if (glr%gpu_queue /= 0_f_address .and. glr%gpu_context /= 0_f_address) then
            Llr(ilr)%gpu_context = Glr%gpu_context
            Llr(ilr)%gpu_queue = Glr%gpu_queue
            call create_ocl_wfd(Llr(ilr)%ocl_wfd, Llr(ilr)%gpu_context, Llr(ilr)%gpu_queue, Llr(ilr)%wfd)
         end if

         ! Sould check if nfu works properly... also relative to locreg!!
         !if the localisation region is isolated build also the bounds
!!$         if (Llr(ilr)%geocode=='F') then
         if (domain_geocode(Llr(ilr)%mesh%dom) == 'F') then
            ! Check whether the bounds shall be calculated. Do this only if the currect process handles
            ! orbitals in the current localization region.
            call ensure_locreg_bounds(Llr(ilr))
            !call locreg_bounds(Llr(ilr)%d%n1,Llr(ilr)%d%n2,Llr(ilr)%d%n3,&
            !       Llr(ilr)%d%nfl1,Llr(ilr)%d%nfu1,Llr(ilr)%d%nfl2,Llr(ilr)%d%nfu2,&
            !Llr(ilr)%d%nfl3,Llr(ilr)%d%nfu3,Llr(ilr)%wfd,Llr(ilr)%bounds)
            !end if
         end if
      end do !on iilr

      !  call make_LLr_MpiType(Llr,nlr,mpiLlr)

      !  call MPI_ALLREDUCE(Llr(1),Llr(1),nlr,mpidtypg,FMPI_SUM,bigdft_mpi%mpi_comm,ierr)
      !after all localisation regions are determined draw them
      !call draw_locregs(nlr,hx,hy,hz,Llr)

    END SUBROUTINE init_llr_from_centers

    subroutine correct_dimensions(is,ie,ns,n,ln,outofzone,correct,periodic)
      implicit none
      logical, intent(in) :: correct
      integer, intent(in) :: ns,n,ln
      integer, intent(inout) :: outofzone
      logical, intent(inout) :: periodic
      integer, intent(inout) :: is,ie

      if (ie - is >= n - 1) then
         is=ns
         ie=ns + n - 1
         periodic = .true.
      else
         if (correct) then
            is=modulo(is,n) + ns
            ie= ln + is
         end if
         if (ie > ns+n-1) then
            outofzone=modulo(ie,n)
         end if
      end if
    end subroutine correct_dimensions


    !subroutine correct_lr_extremes(lr,Glr,geocode,correct,nbox_lr,nbox)
    subroutine correct_lr_extremes(lr,Glr,dom,correct,nbox_lr,nbox)
      use at_domain
      use dictionaries, only: f_err_throw
      implicit none
      logical, intent(in) :: correct
      type(locreg_descriptors), intent(inout) :: lr
      type(locreg_descriptors), intent(in) :: Glr
      !character(len=1), intent(out) :: geocode
      type(domain), intent(out) :: dom
      integer, dimension(2,3), intent(out) :: nbox_lr
      integer, dimension(2,3), intent(in) :: nbox
      !local variables
      logical :: xperiodic,yperiodic,zperiodic
      integer :: isx,iex,isy,iey,isz,iez
      integer :: ln1,ln2,ln3
      logical, dimension(3) :: peri


      !initialize out of zone
      lr%outofzone (:) = 0

      ! Localization regions should have free boundary conditions by default
      isx=nbox(1,1)
      iex=nbox(2,1)
      isy=nbox(1,2)
      iey=nbox(2,2)
      isz=nbox(1,3)
      iez=nbox(2,3)

      ln1 = iex-isx
      ln2 = iey-isy
      ln3 = iez-isz

      !geocode='F'
      dom=change_domain_BC(Glr%mesh%dom,geocode='F')

      xperiodic = .false.
      yperiodic = .false.
      zperiodic = .false.

      peri=domain_periodic_dims(Glr%mesh%dom)
      if (peri(1)) then
         call correct_dimensions(isx,iex,Glr%nboxc(1,1),Glr%mesh_coarse%ndims(1),&
              ln1,lr%outofzone(1),correct,xperiodic)
      else
         isx=max(isx,Glr%nboxc(1,1))
         iex=min(iex,Glr%nboxc(2,1))
         lr%outofzone(1) = 0
      end if
      if (peri(2)) then
         call correct_dimensions(isy,iey,Glr%nboxc(1,2),Glr%mesh_coarse%ndims(2),&
              ln2,lr%outofzone(2),correct,yperiodic)
      else
         isy=max(isy,Glr%nboxc(1,2))
         iey=min(iey,Glr%nboxc(2,2))
         lr%outofzone(2) = 0
      end if
      if (peri(3)) then
         call correct_dimensions(isz,iez,Glr%nboxc(1,3),Glr%mesh_coarse%ndims(3),&
              ln3,lr%outofzone(3),correct,zperiodic)
      else
         isz=max(isz,Glr%nboxc(1,3))
         iez=min(iez,Glr%nboxc(2,3))
         lr%outofzone(3) = 0
      end if
      !if (zperiodic) geocode = 'W'
      if (zperiodic) dom=change_domain_BC(Glr%mesh%dom,geocode='W')
      !if (xperiodic .and. zperiodic) geocode = 'S'
      if (xperiodic .and. zperiodic) dom=change_domain_BC(Glr%mesh%dom,geocode='S')
      !if (xperiodic .and. yperiodic .and. zperiodic) geocode = 'P'
      if (xperiodic .and. yperiodic .and. zperiodic) &
         dom=change_domain_BC(Glr%mesh%dom,geocode='P')

!!$      !assign the starting/ending points and outofzone for the different
!!$      ! geometries
!!$      !!!select case(Glr%geocode)
!!$      select case(cell_geocode(Glr%mesh))
!!$      case('F')
!!$         isx=max(isx,Glr%ns1)
!!$         isy=max(isy,Glr%ns2)
!!$         isz=max(isz,Glr%ns3)
!!$
!!$         iex=min(iex,Glr%ns1+Glr%d%n1)
!!$         iey=min(iey,Glr%ns2+Glr%d%n2)
!!$         iez=min(iez,Glr%ns3+Glr%d%n3)
!!$      case('S')
!!$         ! Get starting and ending for x direction
!!$         if (iex - isx >= Glr%d%n1) then
!!$            isx=Glr%ns1
!!$            iex=Glr%ns1 + Glr%d%n1
!!$            xperiodic = .true.
!!$         else
!!$            if (correct) then
!!$               isx=modulo(isx,Glr%d%n1+1) + Glr%ns1
!!$               iex= ln1 + isx
!!$            end if
!!$            if (iex > Glr%ns1+Glr%d%n1) then
!!$               lr%outofzone(1)=modulo(iex,Glr%d%n1+1)
!!$            end if
!!$         end if
!!$
!!$         ! Get starting and ending for y direction (perpendicular to surface)
!!$         isy=max(isy,Glr%ns2)
!!$         iey=min(iey,Glr%ns2 + Glr%d%n2)
!!$         lr%outofzone(2) = 0
!!$
!!$         !Get starting and ending for z direction
!!$         if (iez - isz >= Glr%d%n3) then
!!$            isz=Glr%ns3
!!$            iez=Glr%ns3 + Glr%d%n3
!!$            zperiodic = .true.
!!$         else
!!$            if (correct) then
!!$               isz=modulo(isz,Glr%d%n3+1) +  Glr%ns3
!!$               iez= ln3 + isz
!!$            end if
!!$            if (iez > Glr%ns3+Glr%d%n3) then
!!$               lr%outofzone(3)=modulo(iez,Glr%d%n3+1)
!!$            end if
!!$         end if
!!$         if(xperiodic .and. zperiodic) then
!!$            geocode = 'S'
!!$         end if
!!$
!!$      case('P')
!!$         ! Get starting and ending for x direction
!!$         if (iex - isx >= Glr%d%n1) then
!!$            isx=Glr%ns1
!!$            iex=Glr%ns1 + Glr%d%n1
!!$            xperiodic = .true.
!!$         else
!!$            if (correct) then
!!$               isx=modulo(isx,Glr%d%n1+1) + Glr%ns1
!!$               iex= ln1 + isx
!!$            end if
!!$            if (iex > Glr%ns1+Glr%d%n1) then
!!$               lr%outofzone(1)=modulo(iex,Glr%d%n1+1)
!!$            end if
!!$         end if
!!$
!!$         ! Get starting and ending for y direction (perpendicular to surface)
!!$         if (iey - isy >= Glr%d%n2) then
!!$            isy=Glr%ns2
!!$            iey=Glr%ns2 + Glr%d%n2
!!$            yperiodic = .true.
!!$         else
!!$            if (correct) then
!!$               isy=modulo(isy,Glr%d%n2+1) + Glr%ns2
!!$               iey= ln2 + isy
!!$            end if
!!$            if (iey > Glr%ns2+Glr%d%n2) then
!!$               lr%outofzone(2)=modulo(iey,Glr%d%n2+1)
!!$            end if
!!$         end if
!!$
!!$         !Get starting and ending for z direction
!!$         if (iez - isz >= Glr%d%n3) then
!!$            isz=Glr%ns3
!!$            iez=Glr%ns3 + Glr%d%n3
!!$            zperiodic = .true.
!!$         else
!!$            if (correct) then
!!$               isz=modulo(isz,Glr%d%n3+1) +  Glr%ns3
!!$               iez= ln3 + isz
!!$            end if
!!$            if (iez > Glr%ns3+Glr%d%n3) then
!!$               lr%outofzone(3)=modulo(iez,Glr%d%n3+1)
!!$            end if
!!$         end if
!!$         if(xperiodic .and. yperiodic .and. zperiodic ) then
!!$            geocode = 'P'
!!$         end if
!!$      case('W')
!!$         call f_err_throw("Wires bc has to be implemented here", &
!!$              err_name='BIGDFT_RUNTIME_ERROR')
!!$      end select
      ! Make sure that the localization regions are not periodic
      if (xperiodic .or. yperiodic .or. zperiodic) then
         call f_err_throw('The localization region '//&
              ' is supposed to be fully free BC.'//&
              ' Reduce the localization radii or use the cubic version',&
              err_name='BIGDFT_RUNTIME_ERROR')
      end if

      nbox_lr(1,1)=isx
      nbox_lr(1,2)=isy
      nbox_lr(1,3)=isz

      nbox_lr(2,1)=iex
      nbox_lr(2,2)=iey
      nbox_lr(2,3)=iez

    end subroutine correct_lr_extremes

    !> initalize the box-related components of the localization regions
    subroutine lr_box(lr,Glr,nbox,correction)
      use liborbs_errors
      use at_domain
      implicit none
      !> Sub-box to iterate over the points (ex. around atoms)
      !! start and end points for each direction
      type(locreg_descriptors), intent(in) :: Glr
      type(locreg_descriptors), intent(inout) :: lr
      integer, dimension(2,3), intent(in) :: nbox
      logical, intent(in) :: correction
      !local variables
      !character(len=1) :: geocode
      integer, dimension(2,3) :: nbox_lr
      type(domain) :: dom

      call f_routine(id='lr_box')

      !call correct_lr_extremes(lr,Glr,geocode,dom,correct,nbox_lr,nbox)
      call correct_lr_extremes(lr,Glr,dom,correction,nbox_lr,nbox)

      !hgridsh=0.5_gp*hgrids
      !call init_lr(lr,geocode,hgridsh,nbox_lr(2,1),nbox_lr(2,2),nbox_lr(2,3),&
      call init_lr(lr,dom,Glr%mesh%hgrids,nbox_lr,Glr%nboxf,.false.,Glr%mesh%dom)

      ! Make sure that the extent of the interpolating functions grid for the
      ! locreg is not larger than the that of the global box.
      if (any(lr%mesh%ndims > Glr%mesh%ndims)) then
         call f_err_throw('The interpolating functions grid for locreg '&
              &//&!trim(yaml_toa(ilr,fmt='(i0)'))//&
              '('//trim(yaml_toa(lr%mesh%ndims,fmt='(i0)'))//')&
              & is larger than that of the global region('//trim(yaml_toa(Glr%mesh%ndims,fmt='(i0)'))//').&
              & Reduce the localization radii or use the cubic scaling version',&
              & err_id=LIBORBS_LOCREG_ERROR())
      end if

      call f_release_routine()

    end subroutine lr_box

    !>get the offset of the isf description of the support function
    pure function get_isf_offset(lr,mesh_global) result(ioffset)
      use box, only: cell
      use at_domain, only: domain_periodic_dims
      use bounds, only: isf_box_buffers
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      type(cell), intent(in) :: mesh_global
      integer, dimension(3) :: ioffset
      !local variables
      logical, dimension(3) :: peri_local,peri_global
      integer, dimension(3) :: nli
      !integer :: nl1, nl2, nl3, nr1, nr2, nr3

      !geocode_buffers
      !conditions for periodicity in the three directions
      peri_local=domain_periodic_dims(lr%mesh%dom)
      peri_global=domain_periodic_dims(mesh_global%dom)

      nli=isf_box_buffers(peri_local,peri_global)

      ! offset
      ioffset(1) = lr%nboxi(1,1) - nli(1) - 1
      ioffset(2) = lr%nboxi(1,2) - nli(2) - 1
      ioffset(3) = lr%nboxi(1,3) - nli(3) - 1

    end function get_isf_offset
    
    subroutine draw_locregs(nlr,hx,hy,hz,Llr)
      use liborbs_precisions
      use compression
      use dynamic_memory
      use f_precisions, only: f_byte
      implicit none
      integer, intent(in) :: nlr
      real(gp), intent(in) :: hx,hy,hz
      type(locreg_descriptors), dimension(nlr), intent(in) :: Llr
      !local variables
      character(len=*), parameter :: subname='draw_locregs'
      character(len=4) :: message
      integer :: i1,i2,i3,ilr,nvctr_tot
      logical(f_byte), dimension(:,:,:), allocatable :: logrid_c,logrid_f

      !calculate total number
      nvctr_tot=0
      do ilr=1,nlr
         nvctr_tot=nvctr_tot+Llr(ilr)%wfd%nvctr_c+Llr(ilr)%wfd%nvctr_f
      end do

      !open file for writing
      open(unit=22,file='locregs.xyz',status='unknown')
      write(22,*) nvctr_tot,' atomic'
      write(22,*)'coarse and fine points of all the different localisation regions'

      do ilr=1,nlr
         !define logrids
         logrid_c = f_malloc(Llr(ilr)%mesh_coarse%ndims,id='logrid_c')
         logrid_f = f_malloc(Llr(ilr)%mesh_coarse%ndims,id='logrid_f')

         call wfd_to_logrids(Llr(ilr)%mesh_coarse%ndims(1), &
              Llr(ilr)%mesh_coarse%ndims(2), Llr(ilr)%mesh_coarse%ndims(3), &
              Llr(ilr)%wfd, logrid_c,logrid_f)

         write(message,'(1a,i0)')'g',ilr
         do i3=1,Llr(ilr)%mesh_coarse%ndims(3)
            do i2=1,Llr(ilr)%mesh_coarse%ndims(2)
               do i1=1,Llr(ilr)%mesh_coarse%ndims(1)
                  if (logrid_c(i1,i2,i3))&
                       write(22,'(a4,2x,3(1x,e10.3))') message, &
                       real(i1-1+Llr(ilr)%nboxc(1,1),gp)*hx, &
                       real(i2-1+Llr(ilr)%nboxc(1,2),gp)*hy, &
                       real(i3-1+Llr(ilr)%nboxc(1,3),gp)*hz
               enddo
            enddo
         end do
         write(message,'(1a,i0)')'G',ilr
         do i3=1,Llr(ilr)%mesh_coarse%ndims(3)
            do i2=1,Llr(ilr)%mesh_coarse%ndims(2)
               do i1=1,Llr(ilr)%mesh_coarse%ndims(1)
                  if (logrid_f(i1,i2,i3))&
                       write(22,'(a4,2x,3(1x,e10.3))') message, &
                       real(i1-1+Llr(ilr)%nboxc(1,1),gp)*hx, &
                       real(i2-1+Llr(ilr)%nboxc(1,2),gp)*hy, &
                       real(i3-1+Llr(ilr)%nboxc(1,3),gp)*hz
               enddo
            enddo
         enddo


         call f_free(logrid_c)
         call f_free(logrid_f)
      end do

      !close file for writing
      close(unit=22)  
    END SUBROUTINE draw_locregs

    subroutine print_lr_compression(lr)
      use compression
      implicit none
      type(locreg_descriptors), intent(in) :: lr

      call print_wfd(lr%wfd)
    end subroutine print_lr_compression

    subroutine lr_broadcast_wfd(lr, source, mpi_comm, global)
      use wrapper_MPI
      implicit none
      integer, intent(in) :: source
      type(locreg_descriptors), intent(inout) :: lr
      integer, intent(in) :: mpi_comm
      logical, intent(in), optional :: global

      call wfd_keys_broadcast(lr%wfd, source, mpi_comm, global)
      call ensure_locreg_bounds(lr)
    end subroutine lr_broadcast_wfd

    !> Accumulate 3d wavefunction in complex form from a tensor produc decomposition
    !! universal routine which should be used for all gautowav operations
    subroutine acc_from_tensprod(lr,ncplx,nterm,wx,wy,wz,ncplx_psi,psi)
      use liborbs_precisions
      use liborbs_errors
      use compression
      implicit none
      integer, intent(in) :: ncplx,nterm,ncplx_psi
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(ncplx,lr%mesh_coarse%ndims(1),2,nterm), intent(in) :: wx
      real(wp), dimension(ncplx,lr%mesh_coarse%ndims(2),2,nterm), intent(in) :: wy
      real(wp), dimension(ncplx,lr%mesh_coarse%ndims(3),2,nterm), intent(in) :: wz
      real(wp), dimension(array_dim(lr)*ncplx_psi), intent(inout) :: psi
      !local variables
      integer :: iseg,i,i0,i1,i2,i3,jj,ind_c,ind_f,iterm,i0jj,cshift,n1p1,np
      real(wp) :: wyz, wyz11, wyz12, wyz21, wyz22
      real(wp) :: wyz11r, wyz11i, wyz12r, wyz12i, wyz21r, wyz21i, wyz22r, wyz22i

      !the filling of the wavefunction should be different if ncplx==1 or 2
      !split such as to avoid intensive call to if statements
      if (f_err_raise(.not. wfd_check(lr%wfd), 'inconsistent number of elements of the projector', &
           err_id = LIBORBS_LOCREG_ERROR())) return

      n1p1 = lr%mesh_coarse%ndims(1)
      np = n1p1 * lr%mesh_coarse%ndims(2)
      if (ncplx == 1) then
         !$omp parallel default(private) &
         !$omp shared(psi,wx,wy,wz,lr) &
         !$omp shared(n1p1,np,nterm)
         !$omp do
         do iseg=1,lr%wfd%nseg_c
            call local_segment_to_grid(lr%wfd, iseg, n1p1, np, i0, i1, i2, i3, jj)
            i0jj=jj-i0
            do iterm=1,nterm
               wyz = wy(1,i2+1,1,iterm) * wz(1,i3+1,1,iterm)
               do i=i0,i1
                  ind_c=i+i0jj
                  psi(ind_c) = psi(ind_c) + wx(1,i+1,1,iterm) * wyz
               end do
            end do
         end do
         !$omp enddo

         !$omp do
         do iseg=lr%wfd%nseg_c+1,lr%wfd%nseg_c+lr%wfd%nseg_f
            call local_segment_to_grid(lr%wfd, iseg, n1p1, np, i0, i1, i2, i3, jj)
            i0jj=7*(jj-i0-1)+lr%wfd%nvctr_c
            do iterm=1,nterm
               wyz11 = wy(1,i2+1,1,iterm)*wz(1,i3+1,1,iterm)
               wyz12 = wy(1,i2+1,1,iterm)*wz(1,i3+1,2,iterm)
               wyz21 = wy(1,i2+1,2,iterm)*wz(1,i3+1,1,iterm)
               wyz22 = wy(1,i2+1,2,iterm)*wz(1,i3+1,2,iterm)
               do i=i0,i1
                  ind_f=7*i+i0jj
                  psi(ind_f+1) = psi(ind_f+1) + wx(1,i+1,2,iterm) * wyz11
                  psi(ind_f+2) = psi(ind_f+2) + wx(1,i+1,1,iterm) * wyz21
                  psi(ind_f+3) = psi(ind_f+3) + wx(1,i+1,2,iterm) * wyz21
                  psi(ind_f+4) = psi(ind_f+4) + wx(1,i+1,1,iterm) * wyz12
                  psi(ind_f+5) = psi(ind_f+5) + wx(1,i+1,2,iterm) * wyz12
                  psi(ind_f+6) = psi(ind_f+6) + wx(1,i+1,1,iterm) * wyz22
                  psi(ind_f+7) = psi(ind_f+7) + wx(1,i+1,2,iterm) * wyz22
               end do
            end do
         end do
         !$omp enddo
         !$omp end parallel

      else if (ncplx == 2) then

         !$omp parallel default(private) shared(ncplx_psi) &
         !$omp shared(psi,wx,wy,wz,lr) &
         !$omp shared(n1p1,np,nterm)
         !$omp do
         do iseg=1,lr%wfd%nseg_c
            call local_segment_to_grid(lr%wfd, iseg, n1p1, np, i0, i1, i2, i3, jj)
            i0jj=jj-i0
            do iterm=1,nterm
               wyz11r = wy(1,i2+1,1,iterm) * wz(1,i3+1,1,iterm) - wy(2,i2+1,1,iterm) * wz(2,i3+1,1,iterm)
               wyz11i = wy(1,i2+1,1,iterm) * wz(2,i3+1,1,iterm) + wy(2,i2+1,1,iterm) * wz(1,i3+1,1,iterm)
               do i=i0,i1
                  ind_c=i+i0jj
                  psi(ind_c) = psi(ind_c) + wx(1,i+1,1,iterm) * wyz11r - wx(2,i+1,1,iterm) * wyz11i
               end do
            end do
         end do
         !$omp enddo

         !$omp do
         do iseg=lr%wfd%nseg_c+1,lr%wfd%nseg_c+lr%wfd%nseg_f
            call local_segment_to_grid(lr%wfd, iseg, n1p1, np, i0, i1, i2, i3, jj)
            i0jj=7*(jj-i0-1)+lr%wfd%nvctr_c
            do iterm=1,nterm
               wyz11r = wy(1,i2+1,1,iterm) * wz(1,i3+1,1,iterm) - wy(2,i2+1,1,iterm) * wz(2,i3+1,1,iterm)
               wyz11i = wy(1,i2+1,1,iterm) * wz(2,i3+1,1,iterm) + wy(2,i2+1,1,iterm) * wz(1,i3+1,1,iterm)
               wyz12r = wy(1,i2+1,1,iterm) * wz(1,i3+1,2,iterm) - wy(2,i2+1,1,iterm) * wz(2,i3+1,2,iterm)
               wyz12i = wy(1,i2+1,1,iterm) * wz(2,i3+1,2,iterm) + wy(2,i2+1,1,iterm) * wz(1,i3+1,2,iterm)
               wyz21r = wy(1,i2+1,2,iterm) * wz(1,i3+1,1,iterm) - wy(2,i2+1,2,iterm) * wz(2,i3+1,1,iterm)
               wyz21i = wy(1,i2+1,2,iterm) * wz(2,i3+1,1,iterm) + wy(2,i2+1,2,iterm) * wz(1,i3+1,1,iterm)
               wyz22r = wy(1,i2+1,2,iterm) * wz(1,i3+1,2,iterm) - wy(2,i2+1,2,iterm) * wz(2,i3+1,2,iterm)
               wyz22i = wy(1,i2+1,2,iterm) * wz(2,i3+1,2,iterm) + wy(2,i2+1,2,iterm) * wz(1,i3+1,2,iterm)
               do i=i0,i1
                  ind_f=7*i+i0jj
                  psi(ind_f+1) = psi(ind_f+1) + wx(1,i+1,2,iterm) * wyz11r - wx(2,i+1,2,iterm) * wyz11i
                  psi(ind_f+2) = psi(ind_f+2) + wx(1,i+1,1,iterm) * wyz21r - wx(2,i+1,1,iterm) * wyz21i
                  psi(ind_f+3) = psi(ind_f+3) + wx(1,i+1,2,iterm) * wyz21r - wx(2,i+1,2,iterm) * wyz21i
                  psi(ind_f+4) = psi(ind_f+4) + wx(1,i+1,1,iterm) * wyz12r - wx(2,i+1,1,iterm) * wyz12i
                  psi(ind_f+5) = psi(ind_f+5) + wx(1,i+1,2,iterm) * wyz12r - wx(2,i+1,2,iterm) * wyz12i
                  psi(ind_f+6) = psi(ind_f+6) + wx(1,i+1,1,iterm) * wyz22r - wx(2,i+1,1,iterm) * wyz22i
                  psi(ind_f+7) = psi(ind_f+7) + wx(1,i+1,2,iterm) * wyz22r - wx(2,i+1,2,iterm) * wyz22i
               end do
            end do
         end do
         !$omp enddo
         
         !now the imaginary part
         if (ncplx_psi == 2) then
            cshift = array_dim(lr)
            !$omp do
            do iseg=1,lr%wfd%nseg_c
               call local_segment_to_grid(lr%wfd, iseg, n1p1, np, i0, i1, i2, i3, jj)
               i0jj=jj-i0+cshift
               do iterm=1,nterm
                  wyz11r = wy(1,i2+1,1,iterm) * wz(2,i3+1,1,iterm) + wy(2,i2+1,1,iterm) * wz(1,i3+1,1,iterm)
                  wyz11i = wy(1,i2+1,1,iterm) * wz(1,i3+1,1,iterm) - wy(2,i2+1,1,iterm) * wz(2,i3+1,1,iterm)
                  do i=i0,i1
                     ind_c=i+i0jj
                     psi(ind_c) = psi(ind_c) + wx(1,i+1,1,iterm) * wyz11r + wx(2,i+1,1,iterm) * wyz11i
                  end do
               end do
            end do
            !$omp enddo

            ! Other terms: fine psiector components
            !$omp do
            do iseg=lr%wfd%nseg_c+1,lr%wfd%nseg_c+lr%wfd%nseg_f
               call local_segment_to_grid(lr%wfd, iseg, n1p1, np, i0, i1, i2, i3, jj)
               i0jj=7*(jj-i0-1)+cshift+lr%wfd%nvctr_c
               do iterm=1,nterm
                  wyz11r = wy(1,i2+1,1,iterm) * wz(2,i3+1,1,iterm) + wy(2,i2+1,1,iterm) * wz(1,i3+1,1,iterm)
                  wyz11i = wy(1,i2+1,1,iterm) * wz(1,i3+1,1,iterm) - wy(2,i2+1,1,iterm) * wz(2,i3+1,1,iterm)
                  wyz12r = wy(1,i2+1,1,iterm) * wz(2,i3+1,2,iterm) + wy(2,i2+1,1,iterm) * wz(1,i3+1,2,iterm)
                  wyz12i = wy(1,i2+1,1,iterm) * wz(1,i3+1,2,iterm) - wy(2,i2+1,1,iterm) * wz(2,i3+1,2,iterm)
                  wyz21r = wy(1,i2+1,2,iterm) * wz(2,i3+1,1,iterm) + wy(2,i2+1,2,iterm) * wz(1,i3+1,1,iterm)
                  wyz21i = wy(1,i2+1,2,iterm) * wz(1,i3+1,1,iterm) - wy(2,i2+1,2,iterm) * wz(2,i3+1,1,iterm)
                  wyz22r = wy(1,i2+1,2,iterm) * wz(2,i3+1,2,iterm) + wy(2,i2+1,2,iterm) * wz(1,i3+1,2,iterm)
                  wyz22i = wy(1,i2+1,2,iterm) * wz(1,i3+1,2,iterm) - wy(2,i2+1,2,iterm) * wz(2,i3+1,2,iterm)
                  do i=i0,i1
                     ind_f=7*i+i0jj
                     psi(ind_f+1) = psi(ind_f+1) + wx(1,i+1,2,iterm) * wyz11r + wx(2,i+1,2,iterm) * wyz11i
                     psi(ind_f+2) = psi(ind_f+2) + wx(1,i+1,1,iterm) * wyz21r + wx(2,i+1,1,iterm) * wyz21i
                     psi(ind_f+3) = psi(ind_f+3) + wx(1,i+1,2,iterm) * wyz21r + wx(2,i+1,2,iterm) * wyz21i
                     psi(ind_f+4) = psi(ind_f+4) + wx(1,i+1,1,iterm) * wyz12r + wx(2,i+1,1,iterm) * wyz12i
                     psi(ind_f+5) = psi(ind_f+5) + wx(1,i+1,2,iterm) * wyz12r + wx(2,i+1,2,iterm) * wyz12i
                     psi(ind_f+6) = psi(ind_f+6) + wx(1,i+1,1,iterm) * wyz22r + wx(2,i+1,1,iterm) * wyz22i
                     psi(ind_f+7) = psi(ind_f+7) + wx(1,i+1,2,iterm) * wyz22r + wx(2,i+1,2,iterm) * wyz22i
                  end do
               end do
            end do
            !$omp enddo
         end if
         !$omp end parallel
      end if
    END SUBROUTINE acc_from_tensprod
    
    !> Accumulate 3d projector in real form from a tensor produc decomposition
    !! using complex gaussians
    subroutine acc_from_tensprod_cossin(lr,ncplx,  cossinfacts ,nterm,wx,wy,wz,psi)
      use liborbs_precisions
      use liborbs_errors
      use compression
      implicit none
      integer, intent(in) :: nterm, ncplx
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(2,ncplx,0:lr%mesh_coarse%ndims(1)-1,2,nterm), intent(in) :: wx
      real(wp), dimension(2,ncplx,0:lr%mesh_coarse%ndims(2)-1,2,nterm), intent(in) :: wy
      real(wp), dimension(2,ncplx,0:lr%mesh_coarse%ndims(3)-1,2,nterm), intent(in) :: wz
      real(wp) :: cossinfacts(2,nterm)
      real(wp), dimension(array_dim(lr)*ncplx), intent(inout) :: psi
      !local variables
      integer :: iseg,i,i0,i1,i2,i3,jj,ind_c,ind_f,iterm,nvctr
      !real(wp) :: re_cmplx_prod,im_cmplx_prod
      !real(wp) :: re_re_cmplx_prod,re_im_cmplx_prod,im_re_cmplx_prod,im_im_cmplx_prod

!!$omp parallel default(private) shared(lr%nseg_c,lr%wfd,lr%d) &
!!$omp shared(psi,wx,wy,wz,lr%wfd%nvctr_c) &
!!$omp shared(nterm,lr%wfd%nvctr_f,lr%wfd%nseg_f)
      if (ncplx == 1) then
         nvctr=0
         do iseg=1,lr%wfd%nseg_c
            call segment_to_grid(lr%wfd,&
                 lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(1)*lr%mesh_coarse%ndims(2),i0,i1,i2,i3,jj)
            do i=i0,i1
               ind_c=i-i0+jj
               do iterm=1,nterm
                  psi(ind_c)=psi(ind_c)+re_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,1,iterm))*cossinfacts(1,iterm)
               end do
               nvctr=nvctr+1
            end do
         end do
         if (f_err_raise(nvctr /=  lr%wfd%nvctr_c, 'nvctr /= nvctr_c ' // trim(yaml_toa(nvctr)) &
              // trim(yaml_toa(lr%wfd%nvctr_c)), err_id = LIBORBS_LOCREG_ERROR())) return
!!$  end if

!!$  if(ithread .eq. 1 .or. nthread .eq. 1) then
         ! Other terms: fine projector components
         nvctr=0
         do iseg=lr%wfd%nseg_c+1,lr%wfd%nseg_c+lr%wfd%nseg_f
            call segment_to_grid(lr%wfd,iseg,&
                 lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(1)*lr%mesh_coarse%ndims(2),i0,i1,i2,i3,jj)
            do i=i0,i1
               ind_f=lr%wfd%nvctr_c+7*(i-i0+jj-1)
               do iterm=1,nterm
                  psi(ind_f+1)=psi(ind_f+1)+re_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,1,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+2)=psi(ind_f+2)+re_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+3)=psi(ind_f+3)+re_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+4)=psi(ind_f+4)+re_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+5)=psi(ind_f+5)+re_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+6)=psi(ind_f+6)+re_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+7)=psi(ind_f+7)+re_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
               end do
               nvctr=nvctr+1
            end do
         end do
         if (f_err_raise(nvctr /=  lr%wfd%nvctr_f, 'nvctr /= nvctr_f ' // trim(yaml_toa(nvctr)) &
              // trim(yaml_toa(lr%wfd%nvctr_f)), err_id = LIBORBS_LOCREG_ERROR())) return
!!$  end if

         !now the imaginary part

!!$  if((ithread == 0 .and. nthread <= 2) .or. ithread == 2) then
         ! Other terms: coarse projector components
         ! coarse part
         do iseg=1,lr%wfd%nseg_c
            call segment_to_grid(lr%wfd,iseg,&
                 lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(1)*lr%mesh_coarse%ndims(2),i0,i1,i2,i3,jj)
            do i=i0,i1
               ind_c=i-i0+jj
               do iterm=1,nterm
                  psi(ind_c)=psi(ind_c)+im_cmplx_prod(&
                       wx(1,1, i,1,iterm),wy(1,1, i2,1,iterm),wz(1,1, i3,1,iterm))*cossinfacts(2,iterm)
               end do
            end do
         end do

!!$  end if

!!$  if((ithread .eq. 1 .and. nthread <=3) .or. nthread .eq. 1 .or. ithread == 3) then
         ! Other terms: fine projector components
         do iseg=lr%wfd%nseg_c+1,lr%wfd%nseg_c+lr%wfd%nseg_f
            call segment_to_grid(lr%wfd,iseg,&
                 lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(1)*lr%mesh_coarse%ndims(2),i0,i1,i2,i3,jj)
            do i=i0,i1
               ind_f=lr%wfd%nvctr_c+7*(i-i0+jj-1)
               do iterm=1,nterm
                  psi(ind_f+1)=psi(ind_f+1)+im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,1,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+2)=psi(ind_f+2)+im_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+3)=psi(ind_f+3)+im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+4)=psi(ind_f+4)+im_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+5)=psi(ind_f+5)+im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+6)=psi(ind_f+6)+im_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+7)=psi(ind_f+7)+im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
               end do
            end do
         end do
      else if (ncplx ==2) then

         nvctr=0
         do iseg=1,lr%wfd%nseg_c
            call segment_to_grid(lr%wfd,iseg,&
                 lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(1)*lr%mesh_coarse%ndims(2),i0,i1,i2,i3,jj)

            do i=i0,i1
               ind_c=i-i0+jj
               do iterm=1,nterm
                  psi(ind_c)=psi(ind_c)+re_re_cmplx_prod(&
                       wx(1,1, i,1,iterm),wy(1,1, i2,1,iterm),wz(1,1, i3,1,iterm))*cossinfacts(1,iterm)
               end do
               nvctr=nvctr+1
            end do

            do i=i0,i1
               ind_c= array_dim(lr) + i-i0+jj
               do iterm=1,nterm
                  psi(ind_c)=psi(ind_c)+im_re_cmplx_prod(&
                       wx(1,1, i,1,iterm),wy(1,1, i2,1,iterm),wz(1,1, i3,1,iterm))*cossinfacts(1,iterm)
               end do
            end do



         end do
         if (f_err_raise(nvctr /=  lr%wfd%nvctr_c, 'nvctr /= nvctr_c ' // trim(yaml_toa(nvctr)) &
              // trim(yaml_toa(lr%wfd%nvctr_c)), err_id = LIBORBS_LOCREG_ERROR())) return
!!$  end if

!!$  if(ithread .eq. 1 .or. nthread .eq. 1) then
         ! Other terms: fine projector components
         nvctr=0
         do iseg=lr%wfd%nseg_c+1,lr%wfd%nseg_c+lr%wfd%nseg_f
            call segment_to_grid(lr%wfd,iseg,&
                 lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(1)*lr%mesh_coarse%ndims(2),i0,i1,i2,i3,jj)
            do i=i0,i1
               ind_f=lr%wfd%nvctr_c+7*(i-i0+jj-1)
               do iterm=1,nterm
                  psi(ind_f+1)=psi(ind_f+1)+re_re_cmplx_prod(&
                       wx(1, 1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,1,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+2)=psi(ind_f+2)+re_re_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+3)=psi(ind_f+3)+re_re_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+4)=psi(ind_f+4)+re_re_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+5)=psi(ind_f+5)+re_re_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+6)=psi(ind_f+6)+re_re_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+7)=psi(ind_f+7)+re_re_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
               end do
               nvctr=nvctr+1
            end do

            do i=i0,i1
               ind_f=array_dim(lr) +  lr%wfd%nvctr_c+7*(i-i0+jj-1)
               do iterm=1,nterm
                  psi(ind_f+1)=psi(ind_f+1)+im_re_cmplx_prod(&
                       wx(1, 1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,1,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+2)=psi(ind_f+2)+im_re_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+3)=psi(ind_f+3)+im_re_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+4)=psi(ind_f+4)+im_re_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+5)=psi(ind_f+5)+im_re_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+6)=psi(ind_f+6)+im_re_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
                  psi(ind_f+7)=psi(ind_f+7)+im_re_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(1,iterm)
               end do
            end do


         end do
         if (f_err_raise(nvctr /=  lr%wfd%nvctr_f, 'nvctr /= nvctr_f ' // trim(yaml_toa(nvctr)) &
              // trim(yaml_toa(lr%wfd%nvctr_f)), err_id = LIBORBS_LOCREG_ERROR())) return
!!$  end if

         !now the imaginary part

!!$  if((ithread == 0 .and. nthread <= 2) .or. ithread == 2) then
         ! Other terms: coarse projector components
         ! coarse part
         do iseg=1,lr%wfd%nseg_c
            call segment_to_grid(lr%wfd,iseg,&
                 lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(1)*lr%mesh_coarse%ndims(2),i0,i1,i2,i3,jj)

            do i=i0,i1
               ind_c=i-i0+jj
               do iterm=1,nterm
                  psi(ind_c)=psi(ind_c)+re_im_cmplx_prod(&
                       wx(1,1, i,1,iterm),wy(1,1, i2,1,iterm),wz(1,1, i3,1,iterm))*cossinfacts(2,iterm)
               end do
            end do

            do i=i0,i1
               ind_c=array_dim(lr) + i-i0+jj
               do iterm=1,nterm
                  psi(ind_c)=psi(ind_c)+im_im_cmplx_prod(&
                       wx(1,1, i,1,iterm),wy(1,1, i2,1,iterm),wz(1,1, i3,1,iterm))*cossinfacts(2,iterm)
               end do
            end do

         end do

!!$  end if

!!$  if((ithread .eq. 1 .and. nthread <=3) .or. nthread .eq. 1 .or. ithread == 3) then
         ! Other terms: fine projector components
         do iseg=lr%wfd%nseg_c+1,lr%wfd%nseg_c+lr%wfd%nseg_f
            call segment_to_grid(lr%wfd,iseg,&
                 lr%mesh_coarse%ndims(1),lr%mesh_coarse%ndims(1)*lr%mesh_coarse%ndims(2),i0,i1,i2,i3,jj)

            do i=i0,i1
               ind_f=lr%wfd%nvctr_c+7*(i-i0+jj-1)
               do iterm=1,nterm
                  psi(ind_f+1)=psi(ind_f+1)+re_im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,1,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+2)=psi(ind_f+2)+re_im_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+3)=psi(ind_f+3)+re_im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+4)=psi(ind_f+4)+re_im_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+5)=psi(ind_f+5)+re_im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+6)=psi(ind_f+6)+re_im_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+7)=psi(ind_f+7)+re_im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
               end do
            end do

            do i=i0,i1
               ind_f=array_dim(lr) +lr%wfd%nvctr_c+7*(i-i0+jj-1)
               do iterm=1,nterm
                  psi(ind_f+1)=psi(ind_f+1)+im_im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,1,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+2)=psi(ind_f+2)+im_im_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+3)=psi(ind_f+3)+im_im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,1,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+4)=psi(ind_f+4)+im_im_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+5)=psi(ind_f+5)+im_im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,1,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+6)=psi(ind_f+6)+im_im_cmplx_prod(&
                       wx(1,1,i,1,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
                  psi(ind_f+7)=psi(ind_f+7)+im_im_cmplx_prod(&
                       wx(1,1,i,2,iterm),wy(1,1,i2,2,iterm),wz(1,1,i3,2,iterm))*cossinfacts(2,iterm)
               end do
            end do

         end do

      end if

!!$omp end parallel

    contains

      !> Real part of the complex product
      pure function re_cmplx_prod(a,b,c)
        use liborbs_precisions
        implicit none
        real(wp), dimension(2), intent(in) :: a,b,c
        real(wp) :: re_cmplx_prod

        re_cmplx_prod=a(1)*b(1)*c(1) &
             -a(1)*b(2)*c(2) &
             -a(2)*b(1)*c(2) &
             -a(2)*b(2)*c(1)

      END FUNCTION re_cmplx_prod


      !> Imaginary part of the complex product
      pure function im_cmplx_prod(a,b,c)
        use liborbs_precisions
        implicit none
        real(wp), dimension(2), intent(in) :: a,b,c
        real(wp) :: im_cmplx_prod

        im_cmplx_prod=-a(2)*b(2)*c(2) &
             +a(2)*b(1)*c(1) &
             +a(1)*b(2)*c(1) &
             +a(1)*b(1)*c(2)

      END FUNCTION im_cmplx_prod


      pure function re_re_cmplx_prod(a,b,c)
        use liborbs_precisions
        implicit none
        real(wp), dimension(2,2), intent(in) :: a,b,c
        real(wp) :: re_re_cmplx_prod
        !  real(wp) :: re_cmplx_prod

        re_re_cmplx_prod=re_cmplx_prod( a(1,1),b(1,1),c(1,1)) &
             -re_cmplx_prod( a(1,1),b(1,2),c(1,2)) &
             -re_cmplx_prod( a(1,2),b(1,1),c(1,2)) &
             -re_cmplx_prod( a(1,2),b(1,2),c(1,1))
      END FUNCTION re_re_cmplx_prod


      pure function im_re_cmplx_prod(a,b,c)
        use liborbs_precisions
        implicit none
        real(wp), dimension(2,2), intent(in) :: a,b,c
        real(wp) :: im_re_cmplx_prod
        !real(wp) :: re_cmplx_prod

        im_re_cmplx_prod=-re_cmplx_prod(a(1,2),b(1,2),c(1,2)) &
             +re_cmplx_prod(a(1,2),b(1,1),c(1,1)) &
             +re_cmplx_prod(a(1,1),b(1,2),c(1,1)) &
             +re_cmplx_prod(a(1,1),b(1,1),c(1,2))

      END FUNCTION im_re_cmplx_prod


      pure function re_im_cmplx_prod(a,b,c)
        use liborbs_precisions
        implicit none
        real(wp), dimension(2,2), intent(in) :: a,b,c
        real(wp) :: re_im_cmplx_prod
        !real(wp) :: im_cmplx_prod

        re_im_cmplx_prod=im_cmplx_prod( a(1,1),b(1,1),c(1,1)) &
             -im_cmplx_prod( a(1,1),b(1,2),c(1,2)) &
             -im_cmplx_prod( a(1,2),b(1,1),c(1,2)) &
             -im_cmplx_prod( a(1,2),b(1,2),c(1,1))

      END FUNCTION re_im_cmplx_prod


      pure function im_im_cmplx_prod(a,b,c)
        use liborbs_precisions
        implicit none
        real(wp), dimension(2,2), intent(in) :: a,b,c
        real(wp) :: im_im_cmplx_prod
        !real(wp) :: im_cmplx_prod

        im_im_cmplx_prod=-im_cmplx_prod(a(1,2),b(1,2),c(1,2)) &
             +im_cmplx_prod(a(1,2),b(1,1),c(1,1)) &
             +im_cmplx_prod(a(1,1),b(1,2),c(1,1)) &
             +im_cmplx_prod(a(1,1),b(1,1),c(1,2))
      END FUNCTION im_im_cmplx_prod

    END SUBROUTINE acc_from_tensprod_cossin
  
    subroutine lr_loc_weights(lr, glr, i3start, j3start, j3end, &
         weightloc_c, weightloc_f, isize_c, isize_f)
      use liborbs_precisions
      use compression
      use liborbs_errors
      implicit none
      type(locreg_descriptors), intent(in) :: lr, glr
      integer, intent(in) :: i3start, j3start, j3end
      real(gp), dimension(0:glr%mesh_coarse%ndims(1)-1,&
           0:glr%mesh_coarse%ndims(2)-1, j3start:j3end), &
           intent(inout) :: weightloc_c, weightloc_f
      integer, intent(inout), optional :: isize_c, isize_f

      integer :: n1p1, np
      integer :: iseg, i0, i1, i2, i3, jj3, i

      if (present(isize_c)) isize_c = isize_c + lr%wfd%nvctr_c
      if (present(isize_f)) isize_f = isize_f + lr%wfd%nvctr_f
      n1p1=glr%mesh_coarse%ndims(1)
      np=n1p1*glr%mesh_coarse%ndims(2)

      !$omp parallel do default(none) &
      !$omp shared(lr, glr, j3start, j3end, np, n1p1, i3start, weightloc_c, weightloc_f) &
      !$omp private(iseg, i3, jj3, i2, i0, i1, i)
      do iseg=1,lr%wfd%nseg_c + lr%wfd%nseg_f
         call segment_to_grid(lr%wfd, iseg, n1p1, np, i0, i1, i2, i3)

         jj3=modulo(i3-i3start,glr%mesh_coarse%ndims(3))+1
         if (jj3>j3end) then
            write(*,'(a,5i8)') 'i3, i3start, glr%d%n3, jj3, j3end',&
                 i3, i3start, glr%mesh_coarse%ndims(3)-1, jj3, j3end
            call f_err_throw('strange 2.1')
         end if
         if (jj3<j3start) then
            write(*,'(a,5i8)') 'i3, i3start, glr%d%n3, jj3, j3start', &
                 i3, i3start, glr%mesh_coarse%ndims(3)-1, jj3, j3start
            call f_err_throw('strange 2.2')
         end if
         if (iseg > lr%wfd%nseg_c) then
            do i=i0,i1
               weightloc_f(i,i2,jj3)=weightloc_f(i,i2,jj3)+1._gp
            end do
         else
            do i=i0,i1
               weightloc_c(i,i2,jj3)=weightloc_c(i,i2,jj3)+1._gp
            end do
         end if
      end do
      !$omp end parallel do
    end subroutine lr_loc_weights

    subroutine lr_bucket(glr, nbuckets, ibucket_c, ibucket_f, weights_c, weights_f, &
         limits_c, limits_f, bucket_c, bucket_f, i3_c, i3_f, sum_c, sum_f, nval_c, nval_f, &
         woffset_c, woffset_f, ioffset_c, ioffset_f, i3start, i3end)
      use liborbs_precisions
      use f_utils
      implicit none
      type(locreg_descriptors), intent(in) :: glr
      integer, intent(in) :: nbuckets
      integer, intent(inout) :: ibucket_c, ibucket_f
      real(gp), dimension(:,:,:), intent(in) :: weights_c, weights_f
      real(gp), dimension(0:nbuckets-1), intent(in) :: limits_c, limits_f
      integer, dimension(2, 0:nbuckets-1), intent(out) :: bucket_c, bucket_f
      integer, dimension(2, 0:nbuckets-1), intent(out) :: i3_c, i3_f
      real(gp), dimension(0:nbuckets-1), intent(out) :: sum_c, sum_f
      integer, dimension(0:nbuckets-1), intent(out) :: nval_c, nval_f
      real(gp), intent(in), optional :: woffset_c, woffset_f
      integer, intent(in), optional :: ioffset_c, ioffset_f, i3start, i3end

      integer :: n1p1, np, iitot_c, iitot_f, i, i0, i1, i2, i3, ii3, i3s, i3e, iseg
      real(gp) :: tt_c, tt_f

      tt_c = 0._gp
      if (present(woffset_c)) tt_c = woffset_c
      iitot_c = 0
      if (present(ioffset_c)) iitot_c = ioffset_c
      tt_f = 0._gp
      if (present(woffset_f)) tt_f = woffset_f
      iitot_f = 0
      if (present(ioffset_f)) iitot_f = ioffset_f

      i3s = 1
      if (present(i3start)) i3s = i3start
      i3e = glr%mesh_coarse%ndims(3)
      if (present(i3end)) i3e = i3end

      call f_zero(bucket_c)
      call f_zero(bucket_f)
      call f_zero(i3_c)
      call f_zero(i3_f)
      call f_zero(nval_c)
      call f_zero(sum_c)
      call f_zero(nval_f)
      call f_zero(sum_f)

      n1p1 = glr%mesh_coarse%ndims(1)
      np = n1p1*glr%mesh_coarse%ndims(2)
      if (ibucket_c == 0 .and. nbuckets == 1) then
         bucket_c(1, 0) = 1
         bucket_c(2, 0) = glr%wfd%nvctr_c
         i3_c(1, 0) = (glr%wfd%keyglob(2, 1)-1)/np + 1
         i3_c(2, 0) = (glr%wfd%keyglob(2, glr%wfd%nseg_c)-1)/np + 1
      end if
      do iseg = 1, glr%wfd%nseg_c
         call segment_to_grid(glr%wfd, iseg, n1p1, np, i0, i1, i2, i3)
         if (i3+1<i3s) cycle
         if (i3+1>i3e) exit
         ii3=i3+1-i3s+1
         do i=i0,i1
            iitot_c = iitot_c + 1
            tt_c = tt_c + weights_c(i+1,i2+1,ii3)
            if (ibucket_c < nbuckets - 1) then
               if (tt_c >= limits_c(ibucket_c + 1)) then
                  if (ibucket_c == 0) then
                     bucket_c(1, 0) = 1
                     i3_c(1, 0) = (glr%wfd%keyglob(2, 1)-1)/np + 1
                  end if
                  ibucket_c = ibucket_c + 1
                  bucket_c(1, ibucket_c) = iitot_c
                  i3_c(1, ibucket_c) = i3 + 1
                  if (ibucket_c == nbuckets-1) then
                     bucket_c(2, ibucket_c) = glr%wfd%nvctr_c
                     i3_c(2, ibucket_c) = (glr%wfd%keyglob(2, glr%wfd%nseg_c)-1)/np + 1
                  end if
               end if
            end if
            nval_c(ibucket_c) = nval_c(ibucket_c) + nint(sqrt(weights_c(i+1,i2+1,ii3))) !total number of grid points to be handled by process jjproc
            sum_c(ibucket_c) = sum_c(ibucket_c) + weights_c(i+1,i2+1,ii3) !total weight to be handled by process jjproc
         end do
      end do

      if (glr%wfd%nseg_f == 0) then
         bucket_f(1, 0) = -1
      else if (ibucket_f == 0 .and. nbuckets == 1) then
         bucket_f(1, 0) = 1
         bucket_f(2, 0) = glr%wfd%nvctr_f
         i3_f(1, 0) = (glr%wfd%keyglob(2, glr%wfd%nseg_c + 1)-1)/np + 1
         i3_f(2, 0) = (glr%wfd%keyglob(2, glr%wfd%nseg_c + glr%wfd%nseg_f)-1)/np + 1
      end if
      do iseg = glr%wfd%nseg_c + 1, glr%wfd%nseg_c + glr%wfd%nseg_f
         call segment_to_grid(glr%wfd, iseg, n1p1, np, i0, i1, i2, i3)
         if (i3+1<i3s) cycle
         if (i3+1>i3e) exit
         ii3=i3+1-i3s+1
         do i=i0,i1
            iitot_f = iitot_f + 1
            tt_f = tt_f + weights_f(i+1,i2+1,ii3)
            if (ibucket_f < nbuckets - 1) then
               if (tt_f >= limits_f(ibucket_f + 1)) then
                  if (ibucket_f == 0) then
                     bucket_f(1, 0) = 1
                     i3_f(1, 0) = (glr%wfd%keyglob(2, glr%wfd%nseg_c + 1)-1)/np + 1
                  end if
                  ibucket_f = ibucket_f + 1
                  bucket_f(1, ibucket_f) = iitot_f
                  i3_f(1, ibucket_f) = i3 + 1
                  if (ibucket_f == nbuckets-1) then
                     bucket_f(2, ibucket_f) = glr%wfd%nvctr_f
                     i3_f(2, ibucket_f) = (glr%wfd%keyglob(2, glr%wfd%nseg_c + glr%wfd%nseg_f)-1)/np + 1
                  end if
               end if
            end if
            nval_f(ibucket_f) = nval_f(ibucket_f) + nint(sqrt(weights_f(i+1,i2+1,ii3))) !total number of grid points to be handled by process jjproc
            sum_f(ibucket_f) = sum_f(ibucket_f) + weights_f(i+1,i2+1,ii3) !total weight to be handled by process jjproc
         end do
      end do

    end subroutine lr_bucket

    ! Warning, currently only read wfd part
    subroutine read_lr_bin(unit, lr)
      implicit none
      type(locreg_descriptors), intent(inout), optional :: lr
      integer, intent(in) :: unit

      if (present(lr)) then
         call read_wfd_bin(unit, lr%wfd)
      else
         call read_wfd_bin(unit)
      end if
    end subroutine read_lr_bin
    subroutine read_lr_txt(unit, lr)
      implicit none
      type(locreg_descriptors), intent(inout), optional :: lr
      integer, intent(in) :: unit

      if (present(lr)) then
         call read_wfd_txt(unit, lr%wfd)
      else
         call read_wfd_txt(unit)
      end if
    end subroutine read_lr_txt

    subroutine read_array_plain_bin(lr, unit, arr)
      use liborbs_precisions
      use liborbs_errors
      use f_utils
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: unit
      real(wp), dimension(0:lr%mesh_coarse%ndims(1)-1, 2, &
           0:lr%mesh_coarse%ndims(2)-1, 2, &
           0:lr%mesh_coarse%ndims(3)-1, 2), intent(out) :: arr

      integer :: istat, i1, i2, i3, j
      real(wp) :: t0, t1, t2, t3, t4, t5, t6, t7

      call f_zero(arr)
      do j = 1, lr%wfd%nvctr_c
         read(unit, iostat = istat) i1, i2, i3, t0
         if (f_err_raise(istat /= 0, "cannot read psi coarse values.", &
              err_id = LIBORBS_LOCREG_ERROR())) return
         arr(i1, 1, i2, 1, i3, 1) = t0
      end do
      do j = 1, lr%wfd%nvctr_f
         read(unit, iostat = istat) i1, i2, i3, t1, t2, t3, t4, t5, t6, t7
         if (f_err_raise(istat /= 0, "cannot read psi fine values.", &
              err_id = LIBORBS_LOCREG_ERROR())) return
         arr(i1, 2, i2, 1, i3, 1) = t1
         arr(i1, 1, i2, 2, i3, 1) = t2
         arr(i1, 2, i2, 2, i3, 1) = t3
         arr(i1, 1, i2, 1, i3, 2) = t4
         arr(i1, 2, i2, 1, i3, 2) = t5
         arr(i1, 1, i2, 2, i3, 2) = t6
         arr(i1, 2, i2, 2, i3, 2) = t7
      end do
    end subroutine read_array_plain_bin

    subroutine read_array_plain_txt(lr, unit, arr)
      use liborbs_precisions
      use liborbs_errors
      use f_utils
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: unit
      real(wp), dimension(0:lr%mesh_coarse%ndims(1)-1, 2, &
           0:lr%mesh_coarse%ndims(2)-1, 2, &
           0:lr%mesh_coarse%ndims(3)-1, 2), intent(out) :: arr

      integer :: istat, i1, i2, i3, j
      real(wp) :: t0, t1, t2, t3, t4, t5, t6, t7

      call f_zero(arr)
      do j = 1, lr%wfd%nvctr_c
         read(unit, *, iostat = istat) i1, i2, i3, t0
         if (f_err_raise(istat /= 0, "cannot read psi coarse values.", &
              err_id = LIBORBS_LOCREG_ERROR())) return
         arr(i1, 1, i2, 1, i3, 1) = t0
      end do
      do j = 1, lr%wfd%nvctr_f
         read(unit, *, iostat = istat) i1, i2, i3, t1, t2, t3, t4, t5, t6, t7
         if (f_err_raise(istat /= 0, "cannot read psi fine values.", &
              err_id = LIBORBS_LOCREG_ERROR())) return
         arr(i1, 2, i2, 1, i3, 1) = t1
         arr(i1, 1, i2, 2, i3, 1) = t2
         arr(i1, 2, i2, 2, i3, 1) = t3
         arr(i1, 1, i2, 1, i3, 2) = t4
         arr(i1, 2, i2, 1, i3, 2) = t5
         arr(i1, 1, i2, 2, i3, 2) = t6
         arr(i1, 2, i2, 2, i3, 2) = t7
      end do
    end subroutine read_array_plain_txt

    subroutine read_array_compress_bin(lr, unit, arr)
      use liborbs_precisions
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: unit
      real(wp), dimension(lr%wfd%nvctr_c+7*lr%wfd%nvctr_f), intent(out) :: arr

      call wfd_read_array_bin(lr%wfd, unit, arr)
    end subroutine read_array_compress_bin

    subroutine read_array_compress_txt(lr, unit, arr)
      use liborbs_precisions
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: unit
      real(wp), dimension(lr%wfd%nvctr_c+7*lr%wfd%nvctr_f), intent(out) :: arr

      call wfd_read_array_txt(lr%wfd, unit, arr)
    end subroutine read_array_compress_txt

    subroutine dump_array_bin(lr, unit, arr)
      use liborbs_precisions
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: unit
      real(wp), dimension(lr%wfd%nvctr_c+7*lr%wfd%nvctr_f), intent(in) :: arr

      call dump_wfd_bin(lr%wfd, unit)
      call wfd_dump_array_bin(lr%wfd, unit, arr(:lr%wfd%nvctr_c), &
           arr(lr%wfd%nvctr_c+min(1, lr%wfd%nvctr_f):), &
           lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(1)*lr%mesh_coarse%ndims(2))
    end subroutine dump_array_bin

    subroutine dump_array_txt(lr, unit, arr)
      use liborbs_precisions
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: unit
      real(wp), dimension(lr%wfd%nvctr_c+7*lr%wfd%nvctr_f), intent(in) :: arr

      call dump_wfd_txt(lr%wfd, unit)
      call wfd_dump_array_txt(lr%wfd, unit, arr(:lr%wfd%nvctr_c), &
           arr(lr%wfd%nvctr_c+min(1, lr%wfd%nvctr_f):), &
           lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(1)*lr%mesh_coarse%ndims(2))
    end subroutine dump_array_txt

    function lr_do_reformat(lr_old, lr, displ, log)
      use liborbs_precisions
      use yaml_output
      implicit none
      type(locreg_descriptors), intent(in) :: lr_old, lr
      real(gp), intent(in), optional :: displ
      logical, intent(in), optional :: log

      real(gp) :: d
      logical :: lr_do_reformat

      d = 0._gp
      if (present(displ)) d = displ
      !reformatting criterion
      lr_do_reformat = any(abs(lr%mesh_coarse%hgrids - lr_old%mesh_coarse%hgrids) > 1.d-7) &
           .or. lr_old%wfd%nvctr_c /= lr%wfd%nvctr_c &
           .or. lr_old%wfd%nvctr_f /= lr%wfd%nvctr_f &
           .or. any(lr_old%mesh_coarse%ndims /= lr%mesh_coarse%ndims) &
           .or. d >=  1.d-3
      if (present(log)) then
         if (log .and. .not. lr_do_reformat) then
            call yaml_map('Reformating Wavefunctions', .false.)
         else if (log .and. lr_do_reformat) then
            call yaml_map('Reformating wavefunctions',.true.)
            call yaml_mapping_open('Reformatting for')
            if (any(abs(lr%mesh_coarse%hgrids - lr_old%mesh_coarse%hgrids) > 1.d-7)) then
               call yaml_mapping_open('hgrid modified',flow=.true.)
               call yaml_map('hgrid_old', lr_old%mesh_coarse%hgrids,fmt='(1pe20.12)')
               call yaml_map('hgrid', lr%mesh_coarse%hgrids, fmt='(1pe20.12)')
               call yaml_mapping_close()
            else if (lr_old%wfd%nvctr_c /= lr%wfd%nvctr_c) then
               call yaml_map('nvctr_c modified', (/ lr_old%wfd%nvctr_c, lr%wfd%nvctr_c /))
            else if (lr_old%wfd%nvctr_f /= lr%wfd%nvctr_f)  then
               call yaml_map('nvctr_f modified', (/ lr_old%wfd%nvctr_f, lr%wfd%nvctr_f /))
            else if (any(lr_old%mesh_coarse%ndims /= lr%mesh_coarse%ndims))  then
               call yaml_map('Cell size has changed', &
                    (/ (lr_old%mesh_coarse%ndims(1)-1),(lr%mesh_coarse%ndims(1)-1), &
                    (lr_old%mesh_coarse%ndims(2)-1),(lr%mesh_coarse%ndims(2)-1), &
                    (lr_old%mesh_coarse%ndims(3)-1),(lr%mesh_coarse%ndims(3)-1) /))
            else
               call yaml_map('Molecule was shifted, norm', d , fmt='(1pe19.12)')
            endif
            call yaml_mapping_close()
         end if
      end if
    end function lr_do_reformat

    subroutine lr_reformat_scf(lr, lr_old, psifscf, psifscfold, displ)
      use liborbs_precisions
      use box
      use at_domain
      use f_utils
      use bounds
      implicit none
      type(locreg_descriptors), intent(in) :: lr, lr_old
      real(gp), dimension(3), intent(in), optional :: displ
      real(wp), dimension(lr%mesh_fine%ndim), intent(out) :: psifscf
      real(wp), dimension(lr_old%mesh_fine%ndims(1), lr_old%mesh_fine%ndims(2), &
           lr_old%mesh_fine%ndims(3)), intent(in) :: psifscfold

      logical :: cif1,cif2,cif3,perx,pery,perz
      integer :: i1,i2,i3,j1,j2,j3,l1,l2,nb1,nb2,nb3,ind,jj1,jj2,jj3a,jj3b,jj3c
      integer :: ind2,ind3,n1o7,n2o7,n3o7
      real(gp) :: hxh_old,hyh_old,hzh_old,x,y,z,dx,dy,dz,xold,yold,zold!,mindist
      real(wp) :: zr,yr,xr,ym1,y00,yp1
      real(wp), dimension(-1:1,-1:1) :: xya
      real(wp), dimension(-1:1) :: xa
      logical, dimension(3) :: peri

      dx=0._gp
      dy=0._gp
      dz=0._gp
      if (present(displ)) then
         dx=displ(1)
         dy=displ(2)
         dz=displ(3)
      end if

      ! transform to new structure
      hxh_old=lr_old%mesh_fine%hgrids(1)
      hyh_old=lr_old%mesh_fine%hgrids(2)
      hzh_old=lr_old%mesh_fine%hgrids(3)

      call f_zero(psifscf)

      !conditions for periodicity in the three directions
      peri=domain_periodic_dims(lr%mesh_coarse%dom)
      perx=peri(1)
      pery=peri(2)
      perz=peri(3)

      !buffers related to periodicity
      !WARNING: the boundary conditions are not assumed to change between new and old
      call ext_buffers_coarse(perx,nb1)
      call ext_buffers_coarse(pery,nb2)
      call ext_buffers_coarse(perz,nb3)

      n1o7=lr_old%mesh_fine%ndims(1) - nb1 - 1
      n2o7=lr_old%mesh_fine%ndims(2) - nb2 - 1
      n3o7=lr_old%mesh_fine%ndims(3) - nb3 - 1

      !$omp parallel do default(none) &
      !$omp shared(nb1,nb2,nb3,lr,lr_old,hxh_old,hyh_old,hzh_old,dx,dy,dz,perx,pery,perz) &
      !$omp shared(n1o7,n2o7,n3o7,psifscf,psifscfold) &
      !$omp private(i1,i2,i3,j1,j2,j3,x,y,z,xold,yold,zold,cif1,cif2,cif3,ind,ind2,ind3,xr,yr,zr) &
      !$omp private(l1,l2,jj1,jj2,jj3a,jj3b,jj3c,ym1,y00,yp1,xya,xa)
      do i3=1,lr%mesh_fine%ndims(3)
         z=real(i3-1-nb3,gp)*lr%mesh_fine%hgrids(3)
         zold=z-dz
         j3=nint(zold/hzh_old)
         cif3=(j3 > -nb3 .and. j3 < n3o7) .or. perz
         if (.not.cif3) cycle

         zr =real((zold-real(j3,gp)*hzh_old)/hzh_old,wp)
         jj3a=modulo(j3-1+nb3,lr_old%mesh_fine%ndims(3))+1
         jj3b=modulo(j3  +nb3,lr_old%mesh_fine%ndims(3))+1
         jj3c=modulo(j3+1+nb3,lr_old%mesh_fine%ndims(3))+1
         ind3=lr%mesh_fine%ndims(1)*lr%mesh_fine%ndims(2)*(i3-1)
         do i2=1,lr%mesh_fine%ndims(2)
            y=real(i2-1-nb2,gp)*lr%mesh_fine%hgrids(2)
            yold=y-dy

            j2=nint(yold/hyh_old)
            cif2=(j2 > -nb2 .and. j2 < n2o7) .or. pery
            if (.not. cif2) cycle

            yr = real((yold-real(j2,gp)*hyh_old)/hyh_old,wp)
            ind2=lr%mesh_fine%ndims(1)*(i2-1)
            do i1=1,lr%mesh_fine%ndims(1)
               x=real(i1-1-nb1,gp)*lr%mesh_fine%hgrids(1)
               xold=x-dx

               j1=nint(xold/hxh_old)
               cif1=(j1 > -nb1 .and. j1 < n1o7) .or. perx

               if (cif1) then
                  ind=i1+ind2+ind3
                  do l2=-1,1
                     jj2=modulo(j2+l2+nb2,lr_old%mesh_fine%ndims(2))+1
                     do l1=-1,1
                        !the modulo has no effect on free BC thanks to the
                        !if statement above
                        jj1=modulo(j1+l1+nb1,lr_old%mesh_fine%ndims(1))+1

                        ym1=psifscfold(jj1,jj2,jj3a)
                        y00=psifscfold(jj1,jj2,jj3b)
                        yp1=psifscfold(jj1,jj2,jj3c)

                        xya(l1,l2)=ym1 + &
                             (1.0_wp + zr)*(y00 - ym1 + zr*(.5_wp*ym1 - y00  + .5_wp*yp1))
                     enddo
                  enddo

                  do l1=-1,1
                     ym1=xya(l1,-1)
                     y00=xya(l1,0)
                     yp1=xya(l1,1)
                     xa(l1)=ym1 + &
                          (1.0_wp + yr)*(y00 - ym1 + yr*(.5_wp*ym1 - y00  + .5_wp*yp1))
                  enddo

                  xr = real((xold-real(j1,gp)*hxh_old)/hxh_old,wp)
                  ym1=xa(-1)
                  y00=xa(0)
                  yp1=xa(1)
                  psifscf(ind)=ym1 + &
                       (1.0_wp + xr)*(y00 - ym1 + xr*(.5_wp*ym1 - y00  + .5_wp*yp1))

               endif

            enddo
         enddo
      enddo
      !$omp end parallel do

    end subroutine lr_reformat_scf
    
    !> Reformat one wavefunction
    subroutine lr_reformat_grid(lr, lr_old, psigold, psi, &
         shift, force, w_psifscf, w_psifscfold, w_psig)
      use liborbs_precisions
      use compression
      use dynamic_memory
      use at_domain
      implicit none
      type(locreg_descriptors), intent(in) :: lr_old, lr
      real(wp), dimension(0:lr_old%mesh_coarse%ndims(1)-1,2, &
           0:lr_old%mesh_coarse%ndims(2)-1,2, &
           0:lr_old%mesh_coarse%ndims(3)-1,2), intent(in) :: psigold
      real(wp), dimension(array_dim(lr)), intent(out) :: psi
      logical, intent(in), optional :: force
      real(gp), dimension(3), intent(in), optional :: shift
      real(wp), dimension(lr%mesh_fine%ndim), intent(out), optional :: w_psifscf
      real(wp), dimension(lr_old%mesh_fine%ndim), intent(out), optional :: w_psifscfold
      real(wp), dimension(0:lr%mesh_coarse%ndims(1)-1,2, &
           0:lr%mesh_coarse%ndims(2)-1,2, &
           0:lr%mesh_coarse%ndims(3)-1,2), intent(out), optional :: w_psig
      !local variables
      real(wp), dimension(:), allocatable :: ww,wwold
      real(wp), dimension(:,:,:,:,:,:), allocatable :: psig
      real(wp), dimension(:), allocatable :: psifscf
      real(wp), dimension(:), allocatable :: psifscfold
      logical :: reformat

      reformat = .false.
      if (present(force)) reformat = force
      reformat = any(lr%mesh_coarse%hgrids /= lr_old%mesh_coarse%hgrids) .or. &
           any(lr%mesh_coarse%ndims /= lr_old%mesh_coarse%ndims) .or. &
           reformat

      if (.not. reformat) then
         call wfd_compress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), &
              lr%mesh_coarse%ndims(3), psigold, psi)
         return
      end if

      call f_routine(id='lr_reformat_grid')

      wwold = f_malloc(lr_old%mesh_fine%ndim,id='wwold')
      if (present(w_psifscfold)) then
         call synthese(domain_geocode(lr_old%mesh_fine%dom), &
              lr_old%mesh_coarse%ndims-1, psigold, w_psifscfold, wwold)
      else
         psifscfold = f_malloc(lr_old%mesh_fine%ndim,id='psifscfold')
         call synthese(domain_geocode(lr_old%mesh_fine%dom), &
              lr_old%mesh_coarse%ndims-1, psigold, psifscfold, wwold)
      end if
      call f_free(wwold)

      if (present(w_psifscfold) .and. present(w_psifscf)) then
         call lr_reformat_scf(lr, lr_old, w_psifscf, w_psifscfold, shift)
      else if (present(w_psifscfold)) then
         psifscf = f_malloc(lr%mesh_fine%ndims,id='psifscf')
         call lr_reformat_scf(lr, lr_old, psifscf, w_psifscfold, shift)
      else if (present(w_psifscf)) then
         call lr_reformat_scf(lr, lr_old, w_psifscf, psifscfold, shift)
         call f_free(psifscfold)
      else
         psifscf = f_malloc(lr%mesh_fine%ndim,id='psifscf')
         call lr_reformat_scf(lr, lr_old, psifscf, psifscfold, shift)
         call f_free(psifscfold)
      end if

      ww = f_malloc(lr%mesh_fine%ndim,id='ww')
      if (present(w_psig) .and. present(w_psifscf)) then
         call analyse(domain_geocode(lr%mesh_fine%dom), &
              lr%mesh_coarse%ndims-1, w_psig, w_psifscf, ww)
      else if (present(w_psig)) then
         call analyse(domain_geocode(lr%mesh_fine%dom), &
              lr%mesh_coarse%ndims-1, w_psig, psifscf, ww)
         call f_free(psifscf)
      else if (present(w_psifscf)) then
         psig = f_malloc(grid_dim(lr),id='psig')
         call analyse(domain_geocode(lr%mesh_fine%dom), &
              lr%mesh_coarse%ndims-1, psig, w_psifscf, ww)
      else
         psig = f_malloc(grid_dim(lr),id='psig')
         call analyse(domain_geocode(lr%mesh_fine%dom), &
              lr%mesh_coarse%ndims-1, psig, psifscf, ww)
         call f_free(psifscf)
      end if
      call f_free(ww)

      if (present(w_psig)) then
         call wfd_compress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), &
              lr%mesh_coarse%ndims(3), w_psig, psi)
      else
         call wfd_compress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), &
              lr%mesh_coarse%ndims(3), psig, psi)
         call f_free(psig)
      end if

      call f_release_routine()

    contains

      subroutine synthese(geocode, n, wvl, scf, w_scf)
        use liborbs_precisions
        implicit none
        character(len = 1), intent(in) :: geocode
        integer, dimension(3), intent(in) :: n
        real(wp), dimension(:,:,:,:,:,:), intent(in) :: wvl
        real(wp), dimension(:), intent(out) :: scf
        real(wp), dimension(:), intent(out) :: w_scf

        select case(geocode)
        case('F')
           call synthese_grow(n(1), n(2), n(3), w_scf, wvl, scf)
        case('S')
           call synthese_slab(n(1), n(2), n(3), w_scf, wvl, scf)
        case('P')
           call synthese_per(n(1), n(2), n(3), w_scf, wvl, scf)
        case('W')
           call synthese_wire(n(1), n(2), n(3), w_scf, wvl, scf)
        end select
      end subroutine synthese

      subroutine analyse(geocode, n, wvl, scf, w_scf)
        use liborbs_precisions
        implicit none
        character(len = 1), intent(in) :: geocode
        integer, dimension(3), intent(in) :: n
        real(wp), dimension(:,:,:,:,:,:), intent(out) :: wvl
        real(wp), dimension(:), intent(in) :: scf
        real(wp), dimension(:), intent(out) :: w_scf

        select case(geocode)
        case('F')
           call analyse_shrink(n(1),n(2),n(3),w_scf,scf,wvl)
        case('S')
           call analyse_slab(n(1),n(2),n(3),w_scf,scf,wvl)
        case('P')
           call analyse_per(n(1),n(2),n(3),w_scf,scf,wvl)
        case('W')
           call analyse_wire(n(1),n(2),n(3),w_scf,scf,wvl)
        end select
      end subroutine analyse
    END SUBROUTINE lr_reformat_grid

    subroutine lr_reformat_array(lr, lr_old, psiold, psi, &
         shift, force, w_psifscf, w_psifscfold, w_psig, w_psigold)
      use liborbs_precisions
      use compression
      use dynamic_memory
      implicit none
      type(locreg_descriptors), intent(in) :: lr_old, lr
      real(wp), dimension(array_dim(lr_old)), intent(in) :: psiold
      real(wp), dimension(array_dim(lr)), intent(out) :: psi
      real(gp), dimension(3), intent(in), optional :: shift
      logical, intent(in), optional :: force
      real(wp), dimension(lr%mesh_fine%ndim), intent(out), optional :: w_psifscf
      real(wp), dimension(lr_old%mesh_fine%ndim), intent(out), optional :: w_psifscfold
      real(wp), dimension(0:lr%mesh_coarse%ndims(1)-1,2, &
           0:lr%mesh_coarse%ndims(2)-1,2, &
           0:lr%mesh_coarse%ndims(3)-1,2), intent(out), optional :: w_psig
      real(wp), dimension(0:lr_old%mesh_coarse%ndims(1)-1,2, &
           0:lr_old%mesh_coarse%ndims(2)-1,2, &
           0:lr_old%mesh_coarse%ndims(3)-1,2), intent(out), optional :: w_psigold
      !local variables
      real(wp), dimension(:,:,:,:,:,:), allocatable :: psigold
      logical :: reformat

      reformat = .false.
      if (present(force)) reformat = force
      if (.not. reformat) reformat = lr_do_reformat(lr_old, lr)

      if (.not. reformat) then
         call f_memcpy(src=psiold, dest=psi)
         return
      end if

      call f_routine(id='lr_reformat_array')

      if (present(w_psigold)) then
         call wfd_decompress(lr_old%wfd, lr_old%mesh_coarse%ndims(1), lr_old%mesh_coarse%ndims(2), &
              lr_old%mesh_coarse%ndims(3), w_psigold, psiold)
         call lr_reformat_grid(lr, lr_old, w_psigold, psi, &
              shift, force, w_psifscf, w_psifscfold, w_psig)
      else
         psigold = f_malloc(grid_dim(lr_old), id = 'psigold')
         call wfd_decompress(lr_old%wfd, lr_old%mesh_coarse%ndims(1), lr_old%mesh_coarse%ndims(2), &
              lr_old%mesh_coarse%ndims(3), psigold, psiold)
         call lr_reformat_grid(lr, lr_old, psigold, psi, &
              shift, force, w_psifscf, w_psifscfold, w_psig)
         call f_free(psigold)
      end if

      call f_release_routine()
    END SUBROUTINE lr_reformat_array

    subroutine lr_rewrap_array(lr, lr_old, psiold, psi, w_psig)
      use liborbs_precisions
      use compression
      use dynamic_memory
      implicit none
      type(locreg_descriptors), intent(in) :: lr, lr_old
      real(wp), dimension(array_dim(lr_old)), intent(in) :: psiold
      real(wp), dimension(array_dim(lr)), intent(out) :: psi
      real(wp), dimension( &
           max(lr%mesh_coarse%ndims(1), lr_old%mesh_coarse%ndims(1)), 2, &
           max(lr%mesh_coarse%ndims(2), lr_old%mesh_coarse%ndims(2)), 2, &
           max(lr%mesh_coarse%ndims(3), lr_old%mesh_coarse%ndims(3)), 2 &
           ), intent(inout), optional :: w_psig

      real(wp), dimension(:,:,:,:,:,:), allocatable :: psigold

      if (present(w_psig)) then
         call wfd_decompress(lr_old%wfd, &
              lr_old%mesh_coarse%ndims(1), &
              lr_old%mesh_coarse%ndims(2), &
              lr_old%mesh_coarse%ndims(3), w_psig, psiold)
         call wfd_compress(lr%wfd, &
              lr%mesh_coarse%ndims(1), &
              lr%mesh_coarse%ndims(2), &
              lr%mesh_coarse%ndims(3), w_psig, psi)
      else
         psigold = f_malloc((/ &
              max(lr%mesh_coarse%ndims(1), lr_old%mesh_coarse%ndims(1)), 2, &
              max(lr%mesh_coarse%ndims(2), lr_old%mesh_coarse%ndims(2)), 2, &
              max(lr%mesh_coarse%ndims(3), lr_old%mesh_coarse%ndims(3)), 2 &
              /), id='psigold')
         call wfd_decompress(lr_old%wfd, &
              lr_old%mesh_coarse%ndims(1), &
              lr_old%mesh_coarse%ndims(2), &
              lr_old%mesh_coarse%ndims(3), psigold, psiold)
         call wfd_compress(lr%wfd, &
              lr%mesh_coarse%ndims(1), &
              lr%mesh_coarse%ndims(2), &
              lr%mesh_coarse%ndims(3), psigold, psi)
         call f_free(psigold)
      end if
    end subroutine lr_rewrap_array

    subroutine lr_rewrap_grid(lr, lr_old, psigold, psi)
      use liborbs_precisions
      use liborbs_errors
      use compression
      use dynamic_memory
      implicit none
      type(locreg_descriptors), intent(in) :: lr, lr_old
      real(wp), dimension(lr_old%mesh_coarse%ndims(1),2, &
           lr_old%mesh_coarse%ndims(2),2, &
           lr_old%mesh_coarse%ndims(3),2), intent(in) :: psigold
      real(wp), dimension(array_dim(lr)), intent(out) :: psi

      if (f_err_raise(any(lr_old%mesh%ndims /= lr%mesh%ndims), &
           "Cannot rewrap, different dimensions", &
           err_id = LIBORBS_LOCREG_ERROR())) return
      call wfd_compress(lr%wfd, &
           lr%mesh_coarse%ndims(1), &
           lr%mesh_coarse%ndims(2), &
           lr%mesh_coarse%ndims(3), psigold, psi)
    end subroutine lr_rewrap_grid

    subroutine lr_resize(lr, ndims0, ndims)
      implicit none
      type(locreg_descriptors), intent(inout) :: lr
      integer, dimension(3), intent(in) :: ndims0, ndims

      integer :: i0, i1, i2, i3, n1, n2, nb1, nb2, iseg, j0, j1
      integer, dimension(3) :: nbuf

      nbuf = (ndims - ndims0) / 2
      do iseg = 1, lr%wfd%nseg_c + lr%wfd%nseg_f
         call segment_to_grid(lr%wfd, iseg, ndims0(1), ndims0(1) * ndims0(2), &
              i0, i1, i2, i3)
         i3=i3+nbuf(3)
         i2=i2+nbuf(2)
         i1=i1+nbuf(1)
         i0=i0+nbuf(1)
         j0=i3*ndims(1)*ndims(2) + i2*ndims(1) + i0+1
         j1=i3*ndims(1)*ndims(2) + i2*ndims(1) + i1+1
         lr%wfd%keyglob(1,iseg)=j0
         lr%wfd%keyglob(2,iseg)=j1
      end do
      n1 = lr%mesh_coarse%ndims(1)
      n2 = lr%mesh_coarse%ndims(1)
      lr%mesh_coarse%ndims = lr%mesh_coarse%ndims + 2 * nbuf
      nb1 = lr%mesh_coarse%ndims(1)
      nb2 = lr%mesh_coarse%ndims(1)
      do iseg = 1, lr%wfd%nseg_c + lr%wfd%nseg_f
         call local_segment_to_grid(lr%wfd, iseg, n1, n1*n2, &
              i0, i1, i2, i3)
         i3=i3+nbuf(3)
         i2=i2+nbuf(2)
         i1=i1+nbuf(1)
         i0=i0+nbuf(1)
         j0=i3*(nb1*nb2) + i2*nb1 + i0+1
         j1=i3*(nb1*nb2) + i2*nb1 + i1+1
         lr%wfd%keygloc(1,iseg)=j0
         lr%wfd%keygloc(2,iseg)=j1
      end do
      lr%nboxc(2,:) = lr%nboxc(2,:) + 2*nbuf
      lr%mesh%ndims = ndims
    end subroutine lr_resize

    function lr_n_c_points(lr, i3start, i3end) result(np)
      use compression
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in), optional :: i3start, i3end

      integer :: np, i3s, i3e, iseg, n1, n12, i0, i1, i2, i3

      i3s = 0
      if (present(i3start)) i3s = i3start
      i3e = lr%mesh_coarse%ndims(3)-1
      if (present(i3end)) i3e = i3end
      if (i3s <= 0 .and. i3e >= lr%mesh_coarse%ndims(3)-1) then
         np = lr%wfd%nvctr_c
      else
         np = 0
         n1=lr%mesh_coarse%ndims(1)
         n12=n1*lr%mesh_coarse%ndims(2)
         do iseg=1,lr%wfd%nseg_c
            call segment_to_grid(lr%wfd, iseg, n1, n12, i0, i1, i2, i3)
            if (i3+1<i3s) cycle
            if (i3+1>i3e) exit
            np = np + i1 - i0 + 1
         end do
      end if
    end function lr_n_c_points

    function lr_n_f_points(lr, i3start, i3end) result(np)
      use compression
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in), optional :: i3start, i3end

      integer :: np, i3s, i3e, iseg, n1, n12, i0, i1, i2, i3

      i3s = 0
      if (present(i3start)) i3s = i3start
      i3e = lr%mesh_coarse%ndims(3)-1
      if (present(i3end)) i3e = i3end
      if (i3s <= 0 .and. i3e >= lr%mesh_coarse%ndims(3)-1) then
         np = lr%wfd%nvctr_f
      else
         np = 0
         n1=lr%mesh_coarse%ndims(1)
         n12=n1*lr%mesh_coarse%ndims(2)
         do iseg=lr%wfd%nseg_c+1,lr%wfd%nseg_c+lr%wfd%nseg_f
            call segment_to_grid(lr%wfd, iseg, n1, n12, i0, i1, i2, i3)
            if (i3+1<i3s) cycle
            if (i3+1>i3e) exit
            np = np + i1 - i0 + 1
         end do
      end if
    end function lr_n_f_points

    subroutine lr_invert(lr, i3offset, toIndex_c, toIndex_f, i3start, i3end)
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: i3offset
      integer, intent(in), optional :: i3start, i3end
      integer, dimension(:, :, :), intent(out) :: toIndex_c, toIndex_f

      integer :: i3s, i3e, iseg, n1, n12, i0, i1, i2, i3, jj, jj3, i

      i3s = 0
      if (present(i3start)) i3s = i3start
      i3e = lr%mesh_coarse%ndims(3)-1
      if (present(i3end)) i3e = i3end

      n1 = lr%mesh_coarse%ndims(1)
      n12 = n1 * lr%mesh_coarse%ndims(2)
      do iseg = 1, lr%wfd%nseg_c + lr%wfd%nseg_f
         call segment_to_grid(lr%wfd, iseg, n1, n12, i0, i1, i2, i3, jj)
         jj3 = modulo(i3 - i3offset, lr%mesh_coarse%ndims(3)) + 1
         if (jj3 >= i3s .and. jj3 <= i3e) then
            if (iseg > lr%wfd%nseg_c) then
               do i = i0, i1
                  toIndex_f(i+1, i2+1, jj3) = i - i0 + jj
               end do
            else
               do i = i0, i1
                  toIndex_c(i+1, i2+1, jj3) = i - i0 + jj
               end do
            end if
         end if
      end do
    end subroutine lr_invert

    subroutine lr_count_bucketing(lr, glr, i3offset, toIndex_c, toIndex_f, &
         nbuckets, bucket_c, bucket_f, count_c, count_f)
      implicit none
      type(locreg_descriptors), intent(in) :: lr, glr
      integer, intent(in) :: i3offset, nbuckets
      integer, dimension(:, :, :), intent(in) :: toIndex_c, toIndex_f
      integer, dimension(2, nbuckets), intent(in) :: bucket_c, bucket_f
      integer, dimension(nbuckets), intent(inout) :: count_c, count_f

      integer :: n1, n12, i0, i1, i2, i3, i, jj3, ind, jproc, iseg

      n1 = glr%mesh_coarse%ndims(1)
      n12 = n1 * glr%mesh_coarse%ndims(2)
      !$omp parallel do default(private) shared(nbuckets,lr,glr,toIndex_c,bucket_c) &
      !$omp shared(bucket_f,toIndex_f,n1,n12,i3offset) &
      !$omp reduction(+:count_c, count_f)
      do iseg = 1, lr%wfd%nseg_c + lr%wfd%nseg_f
         call segment_to_grid(lr%wfd, iseg, n1, n12, i0, i1, i2, i3)
         jj3 = modulo(i3-i3offset, glr%mesh_coarse%ndims(3))+1
         if (iseg > lr%wfd%nseg_c) then
            do i=i0,i1
               ind = toIndex_f(i+1,i2+1,jj3)
               do jproc = 1, nbuckets
                  if (ind >= bucket_f(1,jproc) .and. ind <= bucket_f(2,jproc)) then
                     count_f(jproc) = count_f(jproc) + 1
                     exit
                  end if
               end do
            end do
         else
            do i=i0,i1
               ind = toIndex_c(i+1,i2+1,jj3)
               do jproc = 1, nbuckets
                  if (ind >= bucket_c(1,jproc) .and. ind <= bucket_c(2,jproc)) then
                     count_c(jproc) = count_c(jproc) + 1
                     exit
                  end if
               end do
            end do
         end if
      end do
      !$omp end parallel do

    end subroutine lr_count_bucketing

    subroutine lr_reduce_bucketing(glr, bucket_c, bucket_f, arr_c, arr_f, &
         i3offset_c, i3offset_f, sum_c, sum_f, &
         check_count_c, check_count_f, check_sum_c, check_sum_f)
      use liborbs_precisions
      implicit none
      type(locreg_descriptors), intent(in) :: glr
      integer, intent(in) :: i3offset_c, i3offset_f
      integer, dimension(2), intent(in) :: bucket_c, bucket_f
      real(gp), dimension(:,:,:), intent(in) :: arr_c, arr_f
      integer, dimension(:), intent(out) :: sum_c, sum_f
      integer, intent(out) :: check_count_c, check_count_f
      real(gp), intent(out) :: check_sum_c, check_sum_f

      integer :: n1, n12, i0, i1, i2, i3, i, iseg, ind, jj

      n1 = glr%mesh_coarse%ndims(1)
      n12 = n1 * glr%mesh_coarse%ndims(2)

      check_count_c = 0
      check_sum_c = 0._gp
      check_count_f = 0
      check_sum_f = 0._gp
      do iseg = 1, glr%wfd%nseg_c + glr%wfd%nseg_f
         call segment_to_grid(glr%wfd, iseg, n1, n12, i0, i1, i2, i3, jj)
         if (iseg > glr%wfd%nseg_c) then
            do i = i0, i1
               ind = jj + i - i0
               if(ind >= bucket_f(1) .and. ind <= bucket_f(2)) then
                  check_count_f = check_count_f + 1
                  check_sum_f = check_sum_f + arr_f(i+1,i2+1,i3+1-i3offset_f+1)
                  ind = jj - bucket_f(1) + i - i0 + 1
                  sum_f(ind) = nint(sqrt(arr_f(i+1,i2+1,i3+1-i3offset_f+1)))
               end if
            end do
         else
            do i = i0, i1
               ind = jj + i - i0
               if(ind >= bucket_c(1) .and. ind <= bucket_c(2)) then
                  check_count_c = check_count_c + 1
                  check_sum_c = check_sum_c + arr_c(i+1,i2+1,i3+1-i3offset_c+1)
                  ind = jj - bucket_c(1) + i - i0 + 1
                  sum_c(ind) = nint(sqrt(arr_c(i+1,i2+1,i3+1-i3offset_c+1)))
               end if
            end do
         end if
      end do
    end subroutine lr_reduce_bucketing

    subroutine lr_merge_to_dict(dict, lr, export, lbin)
      use dictionaries
      use f_utils
      use at_domain
      implicit none
      type(dictionary), pointer :: dict
      type(locreg_descriptors), intent(in) :: lr
      character(len = *), intent(in), optional :: export
      logical, intent(in), optional :: lbin
      
      integer :: iunit, islash
      logical :: lbin_

      call domain_merge_to_dict(dict, lr%mesh%dom, withUnits = .false.)
      call set(dict // KEY_HGRIDS, lr%mesh_coarse%hgrids)
      call set(dict // KEY_NDIMS, lr%mesh_coarse%ndims)
      if (any(lr%nboxc(1, :) /= 0)) &
           call set(dict // KEY_OFFSET, lr%nboxc(1, :))
      call set(dict // KEY_NDIMS_FINE, lr%nboxf(2, :) - lr%nboxf(1, :) + 1)
      call set(dict // KEY_NS_FINE, lr%nboxf(1, :))
      if (any(lr%locregCenter /= 0._gp)) &
           call set(dict // KEY_CENTER, lr%locregCenter)
      if (lr%locrad > 0._gp) &
           call set(dict // KEY_RADIUS, lr%locrad)
      
      if (present(export)) then
         lbin_ = .false.
         if (present(lbin)) lbin_ = lbin

         islash = index(export, "/", back = .true.) + 1
         if (lbin_) then
            call set(dict // KEY_BINKEYS, export(islash:))
         else
            call set(dict // KEY_KEYS, export(islash:))
         end if

         iunit = 65
         call f_open_file(iunit, export, binary = lbin_)
         if (lbin_) then
            call dump_wfd_bin(lr%wfd, iunit)
         else
            call dump_wfd_txt(lr%wfd, iunit)
         end if
         call f_close(iunit)
      end if
    end subroutine lr_merge_to_dict

    subroutine lr_set_from_dict(lr, dict, gdom, path)
      use dictionaries
      use at_domain
      use f_utils
      implicit none
      type(dictionary), pointer :: dict
      type(locreg_descriptors), intent(out) :: lr
      type(domain), intent(in), optional :: gdom
      character(len = *), intent(in), optional :: path

      integer :: iunit
      type(domain) :: dom
      real(gp), dimension(3) :: hgrids, hgridsh
      integer, dimension(3) :: ndims, ns, ndimsf, nsf
      integer, dimension(2,3) :: nbox, nboxf

      lr = locreg_null()
      if (.not. associated(dict)) return

      if ((KEY_HGRIDS .in. dict) .and. (KEY_NDIMS .in. dict)) then
         hgrids = dict // KEY_HGRIDS
         ndims = dict // KEY_NDIMS
         ns = 0
         if (KEY_OFFSET .in. dict) ns = dict // KEY_OFFSET
         ndimsf = ndims
         if (KEY_NDIMS_FINE .in. dict) ndimsf = dict // KEY_NDIMS_FINE
         nsf = 0
         if (KEY_NS_FINE .in. dict) nsf = dict // KEY_NS_FINE
         nbox(1,:) = ns
         nbox(2,:) = ns + ndims - 1
         nboxf(1,:) = nsf
         nboxf(2,:) = nsf + ndimsf - 1
         call domain_set_from_dict(dict, dom)
         dom%acell = hgrids * ndims
         hgridsh = 0.5_gp * hgrids
         call init_lr(lr, dom, hgridsh, nbox, nboxf, .false., global_dom = gdom)
         if (KEY_RADIUS .in. dict) lr%locrad = dict // KEY_RADIUS
         if (KEY_CENTER .in. dict) lr%locregCenter = dict // KEY_CENTER

         if (present(path)) then
            if (KEY_KEYS .in. dict) then
               iunit = 65
               call f_open_file(iunit, path // trim(dict_value(dict // KEY_KEYS)), binary = .false.)
               call read_wfd_txt(iunit, lr%wfd)
               call f_close(iunit)
            else if (KEY_BINKEYS .in. dict) then
               iunit = 65
               call f_open_file(iunit, path // trim(dict_value(dict // KEY_BINKEYS)), binary = .true.)
               call read_wfd_bin(iunit, lr%wfd)
               call f_close(iunit)
            end if
         end if
      end if
    end subroutine lr_set_from_dict

  subroutine create_workarrays_tolr_0d(w, lr)
    use dynamic_memory
    implicit none
    type(workarrays_tolr), intent(out) :: w
    type(locreg_descriptors), intent(in) :: lr

    call set_workarrays_tolr(w, lr%wfd)
  end subroutine create_workarrays_tolr_0d

  subroutine create_workarrays_tolr_1d(w, lrs)
    use dynamic_memory
    implicit none
    type(workarrays_tolr), intent(out) :: w
    type(locreg_descriptors), dimension(:), intent(in) :: lrs

    integer :: nseg, i

    nseg = 0
    do i = 1, size(lrs)
       nseg = max(nseg, lrs(i)%wfd%nseg_c + lrs(i)%wfd%nseg_f)
    end do

    w%keyag_lin_cf = f_malloc_ptr(nseg, id="nbsegs_cf")
    nullify(w%nbsegs_cf)
    w%nseg = 0
  end subroutine create_workarrays_tolr_1d

  subroutine workarrays_tolr_add_plr(w, plr)
    use dynamic_memory
    implicit none
    type(workarrays_tolr), intent(inout) :: w
    type(locreg_descriptors), intent(in) :: plr

    call workarrays_tolr_add_wfd(w, plr%wfd)
  end subroutine workarrays_tolr_add_plr
  
  !> initialize the information for matching the localisation region
  !! of each projector to all the localisation regions of the system
  subroutine set_lr_to_lr(Plr,Glr,w,lrs,lr_mask)
    use compression
    use liborbs_errors
    implicit none
    type(interacting_locreg), intent(inout) :: Plr
    type(locreg_descriptors), intent(in) :: Glr !<global simulation domain
    type(workarrays_tolr), intent(inout) :: w
    !> mask array which is associated to the localization regions of interest in this processor
    logical, dimension(:), optional, intent(in) :: lr_mask
    !> descriptors of all the localization regions of the simulation domain
    !! susceptible to interact with the projector
    type(locreg_descriptors), dimension(:), optional, intent(in) :: lrs
    !local variables
    logical :: overlap
    integer :: ilr, iilr, ioverlap

    call f_routine(id='set_wfd_to_wfd')

    call deallocate_wfd_to_wfd_ptr(Plr%tolr)
    call f_free_ptr(Plr%lut_tolr)

    if (Plr%plr%wfd%nseg_c + Plr%plr%wfd%nseg_f == 0) return

    ! Determine the size of tolr and initialize the corresponding lookup table
    Plr%nlr=1
    overlap=.true.
    if (present(lrs)) then
       Plr%nlr=size(lrs)
    end if
    if (Plr%nlr <=0) return

    ! Count how many overlaps exist
    Plr%noverlap=0
    do ilr=1,Plr%nlr
       !control if the projector overlaps with this locreg
       if (present(lrs)) then
          overlap=.true.
          if (present(lr_mask)) overlap=lr_mask(ilr)
          if (overlap) call check_overlap(lrs(ilr),Plr%plr,Glr,overlap)
          !if there is overlap, activate the strategy for the application
          if (overlap) then
             Plr%noverlap=Plr%noverlap+1
          end if
       else
          Plr%noverlap=Plr%noverlap+1
       end if
    end do
    Plr%lut_tolr = f_malloc0_ptr(Plr%noverlap,id='lut_tolr')

    ! Now assign the values
    ioverlap=0
    do ilr=1,Plr%nlr
       !control if the projector overlaps with this locreg
       if (present(lrs)) then
          overlap=.true.
          if (present(lr_mask)) overlap=lr_mask(ilr)
          if (overlap) call check_overlap(lrs(ilr),Plr%plr,Glr,overlap)
          !if there is overlap, activate the strategy for the application
          if (overlap) then
             ioverlap=ioverlap+1
             Plr%lut_tolr(ioverlap)=ilr
          end if
       else
          ioverlap=ioverlap+1
          Plr%lut_tolr(ioverlap)=ilr
       end if
    end do
    if (ioverlap/=Plr%noverlap) stop 'ioverlap/=Plr%noverlap'

    if (present(lrs) .and. present(lr_mask)) then
       if (f_err_raise(Plr%nlr /= size(lr_mask), &
            'The sizes of lr_mask and lrs should coincide',&
            err_id = LIBORBS_LOCREG_ERROR())) return
    end if
    !allocate the pointer with the good size
    allocate(Plr%tolr(Plr%noverlap))
    !then for any of the localization regions check the strategy
    !for applying the projectors
    !ioverlap=0
    do ilr=1,Plr%noverlap
       iilr=Plr%lut_tolr(ilr)
       !this will set to PSP_APPLY_STRATEGY_SKIP the projector application
       call nullify_wfd_to_wfd(Plr%tolr(ilr))
       !now control if the projector overlaps with this locreg
       if (present(lrs)) then
          overlap=.true.
          if (present(lr_mask)) overlap=lr_mask(iilr)
          if (overlap) call check_overlap(lrs(iilr),Plr%plr,Glr,overlap)
          !if there is overlap, activate the strategy for the application
          if (overlap) then
             call init_tolr(Plr%tolr(ilr),lrs(iilr)%wfd,Plr%plr%wfd,w%keyag_lin_cf,w%nbsegs_cf)
          end if
       else
          call init_tolr(Plr%tolr(ilr),Glr%wfd,Plr%plr%wfd,w%keyag_lin_cf,w%nbsegs_cf)
       end if
       !then the best strategy can be decided according to total number of
       !common points
       !complete stategy, the packing array is created after first projector
!!$         if (overlap) tolr(ilr)%strategy=STRATEGY_MASK_PACK
       if (overlap) call tolr_set_strategy(Plr%tolr(ilr),'MASK_PACK')
       !masking is used but packing is not created,
       !useful when only one projector has to be applied
       !tolr(ilr)%strategy=PSP_APPLY_STRATEGY_MASK
       !old scheme, even though mask arrays is created it is never used.
       !most likely this scheme is useful for debugging purposes
       !tolr(ilr)%strategy=PSP_APPLY_STRATEGY_KEYS
    end do

    !!if (ioverlap/=Plr%noverlap) stop 'ioverlap/=Plr%noverlap'

    call f_release_routine()

  end subroutine set_lr_to_lr

  function get_lr_to_lr(Plr, ilr, lr, glr) result(tolr)
    implicit none
    type(interacting_locreg), intent(in) :: Plr
    integer, intent(in) :: ilr
    type(locreg_descriptors), intent(in) :: lr, glr

    type(wfd_to_wfd), pointer :: tolr
    integer :: jlr
    logical :: overlap

    nullify(tolr)
    do jlr = 1, Plr%noverlap
       if (Plr%lut_tolr(jlr) == ilr) then
          if (.not. wfd_to_wfd_skip(Plr%tolr(jlr))) then
             call check_overlap(lr, Plr%plr, glr, overlap)
             if (overlap) tolr => Plr%tolr(jlr)
          end if
          return
       end if
    end do
  end function get_lr_to_lr

end module locregs
