!> @file
!!  Routines to apply the magic filter and an analysis wavelet transform
!! @author 
!!    Copyright (C) 2010-2011 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS 


!> In 3d,            
!! Applies the magic filter transposed, then analysis wavelet transformation.
!! The size of the data is forced to shrink
!! The input array y is not overwritten
subroutine comb_shrink_hyb_c(n1,n2,n3,w1,w2,y,x)
use liborbs_precisions
implicit none
integer,intent(in) ::n1,n2,n3 
real(wp),dimension(0:2*n1+1,0:2*n2+1,0:2*n3+1),intent(in)::y!input
real(wp),dimension         (0:2*n2+1,0:2*n3+1,0:n1), intent(inout)::w1
real(wp),dimension                  (0:2*n3+1,0:n1,0:n2), intent(inout)::w2
real(wp),dimension                           (0:n1,0:n2,0:n3), intent(out)::x!output
!local variables
integer :: nt

! I1,I2,I3 -> I2,I3,i1
nt=(2*n2+2)*(2*n3+2)
call comb_rot_shrink_hyb(nt,y,w1,n1)

! I2,I3,i1 -> I3,i1,i2
nt=(2*n3+2)*(n1+1)
call comb_rot_shrink_hyb(nt,w1,w2,n2)

! I3,i1,i2 -> i1,i2,i3
nt=(n1+1)*(n2+1)
call comb_rot_shrink_hyb(nt,w2,x,n3)

END SUBROUTINE comb_shrink_hyb_c

!> In 3d,            
!! Applies the magic filter transposed, then analysis wavelet transformation.
!! The size of the data is forced to shrink
!! The input array y is not overwritten
subroutine comb_shrink_hyb(n1,n2,n3,nfl1,nfu1,nfl2,nfu2,nfl3,nfu3,w1,w2,y,xc,xf,sb)
  use liborbs_precisions
  use bounds, only: shrink_bounds
  implicit none
  type(shrink_bounds),intent(in):: sb
  integer, intent(in) :: n1,n2,n3,nfl1,nfu1,nfl2,nfu2,nfl3,nfu3
  real(wp), dimension(0:2*n1+1,0:2*n2+1,0:2*n3+1), intent(in) :: y
  real(wp), dimension(max(2*(2*n2+2)*(2*n3+2)*(nfu1-nfl1+1),&
       (2*n2+2)*(2*n3+2)*(n1+1))), intent(inout) :: w1
  real(wp), dimension(max(4*(2*n3+2)*(nfu1-nfl1+1)*(nfu2-nfl2+1),&
       (2*n3+2)*(n1+1)*(n2+1))), intent(inout) :: w2
  real(wp), dimension(0:n1,0:n2,0:n3), intent(inout) :: xc
  real(wp), dimension(7,nfl1:nfu1,nfl2:nfu2,nfl3:nfu3), intent(inout) :: xf

  integer nt

  !perform the combined transform    

  call comb_shrink_hyb_c(n1,n2,n3,w1,w2,y,xc)

  ! I1,I2,I3 -> I2,I3,i1
  nt=(2*n2+2)*(2*n3+2)
  call comb_rot_shrink_hyb_1_ib(nt,n1,nfl1,nfu1,y,w1,sb%ibyyzz_f)

  ! I2,I3,i1 -> I3,i1,i2
  nt=(2*n3+2)*(nfu1-nfl1+1)
  call comb_rot_shrink_hyb_2_ib(nt,w1,w2,nfl2,nfu2,n2,sb%ibzzx_f)

  ! I3,i1,i2 -> i1,i2,i3
  nt=(nfu1-nfl1+1)*(nfu2-nfl2+1)
  call comb_rot_shrink_hyb_3_ib(nt,w2,xf,nfl3,nfu3,n3,sb%ibxy_ff)

END SUBROUTINE comb_shrink_hyb

subroutine comb_grow_all_hybrid(n1,n2,n3,nfl1,nfu1,nfl2,nfu2,nfl3,nfu3,nw1,nw2&
     ,w1,w2,xc,xf,y,gb)
  use liborbs_precisions
  use bounds, only: grow_bounds
  implicit none
  type(grow_bounds),intent(in):: gb
  integer,intent(in)::n1,n2,n3,nfl1,nfu1,nfl2,nfu2,nfl3,nfu3,nw1,nw2
  real(wp), dimension(0:n1,0:n2,0:n3), intent(in) :: xc
  real(wp), dimension(7,nfl1:nfu1,nfl2:nfu2,nfl3:nfu3), intent(in) :: xf
  real(wp), dimension(nw1), intent(inout) :: w1 !work
  real(wp), dimension(nw2), intent(inout) :: w2 ! work
  real(wp), dimension(0:2*n1+1,0:2*n2+1,0:2*n3+1), intent(inout) :: y

  call comb_grow_c_simple(n1,n2,n3,w1,w2,xc,y)

  call comb_rot_grow_ib_1(n1      ,nfl1,nfu1,nfl2,nfu2,nfl3,nfu3,xf,w1,gb%ibyz_ff,gb%ibzxx_f)
  call comb_rot_grow_ib_2(n1,n2   ,          nfl2,nfu2,nfl3,nfu3,w1,w2,gb%ibzxx_f,gb%ibxxyy_f)
  call comb_rot_grow_ib_3(n1,n2,n3,                    nfl3,nfu3,w2,y,gb%ibxxyy_f)

END SUBROUTINE comb_grow_all_hybrid

subroutine comb_grow_c_simple(n1,n2,n3,w1,w2,x,y)
  use liborbs_precisions
  implicit none
  integer,intent(in)::n1,n2,n3
  real(wp),intent(in)::x(0:n1,0:n2,0:n3)   
  real(wp),dimension((2*n1+2)*(n2+1)*(n3+1)), intent(inout) :: w1 !work
  real(wp),dimension((n3+1)*(2*n1+2)*(2*n2+2)), intent(inout) :: w2 ! work
  real(wp),dimension((2*n1+2)*(2*n2+2)*(2*n3+2)), intent(inout) :: y

  integer::nt

  ! i1,i2,i3 -> i2,i3,I1
  nt=(n2+1)*(n3+1)
  call  comb_rot_grow(n1,nt,x,w1) 

  ! i2,i3,I1 -> i3,I1,I2
  nt=(n3+1)*(2*n1+2)
  call  comb_rot_grow(n2,nt,w1,w2) 

  ! i3,I1,I2  -> I1,I2,I3
  nt=(2*n1+2)*(2*n2+2)
  call  comb_rot_grow(n3,nt,w2,y) 
  
END SUBROUTINE comb_grow_c_simple
