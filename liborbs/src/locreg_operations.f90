!> @file
!! Local Region operations
!! @author Copyright (C) 2015-2015 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS


!> Module for the local region operations on orbitals
module locreg_operations
  use liborbs_precisions
  use dynamic_memory
  use locregs
  use f_enums
  use f_utils, only: f_zero
  use f_precisions, only: f_address
  use liborbs_workarrays, only: memspace_work_arrays_sumrho, memspace_work_arrays_locham, memspace_work_arrays_precond
  implicit none

  private

  type, public :: workarrays_ocl_sumrho
     integer(f_address) :: psi_c, psi_f
     integer(f_address) :: work1, work2, work3, d
  end type workarrays_ocl_sumrho

  type, public :: workarrays_ocl_precond
     integer(f_address) :: psi_c_r, psi_f_r
     integer(f_address) :: psi_c_b, psi_f_b
     integer(f_address) :: psi_c_d, psi_f_d
  end type workarrays_ocl_precond

  public :: isf_to_daub_kinetic, daub_to_isf_locham, psi_to_tpsi
  public :: daub_to_isf_ocl, isf_to_daub_ocl
  public :: full_locham_ocl_async
  public :: precondition_residue_ocl
  public :: density_ocl
  public :: Lpsi_to_global2
  public :: global_to_local_parallel
  public :: boundary_weight
  public :: psi_to_locreg2
  public :: global_to_local
  public :: memspace_work_arrays_sumrho,memspace_work_arrays_locham,memspace_work_arrays_precond

  public :: create_ocl_sumrho, deallocate_ocl_sumrho
  public :: set_ocl_sumrho_sync, set_ocl_sumrho_async
  public :: get_ocl_sumrho_sync, get_ocl_sumrho_async

  public :: create_ocl_precond, deallocate_ocl_precond

  contains

    !> Tranform wavefunction between localisation region and the global region
    !!!!!#######!> This routine only works if both locregs have free boundary conditions.
    !! @warning
    !! WARNING: Make sure psi is set to zero where Glr does not collide with Llr (or everywhere)
    subroutine Lpsi_to_global2(ldim, gdim, norb, nspin, Glr, Llr, lpsi, psi)
     implicit none

      ! Subroutine Scalar Arguments
      integer :: Gdim          ! dimension of psi
      integer :: Ldim          ! dimension of lpsi
      integer :: norb          ! number of orbitals
      integer :: nspin         ! number of spins
      type(locreg_descriptors),intent(in) :: Glr  ! Global grid descriptor
      type(locreg_descriptors), intent(in) :: Llr  ! Localization grid descriptors

      !Subroutine Array Arguments
      real(wp),dimension(Gdim),intent(inout) :: psi       !Wavefunction (compressed format)
      real(wp),dimension(Ldim),intent(in) :: lpsi         !Wavefunction in localization region

      !local variables
      integer :: igrid,isegloc,isegG,ix!,iorbs
      integer :: lmin,lmax,Gmin,Gmax
      integer :: icheck      ! check to make sure the dimension of loc_psi does not overflow
      integer :: offset      ! gives the difference between the starting point of Lseg and Gseg
      integer :: length      ! Length of the overlap between Lseg and Gseg
      integer :: lincrement  ! Increment for writing orbitals in loc_psi
      integer :: Gincrement  ! Increment for reading orbitals in psi
      integer :: nseg        ! total number of segments in Llr
      !integer, allocatable :: keymask(:,:)  ! shift for every segment of Llr (with respect to Glr)
      character(len=*), parameter :: subname='Lpsi_to_global'
      integer :: start,Gstart,Lindex
      integer :: lfinc,Gfinc,spinshift,ispin,Gindex,isegstart
      integer :: istart
      !integer :: i_stat

      call f_routine(id=subname)

      !!! This routine is only intended for conversions between locregs with the same boundary conditions.
      !!if (glr%geocode/= 'F' .or. llr%geocode/='F') then
      !!    call f_err_throw('Lpsi_to_global2 can only be used for locregs with free boundary conditions', &
      !!         err_name='BIGDFT_RUNTIME_ERROR')
      !!end if

      if(nspin/=1) stop 'not fully implemented for nspin/=1!'

    ! Define integers
      nseg = Llr%wfd%nseg_c + Llr%wfd%nseg_f
      lincrement = array_dim(Llr)
      Gincrement = array_dim(Glr)
      icheck = 0
      spinshift = Gdim / nspin

    ! Get the keymask: shift for every segment of Llr (with respect to Glr)
    ! allocate(keymask(2,nseg),stat=i_stat)
      !keymask = f_malloc((/2,nseg/),id='keymask')

      !call shift_locreg_indexes(Glr,Llr,keymask,nseg)
      !call shift_locreg_indexes_global(Glr,Llr,keymask,nseg)
      !!keymask = llr%wfd%keyglob

    !####################################################
    ! Do coarse region
    !####################################################
      isegstart=1


      !$omp parallel default(private) &
      !$omp shared(Glr,Llr, lpsi,icheck,psi,norb) &
      !$omp firstprivate(isegstart,nseg,lincrement,Gincrement,spinshift,nspin)

      !$omp do reduction(+:icheck)
      local_loop_c: do isegloc = 1,Llr%wfd%nseg_c
         lmin = llr%wfd%keyglob(1,isegloc)
         lmax = llr%wfd%keyglob(2,isegloc)
         istart = llr%wfd%keyvglob(isegloc)-1


         global_loop_c: do isegG = isegstart,Glr%wfd%nseg_c
            Gmin = Glr%wfd%keyglob(1,isegG)
            Gmax = Glr%wfd%keyglob(2,isegG)

            ! For each segment in Llr check if there is a collision with the segment in Glr
            !if not, cycle
            if(lmin > Gmax) then
                isegstart=isegG
            end if
            if(Gmin > lmax) exit global_loop_c

            !if((lmin > Gmax) .or. (lmax < Gmin))  cycle global_loop_c
            if(lmin > Gmax)  cycle global_loop_c

            ! Define the offset between the two segments
            offset = lmin - Gmin
            if(offset < 0) then
               offset = 0
            end if

            ! Define the length of the two segments
            length = min(lmax,Gmax)-max(lmin,Gmin)

            !Find the common elements and write them to the new global wavefunction
            icheck = icheck + (length + 1)

            ! WARNING: index goes from 0 to length because it is the offset of the element

            do ix = 0,length
               istart = istart + 1
               do ispin=1,nspin
                  Gindex = Glr%wfd%keyvglob(isegG)+offset+ix+spinshift*(ispin-1)
                  Lindex = istart+lincrement*norb*(ispin-1)
                  psi(Gindex) = lpsi(Lindex)
               end do
            end do
         end do global_loop_c
      end do local_loop_c
      !$omp end do


    !##############################################################
    ! Now do fine region
    !##############################################################

      start = Llr%wfd%nvctr_c
      Gstart = Glr%wfd%nvctr_c
      lfinc  = Llr%wfd%nvctr_f
      Gfinc = Glr%wfd%nvctr_f

      isegstart=Glr%wfd%nseg_c+1

      !$omp do reduction(+:icheck)
      local_loop_f: do isegloc = Llr%wfd%nseg_c+1,nseg
         lmin = llr%wfd%keyglob(1,isegloc)
         lmax = llr%wfd%keyglob(2,isegloc)
         istart = llr%wfd%keyvglob(isegloc)-1

         global_loop_f: do isegG = isegstart,Glr%wfd%nseg_c+Glr%wfd%nseg_f

            Gmin = Glr%wfd%keyglob(1,isegG)
            Gmax = Glr%wfd%keyglob(2,isegG)

            ! For each segment in Llr check if there is a collision with the segment in Glr
            ! if not, cycle
            if(lmin > Gmax) then
                isegstart=isegG
            end if
            if(Gmin > lmax)  exit global_loop_f
            !if((lmin > Gmax) .or. (lmax < Gmin))  cycle global_loop_f
            if(lmin > Gmax)  cycle global_loop_f
            offset = lmin - Gmin
            if(offset < 0) offset = 0

            length = min(lmax,Gmax)-max(lmin,Gmin)

            !Find the common elements and write them to the new global wavefunction
            ! First set to zero those elements which are not copied. WARNING: will not work for npsin>1!!

            icheck = icheck + (length + 1)

            ! WARNING: index goes from 0 to length because it is the offset of the element
            do ix = 0,length
            istart = istart + 1
               do igrid=1,7
                  do ispin = 1, nspin
                     Gindex = Gstart + (Glr%wfd%keyvglob(isegG)+offset+ix-1)*7+igrid + spinshift*(ispin-1)
                     Lindex = start+(istart-1)*7+igrid + lincrement*norb*(ispin-1)
                     psi(Gindex) = lpsi(Lindex)
                  end do
               end do
            end do
         end do global_loop_f
      end do local_loop_f
      !$omp end do

      !$omp end parallel

      !Check if the number of elements in loc_psi is valid
      if(icheck .ne. Llr%wfd%nvctr_f+Llr%wfd%nvctr_c) then
        write(*,*)'There is an error in Lpsi_to_global2: sum of fine and coarse points used',icheck
        write(*,*)'is not equal to the sum of fine and coarse points in the region',Llr%wfd%nvctr_f+Llr%wfd%nvctr_c
        stop
      end if

      !!call f_free(keymask)

      call f_release_routine()

    END SUBROUTINE Lpsi_to_global2


    !> Projects a quantity stored with the global indexes (i1,i2,i3) within the localisation region.
    !! @warning
    !!    The quantity must not be stored in a compressed form.
    subroutine global_to_local_parallel(Glr,Llr,size_rho,size_Lrho,rho,Lrho,i1s,i1e,i2s,i2e,i3s,i3e,ni1,ni2, &
               i1shift, i2shift, i3shift, ise)
     implicit none

     ! Arguments
     type(locreg_descriptors),intent(in) :: Llr   !< Local localization region
     type(locreg_descriptors),intent(in) :: Glr   !< Global localization region
     integer, intent(in) :: size_rho  ! size of rho array
     integer, intent(in) :: size_Lrho ! size of Lrho array
     real(wp),dimension(size_rho),intent(in) :: rho  !< quantity in global region
     real(wp),dimension(size_Lrho),intent(out) :: Lrho !< piece of quantity in local region
     integer,intent(in) :: i1s, i1e, i2s, i2e
     integer,intent(in) :: i3s, i3e ! starting and ending indices on z direction (related to distribution of rho when parallel)
     integer,intent(in) :: ni1, ni2 ! x and y extent of rho
     integer,intent(in) :: i1shift, i2shift, i3shift
     integer,dimension(6) :: ise

    ! Local variable
     integer :: ispin,ii1,ii2,ii3  !integer for loops
     !integer :: i1,i2,i3
     integer :: indSmall, indSpin, indLarge ! indexes for the arrays
     integer :: ist2S,ist3S, ist2L, ist3L, istsa, ists, istl
     integer :: ii1shift, ii2shift, ii3shift, i1glob, i2glob, i3glob
     integer :: iii1, iii2, iii3

     call f_routine(id='global_to_local_parallel')

     !THIS ROUTINE NEEDS OPTIMIZING

     !!write(*,'(a,8i8)') 'in global_to_local_parallel: i1s, i1e, i2s, i2e, i3s, i3e, ni1, ni2', i1s, i1e, i2s, i2e, i3s, i3e, ni1, ni2

     ! Cut out a piece of the quantity (rho) from the global region (rho) and
     ! store it in a local region (Lrho).
     indSmall=0
     indSpin=0
     ! Deactivate the spin for the moment
     do ispin=1,1!nspin
         !$omp parallel default(none) &
         !$omp shared(Glr, Llr, Lrho, rho, indSpin, i1s, i1e, i2s, i2e, i3s, i3e) &
         !$omp shared(i1shift, i2shift, i3shift, ni1, ni2, ise) &
         !$omp private(ii1, ii2, ii3, i1glob, i2glob, i3glob, ii1shift, ii2shift, ii3shift) &
         !$omp private(ist3S, ist3L, istsa, ist2S, ist2L, ists, istl, indSmall, indLarge) &
         !$omp private(iii1, iii2, iii3)
         !$omp do
         do ii3=i3s,i3e
             i3glob = ii3+ise(5)-1
             !i3=modulo(i3glob-1,glr%d%n3i)+1
             if (modulo(ii3-1,glr%mesh%ndims(3))+1>modulo(i3e-1,glr%mesh%ndims(3))+1) then
                 !This is a line before the wrap around, i.e. one needs a shift since
                 ii3shift = i3shift
             else
                 ii3shift = 0
             end if
             if (i3glob<=glr%mesh%ndims(3)) then
                 iii3=ii3+i3shift
             else
                 iii3=modulo(i3glob-1,glr%mesh%ndims(3))+1
             end if
             ist3S = (ii3-i3s)*Llr%mesh%ndims(2)*Llr%mesh%ndims(1)
             ist3L = (iii3-1)*ni2*ni1
             istsa=ist3S-i1s+1
             do ii2=i2s,i2e
                 i2glob = ii2+ise(3)-1
                 !i2=modulo(i2glob-1,glr%d%n2i)+1
                 if (modulo(ii2-1,glr%mesh%ndims(2))+1>modulo(i2e-1,glr%mesh%ndims(2))+1) then
                     !This is a line before the wrap around, i.e. one needs a shift since
                     !the potential in the global region starts with the wrapped around part
                     ii2shift = i2shift
                 else
                     ii2shift = 0
                 end if
                 if (i2glob<=glr%mesh%ndims(2)) then
                     iii2=ii2+i2shift
                 else
                     iii2=modulo(i2glob-1,glr%mesh%ndims(2))+1
                 end if
                 ist2S = (ii2-i2s)*Llr%mesh%ndims(1)
                 ist2L = (iii2-1)*ni1
                 ists=istsa+ist2S
                 istl=ist3L+ist2L
                 do ii1=i1s,i1e
                     i1glob = ii1+ise(1)-1
                     !i1=modulo(i1glob-1,glr%d%n1i)+1
                     if (modulo(ii1-1,glr%mesh%ndims(1))+1>modulo(i1e-1,glr%mesh%ndims(1))+1) then
                         !This is a line before the wrap around, i.e. one needs a shift since
                         !the potential in the global region starts with the wrapped around part
                         ii1shift = i1shift
                     else
                         ii1shift = 0
                     end if
                     if (i1glob<=glr%mesh%ndims(1)) then
                         iii1=ii1+i1shift
                     else
                         iii1=modulo(i1glob-1,glr%mesh%ndims(1))+1
                     end if
                     ! indSmall is the index in the local localization region
                     indSmall=ists+ii1
                     ! indLarge is the index in the global localization region.
                     indLarge= iii1+istl
                     Lrho(indSmall)=rho(indLarge+indSpin)
                     !write(600+bigdft_mpi%iproc,'(a,14i7,2es16.8)') 'i1glob, i2glob, i3glob, i1, i2, i3, iii1, iii2, iii3, i1shift, i2shift, i3shift, indsmall, indlarge, val, testval', &
                     !    i1glob, i2glob, i3glob, i1, i2, i3, iii1, iii2, iii3, i1shift, i2shift, i3shift, indsmall, indlarge, Lrho(indSmall), real((i1+(i2-1)*glr%d%n1i+(i3-1)*glr%d%n1i*glr%d%n2i),kind=8)
                     !if (abs(Lrho(indSmall)-real((i1+(i2-1)*glr%d%n1i+(i3-1)*glr%d%n1i*glr%d%n2i),kind=8))>1.d-3) then
                     !    write(700+bigdft_mpi%iproc,'(a,11i7,2es16.8)') 'i1glob, i2glob, i3glob, i1, i2, i3, iii1, iii2, iii3, indsmall, indlarge, val, testval', &
                     !        i1glob, i2glob, i3glob, i1, i2, i3, iii1, iii2, iii3, indsmall, indlarge, Lrho(indSmall), real((i1+(i2-1)*glr%d%n1i+(i3-1)*glr%d%n1i*glr%d%n2i),kind=8)
                     !end if
                 end do
             end do
         end do
         !$omp end do
         !$omp end parallel
         indSpin=indSpin+int(Glr%mesh%ndim)
     end do

     call f_release_routine()

    END SUBROUTINE global_to_local_parallel

    function boundary_weight(hgrids,glr,lr,rad,psi) result(weight_normalized)
      use at_domain, only: domain_periodic_dims
      use compression
      implicit none
      real(gp), intent(in) :: rad
      real(gp), dimension(3) :: hgrids
      type(locreg_descriptors), intent(in) :: glr,lr
      real(wp), dimension(array_dim(lr)), intent(in) :: psi
      real(gp) :: weight_normalized
      !local variables
      integer :: iseg, jj, j0, j1, ii, i3, i2, i0, i1, i, ind
      integer :: ij3, ij2, ij1, jj3, jj2, jj1, ijs3, ijs2, ijs1, ije3, ije2, ije1
      !integer :: iorb, iiorb, ilr
      real(kind=8) :: h, x, y, z, d, weight_inside, weight_boundary, points_inside, points_boundary, ratio
      real(kind=8) :: boundary
      logical :: perx, pery, perz, on_boundary
      logical, dimension(3) :: peri


      ! mean value of the grid spacing
      h = sqrt(hgrids(1)**2+hgrids(2)**2+hgrids(3)**2)

      ! periodicity in the three directions
!!$      perx=(glr%geocode /= 'F')
!!$      pery=(glr%geocode == 'P')
!!$      perz=(glr%geocode /= 'F')
      peri=domain_periodic_dims(glr%mesh%dom)
      perx=peri(1)
      pery=peri(2)
      perz=peri(3)

      ! For perdiodic boundary conditions, one has to check also in the neighboring
      ! cells (see in the loop below)
      if (perx) then
         ijs1 = -1
         ije1 = 1
      else
         ijs1 = 0
         ije1 = 0
      end if
      if (pery) then
         ijs2 = -1
         ije2 = 1
      else
         ijs2 = 0
         ije2 = 0
      end if
      if (perz) then
         ijs3 = -1
         ije3 = 1
      else
         ijs3 = 0
         ije3 = 0
      end if


      boundary = min(rad,lr%locrad)

      weight_boundary = 0.d0
      weight_inside = 0.d0
      points_inside = 0.d0
      points_boundary = 0.d0
      ind = 0
      do iseg=1,lr%wfd%nseg_c
         jj=lr%wfd%keyvglob(iseg)
         j0=lr%wfd%keyglob(1,iseg)
         j1=lr%wfd%keyglob(2,iseg)
         ii=j0-1
         i3=ii/glr%mesh_coarse%ndims(1)/glr%mesh_coarse%ndims(2)
         ii=ii-i3*glr%mesh_coarse%ndims(1)*glr%mesh_coarse%ndims(2)
         i2=ii/glr%mesh_coarse%ndims(1)
         i0=ii-i2*glr%mesh_coarse%ndims(1)
         i1=i0+j1-j0
         do i=i0,i1
            ind = ind + 1
            on_boundary = .false.
            do ij3=ijs3,ije3!-1,1
               jj3=i3+ij3*glr%mesh_coarse%ndims(3)
               z = real(jj3,kind=8)*hgrids(3)
               do ij2=ijs2,ije2!-1,1
                  jj2=i2+ij2*glr%mesh_coarse%ndims(2)
                  y = real(jj2,kind=8)*hgrids(2)
                  do ij1=ijs1,ije1!-1,1
                     jj1=i+ij1*glr%mesh_coarse%ndims(1)
                     x = real(i,kind=8)*hgrids(1)
                     d = sqrt((x-lr%locregcenter(1))**2 + &
                          (y-lr%locregcenter(2))**2 + &
                          (z-lr%locregcenter(3))**2)
                     if (abs(d-boundary)<h) then
                        on_boundary=.true.
                     end if
                  end do
               end do
            end do
            if (on_boundary) then
               ! This value is on the boundary
               !write(*,'(a,2f9.2,3i8,3es16.8)') 'on boundary: boundary, d, i1, i2, i3, x, y, z', &
               !    boundary, d, i, i2, i3, x, y, z
               weight_boundary = weight_boundary + psi(ind)**2
               points_boundary = points_boundary + 1.d0
            else
               weight_inside = weight_inside + psi(ind)**2
               points_inside = points_inside + 1.d0
            end if
         end do
      end do
      ! fine part, to be done only if nseg_f is nonzero
      do iseg=lr%wfd%nseg_c+1,lr%wfd%nseg_c+lr%wfd%nseg_f
         jj=lr%wfd%keyvglob(iseg)
         j0=lr%wfd%keyglob(1,iseg)
         j1=lr%wfd%keyglob(2,iseg)
         ii=j0-1
         i3=ii/glr%mesh_coarse%ndims(1)/glr%mesh_coarse%ndims(2)
         ii=ii-i3*glr%mesh_coarse%ndims(1)*glr%mesh_coarse%ndims(2)
         i2=ii/glr%mesh_coarse%ndims(1)
         i0=ii-i2*glr%mesh_coarse%ndims(1)
         i1=i0+j1-j0
         do i=i0,i1
            ind = ind + 7
            on_boundary = .false.
            do ij3=ijs3,ije3!-1,1
               jj3=i3+ij3*glr%mesh_coarse%ndims(3)
               z = real(jj3,kind=8)*hgrids(3)
               do ij2=ijs2,ije2!-1,1
                  jj2=i2+ij2*glr%mesh_coarse%ndims(2)
                  y = real(jj2,kind=8)*hgrids(2)
                  do ij1=ijs1,ije1!-1,1
                     jj1=i+ij1*glr%mesh_coarse%ndims(1)
                     x = real(i,kind=8)*hgrids(1)
                     d = sqrt((x-lr%locregcenter(1))**2 + &
                          (y-lr%locregcenter(2))**2 + &
                          (z-lr%locregcenter(3))**2)
                     if (abs(d-boundary)<h) then
                        on_boundary=.true.
                     end if
                  end do
               end do
            end do
            if (on_boundary) then
               ! This value is on the boundary
               !write(*,'(a,f9.2,3i8,3es16.8)') 'on boundary: d, i1, i2, i3, x, y, z', d, i, i2, i3, x, y, z
               weight_boundary = weight_boundary + psi(ind-6)**2
               weight_boundary = weight_boundary + psi(ind-5)**2
               weight_boundary = weight_boundary + psi(ind-4)**2
               weight_boundary = weight_boundary + psi(ind-3)**2
               weight_boundary = weight_boundary + psi(ind-2)**2
               weight_boundary = weight_boundary + psi(ind-1)**2
               weight_boundary = weight_boundary + psi(ind-0)**2
               points_boundary = points_boundary + 7.d0
            else
               weight_inside = weight_inside + psi(ind-6)**2
               weight_inside = weight_inside + psi(ind-5)**2
               weight_inside = weight_inside + psi(ind-4)**2
               weight_inside = weight_inside + psi(ind-3)**2
               weight_inside = weight_inside + psi(ind-2)**2
               weight_inside = weight_inside + psi(ind-1)**2
               weight_inside = weight_inside + psi(ind-0)**2
               points_inside = points_inside + 7.d0
            end if
         end do
      end do
      ! Ratio of the points on the boundary with resepct to the total number of points
      ratio = points_boundary/(points_boundary+points_inside)
      weight_normalized = weight_boundary/ratio
      !write(*,'(a,2f9.1,4es16.6)') 'iiorb, pi, pb, weight_inside, weight_boundary, ratio, xi', &
      !    points_inside, points_boundary, weight_inside, weight_boundary, &
      !    points_boundary/(points_boundary+points_inside), &
      !    weight_boundary/ratio

    end function boundary_weight

    !> Tranform one wavefunction between Global region and localisation region
    subroutine psi_to_locreg2(iproc, ldim, gdim, Llr, Glr, gpsi, lpsi)

     implicit none

      ! Subroutine Scalar Arguments
      integer,intent(in) :: iproc                  ! process ID
      integer,intent(in) :: ldim          ! dimension of lpsi
      integer,intent(in) :: gdim          ! dimension of gpsi
      type(locreg_descriptors),intent(in) :: Llr  ! Local grid descriptor
      type(locreg_descriptors),intent(in) :: Glr  ! Global grid descriptor

      !Subroutine Array Arguments
      real(wp),dimension(gdim),intent(in) :: gpsi       !Wavefunction (compressed format)
      real(wp),dimension(ldim),intent(out) :: lpsi   !Wavefunction in localization region

      !local variables
      integer :: igrid,isegloc,isegG,ix!,iorbs
      integer :: lmin,lmax,Gmin,Gmax
      integer :: icheck      ! check to make sure the dimension of loc_psi does not overflow
      integer :: offset      ! gives the difference between the starting point of Lseg and Gseg
      integer :: length      ! Length of the overlap between Lseg and Gseg
      integer :: lincrement  ! Increment for writing orbitals in loc_psi
      integer :: Gincrement  ! Increment for reading orbitals in psi
      integer :: nseg        ! total number of segments in Llr
      integer, allocatable :: keymask(:,:)  ! shift for every segment of Llr (with respect to Glr)
      character(len=*), parameter :: subname='psi_to_locreg'
    !  integer :: i_stat,i_all
      integer :: start,Gstart
      integer :: isegstart,istart

      call f_routine(id=subname)

    ! Define integers
      nseg = Llr%wfd%nseg_c + Llr%wfd%nseg_f
      lincrement = array_dim(Llr)
      Gincrement = array_dim(Glr)
      icheck = 0

    ! Initialize loc_psi
      call f_zero(lpsi)

    ! Get the keymask: shift for every segment of Llr (with respect to Glr)
    ! allocate(keymask(2,nseg),stat=i_stat)
      keymask = f_malloc((/ 2, nseg /),id='keymask')

      call shift_locreg_indexes(Glr,Llr,keymask,nseg)


    !####################################################
    ! Do coarse region
    !####################################################
      isegstart=1
      icheck = 0


    !$omp parallel default(private) &
    !$omp shared(icheck,lpsi,gpsi,Glr,Llr,keymask,lincrement,Gincrement,Gstart) &
    !$omp firstprivate(isegstart,nseg)

      !$omp do reduction(+:icheck)
      local_loop_c: do isegloc = 1,Llr%wfd%nseg_c
         lmin = keymask(1,isegloc)
         lmax = keymask(2,isegloc)
         istart = llr%wfd%keyvloc(isegloc)-1

         global_loop_c: do isegG = isegstart,Glr%wfd%nseg_c
            Gmin = Glr%wfd%keygloc(1,isegG)
            Gmax = Glr%wfd%keygloc(2,isegG)

            ! For each segment in Llr check if there is a collision with the segment in Glr
            ! if not, cycle
            if(lmin > Gmax) then
                isegstart=isegG
            end if
            if(Gmin > lmax) exit global_loop_c
            if((lmin > Gmax) .or. (lmax < Gmin)) cycle global_loop_c

            ! Define the offset between the two segments
            offset = lmin - Gmin
            if(offset < 0) then
               offset = 0
            end if

            ! Define the length of the two segments
            length = min(lmax,Gmax)-max(lmin,Gmin)

            icheck = icheck + (length + 1)

            !Find the common elements and write them to the new localized wavefunction
            ! WARNING: index goes from 0 to length because it is the offset of the element

            do ix = 0,length
               istart = istart + 1
               lpsi(istart) = gpsi(Glr%wfd%keyvloc(isegG)+offset+ix)
            end do
         end do global_loop_c
      end do local_loop_c
      !$omp end do

    ! Check if the number of elements in loc_psi is valid
     ! if(icheck .ne. Llr%wfd%nvctr_c) then
       ! write(*,*)'There is an error in psi_to_locreg2: number of coarse points used',icheck
       ! write(*,*)'is not equal to the number of coarse points in the region',Llr%wfd%nvctr_c
     ! end if

    !##############################################################
    ! Now do fine region
    !##############################################################

      !icheck = 0
      start = Llr%wfd%nvctr_c
      Gstart = Glr%wfd%nvctr_c

      isegstart=Glr%wfd%nseg_c+1

      !$omp do reduction(+:icheck)
      local_loop_f: do isegloc = Llr%wfd%nseg_c+1,nseg
         lmin = keymask(1,isegloc)
         lmax = keymask(2,isegloc)
         istart = llr%wfd%keyvloc(isegloc)-1

         global_loop_f: do isegG = isegstart,Glr%wfd%nseg_c+Glr%wfd%nseg_f

            Gmin = Glr%wfd%keygloc(1,isegG)
            Gmax = Glr%wfd%keygloc(2,isegG)

            ! For each segment in Llr check if there is a collision with the segment in Glr
            ! if not, cycle
            if(lmin > Gmax) then
                isegstart=isegG
            end if
            if(Gmin > lmax)  exit global_loop_f
            if((lmin > Gmax) .or. (lmax < Gmin))  cycle global_loop_f

            offset = lmin - Gmin
            if(offset < 0) offset = 0

            length = min(lmax,Gmax)-max(lmin,Gmin)

            icheck = icheck + (length + 1)

            !Find the common elements and write them to the new localized wavefunction
            ! WARNING: index goes from 0 to length because it is the offset of the element
            do ix = 0,length
               istart = istart+1
               do igrid=1,7
                  lpsi(start+(istart-1)*7+igrid) = gpsi(Gstart+(Glr%wfd%keyvloc(isegG)+offset+ix-1)*7+igrid)
               end do
            end do
         end do global_loop_f
      end do local_loop_f
      !$omp end do

      !$omp end parallel

     !! Check if the number of elements in loc_psi is valid
      if(icheck .ne. Llr%wfd%nvctr_f+Llr%wfd%nvctr_c) then
        write(*,'(a,i0,a,i0)')'process ',iproc,': There is an error in psi_to_locreg: number of fine points used ',icheck
        write(*,'(a,i0)')'is not equal to the number of fine points in the region ',Llr%wfd%nvctr_f+Llr%wfd%nvctr_c
      end if



    !  i_all=-product(shape(keymask))*kind(keymask)
    ! deallocate(keymask,stat=i_stat)
      call f_free(keymask)
      call f_release_routine()

    END SUBROUTINE psi_to_locreg2


    !> Find the shift necessary for the indexes of every segment of Blr
    !!   to make them compatible with the indexes of Alr. These shifts are
    !!   returned in the array keymask(nseg), where nseg should be the number
    !!   of segments in Blr.
    !! @warning
    !!   This routine supposes that the region Blr is contained in the region Alr.
    !!   This should always be the case, if we concentrate on the overlap between two regions.
    subroutine shift_locreg_indexes(Alr,Blr,keymask,nseg)
     use at_domain, only: domain_geocode
     use dictionaries, only: f_err_throw
     implicit none

    ! Arguments
     type(locreg_descriptors),intent(in) :: Alr,Blr   ! The two localization regions
     integer,intent(in) :: nseg
     integer,intent(out) :: keymask(2,nseg)

    ! Local variable
     integer :: iseg      !integer for the loop
     integer :: Bindex    !starting index of segments in Blr
     integer :: x,y,z     !coordinates of start of segments in Blr
     integer :: shift(3)  !shift between the beginning of the segment in Blr and the origin of Alr
     integer ::  tmp


     ! This routine is only intended for conversions between locregs with the same boundary conditions.
!!$     if (blr%geocode/='F') then
     if (domain_geocode(blr%mesh%dom) /= 'F') then
         call f_err_throw('shift_locreg_indexes can only be used for locregs with free boundary conditions', &
              err_name='BIGDFT_RUNTIME_ERROR')
     end if

    !Big loop on all segments
    !$omp parallel do default(private) shared(Blr,nseg,Alr,keymask)
     do iseg=1,nseg

    !##########################################
    ! For the Starting index
        Bindex = Blr%wfd%keygloc(1,iseg)
        tmp = Bindex -1
        z   = tmp / Blr%mesh_coarse%ndims(2) / Blr%mesh_coarse%ndims(1)
        tmp = tmp - z*Blr%mesh_coarse%ndims(2)*Blr%mesh_coarse%ndims(1)
        y   = tmp / Blr%mesh_coarse%ndims(1)
        x   = tmp - y * Blr%mesh_coarse%ndims(1)

    ! Shift between the beginning of the segment and the start of the Alr region
        shift(1) = x + Blr%nboxc(1,1) - Alr%nboxc(1,1)
        shift(2) = y + Blr%nboxc(1,2) - Alr%nboxc(1,2)
        shift(3) = z + Blr%nboxc(1,3) - Alr%nboxc(1,3)

    ! Write the shift in index form
        keymask(1,iseg) = shift(3)*Alr%mesh_coarse%ndims(1)*Alr%mesh_coarse%ndims(2) + &
             shift(2)*Alr%mesh_coarse%ndims(1) + shift(1) + 1

    !######################################
    ! For the ending index

        Bindex = Blr%wfd%keygloc(2,iseg)
        tmp = Bindex -1
        z   = tmp / Blr%mesh_coarse%ndims(2) / Blr%mesh_coarse%ndims(1)
        tmp = tmp - z*Blr%mesh_coarse%ndims(2)*Blr%mesh_coarse%ndims(1)
        y   = tmp / Blr%mesh_coarse%ndims(1)
        x   = tmp - y * Blr%mesh_coarse%ndims(1)

    ! Shift between the beginning of the segment and the start of the Alr region
        shift(1) = x + Blr%nboxc(1,1) - Alr%nboxc(1,1)
        shift(2) = y + Blr%nboxc(1,2) - Alr%nboxc(1,2)
        shift(3) = z + Blr%nboxc(1,3) - Alr%nboxc(1,3)

    ! Write the shift in index form
        keymask(2,iseg) = shift(3)*Alr%mesh_coarse%ndims(1)*Alr%mesh_coarse%ndims(2) + &
             shift(2)*Alr%mesh_coarse%ndims(1) + shift(1) + 1
     end do
    !$omp end parallel do

    END SUBROUTINE shift_locreg_indexes

    !> Projects a quantity stored with the global indexes (i1,i2,i3) within the localisation region.
    !! @warning: The quantity must not be stored in a compressed form.
    subroutine global_to_local(Glr,Llr,nspin,size_rho,size_Lrho,rho,Lrho)
     use at_domain, only: domain_geocode

     implicit none

    ! Arguments
     type(locreg_descriptors),intent(in) :: Llr   ! Local localization region
     type(locreg_descriptors),intent(in) :: Glr   ! Global localization region
     integer, intent(in) :: size_rho  ! size of rho array
     integer, intent(in) :: size_Lrho ! size of Lrho array
     integer, intent(in) :: nspin  !number of spins
     real(wp),dimension(size_rho),intent(in) :: rho  ! quantity in global region
     real(wp),dimension(size_Lrho),intent(out) :: Lrho ! piece of quantity in local region

    ! Local variable
     integer :: ispin,i1,i2,i3,ii1,ii2,ii3  !integer for loops
     integer :: indSmall, indSpin, indLarge ! indexes for the arrays
     logical:: z_inside, y_inside, x_inside
     integer:: iz, iy, m

    ! Cut out a piece of the quantity (rho) from the global region (rho) and
    ! store it in a local region (Lrho).

!!$     if(Glr%geocode == 'F') then
     if(domain_geocode(Glr%mesh%dom) == 'F') then
         ! Use loop unrolling here
         indSmall=0
         indSpin=0
         do ispin=1,nspin
             ! WARNING: I added the factors 2.
             do i3=Llr%nboxi(1,3)+1,Llr%nboxi(2,3)
                 iz=(i3-1)*Glr%mesh%ndims(2)*Glr%mesh%ndims(1)
                 do i2=Llr%nboxi(1,2)+1,Llr%nboxi(2,2)
                     iy=(i2-1)*Glr%mesh%ndims(1)
                     m=mod(Llr%nboxi(2,1)-Llr%nboxi(1,1),4)
                     if(m/=0) then
                         do i1=Llr%nboxi(1,1)+1,Llr%nboxi(1,1)+m
                            indSmall=indSmall+1
                            indLarge=iz+iy+i1
                            Lrho(indSmall)=rho(indLarge+indSpin)
                         end do
                      end if
                      do i1=Llr%nboxi(1,1)+1+m,Llr%nboxi(2,1),4
                         Lrho(indSmall+1)=rho(iz+iy+i1+0+indSpin)
                         Lrho(indSmall+2)=rho(iz+iy+i1+1+indSpin)
                         Lrho(indSmall+3)=rho(iz+iy+i1+2+indSpin)
                         Lrho(indSmall+4)=rho(iz+iy+i1+3+indSpin)
                         indSmall=indSmall+4
                      end do
                 end do
             end do
             indSpin=indSpin+int(Glr%mesh%ndim)
          end do
     else
         ! General case
         indSmall=0
         indSpin=0
         do ispin=1,nspin
             ! WARNING: I added the factors 2.
             do ii3=Llr%nboxi(1,3)+1,Llr%nboxi(2,3)
                 i3 = mod(ii3-1,Glr%mesh%ndims(3))+1
                 z_inside = (i3>0 .and. i3<=Glr%mesh%ndims(3)+1)
                 iz=(i3-1)*Glr%mesh%ndims(2)*Glr%mesh%ndims(1)
                 do ii2=Llr%nboxi(1,2)+1,Llr%nboxi(2,2)
                     i2 = mod(ii2-1,Glr%mesh%ndims(2))+1
                     y_inside = (i2>0 .and. i2<=Glr%mesh%ndims(2)+1)
                     iy=(i2-1)*Glr%mesh%ndims(1)
                     do ii1=Llr%nboxi(1,1)+1,Llr%nboxi(2,1)
                         i1 = mod(ii1-1,Glr%mesh%ndims(1))+1
                         x_inside = (i1 > 0 .and. i1 <= Glr%mesh%ndims(1)+1)
                         ! indSmall is the index in the local localization region
                         indSmall=indSmall+1
                         !!if (i3 > 0 .and. i2 > 0 .and. i1 > 0 .and.&                                       !This initializes the buffers of locreg to zeros if outside the simulation box.
                         !!    i3 <= Glr%d%n3i+1 .and. i2 <= Glr%d%n2i+1 .and. i1 <= Glr%d%n1i+1) then       !Should use periodic image instead... MUST FIX THIS.
                         !!   ! indLarge is the index in the global localization region.
                         !!   indLarge=(i3-1)*Glr%d%n2i*Glr%d%n1i + (i2-1)*Glr%d%n1i + i1
                         if(z_inside .and. y_inside .and. x_inside) then
                            indLarge=iz+iy+i1
                            Lrho(indSmall)=rho(indLarge+indSpin)
                         else
                            Lrho(indSmall)= 0.0_wp
                         end if
                     end do
                 end do
             end do
             indSpin=indSpin+int(Glr%mesh%ndim)
         end do
     end if

    END SUBROUTINE global_to_local

    subroutine daub_to_isf_locham(nspinor,lr,w,psi,psir)
      use liborbs_precisions
      use compression
      use locregs
      use liborbs_workarrays
      use at_domain, only: domain_geocode
      use dynamic_memory
      use f_utils, only: f_zero
      implicit none
      integer, intent(in) :: nspinor
      type(locreg_descriptors), intent(in) :: lr
      type(workarr_locham), intent(inout) :: w
      real(wp), dimension(array_dim(lr),nspinor), intent(in) :: psi
      real(wp), dimension(lr%mesh%ndim,nspinor), intent(out) :: psir
      !local variables
      integer :: idx,i_f,iseg_f
      real(wp), dimension(0:7), parameter :: scal = 1._wp

      !starting point for the fine degrees, to avoid boundary problems
      i_f=min(1,lr%wfd%nvctr_f)
      iseg_f=min(1,lr%wfd%nseg_f)

      !call f_zero((2*n1+31)*(2*n2+31)*(2*n3+31)*nspinor,psir)
      !call MPI_COMM_RANK(bigdft_mpi%mpi_comm,iproc,ierr)
!!$  select case(lr%geocode)
      select case(domain_geocode(lr%mesh%dom))
      case('F')
         call f_zero(psir)
         !call timing(iproc,'CrtDescriptors','ON') !temporary
         do idx=1,nspinor
            call decompress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, &
                 w%x_c(1,idx), w%x_f(1,idx), psi(1,idx), scal, &
                 w%x_f1(1,idx), w%x_f2(1,idx), w%x_f3(1,idx))
            !call timing(iproc,'CrtDescriptors','OF') !temporary
            !call timing(iproc,'CrtLocPot     ','ON') !temporary
            call comb_grow_all(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                 lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),&
                 w%w1,w%w2,w%x_c(1,idx),w%x_f(1,idx), & 
                 psir(1,idx),lr%bounds%kb%ibyz_c,lr%bounds%gb%ibzxx_c,&
                 lr%bounds%gb%ibxxyy_c,lr%bounds%gb%ibyz_ff,&
                 lr%bounds%gb%ibzxx_f,lr%bounds%gb%ibxxyy_f)
            !call timing(iproc,'CrtLocPot     ','OF') !temporary
         end do

      case('S')

         do idx=1,nspinor
            call uncompress_slab_scal(lr%wfd, lr%nboxc, lr%mesh_fine, w%x_c(1,idx), psi(1,idx), scal, psir(1,idx))

            call convolut_magic_n_slab(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1, &
                 w%x_c(1,idx),psir(1,idx),w%y_c(1,idx)) 

         end do

      case('P')

         if (lr%hybrid_on) then

            call f_zero(psir)
            do idx=1,nspinor
               call decompress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, w%x_c(1,idx), w%x_f(1,idx), &
                    psi(1,idx), scal, w%x_f1(1,idx), w%x_f2(1,idx), w%x_f3(1,idx))

               call comb_grow_all_hybrid(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),&
                    w%nw1,w%nw2,&
                    w%w1,w%w2,w%x_c(1,idx),w%x_f(1,idx),psir(1,idx),lr%bounds%gb)
            end do

         else

            do idx=1,nspinor
               call uncompress_per_scal(lr%wfd, lr%nboxc, lr%mesh_fine, w%x_c(1,idx), psi(1,idx), scal, psir(1,idx))

               call convolut_magic_n_per(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1, &
                    w%x_c(1,idx),psir(1,idx),w%y_c(1,idx)) 
            end do

         end if

      case('W')

         do idx=1,nspinor
            call uncompress_wire_scal(lr%wfd, lr%nboxc, lr%mesh_fine, w%x_c(1,idx), psi(1,idx), scal, psir(1,idx))

            call convolut_magic_n_wire(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1, &
                 w%x_c(1,idx),psir(1,idx),w%y_c(1,idx))

         end do

      end select

    END SUBROUTINE daub_to_isf_locham

    subroutine isf_to_daub_kinetic(lr,kx,ky,kz,nspinor,w,psir,hpsi,ekin,k_strten)
      use liborbs_workarrays
      use at_domain, only: domain_geocode
      use liborbs_errors
      implicit none
      integer, intent(in) :: nspinor
      real(gp), intent(in) :: kx,ky,kz
      type(locreg_descriptors), intent(in) :: lr
      type(workarr_locham), intent(inout) :: w
      real(wp), dimension(lr%mesh%ndim,nspinor), intent(in) :: psir
      real(gp), intent(out) :: ekin
      real(wp), dimension(array_dim(lr),nspinor), intent(inout) :: hpsi
      real(wp), dimension(6), optional :: k_strten
      !Local variables
      logical :: usekpts
      integer :: idx,i,i_f,iseg_f,ipsif,isegf
      real(gp) :: ekino
      real(wp), dimension(0:3) :: scal
      real(wp), dimension(6) :: kstrten,kstrteno


      !control whether the k points are to be used
      !real k-point different from Gamma still not implemented
      usekpts = kx**2+ky**2+kz**2 > 0.0_gp .or. nspinor == 2

      do i=0,3
         scal(i)=1.0_wp
      enddo

      !starting point for the fine degrees, to avoid boundary problems
      i_f=min(1,lr%wfd%nvctr_f)
      iseg_f=min(1,lr%wfd%nseg_f)
      ipsif=lr%wfd%nvctr_c+i_f
      isegf=lr%wfd%nseg_c+iseg_f

      !call MPI_COMM_RANK(bigdft_mpi%mpi_comm,iproc,ierr)
      ekin=0.0_gp

      kstrten=0.0_wp
!!$      select case(lr%geocode)
      select case(domain_geocode(lr%mesh%dom))
      case('F')

         !here kpoints cannot be used (for the moment, to be activated for the
         !localisation region scheme
         if (f_err_raise(usekpts, 'K points not allowed for Free BC locham', &
              err_id = LIBORBS_OPERATION_ERROR())) return

         do idx=1,nspinor

            call comb_shrink(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                 lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),&
                 w%w1,w%w2,psir(1,idx),&
                 lr%bounds%kb%ibxy_c,lr%bounds%sb%ibzzx_c,lr%bounds%sb%ibyyzz_c,&
                 lr%bounds%sb%ibxy_ff,lr%bounds%sb%ibzzx_f,lr%bounds%sb%ibyyzz_f,&
                 w%y_c(1,idx),w%y_f(1,idx))

            call ConvolkineticT(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                 lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),  &
                 lr%mesh_coarse%hgrids(1),lr%mesh_coarse%hgrids(2),lr%mesh_coarse%hgrids(3),&
                 lr%bounds%kb%ibyz_c,lr%bounds%kb%ibxz_c,lr%bounds%kb%ibxy_c,&
                 lr%bounds%kb%ibyz_f,lr%bounds%kb%ibxz_f,lr%bounds%kb%ibxy_f, &
                 w%x_c(1,idx),w%x_f(1,idx),&
                 w%y_c(1,idx),w%y_f(1,idx),ekino, &
                 w%x_f1(1,idx),w%x_f2(1,idx),w%x_f3(1,idx),111)
            ekin=ekin+ekino

            call compress_and_accumulate_standard(lr%wfd, lr%nboxc, lr%nboxf, &
                 hpsi(1,idx), w%y_c(1,idx), w%y_f(1,idx))
         end do

      case('S')

         if (usekpts) then
            !first calculate the proper arrays then transpose them before passing to the
            !proper routine
            do idx=1,nspinor
               call convolut_magic_t_slab_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    psir(1,idx),w%y_c(1,idx))
            end do

            !Transposition of the work arrays (use psir as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%x_c,psir,.true.)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%y_c,psir,.true.)

            ! compute the kinetic part and add  it to psi_out
            ! the kinetic energy is calculated at the same time
            ! do this thing for both components of the spinors
            do idx=1,nspinor,2
               call convolut_kinetic_slab_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino,kx,ky,kz)
               ekin=ekin+ekino
            end do

            !re-Transposition of the work arrays (use psir as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%y_c,psir,.false.)

            do idx=1,nspinor
               !new compression routine in mixed form
               call analyse_slab_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),psir(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), psir(1,idx))
            end do

         else
            do idx=1,nspinor
               call convolut_magic_t_slab_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    psir(1,idx),w%y_c(1,idx))

               ! compute the kinetic part and add  it to psi_out
               ! the kinetic energy is calculated at the same time
               call convolut_kinetic_slab_T(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino)
               ekin=ekin+ekino

               !new compression routine in mixed form
               call analyse_slab_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),psir(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), psir(1,idx))
            end do
         end if

      case('P')

         if (lr%hybrid_on) then

            !here kpoints cannot be used, such BC are used in general to mimic the Free BC
            if (f_err_raise(usekpts, 'K points not allowed for hybrid BC locham', &
                 err_id = LIBORBS_OPERATION_ERROR())) return

            !here the grid spacing is not halved
            do idx=1,nspinor
               call comb_shrink_hyb(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),&
                    w%w2,w%w1,psir(1,idx),w%y_c(1,idx),w%y_f(1,idx),lr%bounds%sb)

               call convolut_kinetic_hyb_T(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
                    lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),  &
                    lr%mesh_coarse%hgrids,w%x_c(1,idx),w%x_f(1,idx),w%y_c(1,idx),w%y_f(1,idx),kstrteno,&
                    w%x_f1(1,idx),w%x_f2(1,idx),w%x_f3(1,idx),lr%bounds%kb%ibyz_f,&
                    lr%bounds%kb%ibxz_f,lr%bounds%kb%ibxy_f)
               kstrten=kstrten+kstrteno
               !ekin=ekin+ekino

               call compress_and_accumulate_standard(lr%wfd, lr%nboxc, lr%nboxf, &
                    hpsi(1,idx), w%y_c(1,idx), w%y_f(1,idx))
            end do
         else

            if (usekpts) then
               !first calculate the proper arrays then transpose them before passing to the
               !proper routine
               do idx=1,nspinor
                  call convolut_magic_t_per_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                       psir(1,idx),w%y_c(1,idx))
               end do

               !Transposition of the work arrays (use psir as workspace)
               call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                    w%x_c,psir,.true.)
               call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                    w%y_c,psir,.true.)


               ! compute the kinetic part and add  it to psi_out
               ! the kinetic energy is calculated at the same time
               do idx=1,nspinor,2
                  !print *,'AAA',2*lr%d%n1+1,2*lr%d%n2+1,2*lr%d%n3+1,hgridh

                  call convolut_kinetic_per_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                       lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),kstrteno,kx,ky,kz)
                  kstrten=kstrten+kstrteno
                  !ekin=ekin+ekino
               end do

               !Transposition of the work arrays (use psir as workspace)
               call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                    w%y_c,psir,.false.)

               do idx=1,nspinor

                  call analyse_per_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                       w%y_c(1,idx),psir(1,idx))
                  call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                       hpsi(1,idx), psir(1,idx))
               end do
            else
               !first calculate the proper arrays then transpose them before passing to the
               !proper routine
               do idx=1,nspinor
                  call convolut_magic_t_per_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                       psir(1,idx),w%y_c(1,idx))
                  ! compute the kinetic part and add  it to psi_out
                  ! the kinetic energy is calculated at the same time
                  call convolut_kinetic_per_t(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                       lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),kstrteno)
                  kstrten=kstrten+kstrteno

                  call analyse_per_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                       w%y_c(1,idx),psir(1,idx))
                  call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                       hpsi(1,idx), psir(1,idx))
               end do
            end if

         end if
         ekin=ekin+kstrten(1)+kstrten(2)+kstrten(3)
         if (present(k_strten)) k_strten=kstrten

      case('W')

         if (usekpts) then
            !first calculate the proper arrays then transpose them before passing to the
            !proper routine
            do idx=1,nspinor
               call convolut_magic_t_wire_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    psir(1,idx),w%y_c(1,idx))
            end do

            !Transposition of the work arrays (use psir as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%x_c,psir,.true.)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%y_c,psir,.true.)

            ! compute the kinetic part and add  it to psi_out
            ! the kinetic energy is calculated at the same time
            ! do this thing for both components of the spinors
            do idx=1,nspinor,2
               call convolut_kinetic_wire_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino,kx,ky,kz)
               ekin=ekin+ekino
            end do

            !re-Transposition of the work arrays (use psir as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%y_c,psir,.false.)

            do idx=1,nspinor
               !new compression routine in mixed form
               call analyse_wire_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),psir(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), psir(1,idx))
            end do

         else
            do idx=1,nspinor
               call convolut_magic_t_wire_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    psir(1,idx),w%y_c(1,idx))

               ! compute the kinetic part and add  it to psi_out
               ! the kinetic energy is calculated at the same time
               call convolut_kinetic_wire_T(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino)
               ekin=ekin+ekino

               !new compression routine in mixed form
               call analyse_wire_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),psir(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), psir(1,idx))
            end do
         end if

      end select

    END SUBROUTINE isf_to_daub_kinetic
    
    subroutine psi_to_tpsi(kptv,nspinor,lr,psi,w,hpsi,ekin,k_strten)
      use liborbs_precisions
      use locregs
      use liborbs_workarrays
      use at_domain, only: domain_geocode
      use wrapper_linalg
      use f_utils, only: f_zero
      implicit none
      integer, intent(in) :: nspinor
      real(gp), dimension(3), intent(in) :: kptv
      type(locreg_descriptors), intent(in) :: lr
      type(workarr_locham), intent(inout) :: w
      real(wp), dimension(array_dim(lr),nspinor), intent(in) :: psi
      real(gp), intent(out) :: ekin
      real(wp), dimension(array_dim(lr),nspinor), intent(inout) :: hpsi
      real(wp), dimension(6), optional :: k_strten
      !Local variables
      logical, parameter :: transpose=.false.
      logical :: usekpts
      integer :: idx
      real(gp) :: ekino
      real(wp), dimension(0:7), parameter :: scal = (/ 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp, 1._wp /)
      real(wp), dimension(6) :: kstrten,kstrteno


      !control whether the k points are to be used
      !real k-point different from Gamma still not implemented
      usekpts = nrm2(3,kptv(1),1) > 0.0_gp .or. nspinor == 2

      !call MPI_COMM_RANK(bigdft_mpi%mpi_comm,iproc,ierr)
      ekin=0.0_gp

      kstrten=0.0_wp
!!$  select case(lr%geocode)
      select case(domain_geocode(lr%mesh%dom))
      case('F')

         !here kpoints cannot be used (for the moment, to be activated for the 
         !localisation region scheme
         if (usekpts) stop 'K points not allowed for Free BC locham'

         do idx=1,nspinor
            call decompress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, &
                 w%x_c(1,idx), w%x_f(1,idx), psi(1,idx), scal, &
                 w%x_f1(1,idx), w%x_f2(1,idx), w%x_f3(1,idx))

            call f_zero(w%y_c(1,idx),w%nyc)
            call f_zero(w%y_f(1,idx),w%nyf)

            call ConvolkineticT(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                 lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),  &
                 lr%mesh_coarse%hgrids(1),lr%mesh_coarse%hgrids(2),lr%mesh_coarse%hgrids(3), &
                 lr%bounds%kb%ibyz_c,lr%bounds%kb%ibxz_c,lr%bounds%kb%ibxy_c,&
                 lr%bounds%kb%ibyz_f,lr%bounds%kb%ibxz_f,lr%bounds%kb%ibxy_f, &
                 w%x_c(1,idx),w%x_f(1,idx),&
                 w%y_c(1,idx),w%y_f(1,idx),ekino, &
                 w%x_f1(1,idx),w%x_f2(1,idx),w%x_f3(1,idx),111)
            ekin=ekin+ekino

            !new compression routine in standard form
            call compress_and_accumulate_standard(lr%wfd, lr%nboxc, lr%nboxf, &
                 hpsi(1,idx), w%y_c(1,idx), w%y_f(1,idx))

         end do

      case('S')

         if (usekpts) then
            !first calculate the proper arrays then transpose them before passing to the
            !proper routine
            do idx=1,nspinor
               call uncompress_slab_scal(lr%wfd, lr%nboxc, lr%mesh_fine, &
                    w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))
            end do

            !Transposition of the work arrays (use y_c as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%x_c,w%y_c,.true.)
            call f_zero(w%y_c)

            ! compute the kinetic part and add  it to psi_out
            ! the kinetic energy is calculated at the same time
            ! do this thing for both components of the spinors
            do idx=1,nspinor,2
               call convolut_kinetic_slab_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino,kptv(1),kptv(2),kptv(3))
               ekin=ekin+ekino        
            end do

            !re-Transposition of the work arrays (use x_c as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%y_c,w%x_c,.false.)

            do idx=1,nspinor
               !new compression routine in mixed form
               call analyse_slab_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),w%x_c(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), w%x_c(1,idx))

            end do

         else
            do idx=1,nspinor
               call uncompress_slab_scal(lr%wfd, lr%nboxc, lr%mesh_fine, &
                    w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))

               call f_zero(w%y_c(1,idx),w%nyc)
               ! compute the kinetic part and add  it to psi_out
               ! the kinetic energy is calculated at the same time
               call convolut_kinetic_slab_T(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino)
               ekin=ekin+ekino

               !new compression routine in mixed form
               call analyse_slab_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),w%x_c(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), w%x_c(1,idx))
            end do
         end if

      case('P')

         if (lr%hybrid_on) then

            !here kpoints cannot be used, such BC are used in general to mimic the Free BC
            if (usekpts) stop 'K points not allowed for hybrid BC locham'

            !here the grid spacing is not halved
            do idx=1,nspinor
               call decompress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, w%x_c(1,idx), w%x_f(1,idx), &
                    psi(1,idx), scal, w%x_f1(1,idx), w%x_f2(1,idx), w%x_f3(1,idx))

               call f_zero(w%y_c(1,idx),w%nyc)
               call f_zero(w%y_f(1,idx),w%nyf)

               call convolut_kinetic_hyb_T(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
                    lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),  &
                    lr%mesh_coarse%hgrids,w%x_c(1,idx),w%x_f(1,idx),w%y_c(1,idx),w%y_f(1,idx),kstrteno,&
                    w%x_f1(1,idx),w%x_f2(1,idx),w%x_f3(1,idx),lr%bounds%kb%ibyz_f,&
                    lr%bounds%kb%ibxz_f,lr%bounds%kb%ibxy_f)
               kstrten=kstrten+kstrteno
               !ekin=ekin+ekino

               call compress_and_accumulate_standard(lr%wfd, lr%nboxc, lr%nboxf, &
                    hpsi(1,idx), w%y_c(1,idx), w%y_f(1,idx))

            end do
         else

            if (usekpts) then

               do idx=1,nspinor
                  call uncompress_per_scal(lr%wfd, lr%nboxc, lr%mesh_fine, w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))
               end do

               if (transpose) then
                  !Transposition of the work arrays (use psir as workspace)
                  call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                       w%x_c,w%y_c,.true.)

                  call f_zero(w%y_c)
                  ! compute the kinetic part and add  it to psi_out
                  ! the kinetic energy is calculated at the same time
                  do idx=1,nspinor,2
                     !print *,'AAA',2*lr%d%n1+1,2*lr%d%n2+1,2*lr%d%n3+1,hgridh

                     call convolut_kinetic_per_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                          lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),kstrteno,kptv(1),kptv(2),kptv(3))
                     kstrten=kstrten+kstrteno
                     !ekin=ekin+ekino
                  end do

                  !Transposition of the work arrays (use psir as workspace)
                  call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                       w%y_c,w%x_c,.false.)

               else
                  call f_zero(w%y_c)
                  do idx=1,nspinor,2
                     call convolut_kinetic_per_T_k_notranspose( &
                          lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                          lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),kstrteno,kptv(1),kptv(2),kptv(3))
                     kstrten=kstrten+kstrteno
                  end do
               end if

               do idx=1,nspinor

                  call analyse_per_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                       w%y_c(1,idx),w%x_c(1,idx))
                  call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                       hpsi(1,idx), w%x_c(1,idx))

               end do
            else
               !first calculate the proper arrays then transpose them before passing to the
               !proper routine
               do idx=1,nspinor
                  call uncompress_per_scal(lr%wfd, lr%nboxc, lr%mesh_fine, w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))

                  call f_zero(w%y_c(1,idx),w%nyc)
                  ! compute the kinetic part and add  it to psi_out
                  ! the kinetic energy is calculated at the same time
                  call convolut_kinetic_per_t(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                       lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),kstrteno)
                  kstrten=kstrten+kstrteno

                  call analyse_per_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                       w%y_c(1,idx),w%x_c(1,idx))
                  call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                       hpsi(1,idx), w%x_c(1,idx))

               end do
            end if

         end if
         ekin=ekin+kstrten(1)+kstrten(2)+kstrten(3)
         if (present(k_strten)) k_strten=kstrten 

      case('W')


         if (usekpts) then
            !first calculate the proper arrays then transpose them before passing to the
            !proper routine
            do idx=1,nspinor
               call uncompress_wire_scal(lr%wfd, lr%nboxc, lr%mesh_fine, w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))
            end do

            !Transposition of the work arrays (use y_c as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%x_c,w%y_c,.true.)
            call f_zero(w%y_c(1,1),nspinor*w%nyc)

            ! compute the kinetic part and add  it to psi_out
            ! the kinetic energy is calculated at the same time
            ! do this thing for both components of the spinors
            do idx=1,nspinor,2
               call convolut_kinetic_wire_T_k(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino,kptv(1),kptv(2),kptv(3))
               ekin=ekin+ekino        
            end do

            !re-Transposition of the work arrays (use x_c as workspace)
            call transpose_for_kpoints(nspinor,lr%mesh%ndims(1),lr%mesh%ndims(2),lr%mesh%ndims(3),&
                 w%y_c,w%x_c,.false.)

            do idx=1,nspinor
               !new compression routine in mixed form
               call analyse_wire_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),w%x_c(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), w%x_c(1,idx))

            end do

         else
            do idx=1,nspinor
               call uncompress_wire_scal(lr%wfd, lr%nboxc, lr%mesh_fine, w%x_c(1,idx), psi(1,idx), scal, w%y_c(1,idx))

               call f_zero(w%y_c(1,idx),w%nyc)
               ! compute the kinetic part and add  it to psi_out
               ! the kinetic energy is calculated at the same time
               call convolut_kinetic_wire_T(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
                    lr%mesh_fine%hgrids,w%x_c(1,idx),w%y_c(1,idx),ekino)
               ekin=ekin+ekino

               !new compression routine in mixed form
               call analyse_wire_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
                    w%y_c(1,idx),w%x_c(1,idx))
               call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, &
                    hpsi(1,idx), w%x_c(1,idx))
            end do
         end if
      end select

    END SUBROUTINE psi_to_tpsi

    subroutine daub_to_isf_ocl(lr, queue, psi_r, psi, w)
      use f_precisions, only: f_address
      use at_domain, only: domain_periodic_dims
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer(f_address), intent(in) :: queue
      type(workarrays_ocl_sumrho), intent(inout) :: w
      real(wp), dimension(array_dim(lr)), intent(in) :: psi
      real(wp), dimension(lr%mesh%ndim), intent(out) :: psi_r

      integer, dimension(3) :: periodic
      logical, dimension(3) :: peri

      peri=domain_periodic_dims(lr%mesh%dom)
      periodic=0
      where (peri) periodic=1

      call set_ocl_sumrho_sync(w, queue, lr, psi)
      call f_daub_to_isf(queue, lr%mesh_coarse%ndims, periodic,&
           lr%wfd%nseg_c, lr%wfd%nvctr_c, lr%ocl_wfd%keyg_c, lr%ocl_wfd%keyv_c,&
           lr%wfd%nseg_f, lr%wfd%nvctr_f, lr%ocl_wfd%keyg_f, lr%ocl_wfd%keyv_f,&
           w%psi_c, w%psi_f, w%work1, w%work2, w%work3, w%d)
      call ocl_enqueue_read_buffer(queue, w%work2, lr%mesh%ndim*8, psi_r(1))
    end subroutine daub_to_isf_ocl

    subroutine isf_to_daub_ocl(lr, queue, psi, psi_r, w)
      use f_precisions, only: f_address
      use at_domain, only: domain_periodic_dims
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer(f_address), intent(in) :: queue
      type(workarrays_ocl_sumrho), intent(inout) :: w
      real(wp), dimension(array_dim(lr)), intent(out) :: psi
      real(wp), dimension(lr%mesh%ndim), intent(in) :: psi_r

      integer, dimension(3) :: periodic
      logical, dimension(3) :: peri

      peri=domain_periodic_dims(lr%mesh%dom)
      periodic=0
      where (peri) periodic=1

      call ocl_enqueue_write_buffer(queue, w%work1, lr%mesh%ndim*8, psi_r(1))
      call f_isf_to_daub(queue, lr%mesh_coarse%ndims, periodic, &
           lr%wfd%nseg_c, lr%wfd%nvctr_c, lr%ocl_wfd%keyg_c, lr%ocl_wfd%keyv_c, &
           lr%wfd%nseg_f, lr%wfd%nvctr_f, lr%ocl_wfd%keyg_f, lr%ocl_wfd%keyv_f, &
           w%psi_c, w%psi_f, w%work1, w%work2, w%work3, w%d)
      call get_ocl_sumrho_sync(w, queue, lr, psi)
    end subroutine isf_to_daub_ocl

    subroutine full_locham_ocl_async(lr, queue, kxyz, psi, hpsi, nspinor, &
         rhopot_addr, epot, ekin, w, p_addr, h_addr, h_offset)
      use at_domain, only: domain_periodic_dims
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: nspinor, h_offset
      integer(f_address), intent(in) :: queue
      type(workarrays_ocl_sumrho), dimension(2), intent(inout) :: w
      real(wp), dimension(array_dim(lr), nspinor), intent(out) :: psi
      real(wp), dimension(array_dim(lr), nspinor), intent(out) :: hpsi
      real(gp), dimension(3) :: kxyz
      integer(f_address), intent(in) :: rhopot_addr, h_addr
      integer(f_address), dimension(2, nspinor), intent(in) :: p_addr
      real(gp), dimension(2), intent(out) :: epot, ekin

      integer, dimension(3) :: periodic
      logical, dimension(3) :: peri

      peri=domain_periodic_dims(lr%mesh%dom)
      periodic=0
      where (peri) periodic=1

      call set_ocl_sumrho_async(w(1), queue, lr, psi(1, 1), p_addr(1, 1))
      if (nspinor == 2) call set_ocl_sumrho_async(w(2), queue, lr, psi(1, 2), p_addr(1, 2))
      !calculate the local hamiltonian
      !WARNING: the difference between full_locham and normal locham is inside
      call f_fulllocham_generic_k(queue, lr%mesh_coarse%ndims, periodic, &
           lr%mesh%hgrids, kxyz, &
           lr%wfd%nseg_c, lr%wfd%nvctr_c, lr%ocl_wfd%keyg_c, lr%ocl_wfd%keyv_c, &
           lr%wfd%nseg_f, lr%wfd%nvctr_f, lr%ocl_wfd%keyg_f, lr%ocl_wfd%keyv_f, &
           w(1)%psi_c, w(1)%psi_f, w(2)%psi_c, w(2)%psi_f, rhopot_addr, &
           w(1)%work1, w(1)%work2, w(1)%work3, &
           w(2)%work1, w(2)%work2, w(2)%work3, &
           w(1)%d, w(2)%d, nspinor, epot, ekin)

      call get_ocl_sumrho_async(w(1), queue, lr, hpsi(1, 1), h_addr, h_offset)
      if (nspinor == 2) then
         call get_ocl_sumrho_async(w(2), queue, lr, hpsi(1, 2), h_addr, h_offset+1)
      end if
    end subroutine full_locham_ocl_async
    
    subroutine density_ocl(lr, queue, scal, psi, rhopot_addr, w, p_addr, p_offset)
      use at_domain, only: domain_periodic_dims
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: p_offset
      integer(f_address), intent(in) :: queue
      type(workarrays_ocl_sumrho), intent(inout) :: w
      real(gp), intent(in) :: scal
      real(wp), dimension(array_dim(lr)), intent(in) :: psi
      integer(f_address), intent(in) :: rhopot_addr, p_addr

      integer, dimension(3) :: periodic
      logical, dimension(3) :: peri

      peri=domain_periodic_dims(lr%mesh%dom)
      periodic=0
      where (peri) periodic=1

      if (p_addr > 0_f_address) call ocl_map_write_buffer_async(queue, p_addr, &
           p_offset, lr%wfd%nvctr_c*8)
      call ocl_enqueue_write_buffer(queue, w%psi_c, lr%wfd%nvctr_c*8, psi(1))
      if (p_addr > 0_f_address) call ocl_unmap_mem_object(queue, p_addr, psi(1))

      if (lr%wfd%nvctr_f > 0) then
         if (p_addr > 0_f_address) call ocl_map_write_buffer_async(queue, p_addr, &
              p_offset + lr%wfd%nvctr_c*8, 7*lr%wfd%nvctr_f*8)
         call ocl_enqueue_write_buffer(queue, w%psi_f, 7*lr%wfd%nvctr_f*8, &
              psi(lr%wfd%nvctr_c + 1))
         if (p_addr > 0_f_address) call ocl_unmap_mem_object(queue, p_addr, &
              psi(lr%wfd%nvctr_c + 1))
      end if

      !calculate the density
      call f_locden_generic(queue, lr%mesh_coarse%ndims, periodic, scal, &
           lr%wfd%nseg_c, lr%wfd%nvctr_c, lr%ocl_wfd%keyg_c, lr%ocl_wfd%keyv_c, &
           lr%wfd%nseg_f, lr%wfd%nvctr_f, lr%ocl_wfd%keyg_f, lr%ocl_wfd%keyv_f, &
           w%psi_c, w%psi_f, w%work1, w%work2, w%work3, rhopot_addr)
    end subroutine density_ocl

    subroutine create_ocl_sumrho(GPU, context, lr)
      implicit none
      type(workarrays_ocl_sumrho), intent(out) :: GPU
      integer(f_address), intent(in) :: context
      type(locreg_descriptors), intent(in) :: lr

      !allocate the compressed wavefunctions such as to be used as workspace
      call ocl_create_read_write_buffer(context, lr%wfd%nvctr_c*8,GPU%psi_c)
      call ocl_create_read_write_buffer(context, 7*lr%wfd%nvctr_f*8,GPU%psi_f)
      call ocl_create_read_write_buffer(context, int(lr%mesh%ndim)*8,GPU%work1)
      call ocl_create_read_write_buffer(context, int(lr%mesh%ndim)*8,GPU%work2)
      call ocl_create_read_write_buffer(context, int(lr%mesh%ndim)*8,GPU%work3)
      call ocl_create_read_write_buffer(context, int(lr%mesh%ndim)*8,GPU%d)
    end subroutine create_ocl_sumrho

    subroutine set_ocl_sumrho_sync(GPU, queue, lr, psi, pin) 
      implicit none
      type(workarrays_ocl_sumrho), intent(inout) :: GPU
      integer(f_address), intent(in) :: queue
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(array_dim(lr)), intent(in) :: psi
      integer(f_address), dimension(2), intent(in), optional :: pin

      integer(f_address) :: pin_c, pin_f

      pin_c = 0
      if (present(pin)) pin_c = pin(1)
      if (pin_c > 0) &
           call ocl_pin_read_buffer_async(queue, lr%wfd%nvctr_c*8, psi(1), pin_c)
      call ocl_enqueue_write_buffer(queue, GPU%psi_c, lr%wfd%nvctr_c*8, psi(1))
      if (pin_c > 0) call ocl_release_mem_object(pin_c)

      if (lr%wfd%nvctr_f > 0) then
         pin_f = 0
         if (present(pin)) pin_f = pin(2)
         if (pin_f > 0) &
              call ocl_pin_read_buffer_async(queue, 7*lr%wfd%nvctr_f*8, psi(lr%wfd%nvctr_c + 1), pin_f)
         call ocl_enqueue_write_buffer(queue, GPU%psi_f, 7*lr%wfd%nvctr_f*8, psi(lr%wfd%nvctr_c+1))
         if (pin_f > 0) call ocl_release_mem_object(pin_f)
      end if
    end subroutine set_ocl_sumrho_sync

    subroutine set_ocl_sumrho_async(GPU, queue, lr, psi, pin)
      implicit none
      type(workarrays_ocl_sumrho), intent(inout) :: GPU
      integer(f_address), intent(in) :: queue
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(array_dim(lr)), intent(in) :: psi
      integer(f_address), dimension(2), intent(in), optional :: pin
      integer(f_address) :: pin_c, pin_f

      pin_c = 0
      if (present(pin)) pin_c = pin(1)
      if (pin_c > 0) &
           call ocl_pin_read_buffer_async(queue, lr%wfd%nvctr_c*8, psi(1), pin_c)
      call ocl_enqueue_write_buffer_async(queue, GPU%psi_c, lr%wfd%nvctr_c*8, psi(1))
      if (pin_c > 0) call ocl_release_mem_object(pin_c)

      if (lr%wfd%nvctr_f > 0) then
         pin_f = 0
         if (present(pin)) pin_f = pin(2)
         if (pin_f > 0) &
              call ocl_pin_read_buffer_async(queue, 7*lr%wfd%nvctr_f*8, psi(lr%wfd%nvctr_c + 1), pin_f)
         call ocl_enqueue_write_buffer_async(queue, GPU%psi_f, 7*lr%wfd%nvctr_f*8, psi(lr%wfd%nvctr_c + 1))
         if (pin_f > 0) call ocl_release_mem_object(pin_f)
      end if
    end subroutine set_ocl_sumrho_async

    subroutine get_ocl_sumrho_sync(GPU, queue, lr, psi, pin, offset) 
      implicit none
      type(workarrays_ocl_sumrho), intent(inout) :: GPU
      integer(f_address), intent(in) :: queue
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(array_dim(lr)), intent(out) :: psi
      integer, intent(in), optional :: offset
      integer(f_address), intent(in), optional :: pin

      integer :: off
      integer(f_address) :: p

      off = 0
      if (present(offset)) off = offset
      p = 0
      if (present(pin)) p = pin

      if (p > 0) &
           call ocl_map_read_buffer_async(queue, p, array_dim(lr)*8*off, &
           lr%wfd%nvctr_c*8)
      call ocl_enqueue_read_buffer(queue, GPU%psi_c, lr%wfd%nvctr_c*8, psi(1))
      if (p > 0) call ocl_unmap_mem_object(queue, p, psi(1))

      if (lr%wfd%nvctr_f > 0) then
         if (p > 0) &
              call ocl_map_read_buffer_async(queue, p, array_dim(lr)*8*off + &
              lr%wfd%nvctr_c*8, 7*lr%wfd%nvctr_f*8)
         call ocl_enqueue_read_buffer(queue, GPU%psi_f, 7*lr%wfd%nvctr_f*8, &
              psi(lr%wfd%nvctr_c+1))
         if (p > 0) call ocl_unmap_mem_object(queue, p, psi(lr%wfd%nvctr_c + 1))
      end if
    end subroutine get_ocl_sumrho_sync

    subroutine get_ocl_sumrho_async(GPU, queue, lr, psi, pin, offset)
      implicit none
      type(workarrays_ocl_sumrho), intent(inout) :: GPU
      integer(f_address), intent(in) :: queue
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(array_dim(lr)), intent(out) :: psi
      integer, intent(in), optional :: offset
      integer(f_address), intent(in), optional :: pin

      integer :: off
      integer(f_address) :: p

      off = 0
      if (present(offset)) off = offset
      p = 0
      if (present(pin)) p = pin

      if (p > 0) &
           call ocl_map_read_buffer_async(queue, p, array_dim(lr)*8*off, &
           lr%wfd%nvctr_c*8)
      call ocl_enqueue_read_buffer_async(queue, GPU%psi_c, lr%wfd%nvctr_c*8, psi(1))
      if (p > 0) call ocl_unmap_mem_object(queue, p, psi(1))

      if (lr%wfd%nvctr_f > 0) then
         if (p > 0) &
              call ocl_map_read_buffer_async(queue, p, array_dim(lr)*8*off + &
              lr%wfd%nvctr_c*8, 7*lr%wfd%nvctr_f*8)
         call ocl_enqueue_read_buffer_async(queue, GPU%psi_f, 7*lr%wfd%nvctr_f*8, &
              psi(lr%wfd%nvctr_c + 1))
         if (p > 0) call ocl_unmap_mem_object(queue, p, psi(lr%wfd%nvctr_c + 1))
      end if
    end subroutine get_ocl_sumrho_async

    subroutine deallocate_ocl_sumrho(GPU)
      implicit none
      type(workarrays_ocl_sumrho), intent(inout) :: GPU

      call ocl_release_mem_object(GPU%psi_c)
      call ocl_release_mem_object(GPU%psi_f)
      call ocl_release_mem_object(GPU%d)
      call ocl_release_mem_object(GPU%work1)
      call ocl_release_mem_object(GPU%work2)
      call ocl_release_mem_object(GPU%work3)
    end subroutine deallocate_ocl_sumrho

    subroutine create_ocl_precond(GPU, context, lr)
      implicit none
      type(workarrays_ocl_precond), intent(out) :: GPU
      integer(f_address), intent(in) :: context
      type(locreg_descriptors), intent(in) :: lr

      !allocate the compressed wavefunctions such as to be used as workspace
      call ocl_create_read_write_buffer(context, lr%wfd%nvctr_c*8, GPU%psi_c_r)
      call ocl_create_read_write_buffer(context, 7*lr%wfd%nvctr_f*8, GPU%psi_f_r)
      call ocl_create_read_write_buffer(context, lr%wfd%nvctr_c*8, GPU%psi_c_b)
      call ocl_create_read_write_buffer(context, 7*lr%wfd%nvctr_f*8, GPU%psi_f_b)
      call ocl_create_read_write_buffer(context, lr%wfd%nvctr_c*8, GPU%psi_c_d)
      call ocl_create_read_write_buffer(context, 7*lr%wfd%nvctr_f*8, GPU%psi_f_d)
    end subroutine create_ocl_precond

    subroutine set_ocl_precond_b(GPU, queue, lr, b)
      implicit none
      type(workarrays_ocl_precond), intent(inout) :: GPU
      integer(f_address), intent(in) :: queue
      type(locreg_descriptors), intent(in) :: lr
      real(wp), dimension(array_dim(lr)), intent(in) :: b

      call ocl_enqueue_write_buffer(queue, GPU%psi_c_b, lr%wfd%nvctr_c*8, b)
      if (lr%wfd%nvctr_f > 0) &
           call ocl_enqueue_write_buffer(queue, GPU%psi_f_b, 7*lr%wfd%nvctr_f*8, &
           b(lr%wfd%nvctr_c + 1))
    end subroutine set_ocl_precond_b

    subroutine deallocate_ocl_precond(GPU)
      implicit none
      type(workarrays_ocl_precond), intent(inout) :: GPU

      call ocl_release_mem_object(GPU%psi_c_r)
      call ocl_release_mem_object(GPU%psi_f_r)
      call ocl_release_mem_object(GPU%psi_c_b)
      call ocl_release_mem_object(GPU%psi_f_b)
      call ocl_release_mem_object(GPU%psi_c_d)
      call ocl_release_mem_object(GPU%psi_f_d)
    end subroutine deallocate_ocl_precond

    subroutine precondition_residue_ocl(lr, queue, kxyz, hpsi, ncplx, &
         ncong, cprecr, ekin, w, b, w_sumrho, w_precond, &
         p_addr, h_addr, h_offset)
      use wrapper_linalg
      use liborbs_workarrays
      use at_domain, only: domain_periodic_dims
      implicit none
      type(locreg_descriptors), intent(in) :: lr
      integer, intent(in) :: ncplx, h_offset, ncong
      integer(f_address), intent(in) :: queue
      type(workarrays_ocl_sumrho), dimension(2), intent(inout) :: w_sumrho
      type(workarrays_ocl_precond), dimension(2), intent(inout) :: w_precond
      real(wp), dimension(array_dim(lr), ncplx), intent(inout) :: hpsi
      real(wp), dimension(array_dim(lr), ncplx), intent(inout) :: b
      real(gp), intent(in) :: cprecr
      real(gp), dimension(3), intent(in) :: kxyz
      real(gp), dimension(ncplx), intent(out) :: ekin
      type(workarr_precond), intent(inout) :: w
      integer(f_address), intent(in) :: h_addr
      integer(f_address), dimension(2, ncplx), intent(in) :: p_addr

      integer, dimension(3) :: periodic
      logical, dimension(3) :: peri
      real(gp), dimension(0:7) :: scal

      peri=domain_periodic_dims(lr%mesh%dom)
      periodic=0
      where (peri) periodic=1
      
      call precondition_preconditioner(lr, ncplx, &
           scal, cprecr, w, hpsi(1, 1), b(1,1))

      call set_ocl_sumrho_sync(w_sumrho(1), queue, lr, hpsi(1, 1), p_addr(1,1))
      call set_ocl_precond_b(w_precond(1), queue, lr, b(1,1))

      if(ncplx == 2) then
         call set_ocl_sumrho_sync(w_sumrho(2), queue, lr, hpsi(1, 2), p_addr(1,2))
         call set_ocl_precond_b(w_precond(2), queue, lr, b(1,2))
      endif
      call f_preconditioner_generic_k(queue, lr%mesh_coarse%ndims, periodic,&
           lr%mesh%hgrids, kxyz, cprecr, ncong, &
           lr%wfd%nseg_c, lr%wfd%nvctr_c, lr%ocl_wfd%keyg_c, lr%ocl_wfd%keyv_c, &
           lr%wfd%nseg_f, lr%wfd%nvctr_f, lr%ocl_wfd%keyg_f, lr%ocl_wfd%keyv_f, &
           w_sumrho(1)%psi_c,w_sumrho(1)%psi_f, &
           w_sumrho(2)%psi_c,w_sumrho(2)%psi_f, &
           w_precond(1)%psi_c_r,w_precond(1)%psi_f_r, &
           w_precond(2)%psi_c_r,w_precond(2)%psi_f_r, &
           w_precond(1)%psi_c_b,w_precond(1)%psi_f_b, &
           w_precond(2)%psi_c_b,w_precond(2)%psi_f_b, &
           w_precond(1)%psi_c_d,w_precond(1)%psi_f_d, &
           w_precond(2)%psi_c_d,w_precond(2)%psi_f_d, &
           w_sumrho(1)%d,w_sumrho(1)%work1,w_sumrho(1)%work2,w_sumrho(1)%work3, &
           w_sumrho(2)%d,w_sumrho(2)%work1,w_sumrho(2)%work2,w_sumrho(2)%work3, &
           ncplx, ekin) !buffer for scalars resulting from reductions

      call get_ocl_sumrho_sync(w_sumrho(1), queue, lr, hpsi(1, 1), &
           h_addr, h_offset)
      if ( ncplx == 2 ) then
         call get_ocl_sumrho_sync(w_sumrho(2), queue, lr, hpsi(1, 2), &
              h_addr, h_offset+1)
      endif
    end subroutine precondition_residue_ocl
end module locreg_operations
