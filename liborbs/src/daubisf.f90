!> @file
!!  Daubechies to Interpolation scaling functions routines
!! @author
!!    Copyright (C) 2010-2015 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS 

!> Transform a daubechies function in compressed form to a function in real space via
!! the Magic Filter operation
!! do this for a single component (spinorial and/or complex)
subroutine daub_to_isf(lr,w,psi,psir)
  use liborbs_precisions
  use compression
  use locregs
  use liborbs_workarrays
  use at_domain, only: domain_geocode
  use dynamic_memory
  use f_utils, only: f_zero
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  type(workarr_sumrho), intent(inout) :: w
  real(wp), dimension(array_dim(lr)), intent(in) :: psi
  real(wp), dimension(lr%mesh%ndim), intent(out) :: psir

  call f_routine(id='daub_to_isf')

  select case(domain_geocode(lr%mesh%dom))
  case('F')
     call wfd_decompress(lr%wfd, lr%nboxc, lr%nboxf, w%cf%c, w%cf%f, psi)

     call f_zero(psir)
     call comb_grow_all(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
          lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),&
          w%w1,w%w2,w%cf%c,w%cf%f,  & 
          psir,lr%bounds%kb%ibyz_c,&
          lr%bounds%gb%ibzxx_c,lr%bounds%gb%ibxxyy_c,&
          lr%bounds%gb%ibyz_ff,lr%bounds%gb%ibzxx_f,lr%bounds%gb%ibxxyy_f)

  case('P')
     if (lr%hybrid_on) then
        ! hybrid case
        call wfd_decompress(lr%wfd, lr%nboxc, lr%nboxf, w%cf%c, w%cf%f, psi)

        call f_zero(psir)
        call comb_grow_all_hybrid(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
             lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),&
             w%nw1,w%nw2,w%w1,w%w2,w%cf%c,w%cf%f,psir,lr%bounds%gb)
     else
        call wfd_decompress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), lr%mesh_coarse%ndims(3), &
             psir, psi)

        call synthese_per_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
             psir, w%cf%c)

        call convolut_magic_n_per_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
             w%cf%c,psir) 
     endif

  case('S')
     call wfd_decompress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), lr%mesh_coarse%ndims(3), &
          psir, psi)

     call synthese_slab_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
          psir, w%cf%c)
     
     call convolut_magic_n_slab_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,w%cf%c,&
          psir) 
  case('W')

     call wfd_decompress(lr%wfd, lr%mesh_coarse%ndims(1), lr%mesh_coarse%ndims(2), lr%mesh_coarse%ndims(3), &
          psir, psi)

     call synthese_wire_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
          psir, w%cf%c)

     call convolut_magic_n_wire_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,w%cf%c,&
          psir) 
  end select

  call f_release_routine()


END SUBROUTINE daub_to_isf

!> Transforms a wavefunction written in real space basis into a 
!! wavefunction in Daubechies form
!! does the job for all supported BC
!! @warning: 
!!  - the psir is destroyed for some BCs (slab and periodic)
!!  - psi must already be initialized (to zero) before entering this routine
subroutine isf_to_daub(lr,w,psir,psi)
  use liborbs_precisions
  use dynamic_memory
  use compression
  use locregs
  use liborbs_workarrays
  use at_domain, only: domain_geocode
  implicit none
  type(locreg_descriptors), intent(in) :: lr
  type(workarr_sumrho), intent(inout) :: w
  real(wp), dimension(lr%mesh%ndim), intent(inout) :: psir
  real(wp), dimension(array_dim(lr)), intent(inout) :: psi

  call f_routine(id='isf_to_daub')

  select case(domain_geocode(lr%mesh%dom))
  case('F')
     call comb_shrink(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
          lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),&
          w%w1,w%w2,psir,&
          lr%bounds%kb%ibxy_c,lr%bounds%sb%ibzzx_c,lr%bounds%sb%ibyyzz_c,&
          lr%bounds%sb%ibxy_ff,lr%bounds%sb%ibzzx_f,lr%bounds%sb%ibyyzz_f,&
          w%cf%c,w%cf%f)

     call compress_and_accumulate_standard(lr%wfd, lr%nboxc, lr%nboxf, &
          psi, w%cf%c, w%cf%f)

  case('S')

     call convolut_magic_t_slab_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
          psir(1),w%cf%c(1))

     call analyse_slab_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
          w%cf%c,psir(1))
     call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, psi, psir)

  case('P')

     if (lr%hybrid_on) then

        call comb_shrink_hyb(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
             lr%nboxf(1,1),lr%nboxf(2,1),lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3),&
             w%w2,w%w1,psir(1),w%cf%c(1),w%cf%f(1),lr%bounds%sb)
        call compress_and_accumulate_standard(lr%wfd, lr%nboxc, lr%nboxf, &
             psi, w%cf%c, w%cf%f)

     else

        call convolut_magic_t_per_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
             psir(1),w%cf%c(1))

        call analyse_per_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
             w%cf%c(1),psir(1))
        call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, psi, psir)

     end if

  case('W')

     call convolut_magic_t_wire_self(lr%mesh_fine%ndims(1)-1,lr%mesh_fine%ndims(2)-1,lr%mesh_fine%ndims(3)-1,&
          psir(1),w%cf%c(1))

     call analyse_wire_self(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1,&
          w%cf%c,psir(1))
     call compress_and_accumulate_mixed(lr%wfd, lr%nboxc, psi, psir)

  end select

  call f_release_routine()

END SUBROUTINE isf_to_daub
