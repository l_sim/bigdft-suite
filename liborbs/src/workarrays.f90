!> Module for handling workarrays
module liborbs_workarrays
  use liborbs_precisions
  use f_precisions, only: f_long
  implicit none

  private

  type, public :: workarr_compression
     integer :: nc=0, nf=0
     real(wp), dimension(:), pointer :: c => null(), f => null()
  end type workarr_compression

  !> Contains the work arrays needed for expressing wavefunction in real space
  !! with all the BC
  type, public :: workarr_sumrho
     type(workarr_compression) :: cf
     integer :: nw1=0,nw2=0
     real(wp), dimension(:), pointer :: w1 => null(), w2 => null()
  end type workarr_sumrho

  !> Contains the work arrays needed for hamiltonian application with all the BC
  type, public :: workarr_locham
     integer :: nw1=0,nw2=0,nxc=0,nyc=0,nxf1=0,nxf2=0,nxf3=0,nxf=0,nyf=0
     real(wp), dimension(:), pointer :: w1 => null(), w2 => null()
     !for the periodic BC case, these arrays substitute
     !psifscf,psifscfk,psig,ww respectively
     real(wp), dimension(:,:), pointer :: x_c => null(), y_c => null(), &
          x_f1 => null(), x_f2 => null(), x_f3 => null(), x_f => null(), y_f => null()
  end type workarr_locham

  !> Contains the work arrays needed for th preconditioner with all the BC
  !! Take different pointers depending on the boundary conditions
  type, public :: workarr_precond
     character(len = 1) :: geocode='X'
     integer(f_long) :: nc=int(0,f_long), nf=int(0,f_long), nfi=int(0,f_long)
     integer :: nc1=0, nc2=0, nc3=0, ncplx=0
     integer, dimension(:), pointer :: modul1,modul2,modul3
     real(wp), dimension(:,:), pointer :: af,bf,cf,ef
     real(wp), dimension(:), pointer :: psifscf,ww,kern_k1,kern_k2,kern_k3
     real(wp), dimension(:,:,:,:,:), pointer :: z1,z3 ! work array for FFT
     real(wp), dimension(:), pointer :: x_c,y_c
     real(wp), dimension(:), pointer :: x_f,y_f
     real(wp), dimension(:), pointer :: x_f1,x_f2,x_f3
  end type workarr_precond

  type, public :: workarr_quartic_convolutions
     integer(f_long) :: nc, nf
     integer :: ncmax
     real(wp), dimension(:), pointer :: xx_c, xy_c, xz_c
     real(wp), dimension(:), pointer :: xx_f1
     real(wp), dimension(:), pointer :: xy_f2
     real(wp), dimension(:), pointer :: xz_f4
     real(wp), dimension(:), pointer :: xx_f, xy_f, xz_f
     real(wp), dimension(:), pointer :: y_c
     real(wp), dimension(:), pointer :: y_f
     ! The following arrays are work arrays within the subroutine
     real(wp), dimension(:,:), pointer :: aeff0array, beff0array, ceff0array, eeff0array
     real(wp), dimension(:,:), pointer :: aeff0_2array, beff0_2array, ceff0_2array, eeff0_2array
     real(wp), dimension(:,:), pointer :: aeff0_2auxarray, beff0_2auxarray, ceff0_2auxarray, eeff0_2auxarray
     real(wp), dimension(:), pointer :: xya_c, xyc_c
     real(wp), dimension(:), pointer :: xza_c, xzc_c
     real(wp), dimension(:), pointer :: yza_c, yzb_c, yzc_c, yze_c
     real(wp), dimension(:), pointer :: xya_f, xyb_f, xyc_f, xye_f
     real(wp), dimension(:), pointer :: xza_f, xzb_f, xzc_f, xze_f
     real(wp), dimension(:), pointer :: yza_f, yzb_f, yzc_f, yze_f
  end type workarr_quartic_convolutions

  type, public :: workarr_tensor_prod
     integer, dimension(3) :: ndims
     integer :: ncplx, nterms
     real(wp),pointer,dimension(:,:) :: wprojx
     real(wp),pointer,dimension(:) :: wprojy, wprojz
     real(wp),pointer,dimension(:,:,:) :: work
  end type workarr_tensor_prod

  interface initialize_work_arrays_sumrho
     module procedure initialize_work_arrays_sumrho_nlr
     module procedure initialize_work_arrays_sumrho_llr
  end interface initialize_work_arrays_sumrho

  interface initialize_work_arrays_locham
     module procedure initialize_work_arrays_locham_nlr
     module procedure initialize_work_arrays_locham_llr
  end interface initialize_work_arrays_locham

  interface initialize_work_arrays_precond
     module procedure initialize_work_arrays_precond_nlr
     module procedure initialize_work_arrays_precond_llr
  end interface initialize_work_arrays_precond

  interface initialize_work_arrays_quartic
     module procedure initialize_work_arrays_quartic
     module procedure initialize_work_arrays_quartic_llr
  end interface initialize_work_arrays_quartic

  interface initialize_work_arrays_tensor_prod
     module procedure initialize_work_arrays_tensor_prod
     module procedure initialize_work_arrays_tensor_prod_llr
  end interface initialize_work_arrays_tensor_prod

  public :: nullify_work_arrays_sumrho
  public :: initialize_work_arrays_sumrho,deallocate_work_arrays_sumrho,reset_work_arrays_sumrho
  public :: initialize_work_arrays_locham,deallocate_work_arrays_locham,reset_work_arrays_locham
  public :: initialize_work_arrays_precond,deallocate_work_arrays_precond,reset_work_arrays_precond
  public :: initialize_work_arrays_quartic,deallocate_work_arrays_quartic,reset_work_arrays_quartic
  public :: initialize_work_arrays_tensor_prod, deallocate_work_arrays_tensor_prod, reset_work_arrays_tensor_prod
  public :: workarrays_tensor_prod_null
  public :: memspace_work_arrays_sumrho,memspace_work_arrays_locham,memspace_work_arrays_precond

contains

  subroutine cf_sizes(nw1, nw2, nc, nf, coarse, interp, fine, geocode, hyb)
    implicit none
    integer, intent(out) :: nw1, nw2, nc, nf
    integer, dimension(3), intent(in) :: coarse, interp, fine
    character(len = 1), intent(in) :: geocode
    logical, intent(in) :: hyb

    integer :: n1, n2, n3, n1i, n2i, n3i, n1f, n2f, n3f, nfi

    n1 = coarse(1)
    n2 = coarse(2)
    n3 = coarse(3)
    n1i = interp(1)
    n2i = interp(2)
    n3i = interp(3)
    n1f = fine(1)
    n2f = fine(2)
    n3f = fine(3)

    nw1 = 0
    nw2 = 0
    nc = 0
    nf = 0
    select case(geocode)
    case('F')
       !dimensions of work arrays
       ! shrink convention: nw1>nw2
       nw1=max(n3*n1i*n2i, n1*n2i*n3i,&
            2*(n1f+1)*(2*n2f+31)*(2*n3f+31),&
            2*(n3f+1)*(2*n1f+31)*(2*n2f+31))
       nw2=max(4*(n2f+1)*(n3f+1)*(2*n1f+31),&
            4*(n1f+1)*(n2f+1)*(2*n3f+31),&
            n1*n2*n3i, n1i*n2*n3)

       nfi=(n1f+1)*(n2f+1)*(n3f+1)
       nc = n1*n2*n3
       nf = 7 * nfi
    case('S')
       nc = n1i*n2i*n3i
    case('P')
       if (hyb) then
          ! Wavefunction expressed everywhere in fine scaling functions (for potential and kinetic energy)
          nfi=(n1f+1)*(n2f+1)*(n3f+1)

          nw1=max(4*(n2f+1)*(n3f+1)*n1i,&
               n1i*(n2+1)*(n3+1),&
               2*n3*n1*n2,&      ! for the comb_shrink_hyb_c
               4*n3i*(n1f+1)*(n2f+1)) ! for the _f

          nw2=max(2*(n3f+1)*n1i*n2i,&
               n3*n1i*n2i,&
               4*n2*n3*n1,&   ! for the comb_shrink_hyb_c
               2*n2i*n3i*(n1f+1)) ! for the _f

          nc = n1*n2*n3
          nf = 7*nfi
       else
          nc = n1i*n2i*n3i
       endif
    case('W')
       nc = n1i*n2i*n3i
    end select
  end subroutine cf_sizes

  subroutine initialize_work_arrays_compression(nc, nf, init_to_zero, w)
    use dynamic_memory
    implicit none
    integer, intent(in) :: nc, nf
    logical, intent(in) :: init_to_zero
    type(workarr_compression), intent(out) :: w

    w%nc = nc
    w%nf = nf
    if (nc > 0 .and. init_to_zero) then
       w%c = f_malloc0_ptr(w%nc, id='w%c')
    else if (nc > 0) then
       w%c = f_malloc_ptr(w%nc, id='w%c')
    end if
    if (nf > 0) then
       w%f = f_malloc0_ptr(w%nf, id='w%f')
    end if
  end subroutine initialize_work_arrays_compression
  
  subroutine deallocate_work_arrays_compression(w)
    use dynamic_memory
    implicit none
    type(workarr_compression), intent(inout) :: w

    call f_free_ptr(w%c)
    call f_free_ptr(w%f)
  END SUBROUTINE deallocate_work_arrays_compression

  subroutine initialize_locham_sized(nspinor, nw1, nw2, nc, nf, init_to_zero, w)
    use dynamic_memory
    implicit none
    integer, intent(in) :: nspinor, nw1, nw2, nc, nf
    logical, intent(in) :: init_to_zero
    type(workarr_locham), intent(out) :: w

    w%nw1 = nw1
    w%nw2 = nw2
    w%nyc = nc
    w%nyf = nf
    w%nxc = nc
    w%nxf = nf
    w%nxf1 = nf / 7
    w%nxf2 = nf / 7
    w%nxf3 = nf / 7

    nullify(w%w1)
    nullify(w%w2)
    nullify(w%x_c)
    nullify(w%y_c)
    nullify(w%x_f1)
    nullify(w%x_f2)
    nullify(w%x_f3)
    nullify(w%x_f)
    nullify(w%y_f)
    !allocation of work arrays
    if (nc > 0 .and. init_to_zero) then
       w%x_c = f_malloc0_ptr((/ w%nxc, nspinor /), id='w%x_c')
       w%y_c = f_malloc0_ptr((/ w%nyc, nspinor /), id='w%y_c', info='{alignment: 32}')
    else if (nc > 0) then
       w%x_c = f_malloc_ptr((/ w%nxc, nspinor /), id='w%x_c')
       w%y_c = f_malloc_ptr((/ w%nyc, nspinor /), id='w%y_c', info='{alignment: 32}')
    end if
    if (nf > 0) then
       w%x_f = f_malloc0_ptr((/ w%nxf, nspinor /), id='w%x_f')
       w%y_f = f_malloc0_ptr((/ w%nyf, nspinor /), id='w%y_f')
    end if
    if (nw1 > 0) w%w1 = f_malloc_ptr(w%nw1,id='w%w1')
    if (nw2 > 0) w%w2 = f_malloc_ptr(w%nw2,id='w%w2')
    if (nf > 0) then
       w%x_f1 = f_malloc0_ptr((/ w%nxf1, nspinor /), id='w%x_f1')
       w%x_f2 = f_malloc0_ptr((/ w%nxf2, nspinor /), id='w%x_f2')
       w%x_f3 = f_malloc0_ptr((/ w%nxf3, nspinor /), id='w%x_f3')
    end if
  end subroutine initialize_locham_sized

  !> Initialize work arrays for local hamiltonian
  subroutine initialize_work_arrays_locham_nlr(nlr,lr,w)
    use locregs
    use at_domain, only: domain_periodic_dims,domain_geocode
    use liborbs_errors
    implicit none
    integer, intent(in) :: nlr
    type(locreg_descriptors), dimension(nlr), intent(in) :: lr
    type(workarr_locham), intent(out) :: w
    !local variables
    logical :: hyb
    integer :: ilr
    integer, dimension(3) :: coarse,interp, nfine
    integer :: n1f,n2f,n3f
    character(len=1) :: geocode

    ! Determine the maximum array sizes for all locregs 1,..,nlr
    ! If the sizes for a specific locreg are needed, simply call the routine with nlr=1
    ! For the moment the geocode of all locregs must be the same
    coarse = 0
    interp = 0
    n1f=0
    n2f=0
    n3f=0

    if (nlr > 0) then
       geocode = domain_geocode(lr(1)%mesh%dom)
       hyb=lr(1)%hybrid_on
    else
       geocode = 'F'
       hyb = .false.
    end if

    do ilr=1,nlr
       coarse = max(coarse, lr(ilr)%mesh_coarse%ndims)
       interp = max(interp ,lr(ilr)%mesh%ndims)
       n1f=max(n1f,lr(ilr)%nboxf(2,1)-lr(ilr)%nboxf(1,1))
       n2f=max(n2f,lr(ilr)%nboxf(2,2)-lr(ilr)%nboxf(1,2))
       n3f=max(n3f,lr(ilr)%nboxf(2,3)-lr(ilr)%nboxf(1,3))
       if (any(domain_periodic_dims(lr(ilr)%mesh%dom) .neqv. domain_periodic_dims(lr(1)%mesh%dom))) &
            call f_err_throw('The lrs do not have same BC', err_id = LIBORBS_LOCREG_ERROR())
       if (lr(ilr)%hybrid_on .neqv. hyb) &
            call f_err_throw('lr(ilr)%hybrid_on .neqv. hyb', err_id = LIBORBS_LOCREG_ERROR())
    end do

    nfine = (/ n1f, n2f, n3f /)
    ! Just save the sizes. Reset will allocate.
    call cf_sizes(w%nw1, w%nw2, w%nxc, w%nxf, coarse, interp, &
         nfine, geocode, hyb)
    nullify(w%w1)
  END SUBROUTINE initialize_work_arrays_locham_nlr

  !> Initialize work arrays for local hamiltonian
  subroutine initialize_work_arrays_locham_llr(lr,nspinor,w)
    use locregs
    use at_domain, only: domain_geocode
    implicit none
    integer, intent(in) ::  nspinor
    type(locreg_descriptors), intent(in) :: lr
    type(workarr_locham), intent(out) :: w
    !local variables
    integer :: nw,nww,nf,nc
    integer, dimension(3) :: nfs

    nfs(1) = lr%nboxf(2,1) - lr%nboxf(1,1)
    nfs(2) = lr%nboxf(2,2) - lr%nboxf(1,2)
    nfs(3) = lr%nboxf(2,3) - lr%nboxf(1,3)
    call cf_sizes(nw, nww, nc, nf, lr%mesh_coarse%ndims, lr%mesh%ndims, &
         nfs, domain_geocode(lr%mesh%dom), lr%hybrid_on)
    call initialize_locham_sized(nspinor, nw, nww, nc, nf, &
         domain_geocode(lr%mesh%dom) == 'F' .or. lr%hybrid_on, w)
  END SUBROUTINE initialize_work_arrays_locham_llr

  !> Reset work array sizes for given lr
  subroutine reset_work_arrays_locham(w, lr, nspinor)
    use locregs
    use at_domain, only: domain_geocode
    use liborbs_errors
    use f_utils
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    type(workarr_locham), intent(inout) :: w
    integer, intent(in) :: nspinor
    !local variables
    integer :: nw,nww,nf,nc
    integer :: old_nw,old_nww,old_nf,old_nc,old_nfx
    integer, dimension(3) :: nfs
    logical :: tozero

    tozero = (domain_geocode(lr%mesh%dom) == 'F' .or. lr%hybrid_on .or. domain_geocode(lr%mesh%dom) == 'W')
    if (.not. associated(w%x_c)) then
      nw=w%nw1 
      nww=w%nw2
      nc=w%nxc
      nf=w%nxf
      call initialize_locham_sized(nspinor, nw, nww, nc, nf, tozero, w)
    else if (tozero) then
       call f_zero(w%x_c)
       call f_zero(w%y_c)
       if (associated(w%x_f)) call f_zero(w%x_f)
       if (associated(w%y_f)) call f_zero(w%y_f)
       if (associated(w%x_f1)) call f_zero(w%x_f1)
       if (associated(w%x_f2)) call f_zero(w%x_f2)
       if (associated(w%x_f3)) call f_zero(w%x_f3)
    end if

    nfs(1) = lr%nboxf(2,1) - lr%nboxf(1,1)
    nfs(2) = lr%nboxf(2,2) - lr%nboxf(1,2)
    nfs(3) = lr%nboxf(2,3) - lr%nboxf(1,3)
    call cf_sizes(nw, nww, nc, nf, lr%mesh_coarse%ndims, lr%mesh%ndims, &
         nfs, domain_geocode(lr%mesh%dom), lr%hybrid_on)
    old_nw = 0
    if (associated(w%w1)) old_nw = size(w%w1)
    old_nww = 0
    if (associated(w%w2)) old_nww = size(w%w2)
    old_nc = 0
    if (associated(w%x_c)) then
      old_nc = size(w%x_c, 1)
      !this conditional should be moved here
      if (f_err_raise(nspinor > size(w%x_c, 2), &
                      "too small nspinor size in reset", &
                      err_id = LIBORBS_LOCREG_ERROR())) return
    end if
    old_nf = 0
    if (associated(w%x_f)) old_nf = size(w%x_f, 1)
    old_nfx = 0
    if (associated(w%x_f1)) old_nfx = size(w%x_f1, 1)

    if (f_err_raise(nw > old_nw, "too small w1 size in reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
        f_err_raise(nww > old_nww, "too small w2 size in reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
        f_err_raise(nc > old_nc, "too small x_c size in reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
        f_err_raise(nf > old_nf, "too small x_f size in reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
        f_err_raise(nf / 7 > old_nfx, "too small x_fi size in reset", err_id = LIBORBS_LOCREG_ERROR())) return

    w%nw1 = nw
    w%nw2 = nww
    w%nyc = nc
    w%nyf = nf
    w%nxc = nc
    w%nxf = nf
    w%nxf1 = nf / 7
    w%nxf2 = nf / 7
    w%nxf3 = nf / 7
  end subroutine reset_work_arrays_locham

  subroutine memspace_work_arrays_locham(lr,memwork) !n(c) nspinor (arg:2)
    use locregs
    use at_domain, only: domain_geocode
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    integer(kind=8), intent(out) :: memwork
    !local variables
    integer :: nw1,nw2,nc,nf
    integer, dimension(3) :: nfs

    nfs(1) = lr%nboxf(2,1) - lr%nboxf(1,1)
    nfs(2) = lr%nboxf(2,2) - lr%nboxf(1,2)
    nfs(3) = lr%nboxf(2,3) - lr%nboxf(1,3)

    call cf_sizes(nw1, nw2, nc, nf, lr%mesh_coarse%ndims, lr%mesh%ndims, &
         nfs, domain_geocode(lr%mesh%dom), lr%hybrid_on)
    memwork=nw1+nw2+nc+nf+nc+nf+3*nf/7
  END SUBROUTINE memspace_work_arrays_locham


  subroutine deallocate_work_arrays_locham(w)
    use dynamic_memory
    implicit none
    type(workarr_locham), intent(inout) :: w
    !local variables
    character(len=*), parameter :: subname='deallocate_work_arrays_locham'

    call f_free_ptr(w%y_c,cptr=.true.)
    call f_free_ptr(w%x_c)
    call f_free_ptr(w%x_f1)
    call f_free_ptr(w%x_f2)
    call f_free_ptr(w%x_f3)
    call f_free_ptr(w%y_f)
    call f_free_ptr(w%x_f)
    call f_free_ptr(w%w1)
    call f_free_ptr(w%w2)
  END SUBROUTINE deallocate_work_arrays_locham

  subroutine initialize_sumrho_sized(nw1, nw2, nc, nf, init_to_zero, w)
    use dynamic_memory
    implicit none
    integer, intent(in) :: nw1, nw2, nc, nf
    logical, intent(in) :: init_to_zero
    type(workarr_sumrho), intent(out) :: w

    call initialize_work_arrays_compression(nc, nf, init_to_zero, w%cf)
    
    w%nw1 = nw1
    w%nw2 = nw2

    nullify(w%w1)
    nullify(w%w2)
    !allocation of work arrays
    if (nw1 > 0) w%w1 = f_malloc_ptr(w%nw1, id='w%w1')
    if (nw2 > 0) w%w2 = f_malloc_ptr(w%nw2, id='w%w2')
  end subroutine initialize_sumrho_sized

  subroutine initialize_work_arrays_sumrho_nlr(nlr,lr,w)
    use locregs
    use at_domain, only: domain_periodic_dims,domain_geocode
    use liborbs_errors
    use dynamic_memory
    implicit none
    integer, intent(in) :: nlr
    type(locreg_descriptors), dimension(nlr), intent(in) :: lr
    type(workarr_sumrho), intent(out) :: w
    !local variables
    character(len=*), parameter :: subname='initialize_work_arrays_sumrho'
    logical :: hyb
    integer :: ilr
    integer, dimension(3) :: coarse,interp, nfine
    integer :: n1f,n2f,n3f
    character(len=1) :: geocode

    call f_routine(id='initialize_work_arrays_sumrho')

    call nullify_work_arrays_sumrho(w)

    ! Determine the maximum array sizes for all locregs 1,..,nlr
    ! If the sizes for a specific locreg are needed, simply call the routine with nlr=1
    ! For the moment the geocode of all locregs must be the same
    coarse = 0
    interp = 0
    n1f=0
    n2f=0
    n3f=0

    if (nlr > 0) then
       geocode = domain_geocode(lr(1)%mesh%dom)
       hyb = lr(1)%hybrid_on
    else
       geocode = 'F'
       hyb = .false.
    end if

    do ilr=1,nlr
       coarse = max(coarse, lr(ilr)%mesh_coarse%ndims)
       interp = max(interp ,lr(ilr)%mesh%ndims)
       n1f=max(n1f,lr(ilr)%nboxf(2,1)-lr(ilr)%nboxf(1,1))
       n2f=max(n2f,lr(ilr)%nboxf(2,2)-lr(ilr)%nboxf(1,2))
       n3f=max(n3f,lr(ilr)%nboxf(2,3)-lr(ilr)%nboxf(1,3))
       if (any(domain_periodic_dims(lr(ilr)%mesh%dom) .neqv. domain_periodic_dims(lr(1)%mesh%dom))) &
            call f_err_throw('The lrs do not have same BC', err_id = LIBORBS_LOCREG_ERROR())
       if (lr(ilr)%hybrid_on .neqv. hyb) &
            call f_err_throw('lr(ilr)%hybrid_on .neqv. hyb', err_id = LIBORBS_LOCREG_ERROR())
    end do

    nfine = (/ n1f, n2f, n3f /)
    call cf_sizes(w%nw1, w%nw2, w%cf%nc, w%cf%nf, coarse, interp, &
         nfine, geocode, hyb)

    call f_release_routine()

  END SUBROUTINE initialize_work_arrays_sumrho_nlr

  !> Reset work array sizes for given lr
  subroutine reset_work_arrays_sumrho(w, lr)
    use locregs
    use at_domain, only: domain_geocode
    use liborbs_errors
    use f_utils
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    type(workarr_sumrho), intent(inout) :: w
    !local variables
    integer :: nw,nww,nf,nc
    integer :: old_nw,old_nww,old_nf,old_nc
    integer, dimension(3) :: nfs
    logical :: tozero

    tozero = (domain_geocode(lr%mesh%dom) == 'F' .or. lr%hybrid_on .or. domain_geocode(lr%mesh%dom) == 'W')
    if (.not. associated(w%cf%c)) then
       nw=w%nw1
       nww=w%nw2
       nc=w%cf%nc
       nf=w%cf%nf
       call initialize_sumrho_sized(nw, nww, nc, nf, tozero, w)
    else if (tozero) then
       call f_zero(w%cf%c)
       if (associated(w%cf%f)) call f_zero(w%cf%f)
    end if

    nfs(1) = lr%nboxf(2,1) - lr%nboxf(1,1)
    nfs(2) = lr%nboxf(2,2) - lr%nboxf(1,2)
    nfs(3) = lr%nboxf(2,3) - lr%nboxf(1,3)

    call cf_sizes(nw, nww, nc, nf, lr%mesh_coarse%ndims, lr%mesh%ndims, &
         nfs, domain_geocode(lr%mesh%dom), lr%hybrid_on)
    old_nw = 0
    if (associated(w%w1)) old_nw = size(w%w1)
    old_nww = 0
    if (associated(w%w2)) old_nww = size(w%w2)
    old_nc = 0
    if (associated(w%cf%c)) old_nc = size(w%cf%c)
    old_nf = 0
    if (associated(w%cf%f)) old_nf = size(w%cf%f)
    if (f_err_raise(nw > old_nw, "too small w1 size in reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
         f_err_raise(nww > old_nww, "too small w2 size in reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
         f_err_raise(nc > old_nc, "too small x_c size in reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
         f_err_raise(nf > old_nf, "too small x_f size in reset", err_id = LIBORBS_LOCREG_ERROR())) return

    w%nw1 = nw
    w%nw2 = nww
    w%cf%nc = nc
    w%cf%nf = nf
  end subroutine reset_work_arrays_sumrho

  subroutine initialize_work_arrays_sumrho_llr(lr,w)
    use locregs
    use at_domain, only: domain_geocode
    use dynamic_memory
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    type(workarr_sumrho), intent(out) :: w
    !local variables
    character(len=*), parameter :: subname='initialize_work_arrays_sumrho'
    integer :: nw,nww,nf,nc
    integer, dimension(3) :: nfs

    call f_routine(id='initialize_work_arrays_sumrho')

    nfs(1) = lr%nboxf(2,1) - lr%nboxf(1,1)
    nfs(2) = lr%nboxf(2,2) - lr%nboxf(1,2)
    nfs(3) = lr%nboxf(2,3) - lr%nboxf(1,3)
    call cf_sizes(nw, nww, nc, nf, lr%mesh_coarse%ndims, lr%mesh%ndims, &
         nfs, domain_geocode(lr%mesh%dom), lr%hybrid_on)
    call initialize_sumrho_sized(nw, nww, nc, nf, &
         domain_geocode(lr%mesh%dom) == 'F', w)

    call f_release_routine()

  END SUBROUTINE initialize_work_arrays_sumrho_llr

  pure subroutine nullify_work_arrays_sumrho(ws)
    implicit none
    type(workarr_sumrho),intent(out) :: ws
    nullify(ws%cf%c)
    nullify(ws%cf%f)
    nullify(ws%w1)
    nullify(ws%w2)
  end subroutine nullify_work_arrays_sumrho

  subroutine memspace_work_arrays_sumrho(lr,memwork)
    use locregs
    use at_domain, only: domain_geocode
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    integer(kind=8), intent(out) :: memwork
    !local variables
    integer :: nw1,nw2,nc,nf
    integer, dimension(3) :: nfs

    nfs(1) = lr%nboxf(2,1) - lr%nboxf(1,1)
    nfs(2) = lr%nboxf(2,2) - lr%nboxf(1,2)
    nfs(3) = lr%nboxf(2,3) - lr%nboxf(1,3)

    call cf_sizes(nw1, nw2, nc, nf, lr%mesh_coarse%ndims, lr%mesh%ndims, &
         nfs, domain_geocode(lr%mesh%dom), lr%hybrid_on)
    memwork=nc+nf+nw1+nw2

  END SUBROUTINE memspace_work_arrays_sumrho

  subroutine deallocate_work_arrays_sumrho(w)
    use dynamic_memory
    implicit none
    type(workarr_sumrho), intent(inout) :: w
    !local variables
    character(len=*), parameter :: subname='deallocate_work_arrays_sumrho'

    call f_routine(id='deallocate_work_arrays_sumrho')

    call deallocate_work_arrays_compression(w%cf)
    call f_free_ptr(w%w1)
    call f_free_ptr(w%w2)

    call f_release_routine()

  END SUBROUTINE deallocate_work_arrays_sumrho

  subroutine initialize_precond_sized(w, geocode, nc, nf, nfi, ncs, ncplx, hybrid_on)
    use bounds
    use dynamic_memory
    implicit none
    type(workarr_precond), intent(inout) :: w
    character(len = 1), intent(in) :: geocode
    integer(f_long), intent(in) :: nc, nf, nfi
    integer, dimension(3), intent(in) :: ncs
    integer, intent(in) :: ncplx
    logical, intent(in) :: hybrid_on
    
    integer :: nl, nr
    integer :: nd1,nd2,nd3
    integer :: n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b

    w%geocode = geocode
    w%nc = nc
    w%nf = nf
    w%nfi = nfi
    w%nc1 = ncs(1)
    w%nc2 = ncs(2)
    w%nc3 = ncs(3)

    call ext_buffers(.false., nl, nr)

    select case(w%geocode)
    case('F')
       w%x_c = f_malloc_ptr(w%nc,id='w%x_c')
       w%x_f = f_malloc_ptr(7 * w%nf,id='w%x_f')
       w%y_c = f_malloc_ptr(w%nc,id='w%y_c')
       w%y_f = f_malloc_ptr(7 * w%nf,id='w%y_f')

       w%x_f1 = f_malloc_ptr(w%nf,id='w%x_f1')
       w%x_f2 = f_malloc_ptr(w%nf,id='w%x_f2')
       w%x_f3 = f_malloc_ptr(w%nf,id='w%x_f3')

    case('P')
       if (hybrid_on) then
          call dimensions_fft(w%nc1-1,w%nc2-1,w%nc3-1, &
               nd1,nd2,nd3,n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b)

          w%kern_k1 = f_malloc_ptr(0.to.(w%nc1-1),id='w%kern_k1')
          w%kern_k2 = f_malloc_ptr(0.to.(w%nc2-1),id='w%kern_k2')
          w%kern_k3 = f_malloc_ptr(0.to.(w%nc3-1),id='w%kern_k3')
          w%z1 = f_malloc_ptr((/ 2, nd1b, nd2, nd3, 2 /),id='w%z1')
          w%z3 = f_malloc_ptr((/ 2, nd1, nd2, nd3f, 2 /),id='w%z3')

          w%x_c = f_malloc_ptr(w%nc,id='w%x_c')
          w%x_f = f_malloc_ptr(7 * w%nf,id='w%x_f')
          w%x_f1 = f_malloc_ptr(w%nf,id='w%x_f1')
          w%x_f2 = f_malloc_ptr(w%nf,id='w%x_f2')
          w%x_f3 = f_malloc_ptr(w%nf,id='w%x_f3')
          w%y_f = f_malloc_ptr(7 * w%nf,id='w%y_f')
          w%y_c = f_malloc_ptr(w%nc,id='w%y_c')


       else

          if (ncplx == 1) then
             !periodic, not k-points
             w%modul1 = f_malloc_ptr((-nl).to.(w%nc1+(nr-1)-1),id='w%modul1')
             w%modul2 = f_malloc_ptr((-nl).to.(w%nc2+(nr-1)-1),id='w%modul2')
             w%modul3 = f_malloc_ptr((-nl).to.(w%nc3+(nr-1)-1),id='w%modul3')
             w%af = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%af')
             w%bf = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%bf')
             w%cf = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%cf')
             w%ef = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%ef')
          end if

          w%psifscf = f_malloc_ptr(w%nfi,id='w%psifscf')
          w%ww = f_malloc_ptr(w%nfi,id='w%ww')

       end if

!!$      else if (geocode == 'S') then
       !else if (cell_geocode(mesh) == 'S') then
    case('S')

       if (ncplx == 1) then
          w%modul1 = f_malloc_ptr((-nl).to.(w%nc1+(nr-1)-1),id='w%modul1')
          w%modul3 = f_malloc_ptr((-nl).to.(w%nc3+(nr-1)-1),id='w%modul3')
          w%af = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%af')
          w%bf = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%bf')
          w%cf = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%cf')
          w%ef = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%ef')
       end if

       w%psifscf = f_malloc_ptr(w%nfi,id='w%psifscf')
       w%ww = f_malloc_ptr(w%nfi,id='w%ww')

    case('W')

       if (ncplx == 1) then
          w%modul3 = f_malloc_ptr((-nl).to.(w%nc3+(nr-1)-1),id='w%modul3')
          w%af = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%af')
          w%bf = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%bf')
          w%cf = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%cf')
          w%ef = f_malloc_ptr((/ (-nl).to.(nr-1), 1.to.3 /),id='w%ef')
       end if

       w%psifscf = f_malloc_ptr(w%nfi,id='w%psifscf')
       w%ww = f_malloc_ptr(w%nfi,id='w%ww')
       w%kern_k3 = f_malloc_ptr(0.to.(w%nc3-1),id='w%kern_k3')
       w%x_c = f_malloc_ptr(w%nc,id='w%x_c')

    end select
  end subroutine initialize_precond_sized

  subroutine nullify_work_arrays_precond(w)
    implicit none
    type(workarr_precond), intent(out) :: w
    
    nullify(w%modul1, w%modul2, w%modul3)
    nullify(w%af,w%bf,w%cf,w%ef)
    nullify(w%psifscf)
    nullify(w%ww)
    nullify(w%kern_k1,w%kern_k2,w%kern_k3)
    nullify(w%z1,w%z3)
    nullify(w%x_c,w%y_c)
    nullify(w%x_f,w%y_f)
    nullify(w%x_f1,w%x_f2,w%x_f3)
  end subroutine nullify_work_arrays_precond

  subroutine initialize_work_arrays_precond_llr(lr, ncplx, w)
    use at_domain, only: domain_geocode
    use locregs
    use dynamic_memory
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    integer, intent(in) :: ncplx
    type(workarr_precond), intent(out) :: w
    !local variables
    integer(f_long), dimension(3) :: nfs

    call nullify_work_arrays_precond(w)

    nfs(1) = lr%nboxf(2,1) - lr%nboxf(1,1)
    nfs(2) = lr%nboxf(2,2) - lr%nboxf(1,2)
    nfs(3) = lr%nboxf(2,3) - lr%nboxf(1,3)
    call initialize_precond_sized(w, domain_geocode(lr%mesh%dom), lr%mesh_coarse%ndim, &
         (nfs(1)+1)*(nfs(2)+1)*(nfs(3)+1), ncplx * lr%mesh_fine%ndim, lr%mesh_coarse%ndims, ncplx, lr%hybrid_on)
  END SUBROUTINE initialize_work_arrays_precond_llr

  subroutine initialize_work_arrays_precond_nlr(llr, ncplx, w)
    use at_domain, only: domain_geocode
    use locregs
    use liborbs_errors
    implicit none
    type(locreg_descriptors), dimension(:), intent(in) :: llr
    integer, intent(in) :: ncplx
    type(workarr_precond), intent(out) :: w
    !local variables
    integer :: ilr
    integer, dimension(3) :: nfs

    call nullify_work_arrays_precond(w)

    w%nc = 0
    w%nf = 0
    w%nfi = 0
    w%nc1 = 0
    w%nc2 = 0
    w%nc3 = 0
    if (size(llr) > 0) w%geocode = domain_geocode(llr(1)%mesh%dom)
    do ilr = 1, size(llr)
       if (f_err_raise(w%geocode /= domain_geocode(llr(ilr)%mesh%dom), &
            "all locreg should have the same geocode", err_id = LIBORBS_LOCREG_ERROR())) return
       w%nc = max(w%nc, llr(ilr)%mesh_coarse%ndim)
       nfs(1) = llr(ilr)%nboxf(2,1) - llr(ilr)%nboxf(1,1)
       nfs(2) = llr(ilr)%nboxf(2,2) - llr(ilr)%nboxf(1,2)
       nfs(3) = llr(ilr)%nboxf(2,3) - llr(ilr)%nboxf(1,3)
       w%nf = max(w%nf, (nfs(1)+1)*(nfs(2)+1)*(nfs(3)+1))
       w%nfi = max(w%nfi, ncplx*llr(ilr)%mesh_fine%ndim)
       w%nc1 = max(w%nc1, llr(ilr)%mesh_coarse%ndims(1))
       w%nc2 = max(w%nc2, llr(ilr)%mesh_coarse%ndims(2))
       w%nc3 = max(w%nc3, llr(ilr)%mesh_coarse%ndims(3))
    end do
  END SUBROUTINE initialize_work_arrays_precond_nlr

  subroutine reset_work_arrays_precond(w, lr, ncplx)
    use at_domain, only: domain_geocode
    use locregs
    use liborbs_errors
    implicit none
    type(workarr_precond), intent(inout) :: w
    type(locreg_descriptors), intent(in) :: lr
    integer, intent(in) :: ncplx

    integer, dimension(3) :: nfs
    integer(f_long) :: old_nc, old_nf, old_nfi

    if (.not. associated(w%psifscf) .and. .not. associated(w%x_c)) then
       call initialize_precond_sized(w, w%geocode, w%nc, w%nf, w%nfi, (/ w%nc1, w%nc2, w%nc3 /), ncplx, lr%hybrid_on)
    end if

    nfs(1) = lr%nboxf(2,1) - lr%nboxf(1,1)
    nfs(2) = lr%nboxf(2,2) - lr%nboxf(1,2)
    nfs(3) = lr%nboxf(2,3) - lr%nboxf(1,3)

    old_nc = lr%mesh_coarse%ndim
    if (associated(w%x_c)) old_nc = size(w%x_c)
    old_nf = (nfs(1)+1)*(nfs(2)+1)*(nfs(3)+1)
    if (associated(w%x_f)) old_nf = size(w%x_f) / 7
    old_nfi = ncplx*lr%mesh_fine%ndim
    if (associated(w%psifscf)) old_nfi = size(w%psifscf)

    if (f_err_raise(domain_geocode(lr%mesh%dom) /= w%geocode, &
         "cannot change geocode in precond reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
         f_err_raise(lr%mesh_coarse%ndim > old_nc, &
         "too small nc size in precond reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
         f_err_raise((nfs(1)+1)*(nfs(2)+1)*(nfs(3)+1) > old_nf, &
         "too small nf size in precond reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
         f_err_raise(ncplx*lr%mesh_fine%ndim > old_nfi, &
         "too small nfi size in precond reset", err_id = LIBORBS_LOCREG_ERROR())) return

    w%geocode = domain_geocode(lr%mesh%dom)
    w%nc = lr%mesh_coarse%ndim
    w%nf = (nfs(1)+1)*(nfs(2)+1)*(nfs(3)+1)
    w%nfi = ncplx*lr%mesh_fine%ndim
    w%nc1 = lr%mesh_coarse%ndims(1)
    w%nc2 = lr%mesh_coarse%ndims(2)
    w%nc3 = lr%mesh_coarse%ndims(3)
  end subroutine reset_work_arrays_precond

  subroutine deallocate_work_arrays_precond(w)
    use dynamic_memory
    implicit none
    type(workarr_precond), intent(inout) :: w

    call f_free_ptr(w%x_c)
    call f_free_ptr(w%y_c)
    call f_free_ptr(w%x_f)
    call f_free_ptr(w%y_f)
    call f_free_ptr(w%x_f1)
    call f_free_ptr(w%x_f2)
    call f_free_ptr(w%x_f3)

    call f_free_ptr(w%modul1)
    call f_free_ptr(w%modul2)
    call f_free_ptr(w%modul3)
    call f_free_ptr(w%af)
    call f_free_ptr(w%bf)
    call f_free_ptr(w%cf)
    call f_free_ptr(w%ef)

    call f_free_ptr(w%psifscf)
    call f_free_ptr(w%ww)

    call f_free_ptr(w%z1)
    call f_free_ptr(w%z3)
    call f_free_ptr(w%kern_k1)
    call f_free_ptr(w%kern_k2)
    call f_free_ptr(w%kern_k3)

  END SUBROUTINE deallocate_work_arrays_precond
  
  subroutine memspace_work_arrays_precond(lr,ncplx,memwork)
    use locregs
    use at_domain, only: domain_geocode
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    integer, intent(in) :: ncplx
    integer(kind=8), intent(out) :: memwork
    !local variables
    integer, parameter :: lowfil=-14,lupfil=14
    integer :: nd1,nd2,nd3
    integer :: n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b
    integer :: nf


    select case(domain_geocode(lr%mesh%dom))
    case('F')

       nf=(lr%nboxf(2,1)-lr%nboxf(1,1)+1)*(lr%nboxf(2,2)-lr%nboxf(1,2)+1)*(lr%nboxf(2,3)-lr%nboxf(1,3)+1)

       memwork=2*lr%mesh_coarse%ndim+2*7*nf+3*nf


    case ('P')

       if (lr%hybrid_on) then

          call dimensions_fft(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1, &
               lr%mesh_coarse%ndims(3)-1, &
               nd1,nd2,nd3,n1f,n3f,n1b,n3b,nd1f,nd3f,nd1b,nd3b)

          nf=(lr%nboxf(2,1)-lr%nboxf(1,1)+1)*(lr%nboxf(2,2)-&
               lr%nboxf(1,2)+1)*(lr%nboxf(2,3)-lr%nboxf(1,3)+1)

          memwork=sum(lr%mesh_coarse%ndims)+2*nd1b*nd2*nd3*2+2*nd1*nd2*nd3f*2+&
               lr%mesh_coarse%ndim+2*7*nf+3*nf

       else

          memwork=0
          if (ncplx == 1) then
             memwork=sum(lr%mesh_coarse%ndims)-3+15*(lupfil-lowfil+1)
          end if
          memwork=memwork+2*ncplx*lr%mesh_fine%ndim

       end if

    case ('S')

       memwork=0
       if (ncplx == 1) then
          memwork=lr%mesh_coarse%ndims(1)-1+lr%mesh_coarse%ndims(3)-1+14*(lupfil-lowfil+1)
       end if
       memwork=memwork+2*ncplx*lr%mesh_fine%ndim

    case ('W')
       memwork=0
       if (ncplx == 1) then
          memwork=lr%mesh_coarse%ndims(3)-1+14*(lupfil-lowfil+1)  !!! To be checked !!!
       end if
       memwork=memwork+2*ncplx*lr%mesh_fine%ndim

    end select

  END SUBROUTINE memspace_work_arrays_precond

  subroutine deallocate_work_arrays_quartic(work)
    use dynamic_memory
    implicit none
    type(workarr_quartic_convolutions), intent(inout) :: work

    call f_free_ptr(work%xx_c)
    call f_free_ptr(work%xy_c)
    call f_free_ptr(work%xz_c)
    call f_free_ptr(work%xx_f1)
    call f_free_ptr(work%xx_f)
    call f_free_ptr(work%xy_f2)
    call f_free_ptr(work%xy_f)
    call f_free_ptr(work%xz_f4)
    call f_free_ptr(work%xz_f)
    call f_free_ptr(work%y_c)
    call f_free_ptr(work%y_f)
    call f_free_ptr(work%aeff0array)
    call f_free_ptr(work%beff0array)
    call f_free_ptr(work%ceff0array)
    call f_free_ptr(work%eeff0array)
    call f_free_ptr(work%aeff0_2array)
    call f_free_ptr(work%beff0_2array)
    call f_free_ptr(work%ceff0_2array)
    call f_free_ptr(work%eeff0_2array)
    call f_free_ptr(work%aeff0_2auxarray)
    call f_free_ptr(work%beff0_2auxarray)
    call f_free_ptr(work%ceff0_2auxarray)
    call f_free_ptr(work%eeff0_2auxarray)
    call f_free_ptr(work%xya_c)
    call f_free_ptr(work%xyc_c)
    call f_free_ptr(work%xza_c)
    call f_free_ptr(work%xzc_c)
    call f_free_ptr(work%yza_c)
    call f_free_ptr(work%yzb_c)
    call f_free_ptr(work%yzc_c)
    call f_free_ptr(work%yze_c)
    call f_free_ptr(work%xya_f)
    call f_free_ptr(work%xyb_f)
    call f_free_ptr(work%xyc_f)
    call f_free_ptr(work%xye_f)
    call f_free_ptr(work%xza_f)
    call f_free_ptr(work%xzb_f)
    call f_free_ptr(work%xzc_f)
    call f_free_ptr(work%xze_f)
    call f_free_ptr(work%yza_f)
    call f_free_ptr(work%yzb_f)
    call f_free_ptr(work%yzc_f)
    call f_free_ptr(work%yze_f)

  end subroutine deallocate_work_arrays_quartic

  subroutine initialize_quartic_sized(work, nc, nf, ncmax, with_confpot)
    use dynamic_memory
    use f_utils
    implicit none

    ! Calling arguments
    type(workarr_quartic_convolutions), intent(out):: work
    integer(f_long), intent(in) :: nc, nf
    integer, intent(in) :: ncmax
    logical, intent(in) :: with_confpot

    ! Local variables
    integer,parameter :: lowfil=-14,lupfil=14

    work%nc = nc
    work%nf = nf

    work%xx_c = f_malloc0_ptr(work%nc, id='work%xx_c')
    work%xy_c = f_malloc0_ptr(work%nc, id='work%xy_c')
    work%xz_c = f_malloc0_ptr(work%nc, id='work%xz_c')

    work%xx_f1 = f_malloc0_ptr(work%nf, id='work%xx_f1')
    work%xx_f = f_malloc0_ptr(7 * work%nf, id='work%xx_f')


    work%xy_f2 = f_malloc0_ptr(work%nf, id='work%xy_f2')
    work%xy_f = f_malloc0_ptr(7 * work%nf, id='work%xy_f')


    work%xz_f4 = f_malloc0_ptr(work%nf, id='work%xz_f4')
    work%xz_f = f_malloc0_ptr(7 * work%nf, id='work%xz_f')


    work%y_c = f_malloc0_ptr(work%nc, id='work%y_c')

    work%y_f = f_malloc0_ptr(7 * work%nf, id='work%y_f')

    work%ncmax = ncmax
    work%aeff0array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.work%ncmax /),id='work%aeff0array')
    work%beff0array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.work%ncmax /),id='work%beff0array')
    work%ceff0array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.work%ncmax /),id='work%ceff0array')
    work%eeff0array = f_malloc0_ptr((/ lowfil.to.lupfil, 0.to.work%ncmax /),id='work%eeff0array')

    work%aeff0_2array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.work%ncmax /),id='work%aeff0_2array')
    work%beff0_2array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.work%ncmax /),id='work%beff0_2array')
    work%ceff0_2array = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.work%ncmax /),id='work%ceff0_2array')
    work%eeff0_2array = f_malloc0_ptr((/ lowfil.to.lupfil, 0.to.work%ncmax /),id='work%eeff0_2array')

    work%aeff0_2auxarray = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.work%ncmax /),id='work%aeff0_2auxarray')
    work%beff0_2auxarray = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.work%ncmax /),id='work%beff0_2auxarray')
    work%ceff0_2auxarray = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.work%ncmax /),id='work%ceff0_2auxarray')
    work%eeff0_2auxarray = f_malloc0_ptr((/ -3+lowfil.to.lupfil+3, 0.to.work%ncmax /),id='work%eeff0_2auxarray')

    work%xya_c = f_malloc_ptr(work%nc, id='work%xya_c')
    work%xyc_c = f_malloc_ptr(work%nc, id='work%xyc_c')
    if(with_confpot) then
       call f_zero(work%xya_c)
       call f_zero(work%xyc_c)
    end if

    work%xza_c = f_malloc_ptr(work%nc, id='work%xza_c')
    work%xzc_c = f_malloc_ptr(work%nc, id='work%xzc_c')
    if(with_confpot) then
       call f_zero(work%xza_c)
       call f_zero(work%xzc_c)
    end if

    work%yza_c = f_malloc_ptr(work%nc, id='work%yza_c')
    work%yzb_c = f_malloc_ptr(work%nc, id='work%yzb_c')
    work%yzc_c = f_malloc_ptr(work%nc, id='work%yzc_c')
    work%yze_c = f_malloc_ptr(work%nc, id='work%yze_c')
    if(with_confpot) then
       call f_zero(work%yza_c)
       call f_zero(work%yzb_c)
       call f_zero(work%yzc_c)
       call f_zero(work%yze_c)
    end if

    work%xya_f = f_malloc_ptr(3 * work%nf, id='work%xya_f')
    work%xyb_f = f_malloc_ptr(4 * work%nf, id='work%xyb_f')
    work%xyc_f = f_malloc_ptr(3 * work%nf, id='work%xyc_f')
    work%xye_f = f_malloc_ptr(4 * work%nf, id='work%xye_f')
    if(with_confpot) then
       call f_zero(work%xya_f)
       call f_zero(work%xyb_f)
       call f_zero(work%xyc_f)
       call f_zero(work%xye_f)
    end if

    work%xza_f = f_malloc_ptr(3 * work%nf, id='work%xza_f')
    work%xzb_f = f_malloc_ptr(4 * work%nf, id='work%xzb_f')
    work%xzc_f = f_malloc_ptr(3 * work%nf, id='work%xzc_f')
    work%xze_f = f_malloc_ptr(4 * work%nf, id='work%xze_f')
    if(with_confpot) then
       call f_zero(work%xza_f)
       call f_zero(work%xzb_f)
       call f_zero(work%xzc_f)
       call f_zero(work%xze_f)
    end if

    work%yza_f = f_malloc_ptr(3 * work%nf, id='work%yza_f')
    work%yzb_f = f_malloc_ptr(4 * work%nf, id='work%yzb_f')
    work%yzc_f = f_malloc_ptr(3 * work%nf, id='work%yzc_f')
    work%yze_f = f_malloc_ptr(4 * work%nf, id='work%yze_f')
    if(with_confpot) then
       call f_zero(work%yza_f)
       call f_zero(work%yzb_f)
       call f_zero(work%yzc_f)
       call f_zero(work%yze_f)
    end if

  end subroutine initialize_quartic_sized

  subroutine initialize_work_arrays_quartic(lr, with_confpot, work)
    use locregs
    implicit none

    ! Calling arguments
    type(locreg_descriptors), intent(in) :: lr
    logical, intent(in):: with_confpot
    type(workarr_quartic_convolutions), intent(out):: work

    ! Local variables
    integer :: n1, n2, n3

    n1 = lr%mesh_coarse%ndims(1)-1
    n2 = lr%mesh_coarse%ndims(2)-1
    n3 = lr%mesh_coarse%ndims(3)-1

    call initialize_quartic_sized(work, lr%mesh_coarse%ndim, &
         int(lr%nboxf(2,1) - lr%nboxf(1,1) + 1, f_long) * &
         int(lr%nboxf(2,2) - lr%nboxf(1,2) + 1, f_long) * &
         int(lr%nboxf(2,3) - lr%nboxf(1,3) + 1, f_long), max(n1,n2,n3), with_confpot)
  end subroutine initialize_work_arrays_quartic

  subroutine initialize_work_arrays_quartic_llr(llr, work)
    use locregs
    implicit none

    ! Calling arguments
    type(locreg_descriptors), dimension(:), intent(in) :: llr
    type(workarr_quartic_convolutions), intent(out):: work

    ! Local variables
    integer :: ilr
    integer, dimension(3) :: nfs

    call nullify_work_arrays_quartic(work)
    work%nc = 0
    work%nf = 0
    work%ncmax = 0
    do ilr = 1, size(llr)
       work%nc = max(work%nc, llr(ilr)%mesh_coarse%ndim)
       nfs(1) = llr(ilr)%nboxf(2,1) - llr(ilr)%nboxf(1,1)
       nfs(2) = llr(ilr)%nboxf(2,2) - llr(ilr)%nboxf(1,2)
       nfs(3) = llr(ilr)%nboxf(2,3) - llr(ilr)%nboxf(1,3)
       work%nf = max(work%nf, (nfs(1)+1)*(nfs(2)+1)*(nfs(3)+1))
       work%ncmax = max(work%ncmax, max(llr(ilr)%mesh_coarse%ndims(1), &
            llr(ilr)%mesh_coarse%ndims(2), llr(ilr)%mesh_coarse%ndims(3)) - 1)
    end do
  end subroutine initialize_work_arrays_quartic_llr

  subroutine nullify_work_arrays_quartic(w)
    implicit none
    type(workarr_quartic_convolutions), intent(out) :: w

    nullify(w%xx_c)
    nullify(w%xy_c)
    nullify(w%xz_c)

    nullify(w%xx_f1)
    nullify(w%xx_f)


    nullify(w%xy_f2)
    nullify(w%xy_f)


    nullify(w%xz_f4)
    nullify(w%xz_f)


    nullify(w%y_c)

    nullify(w%y_f)

    nullify(w%aeff0array)
    nullify(w%beff0array)
    nullify(w%ceff0array)
    nullify(w%eeff0array)

    nullify(w%aeff0_2array)
    nullify(w%beff0_2array)
    nullify(w%ceff0_2array)
    nullify(w%eeff0_2array)

    nullify(w%aeff0_2auxarray)
    nullify(w%beff0_2auxarray)
    nullify(w%ceff0_2auxarray)
    nullify(w%eeff0_2auxarray)

    nullify(w%xya_c)
    nullify(w%xyc_c)

    nullify(w%xza_c)
    nullify(w%xzc_c)

    nullify(w%yza_c)
    nullify(w%yzb_c)
    nullify(w%yzc_c)
    nullify(w%yze_c)

    nullify(w%xya_f)
    nullify(w%xyb_f)
    nullify(w%xyc_f)
    nullify(w%xye_f)

    nullify(w%xza_f)
    nullify(w%xzb_f)
    nullify(w%xzc_f)
    nullify(w%xze_f)

    nullify(w%yza_f)
    nullify(w%yzb_f)
    nullify(w%yzc_f)
    nullify(w%yze_f)

  end subroutine nullify_work_arrays_quartic
  
  subroutine reset_work_arrays_quartic(work, lr, with_confpot)
    use locregs
    use dynamic_memory
    use liborbs_errors
    use f_utils
    implicit none

    ! Calling arguments
    type(workarr_quartic_convolutions), intent(inout) :: work
    type(locreg_descriptors), intent(in) :: lr
    logical, intent(in) :: with_confpot
    ! Local variables
    integer :: n1, n2, n3, ncmax
    integer(f_long) :: nf, nc

    call f_routine(id='zero_local_work_arrays')

    if (.not. associated(work%xx_c)) then
       nc=work%nc
       nf=work%nf
       ncmax=work%ncmax
       call initialize_quartic_sized(work, nc, nf, ncmax, with_confpot)
    else
       call f_zero(work%xx_c)
       call f_zero(work%xy_c)
       call f_zero(work%xz_c)

       call f_zero(work%xx_f1)
       call f_zero(work%xx_f)


       call f_zero(work%xy_f2)
       call f_zero(work%xy_f)


       call f_zero(work%xz_f4)
       call f_zero(work%xz_f)


       call f_zero(work%y_c)

       call f_zero(work%y_f)

       call f_zero(work%aeff0array)
       call f_zero(work%beff0array)
       call f_zero(work%ceff0array)
       call f_zero(work%eeff0array)

       call f_zero(work%aeff0_2array)
       call f_zero(work%beff0_2array)
       call f_zero(work%ceff0_2array)
       call f_zero(work%eeff0_2array)

       call f_zero(work%aeff0_2auxarray)
       call f_zero(work%beff0_2auxarray)
       call f_zero(work%ceff0_2auxarray)
       call f_zero(work%eeff0_2auxarray)

       if(with_confpot) then
          call f_zero(work%xya_c)
          call f_zero(work%xyc_c)
       end if

       if(with_confpot) then
          call f_zero(work%xza_c)
          call f_zero(work%xzc_c)
       end if

       if(with_confpot) then
          call f_zero(work%yza_c)
          call f_zero(work%yzb_c)
          call f_zero(work%yzc_c)
          call f_zero(work%yze_c)
       end if

       if(with_confpot) then
          call f_zero(work%xya_f)
          call f_zero(work%xyb_f)
          call f_zero(work%xyc_f)
          call f_zero(work%xye_f)
       end if

       if(with_confpot) then
          call f_zero(work%xza_f)
          call f_zero(work%xzb_f)
          call f_zero(work%xzc_f)
          call f_zero(work%xze_f)
       end if

       if(with_confpot) then
          call f_zero(work%yza_f)
          call f_zero(work%yzb_f)
          call f_zero(work%yzc_f)
          call f_zero(work%yze_f)
       end if
    end if
    
    n1 = lr%mesh_coarse%ndims(1)-1
    n2 = lr%mesh_coarse%ndims(2)-1
    n3 = lr%mesh_coarse%ndims(3)-1

    nf = (lr%nboxf(2,1) - lr%nboxf(1,1) + 1) * &
         (lr%nboxf(2,2) - lr%nboxf(1,2) + 1) * &
         (lr%nboxf(2,3) - lr%nboxf(1,3) + 1)

    if (f_err_raise(lr%mesh_coarse%ndim > size(work%xx_c), &
         "too small nc size in quartic reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
         f_err_raise(nf > size(work%xx_f1), &
         "too small nf size in quartic reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
         f_err_raise(max(n1,n2,n3)+1 > size(work%aeff0array, 2), &
         "too small ncmax size in quartic reset", err_id = LIBORBS_LOCREG_ERROR())) return

    work%nc = lr%mesh_coarse%ndim
    work%nf = nf
    work%ncmax = max(n1,n2,n3)

    call f_release_routine()

  end subroutine reset_work_arrays_quartic

  pure function workarrays_tensor_prod_null() result(wtp)
    implicit none
    type(workarr_tensor_prod) :: wtp
    call nullify_workarrays_tensor_prod(wtp)
  end function workarrays_tensor_prod_null

  pure subroutine nullify_workarrays_tensor_prod(wtp)
    implicit none
    type(workarr_tensor_prod),intent(out) :: wtp
    nullify(wtp%wprojx)
    nullify(wtp%wprojy)
    nullify(wtp%wprojz)
    nullify(wtp%work)
  end subroutine nullify_workarrays_tensor_prod

  subroutine initialize_work_arrays_tensor_prod(lr, nspinor, ncplx, w, maxkbsize, nterms)
    use locregs
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    type(workarr_tensor_prod), intent(out) :: w
    integer, intent(in) :: nspinor, ncplx
    integer, intent(in), optional :: maxkbsize, nterms

    call nullify_workarrays_tensor_prod(w)
    call allocate_workarrays_tensor_prod(lr%mesh_coarse%ndims, nspinor, ncplx, w, &
         maxkbsize, nterms)
  end subroutine initialize_work_arrays_tensor_prod

  subroutine initialize_work_arrays_tensor_prod_llr(llr, w)
    use locregs
    implicit none
    type(locreg_descriptors), dimension(:), intent(in) :: llr
    type(workarr_tensor_prod), intent(out) :: w

    integer :: ilr

    call nullify_workarrays_tensor_prod(w)
    w%ndims = 0
    do ilr = 1, size(llr)
       w%ndims = max(w%ndims, llr(ilr)%mesh_coarse%ndims)
    end do
  end subroutine initialize_work_arrays_tensor_prod_llr

  subroutine reset_work_arrays_tensor_prod(w, lr, nspinor, ncplx, maxkbsize, nterms)
    use liborbs_errors
    use locregs
    implicit none
    type(workarr_tensor_prod), intent(inout) :: w
    type(locreg_descriptors), intent(in) :: lr
    integer, intent(in) :: nspinor, ncplx
    integer, intent(in), optional :: maxkbsize, nterms

    if (.not. associated(w%work)) then
       call allocate_workarrays_tensor_prod(w%ndims, nspinor, ncplx, w, maxkbsize, nterms)
    end if

    if (f_err_raise(w%ncplx * lr%mesh_coarse%ndims(1) * 2 * w%nterms > size(w%wprojx, 1), &
         "too small x size in proj reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
         f_err_raise(w%ncplx * lr%mesh_coarse%ndims(2) * 2 * w%nterms > size(w%wprojy, 1), &
         "too small y size in proj reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
         f_err_raise(w%ncplx * lr%mesh_coarse%ndims(3) * 2 * w%nterms > size(w%wprojz, 1), &
         "too small z size in proj reset", err_id = LIBORBS_LOCREG_ERROR()) .or. &
         f_err_raise(ncplx > w%ncplx, &
         "too small cplx in proj reset", err_id = LIBORBS_LOCREG_ERROR())) return
    w%ndims = lr%mesh_coarse%ndims
  end subroutine reset_work_arrays_tensor_prod

  subroutine allocate_workarrays_tensor_prod(ndims, nspinor, ncplx, wp, maxkbsize, nterms)
    use dynamic_memory
    implicit none
    integer, dimension(3), intent(in) :: ndims
    integer, intent(in) :: nspinor, ncplx
    type(workarr_tensor_prod),intent(inout) :: wp
    integer, intent(in), optional :: maxkbsize, nterms
    integer, parameter :: nw = 131072

    integer :: nterms_

    if (present(maxkbsize)) then
       nterms_ = max(maxkbsize * 1024 / (2 * ncplx * maxval(ndims)), 20)
    else
       nterms_ = max(2048 * 1024 / (2 * ncplx * maxval(ndims)), 20)
    end if
    if (present(nterms)) nterms_ = nterms
    
    wp%ndims = ndims
    wp%ncplx = ncplx
    wp%nterms = nterms_
    wp%wprojx = f_malloc_ptr((/ ncplx * ndims(1) * 2 * nterms_, max(1, nspinor / ncplx) /), id='wprojx')
    wp%wprojy = f_malloc_ptr((/ ncplx * ndims(2) * 2 * nterms_ /), id='wprojy')
    wp%wprojz = f_malloc_ptr((/ ncplx * ndims(3) * 2 * nterms_ /), id='wprojz')
    wp%work = f_malloc_ptr((/ 0.to.nw, 1.to.2, 1.to.ncplx /),id='work')
  end subroutine allocate_workarrays_tensor_prod

  subroutine deallocate_work_arrays_tensor_prod(wtp)
    use dynamic_memory
    implicit none
    type(workarr_tensor_prod),intent(inout) :: wtp
    call f_free_ptr(wtp%wprojx)
    call f_free_ptr(wtp%wprojy)
    call f_free_ptr(wtp%wprojz)
    call f_free_ptr(wtp%work)
  end subroutine deallocate_work_arrays_tensor_prod

  end module liborbs_workarrays
