cmake_minimum_required(VERSION 3.2.0)
project(ATlab
  VERSION 1.0.0
  DESCRIPTION "The atlab library handles the atomic positions and much more"
  #HOMEPAGE_URL "https://gitlab.com/L_Sim/atlab" new in 3.12
  LANGUAGES Fortran C CXX)
option(BUILD_STATIC_LIBS "Build a static library" OFF)
option(BUILD_SHARED_LIBS "Build a shared library" ON)
option(BUILD_OPENMP "Build with OpenMP flag if the compiler support it" ON)
if(NOT CMAKE_BUILD_TYPE)
  # Does not work as an option.
  set(CMAKE_BUILD_TYPE "RelWithDebInfo" CACHE STRING "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel." FORCE)
endif()

find_package(OpenMP)
find_package(PkgConfig REQUIRED)
pkg_check_modules(FUTILE REQUIRED futile>=1.8)
pkg_check_modules(BABEL openbabel-2.0>=2.1.0)
pkg_check_modules(ETSFIO etsf-io>=1.0.0)

# Super hack because <XXX>_LINK_LIBRARIES is included in 3.12
if(FUTILE_FOUND AND NOT FUTILE_LINK_LIBRARIES)
  foreach(lib ${FUTILE_LIBRARIES})
    if(EXISTS ${FUTILE_LIBDIR}/lib${lib}.so)
      list(APPEND FUTILE_LINK_LIBRARIES ${FUTILE_LIBDIR}/lib${lib}.so)
    elseif(EXISTS ${FUTILE_LIBDIR}/lib${lib}.a)
      list(APPEND FUTILE_LINK_LIBRARIES ${FUTILE_LIBDIR}/lib${lib}.a)
    endif()
  endforeach(lib)
endif()
if(BABEL_FOUND AND NOT BABEL_LINK_LIBRARIES)
  foreach(lib ${BABEL_LIBRARIES})
    if(EXISTS ${BABEL_LIBDIR}/lib${lib}.so)
      list(APPEND BABEL_LINK_LIBRARIES ${BABEL_LIBDIR}/lib${lib}.so)
    elseif(EXISTS ${BABEL_LIBDIR}/lib${lib}.a)
      list(APPEND BABEL_LINK_LIBRARIES ${BABEL_LIBDIR}/lib${lib}.a)
    endif()
  endforeach(lib)
endif()
if(ETSFIO_FOUND AND NOT ETSFIO_LINK_LIBRARIES)
  foreach(lib ${ETSFIO_LIBRARIES})
    if(EXISTS ${ETSFIO_LIBDIR}/lib${lib}.so)
      list(APPEND ETSFIO_LINK_LIBRARIES ${ETSFIO_LIBDIR}/lib${lib}.so)
    elseif(EXISTS ${ETSFIO_LIBDIR}/lib${lib}.a)
      list(APPEND ETSFIO_LINK_LIBRARIES ${ETSFIO_LIBDIR}/lib${lib}.a)
    endif()
  endforeach(lib)
endif()

# Hack for bounds check in debug and relwithdebinfo build types.
if(CMAKE_Fortran_COMPILER_ID MATCHES "GNU")
    set(bounds "-fbounds-check")
elseif(CMAKE_Fortran_COMPILER_ID MATCHES "Intel")
    set(bounds "-check bounds")
elseif(CMAKE_Fortran_COMPILER_ID MATCHES "PGI")
    set(bounds "-C")
endif()
if(bounds)
  set(CMAKE_Fortran_FLAGS_DEBUG "${CMAKE_Fortran_FLAGS_DEBUG} ${bounds}")
  set(CMAKE_Fortran_FLAGS_RELWITHDEBINFO "${CMAKE_Fortran_FLAGS_RELWITHDEBINFO} ${bounds}")
endif()

# The sets here are not necessary if atlab.in is following cmake namings.
set(prefix ${CMAKE_INSTALL_PREFIX})
set(exec_prefix ${CMAKE_INSTALL_PREFIX})
set(libdir ${CMAKE_INSTALL_PREFIX}/lib)
set(includedir ${CMAKE_INSTALL_PREFIX}/include)
set(LIB_ATLAB_LIBS -latlab)
if(NOT BUILD_SHARED_LIBS)
  set(LIB_ATLAB_LIBS "${LIB_ATLAB_LIBS} ${FUTILE_LINK_LIBRARIES}")
  if(BUILD_OPENMP AND OpenMP_FOUND)
    # Export OpenMP libs when statically built so OpenMP flag is not enforced
    # on code using ATlab.
    foreach(omplib ${OpenMP_Fortran_LIBRARIES})
      set(LIB_ATLAB_LIBS "${LIB_ATLAB_LIBS} ${omplib}")
    endforeach(omplib)
  endif()
  if(BABEL_FOUND)
    set(LIB_ATLAB_LIBS "${LIB_ATLAB_LIBS} ${BABEL_LINK_LIBRARIES} -stdc++")
  endif()
  if(ETSFIO_FOUND)
    set(LIB_ATLAB_LIBS "${LIB_ATLAB_LIBS} ${ETSFIO_LINK_LIBRARIES}")
  endif()
endif()
set(VERSION ${PROJECT_VERSION})
configure_file(atlab.pc.in ${CMAKE_BINARY_DIR}/atlab.pc @ONLY)
install(FILES ${CMAKE_BINARY_DIR}/atlab.pc DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/pkgconfig)

find_package(PythonInterp)
pkg_get_variable(f_pythondir futile pythondir)
if(f_pythondir)
  # This is a hack to avoid pythondir to disappear zhen build system
  # is automatically rebuilt from make on CMakeLists.txt changes.
  set(FUTILE_PYTHONDIR ${f_pythondir} CACHE INTERNAL "Python directory for Futile.")
endif()
add_custom_target(report ${PYTHON_EXECUTABLE} ${FUTILE_PYTHONDIR}/report.py)

add_subdirectory(src)
add_subdirectory(python)
add_subdirectory(tests)

set(CPACK_SOURCE_GENERATOR "TBZ2")
# To be removed in 3.12
set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})
# To avoid having -Source appended to filename.
set(CPACK_SOURCE_PACKAGE_FILE_NAME ${PROJECT_NAME}-${PROJECT_VERSION})
set(CPACK_SOURCE_IGNORE_FILES
  "${PROJECT_NAME}-.*$"
  "^${PROJECT_SOURCE_DIR}/tmp[^/]*/"
  "^${PROJECT_SOURCE_DIR}/.git/"
  ".gitlab-ci.yml"
  ".gitignore"
  "/debian/"
  "Makefile.in$"
  "^${PROJECT_SOURCE_DIR}/config"
  "^${PROJECT_SOURCE_DIR}/autom4te.cache"
)
include(CPack)
add_custom_target(dist COMMAND ${CMAKE_MAKE_PROGRAM} package_source)
