Quadrature scheme for accurate integrals: the :f:mod:`multipole_preserving` module
==================================================================================

.. f:automodule:: multipole_preserving
