Memory handling
===============

Dynamic allocation / deallocation
---------------------------------

.. f:automodule:: module_f_malloc
.. f:automodule:: dynamic_memory_base


Memory initialisers
-------------------

.. f:automodule:: f_utils
