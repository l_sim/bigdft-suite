subroutine test_alloc_int_1d(ptr, size)
  use dynamic_memory
  use f_precisions, only: f_long
  integer, dimension(:), pointer :: ptr
  integer(kind = f_long), intent(out) :: size
  integer :: i

  size = 5
  ptr = f_malloc_ptr(size)
  do i = 1, int(size)
     ptr(i) = i
  end do
end subroutine test_alloc_int_1d

subroutine test_alloc_dbl_2d(ptr, size)
  use dynamic_memory
  use f_precisions, only: f_double, f_long
  real(kind = f_double), dimension(:,:), pointer :: ptr
  integer(kind = f_long), dimension(2), intent(out) :: size
  integer :: i, s(2)

  size = (/ 3, 5 /)
  s = (/ 3, 5 /)
  ptr = f_malloc_ptr(s)
  do i = 1, int(size(2))
     ptr(:, i) = real(i, f_double)
  end do
end subroutine test_alloc_dbl_2d
