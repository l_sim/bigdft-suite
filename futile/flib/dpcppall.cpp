///@file
///    File which implements dpcpp (sycl) functionalities equivalent to 
///    cudall.cu file.
///    Note that all functions should start with 'dpcpp'.
///    Also note that iso_c_binding is used on these functions, i.e., 
///    no trailing underscores are utilized in the function names.
///    Note that the current interface is the same as the cuda (cudall.cu)
///    interfaces (giving everything by reference). In the future, this may 
///    be changed to have arguments by value as well.
///@author
///    Copyright (C) 2022 Intel
///    This file is distributed under the terms of the
///    GNU General Public License, see ~/COPYING file
///    or http://www.gnu.org/copyleft/gpl.txt .
///    Last changed by Christoph Bauinger

//only compile sycl code if the correct compiler is used, otherwise dummy functions are compiled below
#if defined(SYCL_LANGUAGE_VERSION) //&& defined (__INTEL_LLVM_COMPILER)

#include <sycl/sycl.hpp>
#include <dpct/dpct.hpp>
#include <dpct/blas_utils.hpp>
#include <oneapi/mkl.hpp>
#include <iostream>
#include <exception>





// Our simple asynchronous handler function taken from James Reinders, 
// "Mastering DPC++ for Programming of Heterogeneous Systems using C++ and SYCL" 
auto handle_async_error = [](sycl::exception_list elist) {
    for (auto &e : elist) {
        try{ std::rethrow_exception(e); }
        catch ( sycl::exception& e ) {
            std::cout << "ASYNC EXCEPTION!!\n";
            std::cout << e.what() << "\n";
        }
    }
};




sycl::queue* Q = nullptr;


//put all the functions as 'extern "C" here'
extern "C" void create_queue();
extern "C" void destroy_queue();
extern "C" void dpcpp_synchronize(); 
extern "C" void dpcpp_malloc(const int nelems, double ** __restrict__ d_data, 
    int * const __restrict__ ierr);
extern "C" void dpcpp_memset(double * const __restrict__ d_data, 
    const int value, const int nelems, int * const __restrict__ ierr);
extern "C" void dpcpp_free(double ** __restrict__ d_data);
extern "C" void dpcpp_HtoD(const int nelems, double const * const __restrict__ h_data, 
    double * const __restrict__ d_data);
extern "C" void dpcpp_DtoD(const int nelems, double* const __restrict__ dest_data, 
    double const * const __restrict__ send_data);
extern "C" void dpcpp_DtoH(const int nelems, double * const __restrict__ h_data, 
    double const * const __restrict__ d_data);
extern "C" void dpcpp_get_mem_info(size_t* const __restrict__ freeSize, 
    size_t* const __restrict__ totalSize);
extern "C" void dpcpp_poisson_daxpy(const int size, const double alpha, 
    double const* const __restrict__ d_x, const int facx, 
    double * const __restrict__ d_y, const int facy, const int offset_y);
extern "C" void dpcpp_getdevicecount(const int where, int * const __restrict__ num_devices);
extern "C" void dpcpp_setdevice(const int where, const int device);




//////////////////////////DEFINITIONS OF SYCL FUNCTIONS BELOW THIS//////////////
// void create_queue()
// {
//     if (Q != nullptr)
//         delete Q;

//     try
//     {
//         Q = new sycl::queue(sycl::gpu_selector_v, handle_async_error, sycl::property::queue::in_order());
//     }
//     catch (...)
//     {
//         std::cout << "ERROR: No such SYCL device available in create_queue\n";
//         exit(1);
//     }
// }


void destroy_queue()
{
    if (Q != nullptr)
    {
        delete Q;
        Q = nullptr;
    }
}


///Function equivalent to cuda 'synchronize' function
void dpcpp_synchronize() 
{
    Q->wait();
}


///Function equivalent to cudamalloc function for device allocations
void dpcpp_malloc(const int nelems, 
    double ** __restrict__ d_data, int * const __restrict__ ierr)
{
    try
    {
        (*d_data) = sycl::malloc_device<double>(nelems, *Q);
        Q->memset(*d_data, 0, nelems*sizeof(double)); //0 result
    }
    catch (...)
    {
        *ierr = 1;
    }
}


///Function equivalent to cudamemset
void dpcpp_memset(double * const __restrict__ d_data, 
    const int value, const int nelems, int * const __restrict__ ierr)
{
    try
    {
        Q->memset(d_data, value, sizeof(double) * nelems);
    }
    catch (...)
    {
        *ierr = 1;
    }
}


///Function equivalent to cudafree
void dpcpp_free(double ** __restrict__ d_data)
{
    sycl::free(*d_data, *Q);
    d_data = nullptr;
}


///Function equivalent to reset_gpu_data
void dpcpp_HtoD(const int nelems, double const * const __restrict__ h_data, 
    double * const __restrict__ d_data)
{
    try
    {
        Q->memcpy(d_data, h_data, nelems*sizeof(double));
    }
    catch (...)
    {
        std::cout << "dpcpp_HtoD throws exception.\n";
        exit(1);
    }
}


///Function equivalent to copy_gpu_data
void dpcpp_DtoD(const int nelems, double* const __restrict__ dest_data, 
    double const * const __restrict__ send_data)
{
    try
    {
        Q->memcpy(dest_data, send_data, nelems*sizeof(double));
    }
    catch (...)
    {
        std::cout << "dpcpp_DtoD throws exception.\n";
        exit(1);
    }
}


///Function equivalent to get_gpu_data
void dpcpp_DtoH(const int nelems, double * const __restrict__ h_data, 
    double const * const __restrict__ d_data)
{
    try
    {
        Q->memcpy(h_data, d_data, nelems*sizeof(double));
    }
    catch (...)
    {
        std::cout << "dpcpp_DtoH throws exception.\n";
        exit(1);
    }
}


///Function equivalent to cuda_get_mem_info
///ATTENTION: using intel sycl extension for free memory.
///If this moves to SYCL standard, change sycl::ext::intel::info::device::free_memory
///to sycl::info::device::free_memory
void dpcpp_get_mem_info(size_t* const __restrict__ freeSize, 
    size_t* const __restrict__ totalSize)
{
    try
    {
        *totalSize = Q->get_device().get_info<sycl::info::device::global_mem_size>();
        //*freeSize = Q->get_device().get_info<sycl::ext::intel::info::device::free_memory>();
        *freeSize = *totalSize; //for now, note that this is not very safe
    }
    catch (sycl::exception& e)
    {
        std::cout << "SYCL exception (dpcpp_get_mem_info): " << e.what() << std::endl;
        exit(1);
    }
    catch (std::exception& e)
    {
        std::cout << "STD exception (dpcpp_get_mem_info): " << e.what() << std::endl;
        exit(1);
    }
}

///Function equivalent of poisson_cublas_daxpy. Should call onemkl equivalent.
void dpcpp_poisson_daxpy(const int size, const double alpha, 
    double const* const __restrict__ d_x, const int facx, 
    double * const __restrict__ d_y, const int facy, const int offset_y)
{
    oneapi::mkl::blas::column_major::axpy(*Q, size, alpha, 
        d_x, facx, d_y + offset_y, facy);
}


///Function equivalent to cudagetdevicecount
/// @param[in] : where tell the function if GPU (where = 0) or CPU (where = 1)
void dpcpp_getdevicecount(const int where, int * const __restrict__ num_devices)
{
    try
    {
        if (where == 0)
        {
            auto P = sycl::platform(sycl::gpu_selector_v);
            *num_devices = P.get_devices(sycl::info::device_type::gpu).size();
            if ((*num_devices) == 0) throw std::runtime_error("No GPU for DPC++ version available.");
        }
        else if (where == 1)
        {
            auto P = sycl::platform(sycl::cpu_selector_v);
            *num_devices = P.get_devices(sycl::info::device_type::cpu).size();
            if ((*num_devices) == 0) throw std::runtime_error("No CPU for DPC++ version available.");
        }
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
}


///Function equivalent to cudasetdevice. 
///This function generates the sycl queue used for all the other operations.
///Note that in cuda, the stream and device needs to fit together but are chosen/created
///independently. In DPC++ we only need one queue.
/// @param[in] : where tell the function if GPU (where = 0) or CPU (where = 1)
/// @param[in] : device says the device number (GPU 0, 1, 2, or 3 if there are 4 GPUs available.)
///ATTENTION: right now I am using an in-order queue as equivalent to the stream in cuda
void dpcpp_setdevice(const int where, const int device)
{    
    if (Q != nullptr)
        delete Q;

    try
    {
        if (where == 0)
        {
            auto P = sycl::platform(sycl::gpu_selector_v);
            auto RootDevices = P.get_devices(sycl::info::device_type::gpu);
            Q = new sycl::queue(RootDevices[device], handle_async_error, sycl::property::queue::in_order());
        }
        else if (where == 1)
        {
            auto P = sycl::platform(sycl::cpu_selector_v);
            auto RootDevices = P.get_devices(sycl::info::device_type::cpu);
            Q = new sycl::queue(RootDevices[device], handle_async_error, sycl::property::queue::in_order());
        }
        else
            throw std::invalid_argument("dpcpp_setdevice with invalid where called.");


        //std::cout << "Running on Device: " << Q->get_device().get_info<sycl::info::device::name>() << ", device " << device << std::endl;
    }
    catch(...)
    {
        std::cout << "ERROR: No such device found\n";
        exit(1);
    }

    //std::cout << "Setting SYCL devive to ";
    //std::cout << Q->get_device().get_info<sycl::info::device::name>() << std::endl;
}


#else

#include <stdexcept>

//put all the functions as 'extern "C" here'
extern "C"
{
void create_queue() {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void destroy_queue() {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void dpcpp_synchronize() {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void dpcpp_malloc(const int nelems, double ** __restrict__ d_data, 
    int * const __restrict__ ierr) {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void dpcpp_memset(double * const __restrict__ d_data, 
    const int value, const int nelems, int * const __restrict__ ierr) {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void dpcpp_free(double ** __restrict__ d_data) {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void dpcpp_HtoD(const int nelems, double const * const __restrict__ h_data, 
    double * const __restrict__ d_data) {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void dpcpp_DtoD(const int nelems, double* const __restrict__ dest_data, 
    double const * const __restrict__ send_data) {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void dpcpp_DtoH(const int nelems, double * const __restrict__ h_data, 
    double const * const __restrict__ d_data) {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void dpcpp_get_mem_info(size_t* const __restrict__ freeSize, 
    size_t* const __restrict__ totalSize) {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void dpcpp_poisson_daxpy(const int size, const double alpha, 
    double const* const __restrict__ d_x, const int facx, 
    double * const __restrict__ d_y, const int facy, const int offset_y) {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void dpcpp_getdevicecount(const int where, int * const __restrict__ num_devices) {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
void dpcpp_setdevice(const int where, const int device) {throw std::runtime_error("Calling dpc++ function but code was not compiled with dpc++.");}
}

#endif 
