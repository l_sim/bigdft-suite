!> @file
!!   Basic routines for converting between CheSS and NTPoly.
!! @author
!!   Copyright (C) 2016 CheSS developers
!!
!!   This file is part of CheSS.
!!   
!!   CheSS is free software: you can redistribute it and/or modify
!!   it under the terms of the GNU Lesser General Public License as published by
!!   the Free Software Foundation, either version 3 of the License, or
!!   (at your option) any later version.
!!   
!!   CheSS is distributed in the hope that it will be useful,
!!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!   GNU Lesser General Public License for more details.
!!   
!!   You should have received a copy of the GNU Lesser General Public License
!!   along with CheSS.  If not, see <http://www.gnu.org/licenses/>.

!> Convert a CheSS matrix type to NTPoly type
subroutine chess_to_ntpoly(iproc, nproc, comm, chess_mat, chess_val, &
    & ntpolymat, ispin)
  use futile
  use psmatrixmodule, only : matrix_ps, fillmatrixfromtripletlist, &
     constructemptymatrix
  use tripletlistmodule, only : tripletlist_r, constructtripletlist, &
     destructtripletlist, appendtotripletList
  use tripletmodule, only : triplet_r
  use sparsematrix_base
  use sparsematrix, only : gather_matrix_from_taskgroups
  use sparsematrix_timing
  !> Process information
  integer, intent(in) :: iproc, nproc, comm
  !> Input CheSS sparse matrix type
  type(sparse_matrix), intent(in):: chess_mat
  !> Input CheSS matrix of data.
  type(matrices), intent(in) :: chess_val
  !> The output matrix.
  type(matrix_ps), intent(inout) :: ntpolymat
  !> The spin to convert
  integer, intent(in) :: ispin
  ! Local variables
  real(kind=mp), dimension(:), allocatable :: matrix_compr
  type(tripletlist_r) :: triplet_list
  type(triplet_r) :: trip
  integer :: ind, iseg

  call f_routine(id='chess_to_ntpoly')
  call f_timing(TCAT_NTPOLY_CTN, 'ON')

  ! Gather from the task groups.
  matrix_compr = sparsematrix_malloc(chess_mat, &
       iaction = SPARSE_FULL, id = 'matrix_compr')
  call gather_matrix_from_taskgroups(iproc, nproc, comm, &
       chess_mat, chess_val%matrix_compr, matrix_compr)

  ! Initialize the empty matrix
  call constructemptymatrix(ntpolymat, chess_mat%nfvctr)

  ! We will build the NTPoly matrix from the triplet list.
  call constructtripletlist(triplet_list)

  ! Loop over the segments
  ind = (ispin - 1) * chess_mat%nvctr
  !ind = 0
  do iseg = 1, chess_mat%nseg
    trip%index_column = chess_mat%keyg(1, 2, iseg)
    ! If out of column range, fast forward
    if (trip%index_column .lt. ntpolymat%start_column .or. &
        trip%index_column .ge. ntpolymat%end_column) then
       ind = ind + chess_mat%keyg(2, 1, iseg) - chess_mat%keyg(1, 1, iseg) + 1
    else
       do jorb = chess_mat%keyg(1, 1, iseg), chess_mat%keyg(2, 1, iseg)
         trip%index_row = jorb
         ind = ind + 1
         trip%point_value = matrix_compr(ind)
         if (trip%index_row .ge. ntpolymat%start_row .and. &
             trip%index_row .lt. ntpolymat%end_row) then
           call appendtotripletList(triplet_list, trip)
         end if
       end do
    end if
  end do

  ! Prepartitioned set to true since we have properly accounted for
  ! the location of everything.
  call fillmatrixfromtripletlist(ntpolymat, triplet_list, &
                                 prepartitioned_in=.TRUE.)

  ! Cleanup
  call f_free(matrix_compr)
  call destructtripletlist(triplet_list)
  call f_timing(TCAT_NTPOLY_CTN, 'OFF')
  call f_release_routine()

end subroutine chess_to_ntpoly

!> Convert an ntpoly matrix to CheSS.
subroutine ntpoly_to_chess(iproc, nproc, comm, ntpolymat, chess_mat, &
   chess_val, ispin)
  use futile
  use matrixconversionmodule, only : snapmatrixtosparsitypattern
  use psmatrixmodule, only : matrix_ps, gathermatrixtoprocess, copymatrix, &
     destructmatrix
  use smatrixmodule, only : matrix_lsr, destructmatrix
  use sparsematrix_base
  use sparsematrix_timing
  !> Process information
  integer, intent(in) :: iproc, nproc, comm
  !> The input matrix to convert.
  type(matrix_ps), intent(inout) :: ntpolymat
  !> Output sparse matrix.
  type(sparse_matrix), intent(inout):: chess_mat
  !> Output matrix of values.
  type(matrices), intent(inout) :: chess_val
  !> The spin of the matrix
  integer, intent(in) :: ispin
  ! Local variables
  type(matrix_ps) :: pattern, filtered
  type(matrix_lsr) :: ntlocal
  integer :: nnz
  integer :: shift

  call f_routine(id='ntpoly_to_chess')
  call f_timing(TCAT_NTPOLY_NTC, 'ON')

  ! Build the sparsity pattern.
  call chess_to_ntpoly(iproc, nproc, comm, chess_mat, chess_val, pattern,&
       & ispin)
  call copymatrix(ntpolymat, filtered)

  ! Snap to the sparsity pattern.
  call snapmatrixtosparsitypattern(filtered, pattern)

  ! Gather NTPoly matrix to each process.
  call gathermatrixtoprocess(filtered, ntlocal)

  ! Copy the values.
  shift = (ispin - 1) * chess_mat%nvctr + 1
  nnz = size(ntlocal%values)
  chess_val%matrix_compr(shift:shift + nnz) = ntlocal%values(:)

  ! Cleanup
  call destructmatrix(ntlocal)
  call destructmatrix(pattern)
  call destructmatrix(filtered)

  call f_timing(TCAT_NTPOLY_NTC, 'OFF')
  call f_release_routine()

end subroutine ntpoly_to_chess
