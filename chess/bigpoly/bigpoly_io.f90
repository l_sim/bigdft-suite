!> @file
!!   Basic routines for i/o based on ntpoly.
!! @author
!!   Copyright (C) 2016 CheSS developers
!!
!!   This file is part of CheSS.
!!   
!!   CheSS is free software: you can redistribute it and/or modify
!!   it under the terms of the GNU Lesser General Public License as published by
!!   the Free Software Foundation, either version 3 of the License, or
!!   (at your option) any later version.
!!   
!!   CheSS is distributed in the hope that it will be useful,
!!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!   GNU Lesser General Public License for more details.
!!   
!!   You should have received a copy of the GNU Lesser General Public License
!!   along with CheSS.  If not, see <http://www.gnu.org/licenses/>.


!> Basic routines for i/o based on ntpoly.
module bigpoly_io
  use futile
  use dmatrixmodule, only : matrix_ldr, constructmatrixdfroms, destructmatrix
  use processgridmodule, only : constructprocessgrid, destructprocessgrid
  use psmatrixalgebramodule, only : incrementmatrix
  use psmatrixmodule, only : matrix_ps, constructmatrixfrommatrixmarket, &
     constructmatrixfrombinary, gathermatrixtoprocess, copymatrix, &
     writematrixtomatrixmarket, writematrixtobinary, destructmatrix, &
     getmatrixslice
  use smatrixmodule, only : matrix_lsr, destructmatrix, printmatrix
  use sparsematrix_base, only : sparse_matrix, matrices
  use bigpoly, only : bigpoly_options
  implicit none
  private

  !> Public routines
  public :: bigpoly_read_to_dense
  public :: bigpoly_write_matrix
  public :: bigpoly_write_submatrix

  contains

    !> Read a matrix to a dense local array.
    subroutine bigpoly_read_to_dense(dense, filename, comm, binary)
      !> The dense matrix (preallocated)
      real(8), dimension(:,:) :: dense
      !> The name of the file to read from.
      character(len=*), intent(in) :: filename
      !> The communicator to distribute along.
      integer, intent(in) :: comm
      !> True if it's a binary matrix.
      logical, intent(in) :: binary
      ! Local variables
      type(matrix_ps) :: ntinmat
      type(matrix_lsr) :: local_sparse
      type(matrix_ldr) :: local_dense

      ! Create the process grid
      call constructprocessgrid(comm)

      ! Read NTPoly
      if (binary) then
         call constructmatrixfrombinary(ntinmat, filename)
      else
         call constructmatrixfrommatrixmarket(ntinmat, filename)
      end if

      ! Check that the sizes match.
      if (size(dense, 1) /= ntinmat%actual_matrix_dimension .or. &
          size(dense, 2) /= ntinmat%actual_matrix_dimension) then
         call f_err_throw('Saved file has the wrong dimension.')
      end if


      ! Convert to a dense local matrix
      call gathermatrixtoprocess(ntinmat, local_sparse)
      call constructmatrixdfroms(local_sparse, local_dense)

      ! Extract
      dense = local_dense%data

      ! Cleanup
      call destructmatrix(ntinmat)
      call destructmatrix(local_sparse)
      call destructmatrix(local_dense)

      call destructprocessgrid()
    end subroutine bigpoly_read_to_dense

    !> Write a subset of the matrix to matrix market.
    subroutine bigpoly_write_submatrix(iproc, nproc, comm, inmat, inval, &
        filename, binary, nspin, start_row, end_row, start_column, end_column)
      use yaml_strings, only: f_strcpy
      !> Process information
      integer, intent(in) :: iproc, nproc, comm
      !> Input CheSS sparse matrix type
      type(sparse_matrix), intent(in):: inmat
      !> Input CheSS matrix of data.
      type(matrices), intent(in) :: inval
      !> The name of the file to write to.
      character(len=*), intent(in) :: filename
      !> True if it's a binary matrix.
      logical, intent(in) :: binary
      !> The spin of the system. We need to pass this explicitly because
      !! the overlap matrix internally thinks it is spin=2
      integer, intent(in) :: nspin
      !> Indices of the submatrix to write out.
      integer, intent(in) :: start_row, end_row, start_column, end_column
      
      call bigpoly_write_imp(iproc, nproc, comm, inmat, inval, &
                             filename, binary, nspin, &
                             start_row, end_row, start_column, end_column, &
                             .true.)
    end subroutine bigpoly_write_submatrix

    !> Write a matrix to matrix market.
    subroutine bigpoly_write_matrix(iproc, nproc, comm, inmat, inval, &
        filename, binary, nspin)
      use yaml_strings, only: f_strcpy
      !> Process information
      integer, intent(in) :: iproc, nproc, comm
      !> Input CheSS sparse matrix type
      type(sparse_matrix), intent(in):: inmat
      !> Input CheSS matrix of data.
      type(matrices), intent(in) :: inval
      !> The name of the file to write to.
      character(len=*), intent(in) :: filename
      !> True if it's a binary matrix.
      logical, intent(in) :: binary
      !> The spin of the system. We need to pass this explicitly because
      !! the overlap matrix internally thinks it is spin=2
      integer, intent(in) :: nspin

      call bigpoly_write_imp(iproc, nproc, comm, inmat, inval, &
                             filename, binary, nspin, &
                             0, 0, 0, 0, &
                             .false.)
    end subroutine bigpoly_write_matrix

    !> Implementation of the write routine
    subroutine bigpoly_write_imp(iproc, nproc, comm, inmat, inval, &
        filename, binary, nspin, start_row, end_row, start_column, end_column, &
        submatrix)
      use yaml_strings, only: f_strcpy
      integer, intent(in) :: iproc, nproc, comm
      type(sparse_matrix), intent(in):: inmat
      type(matrices), intent(in) :: inval
      character(len=*), intent(in) :: filename
      logical, intent(in) :: binary
      integer, intent(in) :: nspin
      integer, intent(in) :: start_row, end_row, start_column, end_column
      logical, intent(in) :: submatrix
      type(matrix_ps) :: ntinmat, summat, slicemat
      logical :: serial
      integer :: ispin
      character(len=10) :: sname

      call f_routine(id='bigpoly_write_imp')

      ! Create the process grid
      call constructprocessgrid(comm, process_slices_in=1)

      serial = bigpoly_options//'serial_io' 
      if (nspin .eq. 1) then
        call chess_to_ntpoly(iproc, nproc, comm, inmat, inval, ntinmat, 1)
        if (submatrix) then
          call getmatrixslice(ntinmat, slicemat, &
               & start_row, end_row, start_column, end_column)
        else
          call copymatrix(ntinmat, slicemat)
        end if
        call bigpoly_write_helper(slicemat, trim(filename) // ".mtx", &
             & iproc, serial, binary)
      else
        do ispin = 1, nspin
          call chess_to_ntpoly(iproc, nproc, comm, inmat, inval, ntinmat, &
                               ispin)
          if (submatrix) then
            call getmatrixslice(ntinmat, slicemat, &
                 & start_row, end_row, start_column, end_column)
          else
            call copymatrix(ntinmat, slicemat)
          end if
          if (ispin .eq. 1) then
            call f_strcpy(dest=sname, src="_alpha.mtx")
            call copymatrix(slicemat, summat)
          else
            call f_strcpy(dest=sname, src="_beta.mtx")
            call incrementmatrix(slicemat, summat)
          end if
          call bigpoly_write_helper(slicemat, trim(trim(filename) // sname), &
               & iproc, serial, binary)
        end do
        ! Write the sum
        call bigpoly_write_helper(summat, trim(filename) // ".mtx", &
             & iproc, serial, binary)
      end if

      ! Cleanup
      call destructmatrix(ntinmat)
      call destructmatrix(summat)
      call destructmatrix(slicemat)
      call destructprocessgrid()

      call f_release_routine()
    end subroutine bigpoly_write_imp

    subroutine bigpoly_write_helper(mat, filename, iproc, serial, binary)
      !> The matrix to write
      type(matrix_ps) :: mat
      !> The filename to write
      character(len=*), intent(in) :: filename
      !> Process information
      integer, intent(in) :: iproc
      !> Whether to write in parallel or serial
      logical, intent(in) :: serial
      !> True if it's a binary matrix.
      logical, intent(in) :: binary
      type(matrix_lsr) :: lmat

      if (.not. serial) then
         if (binary) then
            call writematrixtobinary(mat, filename)
         else
            call writematrixtomatrixmarket(mat, filename)
         end if
      else
         call gathermatrixtoprocess(mat, lmat)
         if (iproc .eq. 0) call printmatrix(lmat, filename)
      end if

      call destructmatrix(lmat)
    end subroutine

end module bigpoly_io
