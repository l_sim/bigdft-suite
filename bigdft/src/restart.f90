!> @file
!!  Routines to do restart of the calculation
!! @author
!!    Copyright (C) 2007-2013 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS


!> Copy old wavefunctions from psi to psi_old
subroutine copy_old_wavefunctions(nproc,orbs,psi,&
     lr_old,psi_old)
  use module_precisions
  use module_types
  use module_bigdft_output
  use locregs
  use module_bigdft_arrays
  use module_bigdft_profiling
  implicit none
  integer, intent(in) :: nproc
  type(orbitals_data), intent(in) :: orbs
  type(locreg_descriptors), intent(in) :: lr_old
  real(wp), dimension(:), pointer :: psi,psi_old
  !Local variables
  character(len=*), parameter :: subname='copy_old_wavefunctions'
  !real(kind=8), parameter :: eps_mach=1.d-12
  integer :: j,ind1,iorb,oidx,sidx !n(c) nvctrp_old
  real(kind=8) :: tt
  call f_routine(id=subname)

  !add the number of distributed point for the compressed wavefunction
  !tt=dble(wfd_old%nvctr_c+7*wfd_old%nvctr_f)/dble(nproc)
  !n(c) nvctrp_old=int((1.d0-eps_mach*tt) + tt)

!  psi_old=&
!       f_malloc_ptr((wfd_old%nvctr_c+7*wfd_old%nvc_f)*orbs%norbp*orbs%nspinor,!&
!       id='psi_old')
  psi_old = f_malloc_ptr(array_dim(lr_old)*orbs%norbp*orbs%nspinor,id='psi_old')

  do iorb=1,orbs%norbp
     tt=0.d0
     oidx=(iorb-1)*orbs%nspinor+1
     do sidx=oidx,oidx+orbs%nspinor-1
        do j=1,array_dim(lr_old)
           ind1=j+array_dim(lr_old)*(sidx-1)
           psi_old(ind1)= psi(ind1)
           tt=tt+real(psi(ind1),kind=8)**2
        enddo
     end do

     tt=sqrt(tt)
     if (abs(tt-1.d0) > 1.d-8) then
        call yaml_warning('wrong psi_old' // trim(yaml_toa(iorb)) // trim(yaml_toa(tt)))
        !write(*,*)'wrong psi_old',iorb,tt
        stop
     end if
  enddo
  !deallocation
  call f_free_ptr(psi)

  call f_release_routine()

END SUBROUTINE copy_old_wavefunctions


!> Reformat wavefunctions if the mesh have changed (in a restart)
subroutine reformatmywaves(iproc,npsi,at,&
     lr_old,rxyz_old,psi_old, lr,rxyz,psi)
  use module_precisions
  use module_atoms
  use at_domain
  use locregs
  use module_bigdft_arrays
  implicit none
  integer, intent(in) :: iproc, npsi
  type(locreg_descriptors), intent(in) :: lr_old, lr
  type(atoms_data), intent(in) :: at
  real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz,rxyz_old
  real(wp), dimension(array_dim(lr_old), npsi), intent(in) :: psi_old
  real(wp), dimension(array_dim(lr), npsi), intent(out) :: psi
  !Local variables
  character(len=*), parameter :: subname='reformatmywaves'
  integer :: iat,iorb
  real(gp) :: displ
  real(gp), dimension(3) :: dxyz
  real(wp), dimension(:), allocatable :: psifscf, psifscfold
  real(wp), dimension(:,:,:,:,:,:), allocatable :: psigold, psig

  displ=0.0_gp
  dxyz = 0._gp
  do iat=1,at%astruct%nat
     displ=displ+distance(at%astruct%dom,rxyz(:,iat),rxyz_old(:,iat))**2
     dxyz=dxyz+closest_r(at%astruct%dom,rxyz(:,iat),center=rxyz_old(:,iat))
  enddo
  displ=sqrt(displ)
  dxyz=dxyz/real(at%astruct%nat,gp)

  if (lr_do_reformat(lr_old, lr, displ = displ, log = (iproc == 0))) then
     psifscf = f_malloc(lr%mesh_fine%ndim,id='psifscf')
     psifscfold = f_malloc(lr_old%mesh_fine%ndim,id='psifscfold')
     psigold = f_malloc(grid_dim(lr_old),id='psigold')
     psig = f_malloc(grid_dim(lr),id='psig')
     do iorb = 1, npsi

        call lr_reformat(lr,lr_old,psi_old(:, iorb),psi(:,iorb), &
             shift = dxyz, force = displ > 1.d-2, w_psig = psig, &
             w_psigold = psigold, w_psifscf = psifscf, w_psifscfold = psifscfold)

     end do
     call f_free(psig)
     call f_free(psigold)
     call f_free(psifscfold)
     call f_free(psifscf)
  else
     call f_memcpy(src = psi_old, dest = psi)
  end if
END SUBROUTINE reformatmywaves

!!$subroutine free_wave_to_isf(psiscf)
!!$  use module_base
!!$  implicit none
!!$  real(wp), dimension(:,:,:,:), pointer :: psiscf
!!$
!!$  integer :: i_all, i_stat
!!$
!!$  i_all=-product(shape(psiscf))*kind(psiscf)
!!$  deallocate(psiscf,stat=i_stat)
!!$  call memocc(i_stat,i_all,'psiscf',"free_wave_to_isf_etsf")
!!$END SUBROUTINE free_wave_to_isf

subroutine read_wave_descr(lstat, filename, ln, &
     & norbu, norbd, iorbs, ispins, nkpt, ikpts, nspinor, ispinor)
  use public_enums !module_types
  use module_input_keys
  implicit none
  integer, intent(in) :: ln
  character, intent(in) :: filename(ln)
  integer, intent(out) :: norbu, norbd, nkpt, nspinor
  integer, intent(out) :: iorbs, ispins, ikpts, ispinor
  logical, intent(out) :: lstat

  character(len = 1024) :: filename_
  integer :: iformat, i
  character(len = 1024) :: testf

  write(filename_, "(A)") " "
  do i = 1, ln, 1
     filename_(i:i) = filename(i)
  end do

  ! Find format from name.
  iformat = wave_format_from_filename(1, trim(filename_))

  ! Call proper wraping routine.
  if (iformat == WF_FORMAT_ETSF) then
     call readwavedescr_etsf(lstat, trim(filename_), norbu, norbd, nkpt, nspinor)
     iorbs = 1
     ispins = 1
     ikpts = 1
     ispinor = 1
  else
     call readwavedescr(lstat, trim(filename_), iorbs, ispins, ikpts, ispinor, nspinor, testf)
     norbu = 0
     norbd = 0
     nkpt = 0
  end if
END SUBROUTINE read_wave_descr


!> Make sure we have one locreg which is defined on all MPI so that we can use it for onsite overlap
!! need to make sure that there are no other bigger locrads
!! it would be helpful to make a variable indicating which locreg we have, but don't want to edit structures unnecessarily...
!! just in case there is some noise in the locrads
subroutine identify_on_all_mpi(lzd)
  use module_types, only: local_zone_descriptors
  use module_precisions
  implicit none
  type(local_zone_descriptors), intent(inout) :: Lzd
  !local variables
  integer :: jlr,ilr
  real(gp) :: maxlr, lrtol

  lrtol=1.0e-3_gp
  jlr=1
  maxlr=Lzd%Llr(jlr)%locrad
  do ilr=2,Lzd%nlr
     if (Lzd%Llr(ilr)%locrad > maxlr + lrtol) then
        jlr=ilr
        maxlr=Lzd%Llr(jlr)%locrad
     end if
  end do
  Lzd%llr_on_all_mpi=jlr

  !now broadcast the keys assuming that only one processor has this locreg

end subroutine identify_on_all_mpi


subroutine identify_locreg_proc(ilr,orbs,source)
  use module_types, only: orbitals_data
  implicit none
  integer, intent(in) :: ilr
  type(orbitals_data), intent(in) :: orbs
  integer, intent(out) :: source
  !local variables
  integer :: jorb,jproc,nproc,jjorb

  jorb=0
  nproc=size(orbs%norb_par,1)
  find_proc: do jproc=0,nproc-1       
     do jjorb=1,orbs%norb_par(jproc,0)
        jorb=jorb+1
        if (orbs%inwhichlocreg(jorb) == ilr) then
           source=jproc
           exit find_proc
        end if
     end do
  end do find_proc
end subroutine identify_locreg_proc


subroutine tmb_overlap_onsite(iproc, nproc, imethod_overlap, at, tmb, rxyz)

  use module_precisions
  use module_types
  use locregs
  use module_fragments
  use module_bigdft_errors
  use rototranslations
  use reformatting
  use f_enums
  use dictionaries
  use communications_base, only: comms_linear_null, deallocate_comms_linear, TRANSPOSE_FULL
  use communications_init, only: init_comms_linear
  use communications, only: transpose_localized
  use sparsematrix, only: uncompress_matrix
  use sparsematrix_base, only: matrices, sparse_matrix, &
                               matrices_null, sparse_matrix_null, &
                               deallocate_matrices, deallocate_sparse_matrix, &
                               assignment(=), sparsematrix_malloc_ptr, SPARSE_TASKGROUP
  use sparsematrix_wrappers, only: init_sparse_matrix_wrapper
  use sparsematrix_init, only: init_matrix_taskgroups_wrapper
  use bigdft_matrices, only: check_local_matrix_extents, init_matrixindex_in_compressed_fortransposed
  use transposed_operations, only: calculate_overlap_transposed, normalize_transposed
  use compression
  use module_bigdft_arrays
  use module_bigdft_mpi
  use chess_utils
  !!use bounds, only: ext_buffers
  !!use locreg_operations
  implicit none

  ! Calling arguments
  integer,intent(in) :: iproc, nproc, imethod_overlap
  type(atoms_data), intent(in) :: at
  type(DFT_wavefunction),intent(inout):: tmb
  real(gp),dimension(3,at%astruct%nat),intent(in) :: rxyz

  ! Local variables
  logical :: reformat
  integer :: iorb,jstart,jstart_tmp
  integer :: iiorb,ilr,iiat,j,iis1,iie1,i1,i
  integer :: ilr_tmp,iiat_tmp,ndim_tmp,ndim,norb_tmp
  real(gp), dimension(3) :: centre_old_box, centre_new_box, da
  real(wp), dimension(:,:,:,:,:,:), allocatable :: phigold
  real(wp), dimension(:), pointer :: psi_tmp, psit_c_tmp, psit_f_tmp, norm
  real(wp), dimension(:), pointer :: subpsi
  integer, dimension(0:7) :: reformat_reason
  type(comms_linear) :: collcom_tmp
  type(local_zone_descriptors) :: lzd_tmp
  real(gp) :: tol
  character(len=*),parameter:: subname='tmb_overlap_onsite'
  type(rototranslation) :: frag_trans
  integer :: ierr, ncount, iroot, jproc, ndim_tmp1
  !!integer,dimension(:),allocatable :: workarray, workarray_int
  !!integer(f_long) :: work_il
  !!real(gp),dimension(:),allocatable :: workarray_rl
  type(sparse_matrix),dimension(1) :: smat_tmp
  type(matrices) :: mat_tmp
  integer,dimension(2) :: irow, icol, iirow, iicol
  logical :: wrap_around
  real(gp) :: ddot
  integer :: ind_min, ind_mas,source
  type(linmat_auxiliary) :: aux
  type(f_enumerator) :: strategy
  type(dictionary), pointer :: dict_info
  !!real(kind=gp), dimension(:,:,:), allocatable :: workarraytmp
  !!real(wp), allocatable, dimension(:,:,:) :: psirold
  !!integer, dimension(3) :: nl, nr
  !!logical, dimension(3) :: per
  !!type(workarr_sumrho) :: w
  !!integer :: jjorb, jjat

  call reformatting_init_info(dict_info)
  
  ! move all psi into psi_tmp all centred in the same place and calculate overlap matrix
  tol=1.d-3
  reformat_reason=0

  !identify and broadcast the all_mpi locreg
  call identify_on_all_mpi(tmb%Lzd)
  ! there should be one locreg which is defined on all MPI
  ilr_tmp=tmb%lzd%llr_on_all_mpi
  call identify_locreg_proc(ilr_tmp,tmb%orbs,source)
  call lr_broadcast_wfd(tmb%Lzd%Llr(ilr_tmp),source,bigdft_mpi%mpi_comm)

  iiat_tmp=0
  do iorb=1,tmb%orbs%norb
     if (tmb%orbs%inwhichlocreg(iorb) == ilr_tmp) then
        norb_tmp=iorb
        iiat_tmp=tmb%orbs%onwhichatom(norb_tmp)
        exit
     end if
  end do
  if (iiat_tmp==0) call f_err_throw('iiat_tmp not found',&
       err_name='BIGDFT_RUNTIME_ERROR')

  !get the processor of the

  !if (iproc==0) print*,''
  !if (iproc==0) print*,'NORB TMP',norb_tmp,ilr_tmp,iiat_tmp
  !if (iproc==0) print*,''

  !!if (ilr_tmp /= 1) then 
  !!   call f_err_throw('Problem with locreg in onsite overlap', &
  !!        err_name='BIGDFT_RUNTIME_ERROR')
  !!end if

  ! Determine size of phi_old and phi
  ndim_tmp=0
  ndim=0
  ndim_tmp1=array_dim(tmb%lzd%llr(ilr_tmp))
  do iorb=1,tmb%orbs%norbp
      iiorb=tmb%orbs%isorb+iorb
      ilr=tmb%orbs%inwhichlocreg(iiorb)
      ndim=ndim+array_dim(tmb%lzd%llr(ilr))
      ndim_tmp=ndim_tmp+ndim_tmp1
  end do
  !double check size is ok....
  ! should integrate bettwer with existing reformat routines, but restart needs tidying anyway
  psi_tmp = f_malloc_ptr(ndim_tmp,id='psi_tmp')

  jstart=1
  jstart_tmp=1

  do iorb=1,tmb%orbs%norbp
      iiorb=tmb%orbs%isorb+iorb
      ilr=tmb%orbs%inwhichlocreg(iiorb)
      iiat=tmb%orbs%onwhichatom(iiorb)

      !theta=0.d0*(4.0_gp*atan(1.d0)/180.0_gp)
      !newz=(/1.0_gp,0.0_gp,0.0_gp/)
      !centre_old(:)=rxyz(:,iiat)
      !centre_new(:)=rxyz(:,iiat_tmp)
      !shift(:)=centre_new(:)-centre_old(:)

      frag_trans=rototranslation_identity()
!!$      frag_trans%theta=0.0d0*(4.0_gp*atan(1.d0)/180.0_gp)
!!$      frag_trans%rot_axis=(/1.0_gp,0.0_gp,0.0_gp/)
      call set_translation(frag_trans,src=rxyz(:,iiat),dest=rxyz(:,iiat_tmp))
!!$      frag_trans%rot_center(:)=rxyz(:,iiat)
!!$      frag_trans%rot_center_new(:)=rxyz(:,iiat_tmp)

      strategy=inspect_rototranslation(frag_trans,tol,tmb%lzd%llr(ilr_tmp),tmb%lzd%llr(ilr),&
           tmb%lzd%glr%mesh_coarse,tmb%lzd%glr%mesh_coarse,dict_info)
     !!write(*,'(a,4(I4,x),3(3(F4.2,x),2x))') 'debuga:',iproc,iorb,ilr,ilr_tmp,tmb%lzd%llr(ilr_tmp)%mesh_coarse%hgrids,&
     !!     tmb%lzd%llr(ilr)%mesh_coarse%hgrids,tmb%lzd%glr%mesh_coarse%hgrids
      
      if (strategy== REFORMAT_COPY) then ! copy psi into psi_tmp
         !print*,'no reformat',iproc,iiorb,ilr,iiat
         call f_memcpy(dest = psi_tmp(jstart_tmp), src = tmb%psi(jstart), &
              n = ndim_tmp1)
         jstart = jstart + ndim_tmp1
         jstart_tmp = jstart_tmp + ndim_tmp1
      else
          !!!debug
          !!open(3000+iiorb)
          !!do i=1,array_dim(tmb%lzd%llr(ilr))
          !!write(3000+iiorb,*) i,tmb%psi(jstart+i)
          !!end do
          !!close(3000+iiorb)

          subpsi => f_subptr(tmb%psi, from = jstart, size = array_dim(tmb%lzd%llr(ilr)))
          if (strategy==REFORMAT_FULL) then
             call reformat_one_supportfunction(tmb%lzd%llr(ilr_tmp),tmb%lzd%llr(ilr),&
                  tmb%lzd%glr%mesh_coarse,tmb%lzd%glr%mesh_coarse,&
                  subpsi,frag_trans,psi_tmp(jstart_tmp:))
          else
             call lr_rewrap(tmb%lzd%llr(ilr_tmp), tmb%lzd%llr(ilr), &
                  subpsi, psi_tmp(jstart_tmp:))
          end if

          jstart=jstart+array_dim(tmb%lzd%llr(ilr))
          jstart_tmp=jstart_tmp+ndim_tmp1

      end if

      !!!debug
      !!open(1000+iiorb)
      !!do i=1,ndim_tmp1
      !!   write(1000+iiorb,*) i,psi_tmp(jstart_tmp-ndim_tmp1+i-1),&
      !!        ddot(ndim_tmp1, psi_tmp(jstart_tmp-ndim_tmp1), 1, psi_tmp(jstart_tmp-ndim_tmp1), 1)
      !!end do
      !!close(1000+iiorb)

  end do

  call print_reformat_summary(dict_info, bigdft_mpi%mpi_comm)

  ! now that they are all in one lr, need to calculate overlap matrix
  ! make lzd_tmp contain all identical lrs
  lzd_tmp = local_zone_descriptors_null()
  lzd_tmp%linear=tmb%lzd%linear
  lzd_tmp%nlr=tmb%lzd%nlr
  lzd_tmp%hgrids(:)=tmb%lzd%hgrids(:)
  lzd_tmp%llr_on_all_mpi=tmb%lzd%llr_on_all_mpi

  call nullify_locreg_descriptors(lzd_tmp%glr)
  call copy_locreg_descriptors(tmb%lzd%glr, lzd_tmp%glr)

  iis1=lbound(tmb%lzd%llr,1)
  iie1=ubound(tmb%lzd%llr,1)
  allocate(lzd_tmp%llr(iis1:iie1))

  do i1=iis1,iie1
     call nullify_locreg_descriptors(lzd_tmp%llr(i1))
     call copy_locreg_descriptors(tmb%lzd%llr(ilr_tmp), lzd_tmp%llr(i1))
  end do


  !call nullify_comms_linear(collcom_tmp)
  collcom_tmp=comms_linear_null()
  call init_comms_linear(iproc, nproc, imethod_overlap, ndim_tmp, tmb%orbs, lzd_tmp, &
       tmb%linmat%smat(2)%nspin, collcom_tmp)

  smat_tmp(1) = sparse_matrix_null()
  aux = linmat_auxiliary_null()
  ! Do not initialize the matrix multiplication to save memory. 
  call init_sparse_matrix_wrapper(iproc, nproc, tmb%linmat%smat(1)%nspin, tmb%orbs, &
       lzd_tmp, at%astruct, .false., init_matmul=.false., matmul_optimize_load_balancing=.false., &
       imode=2, smat=smat_tmp(1))
  call init_matrixindex_in_compressed_fortransposed(iproc, nproc, &
       collcom_tmp, collcom_tmp, collcom_tmp, smat_tmp(1), &
       aux)

  !!iirow(1) = smat_tmp%nfvctr
  !!iirow(2) = 1
  !!iicol(1) = smat_tmp%nfvctr
  !!iicol(2) = 1
  !!call get_sparsematrix_local_extent(iproc, nproc, tmb%linmat%smmd, smat_tmp, ind_min, ind_mas)
  call check_local_matrix_extents(iproc, nproc, &
       collcom_tmp, collcom_tmp, tmb%orbs, tmb%linmat%smmd, smat_tmp(1), aux, &
       ind_min, ind_mas)
  !!call get_sparsematrix_local_rows_columns(smat_tmp, ind_min, ind_mas, irow, icol)
  !!iirow(1) = min(irow(1),iirow(1))
  !!iirow(2) = max(irow(2),iirow(2))
  !!iicol(1) = min(icol(1),iicol(1))
  !!iicol(2) = max(icol(2),iicol(2))

  !!call init_matrix_taskgroups(iproc, nproc, bigdft_mpi%mpi_comm, .false., smat_tmp)
  call init_matrix_taskgroups_wrapper(iproc, nproc, bigdft_mpi%mpi_comm, .false., &
       1, smat_tmp(1), (/(/ind_min,ind_mas/)/))


  mat_tmp = matrices_null()
  mat_tmp%matrix_compr = sparsematrix_malloc_ptr(smat_tmp(1), iaction=SPARSE_TASKGROUP,id='mat_tmp%matrix_compr')

  psit_c_tmp = f_malloc_ptr(sum(collcom_tmp%nrecvcounts_c),id='psit_c_tmp')
  psit_f_tmp = f_malloc_ptr(7*sum(collcom_tmp%nrecvcounts_f),id='psit_f_tmp')

  call transpose_localized(iproc, nproc, ndim_tmp, tmb%orbs, collcom_tmp, &
       TRANSPOSE_FULL, psi_tmp, psit_c_tmp, psit_f_tmp, lzd_tmp)

  ! normalize psi
  !skip the normalize psi step
  !norm = f_malloc_ptr(tmb%orbs%norb,id='norm')
  !call normalize_transposed(iproc, nproc, tmb%orbs, tmb%linmat%smat(1)%nspin, collcom_tmp, psit_c_tmp, psit_f_tmp, norm)
  !call f_free_ptr(norm)

  !!call calculate_pulay_overlap(iproc, nproc, tmb%orbs, tmb%orbs, collcom_tmp, collcom_tmp, &
  !!     psit_c_tmp, psit_c_tmp, psit_f_tmp, psit_f_tmp, tmb%linmat%ovrlp_%matrix)
  call calculate_overlap_transposed(iproc, nproc, tmb%orbs, collcom_tmp, &
                 psit_c_tmp, psit_c_tmp, psit_f_tmp, psit_f_tmp, smat_tmp(1), aux, mat_tmp)
  !call uncompress_matrix(iproc, tmb%linmat%smat(1), mat_tmp%matrix_compr, tmb%linmat%ovrlp_%matrix)
  call uncompress_matrix(iproc, nproc, smat_tmp(1), mat_tmp%matrix_compr, tmb%linmat%ovrlp_%matrix)

  !do iiorb=1,tmb%orbs%norb
  !   !if (mod(iiorb,4)/=1) cycle
  !   iiat=tmb%orbs%onwhichatom(iiorb)
  !   if (iiorb>1) then
  !      if (iiat == tmb%orbs%onwhichatom(iiorb-1)) cycle
  !   end if
  !   do jjorb=1,tmb%orbs%norb
  !      !if (mod(jjorb,4)/=1) cycle
  !      jjat=tmb%orbs%onwhichatom(jjorb)
  !      if (jjorb>1) then
  !         if (jjat == tmb%orbs%onwhichatom(jjorb-1)) cycle
  !      end if
  !      if (mod(iiat-jjat,16)/=0) cycle
  !      if (iproc==0) write(*,'(a,x,4(I4,x),F6.4)') 'DEBUGOO2',iiorb,jjorb,iiat,jjat,tmb%linmat%ovrlp_%matrix(iiorb,jjorb,1)
  !   end do
  !end do

  call deallocate_matrices(mat_tmp)
  call deallocate_sparse_matrix(smat_tmp(1))
  call deallocate_linmat_auxiliary(aux)

!!!# DEBUG #######
!!call deallocate_local_zone_descriptors(lzd_tmp)
!!call mpi_finalize(i1)
!!stop
!!!# END DEBUG ###

  call deallocate_comms_linear(collcom_tmp)
  call deallocate_local_zone_descriptors(lzd_tmp)

  call f_free_ptr(psit_c_tmp)
  call f_free_ptr(psit_f_tmp)

  call f_free_ptr(psi_tmp)

END SUBROUTINE tmb_overlap_onsite

!> Copy old support functions from phi to phi_old
subroutine copy_old_supportfunctions(iproc,orbs,lzd,phi,lzd_old,phi_old)
  use module_precisions
  use module_types
  use locregs
  use module_bigdft_output
  use module_bigdft_arrays
  implicit none
  integer,intent(in) :: iproc
  type(orbitals_data), intent(in) :: orbs
  type(local_zone_descriptors), intent(in) :: lzd
  type(local_zone_descriptors), intent(inout) :: lzd_old
  real(wp), dimension(:), pointer :: phi,phi_old
  !Local variables
  character(len=*), parameter :: subname='copy_old_supportfunctions'
  integer :: iseg,j,ind1,iorb,ii,iiorb,ilr
  real(kind=8) :: tt

  ! First copy global quantities
  call nullify_locreg_descriptors(lzd_old%glr)

  call copy_locreg_descriptors(lzd%glr,lzd_old%glr)

  lzd_old%nlr=lzd%nlr
  nullify(lzd_old%llr)
  allocate(lzd_old%llr(lzd_old%nlr))
  do ilr=1,lzd_old%nlr
      call nullify_locreg_descriptors(lzd_old%llr(ilr))
  end do

  lzd_old%hgrids(1)=lzd%hgrids(1)
  lzd_old%hgrids(2)=lzd%hgrids(2)
  lzd_old%hgrids(3)=lzd%hgrids(3)

  ii=0
  do iorb=1,orbs%norbp
      iiorb=orbs%isorb+iorb
      ilr=orbs%inwhichlocreg(iiorb)
      call copy_locreg_descriptors(lzd%llr(ilr), lzd_old%llr(ilr))
      ii = ii + array_dim(lzd_old%llr(ilr))
  end do

  phi_old = f_malloc_ptr(ii,id='phi_old')

  ! Now copy the suport functions
  if (iproc==0) call yaml_map('Check the normalization of the support functions, tolerance',1.d-3,fmt='(1es12.4)')
  ind1=0
  do iorb=1,orbs%norbp
      tt=0.d0
      iiorb=orbs%isorb+iorb
      ilr=orbs%inwhichlocreg(iiorb)
      do j=1,array_dim(lzd_old%llr(ilr))
          ind1=ind1+1
          phi_old(ind1)=phi(ind1)
          tt=tt+real(phi(ind1),kind=8)**2
      end do
      tt=sqrt(tt)
      if (abs(tt-1.d0) > 1.d-3) then
         !write(*,*)'wrong phi_old',iiorb,tt
         call yaml_warning('support function, value:'//trim(yaml_toa(iiorb,fmt='(i6)'))//trim(yaml_toa(tt,fmt='(1es18.9)')))
         !stop
      end if
  end do
!  if (iproc==0) call yaml_mapping_close()

  !!!deallocation
  !!i_all=-product(shape(phi))*kind(phi)
  !!deallocate(phi,stat=i_stat)
  !!call memocc(i_stat,i_all,'phi',subname)

END SUBROUTINE copy_old_supportfunctions


subroutine copy_old_coefficients(norbu_tmb, nfvctr, nspin, coeff, coeff_old)
  use module_bigdft_arrays
  implicit none

  ! Calling arguments
  integer,intent(in):: norbu_tmb, nfvctr, nspin
  real(8),dimension(:,:,:),pointer:: coeff, coeff_old

  ! Local variables
  character(len=*),parameter:: subname='copy_old_coefficients'
!  integer:: istat,iall

  coeff_old = f_malloc_ptr((/ nfvctr, norbu_tmb, nspin /),id='coeff_old')

  call vcopy(nfvctr*norbu_tmb*nspin, coeff(1,1,1), 1, coeff_old(1,1,1), 1)

END SUBROUTINE copy_old_coefficients


subroutine copy_old_inwhichlocreg(norb_tmb, inwhichlocreg, inwhichlocreg_old, onwhichatom, onwhichatom_old)
  use module_bigdft_arrays
  implicit none

  ! Calling arguments
  integer,intent(in):: norb_tmb
  integer,dimension(:),pointer:: inwhichlocreg, inwhichlocreg_old, onwhichatom, onwhichatom_old

  ! Local variables
  character(len=*),parameter:: subname='copy_old_inwhichlocreg'
  !integer :: istat, iall

  inwhichlocreg_old = f_malloc_ptr(norb_tmb,id='inwhichlocreg_old')
  call vcopy(norb_tmb, inwhichlocreg(1), 1, inwhichlocreg_old(1), 1)

  onwhichatom_old = f_malloc_ptr(norb_tmb,id='onwhichatom_old')
  call vcopy(norb_tmb, onwhichatom(1), 1, onwhichatom_old(1), 1)

END SUBROUTINE copy_old_inwhichlocreg


!> Reformat wavefunctions if the mesh have changed (in a restart)
!! NB add_derivatives must be false if we are using phi_array_old instead of psi_old and don't have the keys
subroutine reformat_supportfunctions(iproc,nproc,at,rxyz_old,rxyz,add_derivatives,tmb,ndim_old,lzd_old,&
       frag_trans,psi_old,input_dir,input_frag,ref_frags,max_shift,phi_array_old)
  use module_precisions
  use module_types
  use module_fragments
  use yaml_output
  use rototranslations
  use reformatting
  use at_domain, only: domain_periodic_dims
  use module_bigdft_arrays
  use f_enums
  use dictionaries
  use module_bigdft_mpi
  use locregs
  use compression
  implicit none
  integer, intent(in) :: iproc,nproc
  integer, intent(in) :: ndim_old
  type(atoms_data), intent(in) :: at
  real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz,rxyz_old
  type(DFT_wavefunction), intent(inout) :: tmb
  type(local_zone_descriptors), intent(in) :: lzd_old
  type(rototranslation), dimension(tmb%orbs%norbp), intent(in) :: frag_trans
  real(wp), dimension(:), pointer :: psi_old
  type(phi_array), dimension(tmb%orbs%norbp), optional, intent(in) :: phi_array_old
  logical, intent(in) :: add_derivatives
  character(len=*), intent(in) :: input_dir
  type(fragmentInputParameters), intent(in) :: input_frag
  type(system_fragment), dimension(:), pointer :: ref_frags
  real(gp),intent(out) :: max_shift
  !Local variables
  character(len=*), parameter :: subname='reformat_supportfunctions'
  logical :: reformat
  integer :: iorb,j,jstart,jstart_old,iiorb,ilr,iiat
  integer:: idir,jstart_old_der,ncount,ilr_old
  !!integer :: i
  integer, dimension(3) :: n_old,n,nglr_old,nglr
  real(gp), dimension(3) :: centre_old_box,centre_new_box,da
  real(gp) :: tt,tol,displ
  real(wp), dimension(:,:,:,:,:,:), pointer :: phigold
  real(wp), dimension(:), allocatable :: phi_old_der
  real(wp), dimension(:), pointer :: subpsi
  integer, dimension(0:7) :: reformat_reason
  character(len=12) :: orbname!, dummy
  real(wp), allocatable, dimension(:,:,:) :: psirold
  logical :: psirold_ok
  integer, dimension(3) :: nl, nr
  logical, dimension(3) :: per
  character(len=100) :: fragdir
  integer :: ifrag, ifrag_ref, iforb, isforb
  real(kind=gp), dimension(:,:,:), allocatable :: workarraytmp
  logical :: gperx, gpery, gperz, lperx, lpery, lperz, wrap_around
  integer :: gnbl1, gnbr1, gnbl2, gnbr2, gnbl3, gnbr3, lnbl1, lnbr1, lnbl2, lnbr2, lnbl3, lnbr3
  integer, dimension(2,3) :: nbox
  real(gp), external :: dnrm2
  type(f_enumerator) :: strategy
  type(dictionary), pointer :: dict_info
!  integer :: iat

  call reformatting_init_info(dict_info)
  
  reformat_reason=0
  tol=1.d-3
  max_shift = 0.d0

  ! Get the derivatives of the support functions
  if (add_derivatives) then
     phi_old_der = f_malloc(3*ndim_old,id='phi_old_der')
     if (.not. associated(psi_old)) stop 'psi_old not associated in reformat_supportfunctions'
     call get_derivative_supportfunctions(ndim_old, lzd_old%hgrids(1), lzd_old, tmb%orbs, psi_old, phi_old_der)
     jstart_old_der=1
  end if

  !nglr_old(1)=lzd_old%glr%d%n1
  !nglr_old(2)=lzd_old%glr%d%n1
  !nglr_old(3)=lzd_old%glr%d%n1
  !nglr(1)=tmb%lzd%glr%d%n1
  !nglr(2)=tmb%lzd%glr%d%n1
  !nglr(3)=tmb%lzd%glr%d%n1

  jstart_old=1
  jstart=1
  do iorb=1,tmb%orbs%norbp
      iiorb=tmb%orbs%isorb+iorb
      ilr=tmb%orbs%inwhichlocreg(iiorb)
      iiat=tmb%orbs%onwhichatom(iiorb)

      ilr_old=ilr

!!$      n_old(1)=lzd_old%Llr(ilr_old)%mesh_coarse%ndims(1)-1
!!$      n_old(2)=lzd_old%Llr(ilr_old)%mesh_coarse%ndims(2)-1
!!$      n_old(3)=lzd_old%Llr(ilr_old)%mesh_coarse%ndims(3)-1
!!$      n(1)=tmb%lzd%Llr(ilr)%mesh_coarse%ndims(1)-1
!!$      n(2)=tmb%lzd%Llr(ilr)%mesh_coarse%ndims(2)-1
!!$      n(3)=tmb%lzd%Llr(ilr)%mesh_coarse%ndims(3)-1

      !theta=frag_trans(iorb)%theta!0.0d0*(4.0_gp*atan(1.d0)/180.0_gp)
      !newz=frag_trans(iorb)%rot_axis!(/1.0_gp,0.0_gp,0.0_gp/)
      !centre_old(:)=frag_trans(iorb)%rot_center(:)!rxyz_old(:,iiat)
      !shift(:)=frag_trans(iorb)%dr(:)!rxyz(:,iiat)
      strategy=inspect_rototranslation(frag_trans(iorb),tol,&
           tmb%lzd%llr(ilr),lzd_old%llr(ilr_old),&
           tmb%lzd%glr%mesh_coarse,lzd_old%glr%mesh_coarse,dict_info)
      
      reformat= strategy==REFORMAT_FULL
      wrap_around=strategy==REFORMAT_WRAP
      
      displ = get_displ(dict_info)
      max_shift = max(max_shift,displ)

      ! just copy psi from old to new as reformat not necessary
      if (.not. reformat) then

          ! copy from phi_array_old, can use new keys as they should be identical to old keys
          if (present(phi_array_old)) then
             subpsi => f_subptr(tmb%psi, from = jstart, size = array_dim(tmb%lzd%llr(ilr)))
             call lr_rewrap(tmb%lzd%llr(ilr), lzd_old%Llr(ilr_old), &
                  phi_array_old(iorb)%psig, subpsi)

          ! directly copy psi_old to psi, first check psi_old is actually allocated
          else
             if (.not. associated(psi_old)) stop 'psi_old not associated in reformat_supportfunctions'

             if (.not. wrap_around) then
                call f_memcpy(dest = tmb%psi(jstart), src = psi_old(jstart_old), &
                     n = array_dim(lzd_old%llr(ilr_old)))
             else
               ! THIS CASE NEEDS TESTING
               subpsi => f_subptr(tmb%psi, from = jstart, size = array_dim(tmb%lzd%llr(ilr)))
               call lr_rewrap(tmb%lzd%llr(ilr), lzd_old%llr(ilr), &
                    psi_old(jstart_old:), subpsi)

            end if
            jstart_old=jstart_old+array_dim(lzd_old%llr(ilr))
         end if
         jstart=jstart+array_dim(tmb%lzd%llr(ilr))
      else
          ! Add the derivatives to the basis functions
          if (add_derivatives) then
             do idir=1,3
                 tt=rxyz(idir,iiat)-rxyz_old(idir,iiat)
                 ncount = array_dim(lzd_old%llr(ilr_old))
                 call daxpy(ncount, tt, phi_old_der(jstart_old_der), 1, psi_old(jstart_old), 1)
                 jstart_old_der = jstart_old_der + ncount
             end do
          end if

          !write(100+iproc,*) 'norm phigold ',dnrm2(8*(n1_old+1)*(n2_old+1)*(n3_old+1),phigold,1)
          !write(*,*) 'iproc,norm phigold ',iproc,dnrm2(8*product(n_old+1),phigold,1)

          ! allow for fragment calculation
          if (input_frag%nfrag>1) then
             isforb=0
             do ifrag=1,input_frag%nfrag
                ! find reference fragment this corresponds to
                ifrag_ref=input_frag%frag_index(ifrag)
                ! loop over orbitals of this fragment
                do iforb=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
                   if (iiorb==iforb+isforb) exit
                end do
                if (iforb/=ref_frags(ifrag_ref)%fbasis%forbs%norb+1) exit
                isforb=isforb+ref_frags(ifrag_ref)%fbasis%forbs%norb
             end do
             write(orbname,*) iforb
             fragdir=trim(input_frag%dirname(ifrag_ref))
          else
             write(orbname,*) iiorb
             fragdir=trim(input_frag%dirname(1))
          end if

          ! read psir_old directly from files (don't have lzd_old to rebuild it)
          !psirold_ok=.true.

          !! first check if file exists
          !inquire(file=trim(input_dir)//trim(fragdir)//'tmbisf'//trim(adjustl(orbname))//'.dat',exist=psirold_ok)
          !if (.not. psirold_ok) print*,"psirold doesn't exist for reformatting",iiorb,&
          !     trim(input_dir)//'tmbisf'//trim(adjustl(orbname))//'.dat'

          !! read in psirold
          !if (psirold_ok) then
          !   call timing(iproc,'readisffiles','ON')
          !   open(99,file=trim(input_dir)//trim(fragdir)//'tmbisf'//trim(adjustl(orbname))//'.dat',&
          !        form="unformatted",status='unknown')
          !   read(99) dummy
          !   read(99) lzd_old%llr(ilr_old)%d%n1i,lzd_old%llr(ilr_old)%d%n2i,lzd_old%llr(ilr_old)%d%n3i
          !   read(99) lzd_old%llr(ilr_old)%nsi1,lzd_old%llr(ilr_old)%nsi2,lzd_old%llr(ilr_old)%nsi3
          !   psirold=f_malloc((/lzd_old%llr(ilr_old)%d%n1i,lzd_old%llr(ilr_old)%d%n2i,lzd_old%llr(ilr_old)%d%n3i/),id='psirold')
          !   do k=1,lzd_old%llr(ilr_old)%d%n3i
          !      do j=1,lzd_old%llr(ilr_old)%d%n2i
          !         do i=1,lzd_old%llr(ilr_old)%d%n1i
          !            read(99) psirold(i,j,k)
          !         end do
          !      end do
          !   end do
          !   close(99)
          !   call timing(iproc,'readisffiles','OF')
          !end if


!!$          ! Periodicity in the three directions
!!$          gperx=(tmb%lzd%glr%geocode /= 'F')
!!$          gpery=(tmb%lzd%glr%geocode == 'P')
!!$          gperz=(tmb%lzd%glr%geocode /= 'F')
!!$
!!$          ! Set the conditions for ext_buffers (conditions for buffer size)
!!$          lperx=(lzd_old%llr(ilr)%geocode /= 'F')
!!$          lpery=(lzd_old%llr(ilr)%geocode == 'P')
!!$          lperz=(lzd_old%llr(ilr)%geocode /= 'F')
!!$
!!$          !calculate the size of the buffers of interpolating function grid
!!$          call ext_buffers(gperx,gnbl1,gnbr1)
!!$          call ext_buffers(gpery,gnbl2,gnbr2)
!!$          call ext_buffers(gperz,gnbl3,gnbr3)
!!$          call ext_buffers(lperx,lnbl1,lnbr1)
!!$          call ext_buffers(lpery,lnbl2,lnbr2)
!!$          call ext_buffers(lperz,lnbl3,lnbr3)
!!$
!!$
!!$          lzd_old%llr(ilr_old)%nsi1=2*lzd_old%llr(ilr_old)%ns1 - (Lnbl1 - Gnbl1)
!!$          lzd_old%llr(ilr_old)%nsi2=2*lzd_old%llr(ilr_old)%ns2 - (Lnbl2 - Gnbl2)
!!$          lzd_old%llr(ilr_old)%nsi3=2*lzd_old%llr(ilr_old)%ns3 - (Lnbl3 - Gnbl3)
!!$
!!$          !lzd_old%llr(ilr_old)%d%n1i=2*n_old(1)+31
!!$          !lzd_old%llr(ilr_old)%d%n2i=2*n_old(2)+31
!!$          !lzd_old%llr(ilr_old)%d%n3i=2*n_old(3)+31
!!$          !dimensions of the interpolating scaling functions grid (reduce to +2 for periodic)
!!$          if(lzd_old%llr(ilr)%geocode == 'F') then
!!$             lzd_old%llr(ilr)%d%n1i=2*n_old(1)+31
!!$             lzd_old%llr(ilr)%d%n2i=2*n_old(2)+31
!!$             lzd_old%llr(ilr)%d%n3i=2*n_old(3)+31
!!$          else if(lzd_old%llr(ilr)%geocode == 'S') then
!!$             lzd_old%llr(ilr)%d%n1i=2*n_old(1)+2
!!$             lzd_old%llr(ilr)%d%n2i=2*n_old(2)+31
!!$             lzd_old%llr(ilr)%d%n3i=2*n_old(3)+2
!!$          else
!!$             lzd_old%llr(ilr)%d%n1i=2*n_old(1)+2
!!$             lzd_old%llr(ilr)%d%n2i=2*n_old(2)+2
!!$             lzd_old%llr(ilr)%d%n3i=2*n_old(3)+2
!!$          end if
!!$
!!$          !as psig is already printed in the entire box initialize the lr with the complete box here
!!$          nbox(1,1)=Lzd_old%Llr(ilr)%ns1
!!$          nbox(2,1)=Lzd_old%Llr(ilr)%d%n1+Lzd_old%Llr(ilr)%ns1
!!$          nbox(1,2)=Lzd_old%Llr(ilr)%ns2
!!$          nbox(2,2)=Lzd_old%Llr(ilr)%d%n2+Lzd_old%Llr(ilr)%ns2
!!$          nbox(1,3)=Lzd_old%Llr(ilr)%ns3
!!$          nbox(2,3)=Lzd_old%Llr(ilr)%d%n3+Lzd_old%Llr(ilr)%ns3
!!$          call lr_box(Lzd_old%Llr(ilr),tmb%lzd%glr,lzd_old%hgrids,nbox,.false.)

          psirold_ok=.true.

          call timing(iproc,'Reformatting ','ON')
          ! uncompress or point towards correct phigold as necessary
          if (present(phi_array_old)) then
             call reformat_one_supportfunction(tmb%lzd%llr(ilr),lzd_old%llr(ilr_old),&
                  tmb%lzd%glr%mesh_coarse,lzd_old%glr%mesh_coarse,&
                                !at%astruct%geocode,& !,tmb%lzd%llr(ilr)%geocode,&
                                !lzd_old%hgrids,
                  phi_array_old(iorb)%psig,&
                                !tmb%lzd%hgrids,
                                !centre_old_box,centre_new_box,da,&
                  frag_trans(iorb),tmb%psi(jstart:),build_psirold = psirold_ok)
          else
             call reformat_one_supportfunction(tmb%lzd%llr(ilr),lzd_old%llr(ilr_old),&
                  tmb%lzd%glr%mesh_coarse,lzd_old%glr%mesh_coarse,&
                                !at%astruct%geocode,& !,tmb%lzd%llr(ilr)%geocode,&
                                !lzd_old%hgrids,
                  psi_old(jstart_old:),&
                                !tmb%lzd%hgrids,
                                !centre_old_box,centre_new_box,da,&
                  frag_trans(iorb),tmb%psi(jstart:),build_psirold = psirold_ok)

             jstart_old=jstart_old+array_dim(lzd_old%llr(ilr))

          end if
          jstart=jstart+array_dim(tmb%lzd%llr(ilr))
          !write(*,*) 'iproc,norm psirold ',iproc,dnrm2(product(2*n_old+31),psirold,1),2*n_old+31
          call timing(iproc,'Reformatting ','OF')

       end if

    end do

  ! Get the maximal shift among all tasks
  if (nproc>1) then
      call fmpi_allreduce(max_shift, count=1, op=FMPI_MAX, comm=bigdft_mpi%mpi_comm)
  end if
  if (iproc==0) call yaml_map('max shift of a locreg center',max_shift,fmt='(es9.2)')

  ! Determine the dumping factor for the confinement. In the limit wbohere the atoms
  ! have not moved, it goes to zero; in the limit where they have moved a lot, it goes to one.
  tt = exp(max_shift*3.465735903d0) - 1.d0 !exponential which is 0 at 0.0 and 1 at 0.2
  tt = min(tt,1.d0) !make sure that the value is not larger than 1.0
  tmb%damping_factor_confinement = tt


  if (add_derivatives) then
     call f_free(phi_old_der)
  end if

  call print_reformat_summary(dict_info, bigdft_mpi%mpi_comm)

END SUBROUTINE reformat_supportfunctions

!> Calculate all derviative for the support functions
subroutine get_derivative_supportfunctions(ndim, hgrid, lzd, lorbs, phi, phid)
  use module_types
  use locregs
  implicit none
  
  ! Calling arguments
  integer,intent(in):: ndim
  real(kind=8),intent(in) :: hgrid
  type(local_zone_descriptors),intent(in) :: lzd
  type(orbitals_data),intent(in) :: lorbs
  real(kind=8),dimension(ndim),intent(in) :: phi !< Basis functions
  real(kind=8),dimension(3*ndim),intent(inout) :: phid  !< Derivative basis functions
  
  ! Local variables
  integer :: ist1, iorb, ist, ilr, iiorb

  ist=1
  ist1=1
  do iorb=1,lorbs%norbp
     iiorb=lorbs%isorb+iorb
     ilr=lorbs%inWhichLocreg(iiorb)

     call get_one_derivative_supportfunction(lzd%llr(ilr),phi(ist),phid(ist1))

     ist = ist + array_dim(lzd%llr(ilr))
     ist1 = ist1 + 3*array_dim(lzd%llr(ilr))
  end do

end subroutine get_derivative_supportfunctions

!> Associate to the absolute value of orbital a filename which depends of the k-point and
!! of the spin sign
subroutine filename_of_proj(lbin,filename,ikpt,iat,iproj,icplx,filename_out)
  use module_types
  implicit none
  character(len=*), intent(in) :: filename
  logical, intent(in) :: lbin
  integer, intent(in) :: ikpt,iat,iproj,icplx
  character(len=*), intent(out) :: filename_out
  !local variables
  character(len=1) :: realimag
  character(len=3) :: f2
  character(len=4) :: f3
  character(len=5) :: f4
  character(len=13) :: completename

  !calculate k-point
  write(f3,'(a1,i3.3)') "k", ikpt !not more than 999 kpts

  !see if the wavefunction is real or imaginary
  if(icplx==2) then
     realimag='I'
  else
     realimag='R'
  end if

  !value of the atom
  write(f4,'(a1,i4.4)') "a", iat

  !value of the atom
  write(f2,'(i3.3)') iproj

  !complete the information in the name of the orbital
  completename='-'//f3//'-'//f4//'-'//realimag
  if (lbin) then
     filename_out = trim(filename)//completename//".bin."//f2
     !print *,'complete name <',trim(filename_out),'> end'
 else
     filename_out = trim(filename)//completename//"."//f2
     !print *,'complete name <',trim(filename_out),'> end'
 end if

  !print *,'filename: ',filename_out
end subroutine filename_of_proj

!> Write all projectors
subroutine writemyproj(filename,iformat,orbs,hx,hy,hz,at,rxyz,nl,glr)
  use module_types
  use module_precisions
  use yaml_output
  use locregs
  use psp_projectors_base
  use psp_projectors
  use public_enums, only: WF_FORMAT_ETSF, WF_FORMAT_BINARY
  use f_precisions, only: UNINITIALIZED
  implicit none
  integer, intent(in) :: iformat
  real(gp), intent(in) :: hx,hy,hz
  type(atoms_data), intent(in) :: at
  type(orbitals_data), intent(in) :: orbs
  type(DFT_PSP_projectors), intent(in) :: nl
  type(locreg_descriptors), intent(in) :: glr
  real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz
  character(len=*), intent(in) :: filename
  !Local variables
  type(DFT_PSP_projector_iter) :: psp_it
  integer :: ncount1,ncount2,ncount_rate,ncount_max,nwarnings
  integer :: iat,ikpt,iproj,iskpt,iekpt,istart,icplx,l
  real(kind=4) :: tr0,tr1
  real(kind=8) :: tel
  character(len=500) :: filename_out
  logical :: lbin

  call yaml_map('Write projectors to file', trim(filename) // '.*')
  !if (iproc == 0) write(*,"(1x,A,A,a)") "Write wavefunctions to file: ", trim(filename),'.*'
  if (iformat == WF_FORMAT_ETSF) then
     !call write_waves_etsf(iproc,filename,orbs,n1,n2,n3,hx,hy,hz,at,rxyz,wfd,psi)
     stop "not implemented proj in ETSF"
  else
     call cpu_time(tr0)
     call system_clock(ncount1,ncount_rate,ncount_max)

     !create projectors for any of the k-point hosted by the processor
     !starting kpoint
     if (orbs%norbp > 0) then
        iskpt=orbs%iokpt(1)
        iekpt=orbs%iokpt(orbs%norbp)
     else
        iskpt=1
        iekpt=1
     end if
     lbin = (iformat == WF_FORMAT_BINARY)

     do ikpt=iskpt,iekpt
        call DFT_PSP_projectors_iter_new(psp_it, nl)
        loop_proj: do while (DFT_PSP_projectors_iter_next(psp_it))
           call DFT_PSP_projectors_iter_ensure(psp_it, orbs%kpts(:,ikpt), 0, nwarnings, glr)
           istart = 0
           do iproj = 1, psp_it%mproj
              do icplx = 1, psp_it%ncplx
                 call filename_of_proj(lbin,filename,&
                      & ikpt,psp_it%iregion,iproj,icplx,filename_out)
                 if (lbin) then
                    open(unit=99,file=trim(filename_out),&
                         & status='unknown',form="unformatted")
                 else
                    open(unit=99,file=trim(filename_out),status='unknown')
                 end if
                 if (lbin) then
                    call dump_array_bin(psp_it%pspd%plr, 99, psp_it%coeff(istart + 1:))
                 else
                    call dump_array_txt(psp_it%pspd%plr, 99, psp_it%coeff(istart + 1:))
                 end if
                 close(99)
                 istart = istart + array_dim(psp_it%pspd%plr)
              end do
           end do
           
        end do loop_proj
     enddo

     call cpu_time(tr1)
     call system_clock(ncount2,ncount_rate,ncount_max)
     tel=dble(ncount2-ncount1)/dble(ncount_rate)
     call yaml_sequence_open('Write Proj Time')
     call yaml_sequence(advance='no')
     call yaml_mapping_open(flow=.true.)
     call yaml_map('Timing',(/ real(tr1-tr0,kind=8),tel /),fmt='(1pe10.3)')
     call yaml_mapping_close()
     call yaml_sequence_close()
     !write(*,'(a,i4,2(1x,1pe10.3))') '- WRITE WAVES TIME',iproc,tr1-tr0,tel
     !write(*,'(a,1x,i0,a)') '- iproc',iproc,' finished writing waves'
  end if

END SUBROUTINE writemyproj

subroutine readwavedescr(lstat, filename, iorb, ispin, ikpt, ispinor, nspinor, fileRI)
  use module_types

  implicit none

  character(len = *), intent(in) :: filename
  integer, intent(out) :: iorb, ispin, ikpt, nspinor, ispinor
  logical, intent(out) :: lstat
  character(len = 1024), intent(out) :: fileRI

  logical :: exists
  integer :: i, i_stat
  character(len = 1) :: code

  lstat = .false.

  !find the value of iorb
  read(filename(index(filename, ".", back = .true.)+2:len(filename)),*,iostat = i_stat) iorb
  if (i_stat /= 0) return
  i = index(filename, "-k", back = .true.)+2
  read(filename(i:i+2),*,iostat = i_stat) ikpt
  if (i_stat /= 0) return
  i = index(filename, "-", back = .true.)+1
  read(filename(i:i),*,iostat = i_stat) code
  if (i_stat /= 0) return
  if (code == "U" .or. code == "N") ispin = 1
  if (code == "D") ispin = 2
  ! Test file for other spinor part.
  nspinor = 1
  ispinor = 1
  read(filename(i+1:i+1),*,iostat = i_stat) code
  if (i_stat /= 0) return
  if (code == "R") then
     write(fileRI, "(A)") filename
     fileRI(i+1:i+1) = "I"
     inquire(file=trim(fileRI), exist=exists)
     if (exists) then
        ispinor = 1
        nspinor = 2
     end if
  end if
  if (code == "I") then
     write(fileRI, "(A)") filename
     fileRI(i+1:i+1) = "R"
     inquire(file=trim(fileRI), exist=exists)
     if (exists) then
        ispinor = 2
        nspinor = 2
     end if
  end if

  lstat = .true.
END SUBROUTINE readwavedescr
