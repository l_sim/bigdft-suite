!> @file
!!   Routines to precondition wavefunctions
!! @author
!!    Copyright (C) 2005-2011 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS


!> Generalized for the Linearscaling code
subroutine preconditionall2(iproc,nproc,orbs,Lzd,ncong,npsidim,hpsi,gnrm,gnrm_zero,&
     confdatarr)
  use module_precisions
  use module_types
  use Poisson_Solver, except_dp => dp, except_gp => gp
  use module_bigdft_output
  use locregs
  use liborbs_functions
  use orbitalbasis
  use liborbs_potentials
  use at_domain, only: domain_geocode
  use module_bigdft_mpi
  use module_bigdft_errors
  use module_bigdft_arrays
  use module_bigdft_profiling
  use wrapper_linalg
  implicit none
  integer, intent(in) :: iproc,nproc,ncong,npsidim
  type(local_zone_descriptors), intent(in) :: Lzd
  type(orbitals_data), intent(in) :: orbs
  real(dp), intent(out) :: gnrm,gnrm_zero
  real(wp), dimension(npsidim), intent(inout) :: hpsi
  type(confpot_data), dimension(orbs%norbp), intent(in), optional :: confdatarr !< used in the linear scaling but also for the cubic case
  !local variables
  character(len=*), parameter :: subname='preconditionall2'
  integer :: inds,jorb,ierr,jproc,ncplx
  real(wp) :: scpr2,evalmax,gnrm_orb
!!$  integer :: i_stat,i_all,ispinor,nbox
!!$  logical, parameter :: newp=.true.
!!$  real(gp) :: eh_fake,monop
!!$  real(wp), dimension(:), allocatable :: hpsir
!!$  type(coulomb_operator) :: G_Helmholtz
!!$  real(wp), dimension(:,:), allocatable :: gnrm_per_orb
!!$  type(workarr_sumrho) :: w
  integer, dimension(:,:), allocatable :: ncntdsp
  real(wp), dimension(:), allocatable :: gnrms,gnrmp
  real(wp), dimension(:), pointer :: psi_ptr
  type(orbital_basis) :: psi_ob
  type(ket) :: psi_it
  type(confpot_data) :: confdata
  type(wvf_manager) :: manager
  type(wvf_daub_view) :: view
  !debug
!!$  type(atoms_data) atoms_fake
!!$  integer :: iter=0
!!$  iter=iter+1

  call f_routine(id=subname)

  ! Preconditions all orbitals belonging to iproc
  !and calculate the norm of the residue
  ! norm of gradient
  gnrm=0.0_dp
  !norm of gradient of unoccupied orbitals
  gnrm_zero=0.0_dp

  call nullify_confpot_data(confdata)

  !prepare the arrays for the
  if (get_verbose_level() >= 3) then
     gnrmp = f_malloc(max(orbs%norbp, 1),id='gnrmp')
  end if
  
  call orbital_basis_associate(&
       psi_ob,&
       orbs=orbs,&
       phis_wvl=hpsi,&
       Lzd=Lzd,&
       id='preconditionall2')
  psi_it = orbital_basis_iterator(psi_ob, manager = manager)
  loop_kpt: do while(ket_next_kpt(psi_it))
     !the eval array contains all the values
     !take the max for all k-points
     !one may think to take the max per k-point
     evalmax = orbs%eval((psi_it%ikpt-1)*orbs%norb+1)
     do jorb = 2, orbs%norb
        evalmax = max(orbs%eval((psi_it%ikpt-1)*orbs%norb+jorb), evalmax)
     enddo
     loop_psi: do while(ket_next(psi_it, ikpt = psi_it%ikpt))
        ncplx = merge(2,1,any(psi_it%kpoint /= 0.0_gp) .or. psi_it%nspinor==2)

        if (present(confdatarr)) confdata = confdatarr(psi_it%iorbp)

        call wvf_precondition(psi_it%phi, &
             ncong, orbs%eval(psi_it%iorb), evalmax, scpr2, &
             psi_it%kpoint, ncplx, confdata)

        if (psi_it%occup == 0.0_gp) then
           gnrm_zero = gnrm_zero + psi_it%kwgt*scpr2
        else
           gnrm = gnrm + psi_it%kwgt*scpr2
        end if
        if (get_verbose_level() >= 3) then
           gnrmp(psi_it%iorbp) = sqrt(scpr2)
        end if

     end do loop_psi
  enddo loop_kpt

  call wvf_deallocate_manager(manager)

  call orbital_basis_release(psi_ob)
!!$ if (newp) then
!!$    call deallocate_work_arrays_sumrho(w)
!!$    call f_free(hpsir)
!!$ end if
  !gather the results of the gnrm per orbital in the case of high verbosity
  if (get_verbose_level() >= 3) then
     gnrms = f_malloc0(orbs%norb*orbs%nkpts,id='gnrms')
     !prepare displacements arrays
     ncntdsp = f_malloc((/ nproc, 2 /),id='ncntdsp')
     ncntdsp(1,2)=0
     ncntdsp(1,1)=orbs%norb_par(0,0)
     do jproc=1,nproc-1
        ncntdsp(jproc+1,2)=ncntdsp(jproc,2)+ncntdsp(jproc,1)
        ncntdsp(jproc+1,1)=orbs%norb_par(jproc,0)
     end do
     !call f_zero(orbs%norb*orbs%nkpts,gnrms(1))
     !root mpi task collects the data
     if (nproc > 1) then
        call MPI_GATHERV(gnrmp(1),orbs%norbp,mpitype(gnrmp),gnrms(1),ncntdsp(1,1),&
             ncntdsp(1,2),mpitype(gnrms),0,bigdft_mpi%mpi_comm,ierr)
     else
        call vcopy(orbs%norb*orbs%nkpts,gnrmp(1),1,gnrms(1),1)
     end if

     !if (iproc ==0) print *,'ciao',gnrmp,orbs%nspinor

     !write the values per orbitals
     if (iproc ==0) call write_gnrms(orbs%nkpts,orbs%norb,gnrms)

     call f_free(ncntdsp)
     call f_free(gnrms)
     call f_free(gnrmp)
  end if

  call f_release_routine()

END SUBROUTINE preconditionall2
