!> @file
!!  Routines related to system properties
!! @author
!!    Copyright (C) 2010-2013 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS


!> Initialize the objects needed for the computation: basis sets, allocate required space
subroutine system_initialization(iproc,nproc,dump,inputpsi,input_wf_format,dry_run,&
     & in,atoms,rxyz,GPU,&
     orbs,lnpsidim_orbs,lnpsidim_comp,lorbs,Lzd,Lzd_lin,nlpsp,comms,&
     ref_frags, denspot, locregcenters, inwhichlocreg_old, onwhichatom_old, &
     norb_par_ref, norbu_par_ref, norbd_par_ref,output_grid)
  use module_precisions
  use module_types
  use module_interfaces, only: createWavefunctionsDescriptors, &
       & init_orbitals_data_for_linear, orbitals_descriptors
  use module_xc
  use module_fragments
  use vdwcorrection
  use module_bigdft_output
  use module_atoms, only: set_symmetry_data
  use communications_base, only: comms_cubic
  use communications_init, only: orbitals_communicators
  use Poisson_Solver, only: pkernel_allocate_cavity
  use public_enums
  use f_enums
  use locregs_init, only: initLocregs,lr_set
  use orbitalbasis
  use chess_base, only: chess_init
  use module_dpbox, only: dpbox_set
  use rhopotential, only: set_cfd_data
  use at_domain, only: domain_geocode
  use module_bigdft_mpi
  use module_bigdft_errors
  use module_bigdft_arrays
  use module_bigdft_profiling
  implicit none
  integer, intent(in) :: iproc,nproc
  logical, intent(in) :: dry_run, dump
  integer, intent(out) :: input_wf_format, lnpsidim_orbs, lnpsidim_comp
  type(f_enumerator), intent(inout) :: inputpsi
  type(input_variables), intent(inout) :: in !SM: I had to change this to inout due to the new CheSS parameters within input_variables, maybe to be changed again later...
  type(atoms_data), intent(inout) :: atoms
  real(gp), dimension(3,atoms%astruct%nat), intent(inout) :: rxyz
  type(GPU_pointers), intent(in) :: GPU
  type(orbitals_data), intent(inout) :: orbs, lorbs
  type(local_zone_descriptors), intent(inout) :: Lzd, Lzd_lin
  type(DFT_PSP_projectors), intent(out) :: nlpsp
  type(comms_cubic), intent(out) :: comms
  !real(gp), dimension(atoms%astruct%ntypes,3), intent(in) :: radii_cf
  type(system_fragment), dimension(:), pointer :: ref_frags
  real(kind=8),dimension(3,atoms%astruct%nat),intent(inout),optional :: locregcenters
  integer,dimension(:),pointer,optional:: inwhichlocreg_old, onwhichatom_old
  integer,dimension(0:nproc-1),optional:: norb_par_ref, norbu_par_ref, norbd_par_ref !< support function distribution to be used as a reference
  type(DFT_local_fields), intent(out), optional :: denspot
  logical, intent(in), optional :: output_grid
  !local variables
  character(len = *), parameter :: subname = "system_initialization"
  integer :: nB,nKB,nMB,ii,iat,iorb,nspin_ig,norbe,norbsc,ifrag,nspinor
  logical:: present_inwhichlocreg_old, present_onwhichatom_old, output_grid_, frag_allocated, calculate_bounds
  integer, dimension(:,:), allocatable :: norbsc_arr
  integer, dimension(:), allocatable :: norb_par, norbu_par, norbd_par
  real(kind=8), dimension(:), allocatable :: locrad, times_convol
  integer :: ilr, iilr
  real(kind=8),dimension(:),allocatable :: totaltimes, locrads
  real(kind=8),dimension(2) :: time_max, time_average
  !real(kind=8) :: ratio_before, ratio_after
  logical :: init_projectors_completely
  type(orbital_basis) :: ob
  call f_routine(id=subname)


  output_grid_ = .false.
  if (present(output_grid)) output_grid_ = output_grid

  if (iproc == 0 .and. dump) &
       & call print_atomic_variables(atoms, max(in%hx,in%hy,in%hz), in%ixc)

  !grid spacings of the zone descriptors (not correct, the set is done by system size)
  Lzd=default_lzd()
  !h_input=(/ in%hx, in%hy, in%hz /)
  !call lzd_set_hgrids(Lzd,h_input)
  Lzd%hgrids=(/ in%hx, in%hy, in%hz /) !to be adjusted with the constraints of the box
  ! Create wavefunctions descriptors and allocate them inside the global locreg desc.
  calculate_bounds = .not. (inputpsi .hasattr. 'LINEAR')
  call lr_set(lzd%Glr,iproc,GPU,dump,in%crmult,in%frmult,lzd%hgrids,rxyz,atoms,&
       calculate_bounds,output_grid_)

  if (present(locregcenters)) then
      do iat=1,atoms%astruct%nat
          locregcenters(1:3,iat)=locregcenters(1:3,iat)-atoms%astruct%shift(1:3)
          if (any(locregcenters(:,iat) < 0.0_gp .or. &
               locregcenters(:,iat) > lzd%glr%mesh_coarse%ndims*lzd%glr%mesh_coarse%hgrids)) then
             call f_err_throw('locregcenter outside of global box!', err_name='BIGDFT_RUNTIME_ERROR')
          end if
       end do
  end if

  ! Initialize the object holding the CheSS parameters
  call chess_init(in%chess_dict, in%cp)

  if (present(denspot)) then
     call initialize_DFT_local_fields(denspot, in%ixc, in%nspin, in%alpha_hartree_fock)

     !here the initialization of dpbox can be set up
     call dpbox_set(denspot%dpbox,Lzd%Glr%mesh,denspot%xc,iproc,nproc,bigdft_mpi%mpi_comm, &
          in%SIC%approach, in%nspin)

     ! Create the Poisson solver kernels.
     call system_initKernels(.true.,iproc,nproc,domain_geocode(atoms%astruct%dom),in,denspot)
     call system_createKernels(in,denspot, (get_verbose_level() > 1))
     if (denspot%pkernel%method .hasattr. 'rigid') then
        call epsilon_cavity(atoms,rxyz,denspot%pkernel)
        !allocate cavity, in the case of nonvacuum treatment
     else if (denspot%pkernel%method /= 'VAC') then
          call pkernel_allocate_cavity(denspot%pkernel,&
          vacuum=.not. (denspot%pkernel%method .hasattr. 'sccs'))

          call epsinnersccs_cavity(atoms,rxyz,denspot%pkernel)

     end if
  end if

  ! Create global orbs data structure.
  if(in%nspin==4) then
     nspinor=4
  else
     nspinor=1
  end if
  call orbitals_descriptors(iproc, nproc,in%gen_norb,in%gen_norbu,in%gen_norbd,in%nspin,nspinor,&
       in%gen_nkpt,in%gen_kpt,in%gen_wkpt,orbs,LINEAR_PARTITION_NONE)
  !!write(*,*) 'orbs%norbu', orbs%norbu
  !!write(*,*) 'orbs%norbd', orbs%norbd
  !!write(*,*) 'orbs%norb', orbs%norb
  !!write(*,*) 'orbs%norbup', orbs%norbup
  !!write(*,*) 'orbs%norbdp', orbs%norbdp
  !!write(*,*) 'orbs%norbp', orbs%norbp
  orbs%occup(1:orbs%norb*orbs%nkpts) = in%gen_occup
  if (dump .and. iproc==0) call print_orbitals(orbs, domain_geocode(atoms%astruct%dom))

  ! See if linear scaling should be activated and build the correct Lzd
  call check_linear_and_create_Lzd(iproc,nproc,in%linear,Lzd,atoms,orbs,in%nspin,rxyz)
  lzd_lin=default_lzd()
  call nullify_local_zone_descriptors(lzd_lin)
  lzd_lin%nlr = 0

  ! Create linear orbs data structure.
  !if (inputpsi == INPUT_PSI_LINEAR_AO .or. inputpsi == INPUT_PSI_DISK_LINEAR &
  !    .or. inputpsi == INPUT_PSI_MEMORY_LINEAR) then
  if (inputpsi .hasattr. 'LINEAR') then
     if (.not. (inputpsi .hasattr. 'MEMORY')) then ! == INPUT_PSI_LINEAR_AO .or. inputpsi == INPUT_PSI_DISK_LINEAR) then
        ! First do a simple redistribution
        call init_linear_orbs(LINEAR_PARTITION_SIMPLE)
     else
        ! Directly used the reference distribution
        norb_par = f_malloc(0.to.nproc-1,id='norb_par')
        norbu_par = f_malloc(0.to.nproc-1,id='norbu_par')
        norbd_par = f_malloc(0.to.nproc-1,id='norbd_par')
        if (.not.present(norb_par_ref)) then
           call f_err_throw('norb_par_ref not present', err_name='BIGDFT_RUNTIME_ERROR')
        end if
        call vcopy(nproc, norb_par_ref(0), 1, norb_par(0), 1)
        if (.not.present(norbu_par_ref)) then
           call f_err_throw('norbu_par_ref not present', err_name='BIGDFT_RUNTIME_ERROR')
        end if
        call vcopy(nproc, norbu_par_ref(0), 1, norbu_par(0), 1)
        if (.not.present(norbd_par_ref)) then
           call f_err_throw('norbd_par_ref not present', err_name='BIGDFT_RUNTIME_ERROR')
        end if
        call vcopy(nproc, norbd_par_ref(0), 1, norbd_par(0), 1)
        call init_linear_orbs(LINEAR_PARTITION_OPTIMAL)
        call f_free(norb_par)
        call f_free(norbu_par)
        call f_free(norbd_par)
     end if
     call fragment_stuff()
     call init_lzd_linear()
     ! For restart calculations, the suport function distribution must not be modified
     !if (inputpsi == INPUT_PSI_LINEAR_AO .or. inputpsi == INPUT_PSI_DISK_LINEAR .or. in%lin%fragment_calculation) then
     !SM: added the ".or. fin%lin%fragment_calculation", as this came from a merge with Laura...
     if (.not. (inputpsi .hasattr. 'MEMORY') .or. in%lin%fragment_calculation) then
         times_convol = f_malloc(lorbs%norb,id='times_convol')
         call test_preconditioning()
         time_max(1) = sum(times_convol(lorbs%isorb+1:lorbs%isorb+lorbs%norbp))
         time_average(1) = time_max(1)/real(nproc,kind=8)
         norb_par = f_malloc(0.to.nproc-1,id='norb_par')
         norbu_par = f_malloc(0.to.nproc-1,id='norbu_par')
         norbd_par = f_malloc(0.to.nproc-1,id='norbd_par')
         call optimize_loadbalancing2()
         call deallocate_orbitals_data(lorbs)
         call deallocate_local_zone_descriptors(lzd_lin)
         lzd_lin=default_lzd()
         call nullify_local_zone_descriptors(lzd_lin)
         lzd_lin%nlr = 0
         ! Deallocate here fragment stuff
         !if (.not.(frag_allocated .and. (.not. in%lin%fragment_calculation) .and. inputpsi /= INPUT_PSI_DISK_LINEAR)) then
         !if (frag_allocated) then
         !if (inputpsi == INPUT_PSI_DISK_LINEAR .or. in%lin%fragment_calculation) then
         if (frag_allocated) then
             do ifrag=1,in%frag%nfrag_ref
                call fragment_free(ref_frags(ifrag))
                ref_frags(ifrag)%astruct_frg%nat=-1
                ref_frags(ifrag)%fbasis%forbs=minimal_orbitals_data_null()
                !ref_frags(ifrag)=fragment_null()
                !!call f_free_ptr(ref_frags(ifrag)%astruct_frg%iatype)
                !!call f_free_ptr(ref_frags(ifrag)%astruct_frg%ifrztyp)
                !!call f_free_ptr(ref_frags(ifrag)%astruct_frg%input_polarization)
                !!call f_free_ptr(ref_frags(ifrag)%astruct_frg%rxyz)
                !!call f_free_ptr(ref_frags(ifrag)%astruct_frg%rxyz_int)
                !!call f_free_ptr(ref_frags(ifrag)%astruct_frg%ixyz_int)
             end do
            deallocate(ref_frags)
         end if
         call init_linear_orbs(LINEAR_PARTITION_OPTIMAL)
         totaltimes = f_malloc0(nproc,id='totaltimes')
         call fragment_stuff()
         call init_lzd_linear()
         call test_preconditioning()
         time_max(2) = sum(times_convol(lorbs%isorb+1:lorbs%isorb+lorbs%norbp))
         time_average(2) = time_max(2)/real(nproc,kind=8)
         totaltimes(iproc+1) = time_max(2)
         if (nproc>1) then
             call fmpi_allreduce(time_max, FMPI_MAX, comm=bigdft_mpi%mpi_comm)
             call fmpi_allreduce(time_average, FMPI_SUM, comm=bigdft_mpi%mpi_comm)
             call fmpi_allreduce(totaltimes, FMPI_SUM, comm=bigdft_mpi%mpi_comm)
         end if
         !ratio_before = real(time_max(1),kind=8)/real(max(1.d0,time_min(1)),kind=8) !max to prevent divide by zero
         !ratio_after = real(time_max(2),kind=8)/real(max(1.d0,time_min(2)),kind=8) !max to prevent divide by zero
         !if (iproc==0) call yaml_map('preconditioning load balancing min/max before',(/time_min(1),time_max(1)/),fmt='(es9.2)')
         !if (iproc==0) call yaml_map('preconditioning load balancing min/max after',(/time_min(2),time_max(2)/),fmt='(es9.2)')
         if (iproc==0) call yaml_map('preconditioning load balancing before',time_max(1)/time_average(1),fmt='(es9.2)')
         if (iproc==0) call yaml_map('preconditioning load balancing after',time_max(2)/time_average(2),fmt='(es9.2)')
         if (iproc==0) call yaml_map('task with max load',maxloc(totaltimes)-1)
         call f_free(norb_par)
         call f_free(norbu_par)
         call f_free(norbd_par)
         call f_free(times_convol)
         call f_free(totaltimes)
     end if
  end if

  !In the case in which the number of orbitals is not "trivial" check whether they are too many
  if (inputpsi /= 'INPUT_PSI_RANDOM') then

     ! Allocations for readAtomicOrbitals (check inguess.dat and psppar files)
     norbsc_arr = f_malloc((/ atoms%natsc+1, in%nspin /),id='norbsc_arr')
     locrad = f_malloc(atoms%astruct%nat,id='locrad')

     !calculate the inputguess orbitals
     !spin for inputguess orbitals
     if (in%nspin==4) then
        nspin_ig=1
     else
        nspin_ig=in%nspin
     end if

     ! Read the inguess.dat file or generate the input guess via the inguess_generator
     call readAtomicOrbitals(atoms,norbe,norbsc,nspin_ig,orbs%nspinor,&
          norbsc_arr,locrad)

     if (in%nspin==4) then
        !in that case the number of orbitals doubles
        norbe=2*norbe
     end if

     ! De-allocations
     call f_free(locrad)
     call f_free(norbsc_arr)

     !Check if orbitals and electrons are present
     if (orbs%norb*orbs%nkpts == 0) &
        & call f_err_throw('No electrons in the system! Check your input variables or atomic positions.', &
        & err_id=BIGDFT_INPUT_VARIABLES_ERROR)
     ! Check the maximum number of orbitals
     if (in%nspin==1 .or. in%nspin==4) then
        if (orbs%norb>norbe) then
           call f_err_throw('The number of orbitals ('+yaml_toa(orbs%norb)// &
                &   ') must not be greater than the number of orbitals ('+yaml_toa(norbe)// &
                &   ') generated from the input guess.',err_id=BIGDFT_INPUT_VARIABLES_ERROR)
        end if
     else if (in%nspin == 2) then
        if (orbs%norbu > norbe) then
           call f_err_throw('The number of orbitals up ('+yaml_toa(orbs%norbu)// &
                &   ') must not be greater than the number of orbitals ('+yaml_toa(norbe)// &
                &   ') generated from the input guess.',err_id=BIGDFT_INPUT_VARIABLES_ERROR)
        end if
        if (orbs%norbd > norbe) then
           call f_err_throw('The number of orbitals down ('+yaml_toa(orbs%norbd) //&
                &   ') must not be greater than the number of orbitals ('+yaml_toa(norbe) //&
                &   ') generated from the input guess.',err_id=BIGDFT_INPUT_VARIABLES_ERROR)
        end if
     end if
  end if

  !allocate communications arrays (allocate it before Projectors because of the definition
  !of iskpts and nkptsp)
  call orbitals_communicators(iproc,nproc,Lzd%Glr,orbs,comms)
  !if (inputpsi == INPUT_PSI_LINEAR_AO .or. inputpsi == INPUT_PSI_DISK_LINEAR &
  !.or. inputpsi == INPUT_PSI_MEMORY_LINEAR) then
  if ((inputpsi .hasattr. 'LINEAR') .and. .not. dry_run) then
     if(iproc==0 .and. dump) call print_orbital_distribution(iproc, nproc, lorbs)
  end if

  if (.not.(inputpsi .hasattr. 'LINEAR') .and. iproc == 0 .and. dump) then
     nB=max(orbs%npsidim_orbs,orbs%npsidim_comp)*8
     nMB=nB/1024/1024
     nKB=(nB-nMB*1024*1024)/1024
     nB=modulo(nB,1024)
     call yaml_map('Wavefunctions memory occupation for root MPI process',&
          trim(yaml_toa(nMB,fmt='(i5)'))//' MB'//trim(yaml_toa(nKB,fmt='(i5)'))//&
          ' KB'//trim(yaml_toa(nB,fmt='(i5)'))//' B')
 !!$     write(*,'(1x,a,3(i5,a))') &
 !!$       'Wavefunctions memory occupation for root MPI process: ',&
 !!$       nMB,' MB ',nKB,' KB ',nB,' B'
  end if
  ! Done orbs

  !!! fragment initializations - if not a fragment calculation, set to appropriate dummy values
  !if (inputpsi /= INPUT_PSI_LINEAR_AO .and. inputpsi /= INPUT_PSI_DISK_LINEAR &
  !    .and. inputpsi /= INPUT_PSI_MEMORY_LINEAR) then
  if (.not. (inputpsi .hasattr. 'LINEAR')) then
      call fragment_stuff()
  end if
  !!frag_allocated=.false.
  !!if (inputpsi == INPUT_PSI_DISK_LINEAR .or. in%lin%fragment_calculation) then
  !!   allocate(ref_frags(in%frag%nfrag_ref))
  !!   do ifrag=1,in%frag%nfrag_ref
  !!      ref_frags(ifrag)=fragment_null()
  !!   end do
  !!   call init_fragments(in,lorbs,atoms%astruct,ref_frags)
  !!  frag_allocated=.true.
  !!else
  !!   nullify(ref_frags)
  !!end if

  !!call input_check_psi_id(inputpsi, input_wf_format, in%dir_output, &
  !!     orbs, lorbs, iproc, nproc, in%frag%nfrag_ref, in%frag%dirname, ref_frags)

  !!! we need to deallocate the fragment arrays we just allocated as not a restart calculation so this is no longer needed
  !!if (frag_allocated .and. (.not. in%lin%fragment_calculation) .and. inputpsi /= INPUT_PSI_DISK_LINEAR) then
  !!    do ifrag=1,in%frag%nfrag_ref
  !!       ref_frags(ifrag)%astruct_frg%nat=-1
  !!       ref_frags(ifrag)%fbasis%forbs=minimal_orbitals_data_null()
  !!       call fragment_free(ref_frags(ifrag))
  !!       !ref_frags(ifrag)=fragment_null()
  !!    end do
  !!   deallocate(ref_frags)
  !!end if

  ! Calculate all projectors, or allocate array for on-the-fly calculation
  ! SM: For a linear scaling calculation, some parts can be done later.
  ! SM: The following flag is false for linear scaling and true otherwise.
  init_projectors_completely =   .not.(inputpsi .hasattr. 'LINEAR')
  !(inputpsi /= INPUT_PSI_LINEAR_AO .and. &
  !                              inputpsi /= INPUT_PSI_DISK_LINEAR .and. &
  !                              inputpsi /= INPUT_PSI_MEMORY_LINEAR)
  call orbital_basis_associate(ob,orbs=orbs,Lzd=Lzd,id='system_initialization')
  call createProjectorsArrays(Lzd%Glr,rxyz,atoms,ob%orbs,&
       in%frmult,in%frmult,in%projection,dry_run,nlpsp,init_projectors_completely)
  call orbital_basis_release(ob)
  if (iproc == 0 .and. dump) call print_nlpsp(nlpsp)
  if (iproc == 0 .and. .not. nlpsp%on_the_fly .and. .false.) then
     call writemyproj("proj",WF_FORMAT_BINARY,orbs,Lzd%hgrids(1),Lzd%hgrids(2),&
       Lzd%hgrids(3),atoms,rxyz,nlpsp,lzd%glr)
  end if
  !the complicated part of the descriptors has not been filled
  if (dry_run) then
     call f_release_routine()
     return
  end if
  !calculate the partitioning of the orbitals between the different processors
!  print *,'here the localization regions should have been filled already'
!  stop

  if (present(denspot)) then
     !here dpbox can be put as input
     call density_descriptors(iproc,nproc,denspot%xc,in%nspin,in%crmult,in%frmult,atoms,&
          denspot%dpbox,in%rho_commun,rxyz,denspot%rhod)
     !allocate the arrays.
     call allocateRhoPot(in%nspin,atoms,rxyz,denspot)
     !here insert the conditional for the constrained field dynamics
     if (in%calculate_magnetic_torque) then
        call set_cfd_data(denspot%cfd,denspot%dpbox%mesh,atoms%astruct,rxyz)
     end if
     if (in%do_spin_dynamics) then
        if (in%calculate_magnetic_torque) then
           print *,'spin dynamics is go for launch'
           !call asd_allocate(asd)
        else
           print *,'spin dynamics needs constraining fields..'
        end if
     end if
  end if

  !calculate the irreductible zone for this region, if necessary.
  call set_symmetry_data(atoms%astruct%sym,domain_geocode(atoms%astruct%dom), &
       & Lzd%Glr%mesh%ndims(1),Lzd%Glr%mesh%ndims(2),Lzd%Glr%mesh%ndims(3), in%nspin)

  ! A message about dispersion forces.
  call vdwcorrection_initializeparams(in%ixc, in%dispersion)

  !check the communication distribution
  !if(inputpsi /= INPUT_PSI_LINEAR_AO .and. inputpsi /= INPUT_PSI_DISK_LINEAR &
  !   .and. inputpsi /= INPUT_PSI_MEMORY_LINEAR) then
  if (.not.(inputpsi .hasattr. 'LINEAR')) then
     call check_communications(iproc,nproc,orbs,Lzd,comms)
  else
      ! Do not call check_communication, since the value of orbs%npsidim_orbs is wrong
      !if(iproc==0) call yaml_warning('Do not call check_communications in the linear scaling version!')
      !if(iproc==0) write(*,*) 'WARNING: do not call check_communications in the linear scaling version!'
  end if

  call f_release_routine()
  !---end of system definition routine


  contains


    subroutine init_linear_orbs(linear_partition)
      use module_interfaces, only: init_orbitals_data_for_linear
     implicit none
     integer,intent(in) :: linear_partition

     if (linear_partition==LINEAR_PARTITION_SIMPLE) then
         if (present(locregcenters)) then
             call init_orbitals_data_for_linear(iproc, nproc, orbs%nspinor, in, atoms%astruct, locregcenters, lorbs)
         else
             call init_orbitals_data_for_linear(iproc, nproc, orbs%nspinor, in, atoms%astruct, atoms%astruct%rxyz, lorbs)
         end if
     else if (linear_partition==LINEAR_PARTITION_OPTIMAL) then
         if (present(locregcenters)) then
             call init_orbitals_data_for_linear(iproc, nproc, orbs%nspinor, in, atoms%astruct, locregcenters, lorbs, &
                  norb_par, norbu_par, norbd_par)
         else
             call init_orbitals_data_for_linear(iproc, nproc, orbs%nspinor, in, atoms%astruct, atoms%astruct%rxyz, lorbs, &
                  norb_par, norbu_par, norbd_par)
         end if
     else
         !stop 'init_linear_orbs: wrong value of linear_partition'
         call f_err_throw('init_linear_orbs: wrong value of linear_partition',err_name='BIGDFT_RUNTIME_ERROR')
     end if

       ! There are needed for the restart (at least if the atoms have moved...)
       present_inwhichlocreg_old = present(inwhichlocreg_old)
       present_onwhichatom_old = present(onwhichatom_old)
       if (present_inwhichlocreg_old .and. .not.present_onwhichatom_old &
           .or. present_onwhichatom_old .and. .not.present_inwhichlocreg_old) then
           call f_err_throw('inwhichlocreg_old and onwhichatom_old should be present at the same time', &
           & err_name='BIGDFT_INPUT_VARIABLES_ERROR')
           !stop
       end if
       if (present_inwhichlocreg_old .and. present_onwhichatom_old) then
           call vcopy(lorbs%norb, onwhichatom_old(1), 1, lorbs%onwhichatom(1), 1)
           !call vcopy(lorbs%norb, inwhichlocreg_old(1), 1, lorbs%inwhichlocreg(1), 1)
           !use onwhichatom to build the new inwhichlocreg (because the old inwhichlocreg can be ordered differently)
           ii = 0
           do iat=1, atoms%astruct%nat
              !only want to do this for spin=1!
              do iorb=1,lorbs%norbu
                 if(iat ==  lorbs%onwhichatom(iorb)) then
                    ii = ii + 1
                    lorbs%inwhichlocreg(iorb)= ii
                 end if
              end do
           end do

           ! LR: not sure if this is the best way to do this but it seems to work...
           ! Correction for spin polarized systems. For non polarized systems, norbu=norb and the loop does nothing.
           do iorb=lorbs%norbu+1,lorbs%norb
               lorbs%inwhichlocreg(iorb)=lorbs%inwhichlocreg(iorb-lorbs%norbu)+lorbs%norbu
               lorbs%onwhichatom(iorb)=lorbs%onwhichatom(iorb-lorbs%norbu)+lorbs%norbu
           end do
       end if
     end subroutine init_linear_orbs

     subroutine init_lzd_linear()
       use module_interfaces, only: initialize_linear_from_file
       use locregs_init, only: initLocregs
       use locregs, only: copy_locreg_descriptors
       use sparsematrix_wrappers, only: check_kernel_cutoff
       implicit none
       call copy_locreg_descriptors(Lzd%Glr, lzd_lin%glr)
       !call lzd_set_hgrids(lzd_lin, Lzd%hgrids)
       lzd_lin%hgrids=Lzd%hgrids
       if (inputpsi == 'INPUT_PSI_LINEAR_AO' .or. inputpsi == 'INPUT_PSI_MEMORY_LINEAR') then
           !!write(*,*) 'rxyz',rxyz
           !!write(*,*) 'locregcenters',locregcenters
           if (present(locregcenters)) then
              call lzd_init_llr(iproc, nproc, in, atoms%astruct, locregcenters, lorbs, lzd_lin)
          else
              call lzd_init_llr(iproc, nproc, in, atoms%astruct, atoms%astruct%rxyz, lorbs, lzd_lin)
          end if

       else
          call initialize_linear_from_file(iproc,nproc,in%frag,atoms%astruct,rxyz,lorbs,lzd_lin,&
               in%dir_output,'minBasis',ref_frags)
          !what to do with derivatives?
          ! These values are not read from file, not very nice this way
          do ilr=1,lzd_lin%nlr
              iilr=mod(ilr-1,lorbs%norbu)+1 !correct value for a spin polarized system
              lzd_lin%llr(ilr)%locrad_kernel=in%lin%locrad_kernel(iilr)
              lzd_lin%llr(ilr)%locrad_mult=in%lin%locrad_mult(iilr)
          end do
       end if

       ! Make sure that the cutoff for the multiplications
       ! is larger than the kernel cutoff
       call check_kernel_cutoff(iproc, lorbs, atoms, in%hamapp_radius_incr, lzd_lin)
       do ilr=1,lzd_lin%nlr
          if (lzd_lin%llr(ilr)%locrad_mult < lzd_lin%llr(ilr)%locrad_kernel) then
             call f_err_throw('rloc_kernel_foe ('//trim(yaml_toa(lzd_lin%llr(ilr)%locrad_mult,fmt='(f5.2)'))//&
                  &') too small, must be at least as big as rloc_kernel('&
                  &//trim(yaml_toa(lzd_lin%llr(ilr)%locrad_kernel,fmt='(f5.2)'))//')', err_id=BIGDFT_RUNTIME_ERROR)
          end if
       end do

       if (.not. dry_run) then
          locrads = f_malloc(lzd_lin%nlr)
          locrads = lzd_lin%llr(:)%locrad
          call initLocregs(iproc, nproc, lzd_lin, atoms%astruct%rxyz,locrads, lorbs, Lzd_lin%Glr, 's')
          call update_wavefunctions_size(lzd_lin,lnpsidim_orbs,lnpsidim_comp,lorbs,iproc,nproc)
          call f_free(locrads)
       else
          call yaml_warning("Locregs not initialized for linear.")
       end if
     end subroutine init_lzd_linear


     !this routine should go in the locreg_operation module
     subroutine test_preconditioning()
       use locregs
       use mpif_module, only: mpi_wtime
       implicit none

       !Local variables
       integer :: iorb, iiorb, ilr, ncplx, ist, i, ierr, ii, jj
       logical :: with_confpot
       real(gp) :: kx, ky, kz
       real(kind=8),dimension(:),allocatable :: phi
       real(kind=8) :: t1, t2, time, tt, maxtime
       integer,parameter :: nit=5
       real(kind=8),dimension(2*nit+1) :: times

      call f_zero(times_convol)
      do iorb=1,lorbs%norbp
          iiorb=lorbs%isorb+iorb
          ilr=lorbs%inwhichlocreg(iiorb)
          ii = int(lzd_lin%llr(ilr)%mesh_coarse%ndim)
          jj = 7*(lzd_lin%llr(ilr)%nboxf(2,1)-lzd_lin%llr(ilr)%nboxf(1,1)+1)*&
                 (lzd_lin%llr(ilr)%nboxf(2,2)-lzd_lin%llr(ilr)%nboxf(1,2)+1)*&
                 (lzd_lin%llr(ilr)%nboxf(2,3)-lzd_lin%llr(ilr)%nboxf(1,3)+1)
          times_convol(iiorb) = real(ii+jj,kind=8)
      end do
      if (nproc>1) then
          call fmpi_allreduce(times_convol, FMPI_SUM, comm=bigdft_mpi%mpi_comm)
      end if

      return !###############################################3

       phi = f_malloc(lnpsidim_orbs,id='phi')
       phi=1.d-5

      call f_zero(times_convol)

      if (nproc>1) then
          call mpi_barrier(bigdft_mpi%mpi_comm, ierr)
      end if
       ist=0
       tt = 0.d0
       do iorb=1,lorbs%norbp
           iiorb = lorbs%isorb + iorb
           ilr = lorbs%inwhichlocreg(iiorb)
           kx=lorbs%kpts(1,lorbs%iokpt(iorb))
           ky=lorbs%kpts(2,lorbs%iokpt(iorb))
           kz=lorbs%kpts(3,lorbs%iokpt(iorb))
           if (kx**2+ky**2+kz**2 > 0.0_gp .or. lorbs%nspinor==2 ) then
              ncplx=2
           else
              ncplx=1
           end if
           do i=1,2*nit+1
               t1 = mpi_wtime()
               call solvePrecondEquation(lzd_lin%llr(ilr), ncplx, 6, -0.5d0, &
                    lzd_lin%hgrids(1), lzd_lin%hgrids(2), lzd_lin%hgrids(3), &
                    lorbs%kpts(1,lorbs%iokpt(iorb)), lorbs%kpts(1,lorbs%iokpt(iorb)), lorbs%kpts(1,lorbs%iokpt(iorb)), &
                    phi(1+ist), lzd_lin%llr(ilr)%locregCenter,&
                    1.d-3, 4)
               t2 = mpi_wtime()
               times(i) = t2-t1
           end do
           ! Take the median
           write(20000+iproc,'(a,i7,2es13.2,5x,11es12.2)') 'iiorb, time, tottime, alltimes', iiorb, time, tt, times
           maxtime = maxval(times)
           do i=1,nit
               times(minloc(times,1)) = maxtime
           end do
           do i=1,2*nit
               times(maxloc(times,1)) = 0.d0
           end do
           time = maxval(times)
           tt = tt + time
           write(20000+iproc,'(a,i7,2es13.2,5x,11es12.2)') 'iiorb, time, tottime, alltimes', iiorb, time, tt, times
           times_convol(iiorb) = time
           ist = ist + array_dim(lzd_lin%llr(ilr))*ncplx
       end do
       write(20000+iproc,'(a)') '==========================='

       call f_free(phi)

       if (nproc>1) then
           call fmpi_allreduce(times_convol, FMPI_SUM, comm=bigdft_mpi%mpi_comm)
       end if

     end subroutine test_preconditioning

     !!subroutine optimize_loadbalancing()
     !!  implicit none

     !!  ! Local variables
     !!  integer(kind=8) :: isize, isize_ideal

     !!  ! Sum up the total size of all support functions
     !!  isize = int(lnpsidim_orbs,kind=8)
     !!  if (nproc>1) then
     !!      call fmpi_allreduce(isize, 1, FMPI_SUM, bigdft_mpi%mpi_comm)
     !!  end if

     !!  ! Ideal size per task (integer division)
     !!  isize_ideal = isize/int(nproc,kind=8)

     !!  ! Redistribute the support functions such that the load balancing is optimal
     !!  call redistribute(lorbs%norb, isize_ideal, norb_par)

     !!  ! The same for the up and down orbitals
     !!  isize_ideal = isize_ideal/int(in%nspin,kind=8)
     !!  call redistribute(lorbs%norbu, isize_ideal, norbu_par)
     !!  call redistribute(lorbs%norbd, isize_ideal, norbd_par)

     !!end subroutine optimize_loadbalancing

     !!subroutine redistribute(norb, isize_ideal, norb_par)
     !!  implicit none
     !!  integer,intent(in) :: norb
     !!  integer(kind=8),intent(in) :: isize_ideal
     !!  integer,dimension(0:nproc-1),intent(out) :: norb_par
     !!  integer :: jjorbtot, jjorb, jproc, jlr, jorb
     !!  integer(kind=8) :: ncount

     !!  call f_zero(norb_par)
     !!  ncount = int(0,kind=8)
     !!  jproc = 0
     !!  jjorb = 0
     !!  jjorbtot = 0
     !!  do jorb=1,norb
     !!      jjorb = jjorb + 1
     !!      jlr = lorbs%inwhichlocreg(jorb)
     !!      ncount = ncount + array_dim(lzd_lin%llr(jlr))
     !!      if (ncount>=isize_ideal*int(jproc+1,kind=8)) then
     !!          norb_par(jproc) = jjorb
     !!          jjorbtot = jjorbtot + jjorb
     !!          jjorb = 0
     !!          jproc = jproc + 1
     !!      end if
     !!      if (jproc==nproc-1) exit
     !!  end do
     !!  norb_par(nproc-1) = (norb - jjorbtot) !+jjorb -> why was this needed?! !take the rest
     !!  do jproc=0,nproc-1
     !!      !if (iproc==0) write(*,*) 'jproc, norb_par(jproc)', jproc, norb_par(jproc)
     !!  end do
     !!end subroutine redistribute


     subroutine optimize_loadbalancing2()
       use sparsematrix_init, only: redistribute
       implicit none

       ! Local variables
       real(kind=8) :: time, time_ideal

       ! Sum up the total size of all support functions
       time = sum(times_convol)

       ! Ideal size per task (integer division)
       time_ideal = time/int(nproc,kind=8)

       ! Redistribute the support functions such that the load balancing is optimal
       !!call redistribute2(lorbs%norb, time_ideal, norb_par)
       call redistribute(nproc, lorbs%norb, times_convol, time_ideal, norb_par)

       ! The same for the up and down orbitals
       time_ideal = time_ideal/int(in%nspin,kind=8)
       !!call redistribute2(lorbs%norbu, time_ideal, norbu_par)
       !!call redistribute2(lorbs%norbd, time_ideal, norbd_par)
       call redistribute(nproc, lorbs%norbu, times_convol, time_ideal, norbu_par)
       call redistribute(nproc, lorbs%norbd, times_convol, time_ideal, norbd_par)

     end subroutine optimize_loadbalancing2

     !!subroutine redistribute2(norb, time_ideal, norb_par)
     !!  implicit none
     !!  integer,intent(in) :: norb
     !!  real(kind=8),intent(in) :: time_ideal
     !!  integer,dimension(0:nproc-1),intent(out) :: norb_par
     !!  integer :: jjorbtot, jjorb, jproc, jlr, jorb
     !!  real(kind=8) :: tcount
       !integer :: jlr

     !!  call f_zero(norb_par)
     !!  if (norb>=nproc) then
     !!      tcount = 0.d0
     !!      jproc = 0
     !!      jjorb = 0
     !!      jjorbtot = 0
     !!      do jorb=1,norb
     !!          if (jproc==nproc-1) exit
     !!          jjorb = jjorb + 1
     !!          if(jorb==norb) exit !just to besure that no out of bound happens
     !!          tcount = tcount + times_convol(jorb)
     !!          !if (iproc==0) write(*,'(a,2i8,2es14.5)') 'jorb, jproc, tcount, diff to target', jorb, jproc, tcount, abs(tcount-time_ideal*real(jproc+1,kind=8))
     !!          if (abs(tcount-time_ideal*real(jproc+1,kind=8)) <= &
     !!                  abs(tcount+times_convol(jorb+1)-time_ideal*real(jproc+1,kind=8))) then
     !!              norb_par(jproc) = jjorb
     !!              jjorbtot = jjorbtot + jjorb
     !!              jjorb = 0
     !!              jproc = jproc + 1
     !!          end if
     !!      end do
     !!      norb_par(nproc-1) = jjorb + (norb - jjorbtot) !take the rest
     !!      !do jproc=0,nproc-1
     !!      !    if (iproc==0) write(*,*) 'jproc, norb_par(jproc)', jproc, norb_par(jproc)
     !!      !end do
     !!  else
     !!      ! Equal distribution
     !!      norb_par(0:norb-1) = 1
     !!  end if
     !!end subroutine redistribute2


     subroutine fragment_stuff()
       use io, only: input_check_psi_id
       implicit none
       integer :: iorbn
       integer, allocatable, dimension(:) :: inwhichlocreg_tmp, onwhichatom_tmp

       frag_allocated=.false.
       if (inputpsi == 'INPUT_PSI_DISK_LINEAR' .or. in%lin%fragment_calculation) then
          allocate(ref_frags(in%frag%nfrag_ref))
          do ifrag=1,in%frag%nfrag_ref
             ref_frags(ifrag)=fragment_null()
          end do
          call init_fragments(in,lorbs,atoms%astruct,ref_frags)
         frag_allocated=.true.
         call input_check_psi_id(inputpsi, in%dir_output, &
              orbs, lorbs, in%frag%nfrag_ref, in%lin%fragment_calculation, &
              in%frag%dirname, ref_frags)
       else
          nullify(ref_frags)
         frag_allocated=.false.
       end if

       ! we need to deallocate the fragment arrays we just allocated as not a restart calculation so this is no longer needed
       if (frag_allocated .and. (.not. in%lin%fragment_calculation) .and. inputpsi /= 'INPUT_PSI_DISK_LINEAR') then
           do ifrag=1,in%frag%nfrag_ref
              call fragment_free(ref_frags(ifrag))
              ref_frags(ifrag)%astruct_frg%nat=-1
              ref_frags(ifrag)%fbasis%forbs=minimal_orbitals_data_null()
              !ref_frags(ifrag)=fragment_null()
           end do
          deallocate(ref_frags)
         frag_allocated=.false.
       end if

       ! not sure if this will mess up load balancing but we want the tmbs to stay in input file order
       ! for reading for disk this is taken care of automatically
       if (in%lin%fragment_calculation .and. inputpsi /= 'INPUT_PSI_DISK_LINEAR') then
          inwhichlocreg_tmp = f_malloc(lorbs%norb,id='inwhichlocreg_tmp')
          onwhichatom_tmp = f_malloc(lorbs%norb,id='onwhichatom_tmp')

          call vcopy(lorbs%norb,lorbs%inwhichlocreg(1),1,inwhichlocreg_tmp(1),1)
          call vcopy(lorbs%norb,lorbs%onwhichatom(1),1,onwhichatom_tmp(1),1)

          !!print*,lorbs%norb
          !!print*,lorbs%inwhichlocreg
          !!print*,lorbs%onwhichatom

          ! might be a better way of doing this...
          ! assume that norbu==norbd etc, check if this is always true...
          iorbn=0
          do iat=1,atoms%astruct%nat
             do iorb=1,lorbs%norbu
                if (onwhichatom_tmp(iorb)/=iat) cycle
                iorbn = iorbn+1
                lorbs%onwhichatom(iorbn) = iat
                lorbs%inwhichlocreg(iorbn) = inwhichlocreg_tmp(iorb)
                ! perform the same change for spin down
                if (lorbs%nspinor==2) then
                   lorbs%onwhichatom(iorbn+lorbs%norbu) = iat
                   lorbs%inwhichlocreg(iorbn+lorbs%norbu) = inwhichlocreg_tmp(iorb+lorbs%norbu)
                end if
                ! double check - might not always be true in future
                if (iorbn /= inwhichlocreg_tmp(iorb)) then
                   write(*,*) 'Error reordering tmbs for fragment calculation (1)',iorbn,inwhichlocreg_tmp(iorb)
                   stop
                end if
             end do
          end do
          if (iorbn /= lorbs%norbu) then
             write(*,*) 'Error reordering tmbs for fragment calculation (2)',iorbn,lorbs%norb
             stop
          end if


          call f_free(inwhichlocreg_tmp)
          call f_free(onwhichatom_tmp)
       end if

     end subroutine fragment_stuff

END SUBROUTINE system_initialization

!> See if linear scaling should be activated and build the correct Lzd 
subroutine check_linear_and_create_Lzd(iproc,nproc,linType,Lzd,atoms,orbs,nspin,rxyz)
  use module_precisions
  use module_types
  use module_xc
  use ao_inguess, only: atomic_info
  use locregs, only: locreg_null,copy_locreg_descriptors
  use public_enums
  use locregs_init, only: check_linear_inputguess,initlocregs
  use module_bigdft_arrays
  implicit none

  integer, intent(in) :: iproc,nproc,nspin
  type(local_zone_descriptors), intent(inout) :: Lzd
  type(atoms_data), intent(in) :: atoms
  type(orbitals_data), intent(inout) :: orbs
  real(gp), dimension(3,atoms%astruct%nat), intent(in) :: rxyz
  integer, intent(in) :: linType
!  real(gp), dimension(atoms%astruct%ntypes,3), intent(in) :: radii_cf
  !Local variables
  character(len=*), parameter :: subname='check_linear_and_create_Lzd'
  logical :: linear
  real(gp) :: rcov
  integer :: iat,ityp,nspin_ig,ilr
  real(gp), dimension(:), allocatable :: locrad

  !default variables
  Lzd%nlr = 1

  if (nspin == 4) then
     nspin_ig=1
  else
     nspin_ig=nspin
  end if

  linear  = .true.
  if (linType == INPUT_IG_FULL) then
     Lzd%nlr=atoms%astruct%nat
     locrad = f_malloc(Lzd%nlr,id='locrad')
     ! locrad read from last line of  psppar
     do iat=1,atoms%astruct%nat
        ityp = atoms%astruct%iatype(iat)
        call atomic_info(atoms%nzatom(ityp),atoms%nelpsp(ityp),rcov=rcov)
        locrad(iat) =  rcov * 10.0_gp ! locrad(iat) = atoms%rloc(ityp,1)
     end do
     call timing(iproc,'check_IG      ','ON')
     call check_linear_inputguess(iproc,Lzd%nlr,rxyz,locrad,&
          Lzd%hgrids(1),Lzd%hgrids(2),Lzd%hgrids(3),&
          Lzd%Glr,linear)
     call timing(iproc,'check_IG      ','OF')
     if(nspin >= 4) linear = .false.
  end if

  ! If we are using cubic code : by choice or because locregs are too big
  Lzd%linear = .true.
  if (linType == INPUT_IG_LIG .or. linType == INPUT_IG_OFF .or. .not. linear) then
     Lzd%linear = .false.
     Lzd%nlr = 1
  end if


  allocate(Lzd%Llr(Lzd%nlr))
  do ilr=1,Lzd%nlr
     Lzd%Llr(ilr)=locreg_null()
  end do
  !for now, always true because we want to calculate the hamiltonians for all locregs
  if(.not. Lzd%linear) then
     !copy Glr to Llr(1)
     call copy_locreg_descriptors(Lzd%Glr,Lzd%Llr(1))
  else
     ! Assign orbitals to locreg (for LCAO IG each orbitals corresponds to an atomic function. WILL NEED TO CHANGE THIS)
     call assignToLocreg(iproc,nproc,orbs%nspinor,nspin_ig,atoms,orbs,Lzd)

     ! determine the localization regions
     ! calculateBounds indicate whether the arrays with the bounds (for convolutions...) shall also
     ! be allocated and calculated. In principle this is only necessary if the current process has orbitals
     ! in this localization region.
     call initLocregs(iproc, nproc, lzd, rxyz,locrad, orbs,Lzd%Glr,'c')
!!$        calculateBounds = f_malloc(lzd%nlr,id='calculateBounds')
!!$        calculateBounds=.true.
!!$!        call determine_locreg_periodic(iproc,Lzd%nlr,rxyz,locrad,hx,hy,hz,Lzd%Glr,Lzd%Llr,calculateBounds)
!!$        call determine_locreg_parallel(iproc,nproc,Lzd%nlr,rxyz,locrad,&
!!$             Lzd%hgrids(1),Lzd%hgrids(2),Lzd%hgrids(3),Lzd%Glr,Lzd%Llr,&
!!$             orbs,calculateBounds)
!!$        call f_free(calculateBounds)
     call f_free(locrad)

     ! determine the wavefunction dimension
     call wavefunction_dimension(Lzd,orbs)
  end if


!DEBUG
!!if(iproc==0)then
!!print *,'###################################################'
!!print *,'##        General information:                   ##'
!!print *,'###################################################'
!!print *,'Lzd%nlr,linear, ndimpotisf :',Lzd%nlr,Lzd%linear,Lzd%ndimpotisf
!!print *,'###################################################'
!!print *,'##        Global box information:                ##'
!!print *,'###################################################'
!!write(*,'(a24,3i4)')'Global region n1,n2,n3:',Lzd%Glr%d%n1,Lzd%Glr%d%n2,Lzd%Glr%d%n3
!!write(*,*)'Global fine grid: nfl',Lzd%Glr%d%nfl1,Lzd%Glr%d%nfl2,Lzd%Glr%d%nfl3
!!write(*,*)'Global fine grid: nfu',Lzd%Glr%d%nfu1,Lzd%Glr%d%nfu2,Lzd%Glr%d%nfu3
!!write(*,*)'Global inter. grid: ni',Lzd%Glr%d%n1i,Lzd%Glr%d%n2i,Lzd%Glr%d%n3i
!!write(*,'(a27,f6.2,f6.2,f6.2)')'Global dimension (1x,y,z):',Lzd%Glr%d%n1*hx,Lzd%Glr%d%n2*hy,Lzd%Glr%d%n3*hz
!!write(*,'(a17,f12.2)')'Global volume: ',Lzd%Glr%d%n1*hx*Lzd%Glr%d%n2*hy*Lzd%Glr%d%n3*hz
!!print *,'Global wfd statistics:',Lzd%Glr%wfd%nseg_c,Lzd%Glr%wfd%nseg_f,Lzd%Glr%wfd%nvctr_c,Lzd%Glr%wfd%nvctr_f
!!print *,'###################################################'
!!print *,'##        Local boxes information:               ##'
!!print *,'###################################################'
!!do i_stat =1, Lzd%nlr
!!   write(*,*)'=====> Region:',i_stat
!!   write(*,'(a24,3i4)')'Local region n1,n2,n3:',Lzd%Llr(i_stat)%d%n1,Lzd%Llr(i_stat)%d%n2,Lzd%Llr(i_stat)%d%n3
!!   write(*,*)'Local fine grid: nfl',Lzd%Llr(i_stat)%d%nfl1,Lzd%Llr(i_stat)%d%nfl2,Lzd%Llr(i_stat)%d%nfl3
!!   write(*,*)'Local fine grid: nfu',Lzd%Llr(i_stat)%d%nfu1,Lzd%Llr(i_stat)%d%nfu2,Lzd%Llr(i_stat)%d%nfu3
!!   write(*,*)'Local inter. grid: ni',Lzd%Llr(i_stat)%d%n1i,Lzd%Llr(i_stat)%d%n2i,Lzd%Llr(i_stat)%d%n3i
!!   write(*,'(a27,f6.2,f6.2,f6.2)')'Local dimension (1x,y,z):',Lzd%Llr(i_stat)%d%n1*hx,Lzd%Llr(i_stat)%d%n2*hy,&
!!            Lzd%Llr(i_stat)%d%n3*hz
!!   write(*,'(a17,f12.2)')'Local volume: ',Lzd%Llr(i_stat)%d%n1*hx*Lzd%Llr(i_stat)%d%n2*hy*Lzd%Llr(i_stat)%d%n3*hz
!!   print *,'Local wfd statistics:',Lzd%Llr(i_stat)%wfd%nseg_c,Lzd%Llr(i_stat)%wfd%nseg_f,Lzd%Llr(i_stat)%wfd%nvctr_c,&
!!            Lzd%Llr(i_stat)%wfd%nvctr_f
!!end do
!!end if
!!call mpi_finalize(i_stat)
!!stop
!END DEBUG

end subroutine check_linear_and_create_Lzd

!> Initialize the orbitals date for the linear version.
subroutine init_orbitals_data_for_linear(iproc, nproc, nspinor, input, astruct, rxyz, lorbs, &
           norb_par_ref, norbu_par_ref, norbd_par_ref)
  use module_types
  use module_interfaces, only: orbitals_descriptors
  use public_enums
  use locregs_init, only: assign_to_atoms_and_locregs
  use module_bigdft_arrays
  use module_bigdft_profiling
  implicit none

  ! Calling arguments
  integer, intent(in) :: iproc, nproc, nspinor
  type(input_variables), intent(in) :: input
  type(atomic_structure), intent(in) :: astruct
  real(kind=8),dimension(3,astruct%nat), intent(in) :: rxyz
  type(orbitals_data), intent(out) :: lorbs
  integer,dimension(0:nproc-1),intent(in),optional :: norb_par_ref, norbu_par_ref, norbd_par_ref

  ! Local variables
  integer :: norb, norbu, norbd, ityp, iat, ilr, iorb, nlr, iiat, ispin
  integer, dimension(:), allocatable :: norbsPerLocreg, norbsPerAtom
  real(kind=8),dimension(:,:), allocatable :: locregCenter
  character(len=*), parameter :: subname='init_orbitals_data_for_linear'
  logical :: with_optional
  logical,dimension(3) :: optional_present

  call timing(iproc,'init_orbs_lin ','ON')

  call f_routine(id='init_orbitals_data_for_linear')

  ! Check the arguments
  optional_present(1) = present(norb_par_ref)
  optional_present(2) = present(norbu_par_ref)
  optional_present(3) = present(norbd_par_ref)
  if (any(optional_present)) then
      if (all(optional_present)) then
          with_optional = .true.
      else
          stop 'init_orbitals_data_for_linear: not all optional arguments present'
      end if
  else
          with_optional = .false.
  end if


  call nullify_orbitals_data(lorbs)


  ! Count the number of basis functions.
  norbsPerAtom = f_malloc(astruct%nat*input%nspin,id='norbsPerAtom')
  norbu=0
  nlr=0
  iiat=0
  do ispin=1,input%nspin
      do iat=1,astruct%nat
          iiat=iiat+1
          ityp=astruct%iatype(iat)
          norbsPerAtom(iiat)=input%lin%norbsPerType(ityp)
          if (ispin==1) then
              norbu=norbu+input%lin%norbsPerType(ityp)
          end if
          !nlr=nlr+input%lin%norbsPerType(ityp)
      end do
  end do

  ! For spin polarized systems, use twice the number of basis functions (i.e. norbd=norbu)
  if (input%nspin==1) then
      norbd=0
  else
      norbd=norbu
  end if
  norb=norbu+norbd

  nlr=norb


  ! Distribute the basis functions among the processors.
  if (with_optional) then
      call orbitals_descriptors(iproc, nproc, norb, norbu, norbd, input%nspin, nspinor,&
           input%gen_nkpt, input%gen_kpt, input%gen_wkpt, lorbs,LINEAR_PARTITION_OPTIMAL,&
           norb_par_ref, norbu_par_ref, norbd_par_ref)
  else
      call orbitals_descriptors(iproc, nproc, norb, norbu, norbd, input%nspin, nspinor,&
           input%gen_nkpt, input%gen_kpt, input%gen_wkpt, lorbs,LINEAR_PARTITION_SIMPLE) !simple repartition
   end if

  !!locregCenter = f_malloc((/ 3, nlr /),id='locregCenter')

  !!
  !!! this loop does not take into account the additional TMBs required for spin polarized systems
  !!ilr=0
  !!do iat=1,astruct%nat
  !!    ityp=astruct%iatype(iat)
  !!    do iorb=1,input%lin%norbsPerType(ityp)
  !!        ilr=ilr+1
  !!        locregCenter(:,ilr)=rxyz(:,iat)
  !!        ! DEBUGLR write(10,*) iorb,locregCenter(:,ilr)
  !!    end do
  !!end do

  !!! Correction for spin polarized systems. For non polarized systems, norbu=norb and the loop does nothing.
  !!do iorb=lorbs%norbu+1,lorbs%norb
  !!    locregCenter(:,iorb)=locregCenter(:,iorb-lorbs%norbu)
  !!end do

 
  !!norbsPerLocreg = f_malloc(nlr,id='norbsPerLocreg')
  !!norbsPerLocreg=1 !should be norbsPerLocreg
    
  !!call f_free_ptr(lorbs%inWhichLocreg)
  !!call assignToLocreg2(iproc, nproc, lorbs%norb, lorbs%norbu, lorbs%norb_par, astruct%nat, nlr, &
  !!     input%nspin, norbsPerLocreg, lorbs%spinsgn, locregCenter, lorbs%inwhichlocreg)

  !!call f_free_ptr(lorbs%onwhichatom)
  !!call assignToLocreg2(iproc, nproc, lorbs%norb, lorbs%norbu, lorbs%norb_par, astruct%nat, astruct%nat, &
  !!     input%nspin, norbsPerAtom, lorbs%spinsgn, rxyz, lorbs%onwhichatom)

  !!if (iproc==0) write(*,*) 'OLD: iwl',lorbs%inwhichlocreg
  !!if (iproc==0) write(*,*) 'OLD: owa',lorbs%onwhichatom

  call f_free_ptr(lorbs%inWhichLocreg)
  call f_free_ptr(lorbs%onwhichatom)
  call assign_to_atoms_and_locregs(iproc, nproc, lorbs%norb, astruct%nat, input%nspin, norbsPerAtom, rxyz, &
       lorbs%onwhichatom, lorbs%inwhichlocreg)
  !!if (iproc==0) write(*,*) 'NEW: iwl',lorbs%inwhichlocreg
  !!if (iproc==0) write(*,*) 'NEW: owa',lorbs%onwhichatom


  lorbs%eval = f_malloc_ptr(lorbs%norb,id='lorbs%eval')
  lorbs%eval=-.5d0

  !!call f_free(norbsPerLocreg)
  !!call f_free(locregCenter)
  call f_free(norbsPerAtom)

  call f_release_routine()

  call timing(iproc,'init_orbs_lin ','OF')

end subroutine init_orbitals_data_for_linear

! initializes locrad and locregcenter
subroutine lzd_init_llr(iproc, nproc, input, astruct, rxyz, orbs, lzd)
  use module_types
  use locregs, only: locreg_null
  use module_bigdft_mpi
  use module_bigdft_arrays
  use module_bigdft_profiling
  use mpif_module, only: mpi_wtime
  implicit none

  ! Calling arguments
  integer, intent(in) :: iproc, nproc
  type(input_variables), intent(in) :: input
  type(atomic_structure), intent(in) :: astruct
  real(kind=8),dimension(3,astruct%nat), intent(in) :: rxyz
  type(orbitals_data), intent(in) :: orbs
  type(local_zone_descriptors), intent(inout) :: lzd

  ! Local variables
  integer :: iat, ityp, ilr, istat, iorb, iilr
  real(kind=8),dimension(:,:), allocatable :: locregCenter
  character(len=*), parameter :: subname='lzd_init_llr'
  real(8):: t1, t2

  call timing(iproc,'init_locregs  ','ON')

  call f_routine(id='lzd_init_llr')

  t1=mpi_wtime()

  nullify(lzd%llr)

  lzd%nlr=orbs%norbu

  locregCenter = f_malloc((/ 3, orbs%norbu /),id='locregCenter')

  ilr=0
  do iat=1,astruct%nat
      ityp=astruct%iatype(iat)
      do iorb=1,input%lin%norbsPerType(ityp)
          ilr=ilr+1
          locregCenter(:,ilr)=rxyz(:,iat)
      end do
  end do

  ! Allocate the array of localisation regions
  allocate(lzd%Llr(lzd%nlr),stat=istat)
  do ilr=1,lzd%nlr
     lzd%llr(ilr)=locreg_null()
  end do
  do ilr=1,lzd%nlr
      iilr=mod(ilr-1,orbs%norbu)+1 !correct value for a spin polarized system
      !!write(*,*) 'ilr, iilr', ilr, iilr
      lzd%llr(ilr)%locrad=input%lin%locrad(iilr)
      lzd%llr(ilr)%locrad_kernel=input%lin%locrad_kernel(iilr)
      lzd%llr(ilr)%locrad_mult=input%lin%locrad_mult(iilr)
      lzd%llr(ilr)%locregCenter=locregCenter(:,iilr)
      !!if (iproc==0) then
      !!    write(*,'(a,i3,3x,es10.3,3x,es10.3,3x,es10.3,3x,3es10.3)') 'ilr, locrad, locrad_kernel, locrad_mult, locregCenter', &
      !!    ilr, lzd%llr(ilr)%locrad, lzd%llr(ilr)%locrad_kernel, &
      !!    lzd%llr(ilr)%locrad_mult, lzd%llr(ilr)%locregCenter
      !!end if
  end do

  call f_free(locregCenter)

  t2=mpi_wtime()
  !if(iproc==0) write(*,*) 'in lzd_init_llr: time',t2-t1

  call f_release_routine()

  call timing(iproc,'init_locregs  ','OF')

end subroutine lzd_init_llr

!> Initializes onwhichatom, inwhichlocreg, locrad and locregcenter from file
subroutine initialize_linear_from_file(iproc,nproc,input_frag,astruct,rxyz,orbs,Lzd,&
     dir_output,filename,ref_frags,orblist)
  use module_precisions
  use module_types
  use module_precisions
  use module_bigdft_output
  use module_fragments
  use locregs, only: locreg_null
  use io
  use liborbs_io, only: io_descr_onwhichatom, io_descr_locrad
  use dictionaries
  use public_enums
  use module_bigdft_arrays
  use module_bigdft_mpi
  implicit none
  integer, intent(in) :: iproc, nproc
  type(fragmentInputParameters),intent(in) :: input_frag
  type(atomic_structure), intent(in) :: astruct
  real(gp), dimension(3,astruct%nat), intent(in) :: rxyz
  type(orbitals_data), intent(inout) :: orbs  !< orbs related to the basis functions, inwhichlocreg and onwhichatom generated in this routine
  type(local_zone_descriptors), intent(inout) :: Lzd !< must already contain Glr and hgrids
  type(system_fragment), dimension(input_frag%nfrag_ref), intent(inout) :: ref_frags
  character(len=*), intent(in) :: filename, dir_output
  integer, dimension(orbs%norb), optional :: orblist

  !Local variables
  character(len=*), parameter :: subname='initialize_linear_from_file'
  character(len =256) :: error
  logical :: lstat
  integer :: ilr, iorb_old, iorb, ispinor, iorb_out, iforb, isforb, isfat, iiorb, iorbp, ifrag, ifrag_ref, forb
  integer, dimension(2,3) :: nb_old
  integer :: confPotOrder, iat,unitwf
  real(gp), dimension(3) :: hgrids_old
  real(kind=8) :: eval, confPotprefac
  real(gp), dimension(orbs%norb):: locrad
  real(gp), dimension(3) :: locregCenter
  character(len=256) :: full_filename
  type(io_descriptor) :: fragwaves
  logical :: ok

  ! to be fixed
  if (present(orblist)) then
     stop 'orblist no longer functional in initialize_linear_from_file due to addition of fragment calculation'
  end if

  ! NOTES:
  ! The orbs%norb family must be all constructed before this routine
  ! This can be done from the input.lin since the number of basis functions should be fixed.
  ! Fragment structure must also be fully initialized, even if this is not a fragment calculation

  ! Allocate the array of localisation regions
  Lzd%nlr = orbs%norb
  allocate(lzd%Llr(lzd%nlr))
  do ilr=1,lzd%nlr
     lzd%Llr(ilr)=locreg_null()
  end do

  unitwf=99
  ! loop over fragments in current system (will still be treated as one fragment in calculation, just separate for restart)
  isforb=0
  isfat=0
  do ifrag=1,input_frag%nfrag
     ! find reference fragment this corresponds to
     ifrag_ref=input_frag%frag_index(ifrag)

     ! if this is a fragment calculation frag%dirname will contain fragment directory, otherwise it will be empty
     ! bit of a hack to use orbs here not forbs, but different structures so this is necessary - to clean somehow
     full_filename = trim(dir_output)//trim(input_frag%dirname(ifrag_ref))//trim(filename)

     fragwaves = io_read_description(trim(full_filename), &
          astruct%nat, orbs, lzd%glr%mesh_coarse%dom, bigdft_mpi)

     loop_iforb: do iforb=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
        ! need to calculate here the correct iiorb which takes into account the different ordering due to spin+fragments
        forb = iforb+isforb
        if (ref_frags(ifrag_ref)%fbasis%forbs%spinsgn(iforb) == -1.0d0) then
           forb = forb + orbs%norbu - ref_frags(ifrag_ref)%fbasis%forbs%norbu
        end if
        orbs%onwhichatom(forb) = io_descr_onwhichatom(fragwaves%parent, iforb) + isfat
        locrad(forb) = io_descr_locrad(fragwaves%parent, iforb)
     end do loop_iforb
     isforb=isforb+ref_frags(ifrag_ref)%fbasis%forbs%norbu
     isfat=isfat+ref_frags(ifrag_ref)%astruct_frg%nat

     call io_deallocate_descriptor(fragwaves)

  end do

  ! Put the llr in posinp order
  ilr=0
  do iat=1,astruct%nat
     do iorb=1,orbs%norb
        if(iat == orbs%onwhichatom(iorb)) then
           ilr = ilr + 1
           lzd%llr(ilr)%locregCenter = rxyz(:, iat)
           lzd%llr(ilr)%locrad = locrad(iorb)
           orbs%inwhichlocreg(iorb) = ilr
        end if
     end do
  end do

END SUBROUTINE initialize_linear_from_file

!> assignToLocreg does not take into account the Kpts yet !!
!! @warning assignToLocreg does not take into account the Kpts yet !!
subroutine assignToLocreg(iproc,nproc,nspinor,nspin,atoms,orbs,Lzd)
  use module_types
  use module_bigdft_arrays
  use module_bigdft_mpi
  !use ao_inguess, only: count_atomic_shells, ao_nspin_ig
  implicit none

  integer,intent(in):: iproc,nproc,nspin,nspinor
  type(atoms_data),intent(in) :: atoms
  type(orbitals_data),intent(inout):: orbs
  type(local_zone_descriptors) :: Lzd
  ! Local variables
  integer :: jproc,iiOrb,iorb,jorb,jat,orbsc!,ispin
  integer :: ind,noncoll,ilr!,ierr,dimtot,iat,npsidim,Lnorb
  character(len=*), parameter :: subname='assignToLocreg'
  integer, dimension(:), allocatable :: Localnorb
  !integer, parameter :: lmax=3,noccmax=2,nelecmax=32
  !integer, dimension(lmax+1) :: nmoments
  !real(gp), dimension(noccmax,lmax+1) :: occup              !dummy variable


! in the non-collinear case the number of orbitals double
! nspin_ig*noncoll is always <= 2
  if (nspinor == 4) then
     noncoll=2
  else
     noncoll=1
  end if

  Localnorb = f_malloc(Lzd%nlr,id='Localnorb')

! NOTES: WORKS ONLY BECAUSE Llr coincides with the atoms !!
! NOTES: K-Points??
  !nmoments = 0
  do ilr = 1, Lzd%nlr
     !call count_atomic_shells(ao_nspin_ig(nspin,nspinor=nspinor),&
     !     atoms%aoig(ilr)%aocc,occup,nmoments)
     !Lnorb=(nmoments(1)+3*nmoments(2)+5*nmoments(3)+7*nmoments(4))
     Localnorb(ilr) =atoms%aoig(ilr)%nao! Lnorb
  end do

!!  already associated = 1 by default
!  allocate(orbs%inWhichLocregP(max(1,orbs%norb_par(iproc,0))),stat=i_stat)

! initialize inwhichlocreg
  orbs%inWhichLocreg = 0
  !orbs%inWhichLocregP = 0

  ind = 0
  jproc=0
  jat=1
  jorb=ind
  iiOrb=0
  do iorb=ind,orbs%norb

      ! Switch to the next MPI process if the numbers of orbitals for a given
      ! MPI process is reached.
      if(jorb==orbs%norb_par(jproc,0)) then
          jproc=jproc+1
          jorb=ind
          if (jproc==nproc) exit
      end if
      orbsc = 0
      ! Switch to the next atom if the number of basis functions for this atom is reached.
      if(iiOrb==(Localnorb(jat)-orbsc)*noncoll) then
          jat=jat+1
          iiOrb=0
      end if
      if(jat > atoms%astruct%nat) then
        jat = 1
      end if
      jorb=jorb+1
      iiOrb=iiOrb+1
      if(iproc==jproc .and. orbs%norb_par(jproc,0)> 0) then
         !orbs%inWhichLocregP(jorb)=jat
         orbs%inWhichLocreg(jorb+orbs%isorb)=jat
      end if
  end do

  if (nproc > 1) then
     call fmpi_allreduce(orbs%inWhichLocreg(1),orbs%norb,FMPI_SUM,comm=bigdft_mpi%mpi_comm)
  end if

! Calculate the dimension of the total wavefunction
!!  dimtot = 0
!!  if(orbs%norbp > 0) then
!!     do iorb = 1,orbs%norbp
!!        ilr = orbs%inwhichlocreg(iorb+orbs%isorb)
!!        npsidim = array_dim(Lzd%Llr(ilr))*nspinor
!!        dimtot = dimtot + npsidim
!!     end do
!!  else if (orbs%norbp == 0) then
!!       dimtot = orbs%npsidim
!!  end if
!!  Lzd%Lpsidimtot = dimtot

  call f_free(Localnorb)

end subroutine assignToLocreg
