module module_bigdft_scf_cycle
  implicit none
  private

  public :: write_energies, write_band_structure_energies, energies_consistency
  public :: FullHamiltonianApplication
  public :: NonLocalHamiltonianApplication
  
contains

  !> Write the energies for a given iteration
  subroutine write_band_structure_energies(energs,label)
    use module_types
    use module_bigdft_output
    implicit none
    !Arguments
    type(band_structure_energies), intent(in) :: energs
    character(len=*), intent(in):: label 
    !local variables
    logical :: yesen

    yesen=get_verbose_level() > 0

    if (yesen) then
       call yaml_newline()
       call yaml_mapping_open(trim(label),flow=.true.)
       call dump_component('Ekin',energs%ekin)
       call dump_component('Epot',energs%epot)
       call dump_component('Enl',energs%eproj)
       call dump_component('EexctX',energs%eexctX)
       call dump_component('EvSIC',energs%evsic)
       call yaml_mapping_close()
    end if

  contains

    subroutine dump_component(lbl, val)
      use module_precisions
      implicit none
      character(len=*), intent(in) :: lbl
      real(gp), intent(in) :: val

      if (val /= 0.0_gp) call yaml_map(trim(lbl),val,fmt='(1pe18.11)')

    end subroutine dump_component
  end subroutine write_band_structure_energies

  !> Write the energies for a given iteration
  subroutine write_energies(iter,energs,gnrm,gnrm_zero,comment,scf_mode,only_energies,label)
    use module_precisions
    use module_types
    use module_bigdft_output
    use f_enums
    implicit none
    !Arguments
    integer, intent(in) :: iter !< Iteration Id
    type(energy_terms), intent(in) :: energs
    real(gp), intent(in) :: gnrm,gnrm_zero
    character(len=*), intent(in) :: comment
    logical,intent(in),optional :: only_energies
    type(f_enumerator), intent(in), optional :: scf_mode
    character(len=*), intent(in), optional :: label !< label of the mapping (usually 'Energies', but can also be different)
    !local variables
    logical :: write_only_energies,yesen,noen
    character(len=128) :: label_

    if (present(only_energies)) then
       write_only_energies=only_energies
    else
       write_only_energies=.false.
    end if
    noen=.false.
    if (present(scf_mode)) noen=scf_mode .hasattr. 'MIXING'
    label_ = 'Energies'
    if (present(label)) label_ = trim(label)

    if (len(trim(comment)) > 0 .and. .not.write_only_energies) then
       if (get_verbose_level() >0) call yaml_newline()
       call write_iter()
       if (get_verbose_level() >0) call yaml_comment(trim(comment))
    end if

    yesen=get_verbose_level() > 0
    if (present(scf_mode)) yesen=yesen .and. .not. (scf_mode .hasattr. 'MIXING')

    if (yesen) then
       call yaml_newline()
       call yaml_mapping_open(trim(label_),flow=.true.)
       call dump_component('Ekin',energs%eH_KS%ekin)
       call dump_component('Epot',energs%eH_KS%epot)
       call dump_component('Enl',energs%eH_KS%eproj)
       call dump_component('EH',energs%eh)
       call dump_component('EXC',energs%exc)
       call dump_component('EvXC',energs%evxc)
       call dump_component('EexctX',energs%eH_KS%eexctX)
       call dump_component('EvSIC',energs%eH_KS%evsic)
       call dump_component('Epaw',energs%epawdc)
       if (len(trim(comment)) > 0) then
          call dump_component('Eion',energs%eion)
          call dump_component('Edisp',energs%edisp)
          call dump_component('Exc(rhoc)',energs%excrhoc)
          call dump_component('TS',energs%eTS)
       end if
       call yaml_mapping_close()
    end if

    if (.not.write_only_energies) then
       call yaml_newline()
       if (len(trim(comment)) == 0) then
          call write_iter()
          if (get_verbose_level() >0) call yaml_newline()
       else if (get_verbose_level() > 1 .and. present(scf_mode)) then
          call yaml_map('SCF criterion',scf_mode)
       end if
    end if


  contains

    subroutine dump_component(lbl, val)
      implicit none
      character(len=*), intent(in) :: lbl
      real(gp), intent(in) :: val

      if (val /= 0.0_gp) call yaml_map(trim(lbl),val,fmt='(1pe18.11)')

    end subroutine dump_component


    subroutine write_iter()
      implicit none
      if (iter > 0) call yaml_map('iter',iter,fmt='(i6)')
      if (noen) then
         call yaml_map('tr(H)',energs%trH,fmt='(1pe24.17)')
      else
         if (energs%eTS==0.0_gp) then
            call yaml_map('EKS',energs%energy,fmt='(1pe24.17)')
         else
            call yaml_map('FKS',energs%energy,fmt='(1pe24.17)')
         end if
      end if
      if (gnrm > 0.0_gp) call yaml_map('gnrm',gnrm,fmt='(1pe9.2)')
      if (gnrm_zero > 0.0_gp) &
           call yaml_map('gnrm0',gnrm_zero,fmt='(1pe8.1)')
      if (noen) then
         if (energs%trH_prev /=0.0_gp) &
              call yaml_map('D',energs%trH-energs%trH_prev,fmt='(1pe9.2)')
      else
         if (energs%e_prev /=0.0_gp) &
              call yaml_map('D',energs%energy-energs%e_prev,fmt='(1pe9.2)')
      end if

    end subroutine write_iter
  end subroutine write_energies

  subroutine energies_consistency(e_BS,trH, message)
    use f_utils, only: f_get_option
    use yaml_output
    use module_precisions
    implicit none
    real(gp), intent(in) :: e_BS,trH
    character(len=*), intent(in), optional :: message
    !local variables
    character(len=*), parameter :: msg='Trace of the Hamiltonian'
    character(len=len(msg)) :: msg_
    real(gp) :: tt

    msg_= f_get_option(msg,len(msg_),message)

    tt=(e_BS-trH)/trH
    if (abs(tt) <= 1.d-10 .or. abs(trH-e_BS) <= 1.d-8) return
    call yaml_newline()
    call yaml_mapping_open('Energy inconsistencies')
    call yaml_map('Band Structure Energy',e_BS,fmt='(1pe22.14)')
    call yaml_map(trim(msg_),trH,fmt='(1pe22.14)')
    if (trH /= 0.0_gp) call yaml_map('Relative inconsistency',tt,fmt='(1pe9.2)')
    call yaml_mapping_close()
  end subroutine energies_consistency

  !> Application of the Full Hamiltonian
  subroutine FullHamiltonianApplication(iproc,nproc,lt,at,orbs,&
       Lzd,nlpsp,psi,hpsi,paw,&
       eH_KS,GPU,orbsocc,psirocc,pspandkin,restore)
    use module_precisions
    use module_types
    use public_enums, only: PSPCODE_PAW
    use yaml_output
    use orbitalbasis
    use rhopotential
    use locregs
    use module_bigdft_profiling
    use module_bigdft_arrays
    use module_bigdft_errors
    use module_bigdft_mpi
    use wrapper_linalg
    implicit none
    !Arguments
    integer, intent(in) :: iproc,nproc
    type(local_operator), intent(inout) :: lt
    type(atoms_data), intent(in) :: at
    type(orbitals_data), intent(in) :: orbs
    type(local_zone_descriptors),intent(in) :: Lzd
    type(DFT_PSP_projectors), intent(inout) :: nlpsp
    real(wp), dimension(:), intent(in) :: psi
    type(band_structure_energies), intent(inout) :: eH_KS
    real(wp), target, dimension(:), intent(out) :: hpsi
    type(GPU_pointers), intent(inout) :: GPU
    !PAW variables:
    type(paw_objects),intent(inout)::paw
    type(orbitals_data), intent(in), optional, target :: orbsocc
    real(wp), dimension(:), pointer, optional :: psirocc
    real(wp), dimension(:), intent(inout), optional :: pspandkin
    logical, intent(in), optional :: restore
    !Local variables
    integer :: ilr, iorb, ispsi
    !real(gp), intent(out) :: ekin_sum,epot_sum,eexctX,eproj_sum,evsic
    type(orbital_basis) :: psi_ob
    logical :: doRestore

    call f_routine(id='FullHamiltonianApplication')

    !write(*,*) 'lzd%ndimpotisf', lzd%ndimpotisf
    !do i=1,lzd%ndimpotisf
    !    write(210,*) pot(i)
    !end do

    if (GPU%OCLconv) then
       !put to zero hpsi array (now important since any of the pieces of the hamiltonian is accumulating)
       call f_zero(hpsi)

       !pin potential
       if (.not. associated(lt%pot)) then
          call local_operator_gather(lt, lzd, orbs)
       end if
       !call timing(iproc,'ApplyLocPotKin','ON')
       GPU%hpsi_ASYNC = f_malloc_ptr(max(1, size(hpsi)),id='GPU%hpsi_ASYNC')
       call local_hamiltonian_OCL(orbs,lzd%glr,&
            orbs%nspin,lt%pot,psi,GPU%hpsi_ASYNC,eH_KS%ekin,eH_KS%epot,GPU)
       eH_KS%eexctX = 0._gp ! No support of exact exchange in OCL
       !call timing(iproc,'ApplyLocPotKin','OF')

       call NonLocalHamiltonianApplication(iproc,at,size(psi),orbs,&
            Lzd,nlpsp,psi,hpsi,eH_KS%eproj,paw)

       call finish_hamiltonian_OCL(orbs,eH_KS%ekin,eH_KS%epot,GPU)
       ispsi=1
       do iorb=1,orbs%norbp
          ilr=orbs%inWhichLocreg(orbs%isorb+iorb)
          call axpy(array_dim(Lzd%Llr(ilr))*orbs%nspinor,&
               1.0_wp,GPU%hpsi_ASYNC(ispsi),1,hpsi(ispsi),1)
          ispsi=ispsi+array_dim(Lzd%Llr(ilr))*orbs%nspinor
       end do
       call f_free_ptr(GPU%hpsi_ASYNC)
    else
       call orbital_basis_associate(&
            psi_ob,&
            orbs=orbs,&
            phis_wvl=psi,&
            Lzd=Lzd,&
            orbsocc=orbsocc,&
            psirocc=psirocc,&
            id='LochamPotKin')
       doRestore = .false.
       if (present(restore)) doRestore = restore .and. present(pspandkin)
       if (doRestore) then
          call f_memcpy(dest = hpsi, src = pspandkin)
       else
          !put to zero hpsi array (now important since any of the pieces of the hamiltonian is accumulating)
          call f_zero(hpsi)

          call NonLocalHamiltonianApplication(iproc,at,size(psi),orbs,&
               Lzd,nlpsp,psi,hpsi,eH_KS%eproj,paw)

          if (associated(lt%comgp%recvBuf)) then
             call timing(bigdft_mpi%iproc,'ApplyLocKin','ON')
             call apply_kinetic_operator(psi_ob, hpsi, eH_KS%ekin)
             call timing(bigdft_mpi%iproc,'ApplyLocKin','OF')
          end if

          if (present(pspandkin)) then
             call f_memcpy(src = hpsi, dest = pspandkin)
          end if
       end if

       if (.not. associated(lt%pot)) then
          call local_operator_gather(lt, lzd, psi_ob%orbs)
       end if

       if (associated(lt%comgp%recvBuf)) then
          call timing(bigdft_mpi%iproc,'ApplyLocPot','ON')
          call apply_local_potential(psi_ob, lt, hpsi, &
               eH_KS%epot, eH_KS%evsic, eH_KS%eexctX, eH_KS%econf)
          call timing(bigdft_mpi%iproc,'ApplyLocPot','OF')
       else
          call timing(bigdft_mpi%iproc,'ApplyLocPotKin','ON')
          call apply_local_potential(psi_ob, lt, hpsi, &
               eH_KS%epot, eH_KS%evsic, eH_KS%eexctX, eH_KS%econf, ekin = eH_KS%ekin)
          call timing(bigdft_mpi%iproc,'ApplyLocPotKin','OF')
       end if

       call orbital_basis_release(psi_ob)

       call energies_ensure(eH_KS)
    end if
    if (bigdft_mpi%iproc == 0 .and. eH_KS%eexctX /= 0._gp) &
         call yaml_map('Exact Exchange Energy',eH_KS%eexctX,fmt='(1pe18.11)')

    call energies_synchronization(eH_KS,nproc,bigdft_mpi%mpi_comm)

    call f_release_routine()
  END SUBROUTINE FullHamiltonianApplication

  subroutine NonLocalHamiltonianApplication(iproc,at,npsidim_orbs,orbs,&
       Lzd,nl,psi,hpsi,eproj_sum,paw)
    use module_precisions
    use module_types
    use yaml_output
    use psp_projectors
    use module_atoms
    use orbitalbasis
    use liborbs_functions
    use ao_inguess, only: lmax_ao
    use module_bigdft_profiling
    use module_bigdft_arrays
    implicit none
    integer, intent(in) :: iproc, npsidim_orbs
    type(atoms_data), intent(in) :: at
    type(orbitals_data),  intent(in) :: orbs
    type(local_zone_descriptors), intent(in) :: Lzd
    type(DFT_PSP_projectors), intent(inout) :: nl
    real(wp), dimension(npsidim_orbs), intent(in) :: psi
    real(wp), dimension(npsidim_orbs), intent(inout) :: hpsi
    type(paw_objects),intent(inout)::paw
    real(gp), intent(out) :: eproj_sum
    !local variables
    character(len=*), parameter :: subname='NonLocalHamiltonianApplication'
    integer :: nwarnings
    type(ket) :: psi_it
    type(orbital_basis) :: psi_ob
    type(DFT_PSP_projector_iter) :: psp_it
    type(wvf_manager) :: manager
    real(wp) :: eproj

    !integer :: ierr
    !real(kind=4) :: tr0, tr1, t0, t1
    !real(kind=8) :: time0, time1, time2, time3, time4, time5, ttime
    !real(kind=8), dimension(0:4) :: times

    call f_routine(id=subname)

    eproj_sum=0.0_gp

    ! apply all PSP projectors for all orbitals belonging to iproc
    call timing(iproc,'ApplyProj     ','ON')

    !initialize the orbital basis object, for psi and hpsi
    call orbital_basis_associate(psi_ob,orbs=orbs,phis_wvl=psi,Lzd=Lzd,id='nonlocalham')
    !should we calculate the density matrix we have to zero it
    if (associated(nl%iagamma)) call f_zero(nl%gamma_mmp)
    !here we might rework the value of gamma in case we would like to apply some extra

    nwarnings=0
    !if(paw%usepaw) call f_zero(orbs%npsidim_orbs, paw%spsi(1))
    if(paw%usepaw) call f_zero(paw%spsi)

    !here the localisation region should be changed, temporary only for cubic approach

    !apply the projectors  k-point of the processor
    !iterate over the orbital_basis
    psi_it=orbital_basis_iterator(psi_ob, manager = manager)
    loop_kpt: do while(ket_next_kpt(psi_it))
       loop_lr: do while(ket_next_locreg(psi_it,ikpt=psi_it%ikpt))
          call DFT_PSP_projectors_iter_new(psp_it, nl)
          loop_proj: do while (DFT_PSP_projectors_iter_next(psp_it, ilr = psi_it%ilr, &
               & lr = psi_it%lr, glr = lzd%glr))
             call DFT_PSP_projectors_iter_ensure(psp_it, psi_it%kpoint, 0, nwarnings, Lzd%Glr)
             loop_psi_kpt: do while(ket_next(psi_it,ikpt=psi_it%ikpt,ilr=psi_it%ilr))
                call DFT_PSP_projectors_iter_apply(psp_it, psi_it, at, eproj, hpsi, paw)
                eproj_sum = eproj_sum + psi_it%kwgt * psi_it%occup * eproj
             end do loop_psi_kpt
          end do loop_proj
       end do loop_lr
    end do loop_kpt
    call wvf_deallocate_manager(manager)

    call orbital_basis_release(psi_ob)

    !used on the on-the-fly projector creation
    if (nwarnings /= 0 .and. iproc == 0) then
       call yaml_map('Calculating wavelets expansion of projectors. Found warnings',nwarnings,fmt='(i0)')
       call yaml_newline()
       call yaml_warning('Projectors too rough: Consider modifying hgrid and/or the localisation radii.')
    end if

    call timing(iproc,'ApplyProj     ','OF')

    call f_release_routine()
  end subroutine NonLocalHamiltonianApplication

!!$subroutine NonLocalHamiltonianApplication_new(iproc,at,npsidim_orbs,orbs,&
!!$     Lzd,nl,psi,hpsi,eproj_sum,paw)
!!$  use module_base
!!$  use module_types
!!$  use yaml_output
!!$  !  use module_interfaces
!!$  use psp_projectors_base, only: PSP_APPLY_SKIP
!!$  use psp_projectors, only: projector_has_overlap,get_proj_locreg
!!$  use public_enums, only: PSPCODE_PAW
!!$  use module_atoms
!!$  use orbitalbasis
!!$  implicit none
!!$  integer, intent(in) :: iproc, npsidim_orbs
!!$  type(atoms_data), intent(in) :: at
!!$  type(orbitals_data),  intent(in) :: orbs
!!$  type(local_zone_descriptors), intent(in) :: Lzd
!!$  type(DFT_PSP_projectors), intent(inout) :: nl
!!$  real(wp), dimension(npsidim_orbs), intent(in) :: psi
!!$  real(wp), dimension(npsidim_orbs), intent(inout) :: hpsi
!!$  type(paw_objects),intent(inout)::paw
!!$  real(gp), intent(out) :: eproj_sum
!!$  !local variables
!!$  character(len=*), parameter :: subname='NonLocalHamiltonianApplication'
!!$  logical :: overlap
!!$  integer :: istart_ck,nwarnings
!!$  integer :: iproj,istart_c,mproj,iilr
!!$  type(ket) :: psi_it
!!$  type(orbital_basis) :: psi_ob
!!$  type(atoms_iterator) :: atit
!!$  real(wp), dimension(:), pointer :: hpsi_ptr,spsi_ptr
!!$
!!$  !integer :: ierr
!!$  !real(kind=4) :: tr0, tr1, t0, t1
!!$  !real(kind=8) :: time0, time1, time2, time3, time4, time5, ttime
!!$  !real(kind=8), dimension(0:4) :: times
!!$
!!$  call f_routine(id='NonLocalHamiltonianApplication')
!!$
!!$  eproj_sum=0.0_gp
!!$
!!$  ! apply all PSP projectors for all orbitals belonging to iproc
!!$  call timing(iproc,'ApplyProj     ','ON')
!!$
!!$  !initialize the orbital basis object, for psi and hpsi
!!$  call orbital_basis_associate(psi_ob,orbs=orbs,phis_wvl=psi,Lzd=Lzd)
!!$
!!$  nwarnings=0
!!$  if(paw%usepaw) call f_zero(orbs%npsidim_orbs, paw%spsi(1))
!!$
!!$  !here the localisation region should be changed, temporary only for cubic approach
!!$
!!$  !apply the projectors following the strategy (On-the-fly calculation or not)
!!$
!!$  !apply the projectors  k-point of the processor
!!$  !iterate over the orbital_basis
!!$  psi_it=orbital_basis_iterator(psi_ob)
!!$
!!$  !iterate over the projectors
!!$  prit=projector_iterator(nl)
!!$
!!$  loop_kpt: do while(ket_next_kpt(psi_it))
!!$     loop_lr: do while(ket_next_locreg(psi_it,ikpt=psi_it%ikpt))
!!$        if (nl%on_the_fly) then
!!$           loop_proj: do while(projector_next(prit,ilr=psi_it%ilr,lr=psi_it%lr))
!!$              loop_psi_kpt: do while(ket_next(psi_it,ikpt=psi_it%ikpt,ilr=psi_it%ilr))
!!$                 hpsi_ptr => ob_ket_map(hpsi,psi_it)
!!$                 if (paw%usepaw) then
!!$                    spsi_ptr => ob_ket_map(paw%spsi,psi_it)
!!$                    call apply_atproj_iorb_paw(prit%iat,psi_it%iorbp,prit%istart_c,&
!!$                         at,psi_it%ob%orbs,psi_it%lr,nl,&
!!$                         psi_it%phi_wvl,hpsi_ptr,spsi_ptr,eproj_sum,&
!!$                         paw)
!!$                 else
!!$                    if (psi_it%nspinor > 1) then !which means 2 or 4
!!$                       ncplx_w=2
!!$                       n_w=psi_it%nspinor/2
!!$                    else
!!$                       ncplx_w=1
!!$                       n_w=1
!!$                    end if
!!$                    call NL_HGH_application(prit%hij,&
!!$                         prit%ncplx,prit%mproj,prit%lr,prit%proj,&
!!$                         ncplx_w,n_w,psi_it%lr,prit%tolr,prit%nl%wpack,prit%nl%scpr,prit%nl%cproj,prit%nl%hcproj,&
!!$                         psi_it%phi_wvl,hpsi_ptr,eproj)
!!$                    eproj_sum=eproj_sum+psi_it%kwgt*psi_it%occup*eproj
!!$                 end if
!!$              end do loop_psi_kpt
!!$           end do loop_proj
!!$        else
!!$           loop_psi_kpt2: do while(ket_next(psi_it,ikpt=psi_it%ikpt,ilr=psi_it%ilr))
!!$              loop_proj_2: do while(projector_next(prit,ilr=psi_it%ilr,lr=psi_it%lr))
!!$                 hpsi_ptr => ob_ket_map(hpsi,psi_it)
!!$                 if (paw%usepaw) then
!!$                    spsi_ptr => ob_ket_map(paw%spsi,psi_it)
!!$                    call apply_atproj_iorb_paw(prit%iat,psi_it%iorbp,prit%istart_c,&
!!$                         at,psi_it%ob%orbs,psi_it%lr,nl,&
!!$                         psi_it%phi_wvl,hpsi_ptr,spsi_ptr,eproj_sum,&
!!$                         paw)
!!$                 else
!!$                    if (psi_it%nspinor > 1) then !which means 2 or 4
!!$                       ncplx_w=2
!!$                       n_w=psi_it%nspinor/2
!!$                    else
!!$                       ncplx_w=1
!!$                       n_w=1
!!$                    end if
!!$                    call NL_HGH_application(prit%hij,&
!!$                         prit%ncplx,prit%mproj,prit%lr,prit%proj,&
!!$                         ncplx_w,n_w,psi_it%lr,prit%tolr,prit%nl%wpack,prit%nl%scpr,prit%nl%cproj,prit%nl%hcproj,&
!!$                         psi_it%phi_wvl,hpsi_ptr,eproj)
!!$                    eproj_sum=eproj_sum+psi_it%kwgt*psi_it%occup*eproj
!!$                 end if
!!$              end do loop_proj_2
!!$           end do loop_psi_kpt2
!!$        end if
!!$     end do loop_lr
!!$  end do loop_kpt
!!$
!!$  call orbital_basis_release(psi_ob)
!!$
!!$  if (.not. nl%on_the_fly .and. Lzd%nlr==1) then !TO BE REMOVED WITH NEW PROJECTOR APPLICATION
!!$     if (prit%istart_ck-1 /= nl%nprojel .and. orbs%norbp>0) then
!!$        call f_err_throw('Incorrect once-and-for-all psp application, istart_ck, nprojel= '+&
!!$             yaml_toa([prit%istart_ck,nl%nprojel]),err_name='BIGDFT_RUNTIME_ERROR')
!!$     end if
!!$  end if
!!$
!!$  !used on the on-the-fly projector creation
!!$  if (prit%nwarnings /= 0 .and. iproc == 0) then
!!$     call yaml_map('Calculating wavelets expansion of projectors. Found warnings',nwarnings,fmt='(i0)')
!!$     call yaml_newline()
!!$     call yaml_warning('Projectors too rough: Consider modifying hgrid and/or the localisation radii.')
!!$  end if
!!$
!!$  call timing(iproc,'ApplyProj     ','OF')
!!$
!!$  call f_release_routine()
!!$
!!$end subroutine NonLocalHamiltonianApplication_new
end module module_bigdft_scf_cycle
