module io
  use public_enums
  use dictionaries, only: dictionary
  use module_atoms, only: atomic_structure
  use locregs, only: locreg_descriptors
  use liborbs_io, only: liborbs_io_descriptor => io_descriptor
  implicit none

  private

  !> Public routines
  public :: writemywaves
  public :: writemywaves_linear_fragments
  public :: readmywaves
  public :: readmywaves_linear_new

  public :: input_check_psi_id

  public :: write_linear_matrices
  public :: write_partial_charges
  public :: read_coeff_minbasis
  public :: read_matrix_local
  public :: get_sparse_matrix_format
  public :: readrhoij

  public :: io_error, io_warning, io_open
  public :: plot_density
  public :: plot_locreg_grids

  public :: filename_of_iorbp

  type io_descriptor
     type(liborbs_io_descriptor) :: parent
     type(atomic_structure) :: astruct
  end type io_descriptor

  public :: io_descriptor
  public :: io_read_description, io_deallocate_descriptor
  public :: io_descr_rxyz, io_descr_files_exist

  character(len = *), parameter :: LOCREG = "locreg"
  character(len = *), parameter :: TAG_ASTRUCT = "ASTRUCT"
  character(len = *), parameter :: TAG_GLOBAL = "GLOBAL"
  character(len = *), parameter :: LREGS = "locregs"
  character(len = *), parameter :: KPTS = "kpts"
  character(len = *), parameter :: KPT = "kpt"
  character(len = *), parameter :: IND = "index"
  character(len = *), parameter :: FUNCS= "functions"
  character(len = *), parameter :: ATOM_STRUCT = "atomic structure"

contains

  !> Check for the input psi (wavefunctions)
  subroutine input_check_psi_id(inputpsi, dir_output, orbs, lorbs, nfrag, &
       & frag_calc, frag_dir, ref_frags)
    use module_precisions
    use module_types
    use yaml_output
    use module_fragments
    use dictionaries, only: f_err_throw
    use public_enums
    use f_enums
    use module_input_keys, only: inputpsiid_set_policy
    use module_bigdft_arrays
    use module_bigdft_mpi
    implicit none
    type(f_enumerator), intent(inout) :: inputpsi   !< (in) indicate how check input psi, (out) give how to build psi
    integer, intent(in) :: nfrag                    !< number of fragment directories which need checking
    logical, intent(in) :: frag_calc                !< whether or not it's a fragment calculation
    type(system_fragment), dimension(nfrag), intent(in) :: ref_frags  !< number of orbitals for each fragment
    character(len=100), dimension(nfrag), intent(in) :: frag_dir !< label for fragment subdirectories (blank if not a fragment calculation)
    character(len = *), intent(in) :: dir_output
    type(orbitals_data), intent(in) :: orbs, lorbs

    logical :: ok
    integer :: ifrag
    type(orbitals_data) :: fake_orbs
    type(io_descriptor) :: allwaves

    !for the inputPsi == WF_FORMAT_NONE case, check
    !if the wavefunctions are all present
    !otherwise switch to normal input guess
    ok=.true.
    if (inputpsi == 'INPUT_PSI_DISK_WVL') then
       allwaves = io_read_description(trim(dir_output) // "wavefunction", &
            orbs = orbs, mpi_env = bigdft_mpi)
       ok = io_descr_files_exist(allwaves, bigdft_mpi)
       call io_deallocate_descriptor(allwaves)
    else if (inputpsi == 'INPUT_PSI_DISK_LINEAR') then
       do ifrag=1,nfrag
          if (.not. frag_calc) then
             allwaves = io_read_description(trim(dir_output) // trim(frag_dir(ifrag)) // "minBasis", &
                  orbs = lorbs, mpi_env = bigdft_mpi)
             ok = io_descr_files_exist(allwaves, bigdft_mpi)
             call io_deallocate_descriptor(allwaves)
          else
             ! there might be a better way of doing this rather than making the fake_orbs, but this seems easiest for now
             call nullify_orbitals_data(fake_orbs)
             call min_orbs_to_orbs_point(ref_frags(ifrag)%fbasis%forbs,fake_orbs)
             ! only at the gamma point
             fake_orbs%iokpt = f_malloc_ptr(fake_orbs%norb,id='fake_orbs%iokpt')
             fake_orbs%iokpt = 1
             fake_orbs%isorb = 0
             fake_orbs%norbp = fake_orbs%norb
             allwaves = io_read_description(trim(dir_output) // trim(frag_dir(ifrag)) // "minBasis", &
                  orbs = fake_orbs, mpi_env = bigdft_mpi)
             ok = io_descr_files_exist(allwaves, bigdft_mpi)
             call io_deallocate_descriptor(allwaves)
             call deallocate_orbs(fake_orbs)
          end if
       end do
    end if
    if (.not. ok) then
       if (bigdft_mpi%iproc==0) call yaml_warning('Missing wavefunction files, switch to normal input guess')
       call inputpsiid_set_policy(ENUM_SCRATCH,inputpsi)
    end if
  END SUBROUTINE input_check_psi_id

    function dumpmywaves(filename, lbin, llr, orbs, astruct, rxyz, psi, &
         isat, isorb, map) result(descr)
      use module_precisions
      use module_types
      use locregs
      use f_utils
      use yaml_output
      use module_bigdft_mpi
      use module_bigdft_output
      use module_bigdft_arrays
      use module_atoms
      use dictionaries
      use liborbs_io, only: io_descr_add_func
      implicit none
      logical, intent(in) :: lbin
      character(len=*), intent(in) :: filename
      type(atomic_structure), intent(in) :: astruct
      real(gp), dimension(:, :), intent(in) :: rxyz
      type(orbitals_data), intent(in) :: orbs
      type(locreg_descriptors), dimension(:), intent(in) :: llr
      real(wp), dimension(:), intent(in) :: psi
      integer, intent(in), optional :: isat, isorb
      integer, dimension(:,:), intent(out), optional :: map
      type(io_descriptor) :: descr

      integer :: isat_, islash, isorb_, ispin, ikpt, ior, jorb
      integer :: shift, ilr, iat, ispinor, iorb
      character(len = max_field_length), dimension(4) :: fwave
      real(wp), dimension(:), pointer :: subpsi
      logical :: withLocregs
      type(dictionary), pointer :: a
      real(gp), dimension(:,:), allocatable :: rxyz_frag

      descr = io_init_description(filename)

      isorb_ = 0
      if (present(isorb)) isorb_ = isorb
      isat_ = 0
      if (present(isat)) isat_ = isat

      islash = index(filename, "/", back = .true.) + 1
      withLocregs = any(orbs%inwhichlocreg > 1)
      shift = 1
      do ikpt = 1, orbs%nkpts
         jorb = 0
         do iorb = 1, orbs%norb
            nullify(subpsi)

            ior = iorb + (ikpt - 1) * orbs%norb
            ilr = orbs%inwhichlocreg(ior)
            if (ior > orbs%isorb .and. ior <= orbs%isorb + orbs%norbp) then
               subpsi => f_subptr(psi, from = shift, size = array_dim(Llr(ilr)) * orbs%nspinor)
               shift = shift + array_dim(Llr(ilr)) * orbs%nspinor
            end if
            iat = orbs%onwhichatom(ior) - isat_
            ! Only export orbital that has a matching atom in astruct
            if (withLocregs .and. (iat < 1 .or. iat > astruct%nat)) cycle
            jorb = jorb + 1
            if (present(map)) then
               if (jorb <= size(map)) then
                  map(jorb, 1) = ior
                  map(jorb, 2) = iat
                  map(iat, 3) = iat + isat_
               end if
            end if

            do ispinor = 1, orbs%nspinor
               call filename_of_iorb(lbin, filename(islash:), orbs, ikpt, jorb + isorb_, ispinor, &
                    fwave(ispinor))
            end do

            ispin = 0
            if (orbs%norbu /= orbs%norb) then
               if (iorb > orbs%norbu) then
                  ispin = -1
               else
                  ispin = 1
               end if
            end if
            if (withLocregs) then
               call io_descr_add_func(descr%parent, fwave(1:orbs%nspinor), lbin, &
                    ikpt, ispin, ilr, iat, lr = Llr(ilr), phi = subpsi)
            else
               call io_descr_add_func(descr%parent, fwave(1:orbs%nspinor), lbin, &
                    ikpt, ispin, lr = Llr(ilr), phi = subpsi, eigenvalue = orbs%eval(ior))
            end if
            if (associated(subpsi) .and. get_verbose_level() >= 2 .and. bigdft_mpi%iproc==0) &
                 call yaml_map('Wavefunction written No.', ior)
         end do
      end do

      ! Lack of copy operator ?!
      call dict_init(a)
      if (present(map)) then
         rxyz_frag = f_malloc((/ 3, astruct%nat /), id = 'rxyz_frag')
         do iat = 1, astruct%nat
            rxyz_frag(:, iat) = rxyz(:, map(iat, 3))
         end do
         call astruct_merge_to_dict(a, astruct, rxyz_frag)
         call f_free(rxyz_frag)
      else
         call astruct_merge_to_dict(a, astruct, rxyz)
      end if
      call astruct_set_from_dict(a, descr%astruct)
      call dict_free(a)
    end function dumpmywaves

    subroutine writemywaves(iproc,filename,iformat,Lzd,orbs,at,rxyz,psi, &
         nelec,nfvctr,coeff, paw, iorb_shift, exportGlr)
      use module_types
      use module_precisions
      use module_bigdft_output
      use module_bigdft_errors
      use module_bigdft_arrays
      use locregs
      implicit none
      integer, intent(in) :: iproc,iformat
      type(atoms_data), intent(in) :: at
      type(orbitals_data), intent(in) :: orbs         !< orbs describing the basis functions
      type(local_zone_descriptors), intent(in) :: Lzd
      real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz
      real(wp), dimension(:), intent(in) :: psi  ! Should be the real linear dimension and not the global
      character(len=*), intent(in) :: filename
      integer, intent(in), optional :: nelec,nfvctr,iorb_shift
      real(wp), dimension(:,:,:), intent(in), optional :: coeff
      type(paw_objects), intent(in), optional :: paw
      character(len = *), intent(in), optional :: exportGlr
      !Local variables
      logical :: binary, is_etsf
      integer :: ncount1,ncount_rate,ncount_max,ncount2,unitwf
      real(kind=4) :: tr0,tr1
      real(kind=8) :: tel
      type(io_descriptor) :: descr

      unitwf=99
      binary=(iformat/=WF_FORMAT_PLAIN)
      is_etsf=(iformat==WF_FORMAT_ETSF)

      if (iproc == 0) call yaml_map('Write wavefunctions to file', trim(filename)//'.*')
      !if (iproc == 0) write(*,"(1x,A,A,a)") "Write wavefunctions to file: ", trim(filename),'.*'

      !if (binary) then
      if (is_etsf) then
         if (present(nelec) .and. present(nfvctr) .and. present(coeff)) then
            call f_err_throw('Linear scaling with ETSF writing not implemented yet')
         else if (present(paw)) then
            call f_err_throw('PAW with ETSF writing not implemented yet')
         else
            call write_waves_etsf(iproc,filename,orbs,at,rxyz,Lzd%glr,psi)
         end if
      else
         call cpu_time(tr0)
         call system_clock(ncount1,ncount_rate,ncount_max)

         descr = dumpmywaves(filename, binary, lzd%llr, orbs, at%astruct, rxyz, psi, isorb = iorb_shift)
         
         if(iproc == 0) then
            call writeheader(descr, binary, filename, lzd%glr, orbs, exportGlr)

            if (present(nelec) .and. present(nfvctr) .and. present(coeff)) then
               ! Now write the coefficients to file
               ! Must be careful, the orbs%norb is the number of basis functions
               ! while the norb is the number of orbitals.
               if (binary) then
                  call f_open_file(unitwf,file=filename//'_coeff.bin',&
                       binary=.true.)
               else
                  call f_open_file(unitwf,file=filename//'_coeff',&
                       binary=.false.)
               end if
               call writeLinearCoefficients(unitwf,.not. binary,at%astruct%nat,rxyz,orbs%norb,&
                    nelec,nfvctr,orbs%nspin,coeff,orbs%eval)
               call f_close(unitwf)
            end if

            ! Additional PAW data.
            if (present(paw)) then
               if (paw%usepaw .and. associated(paw%pawrhoij)) then
                  if (binary) then
                     call f_open_file(unitwf, file = filename // "-rhoij.bin", binary = .true.)
                  else
                     call f_open_file(unitwf, file = filename // "-rhoij", binary = .false.)
                  end if
                  call writerhoij(unitwf, binary, at%astruct%nat, paw%pawrhoij)
                  call f_close(unitwf)
               end if
            end if
         end if

         call io_deallocate_descriptor(descr)

         call cpu_time(tr1)
         call system_clock(ncount2,ncount_rate,ncount_max)
         tel=dble(ncount2-ncount1)/dble(ncount_rate)
         if (iproc == 0) then
            call yaml_sequence_open('Write Waves Time')
            call yaml_sequence(advance='no')
            call yaml_mapping_open(flow=.true.)
            call yaml_map('Process',iproc)
            call yaml_map('Timing',(/ real(tr1-tr0,kind=8),tel /),fmt='(1pe10.3)')
            call yaml_mapping_close()
            call yaml_sequence_close()
         end if
         !write(*,'(a,i4,2(1x,1pe10.3))') '- WRITE WAVES TIME',iproc,tr1-tr0,tel
         !write(*,'(a,1x,i0,a)') '- iproc',iproc,' finished writing waves'
      end if

    END SUBROUTINE writemywaves


    !> Write all my wavefunctions for fragments
    ! NB spin needs fixing!
    subroutine writemywaves_linear_fragments(iproc,filename,iformat,mat_format,npsidim,Lzd,orbs,at,rxyz,psi,&
         dir_output,input_frag,ref_frags,linmat,&
         num_neighbours,neighbour_cutoff)
      use module_types
      use module_atoms, only: astruct_dump_to_file, deallocate_atomic_structure, nullify_atomic_structure
      use module_precisions
      use module_fragments
      use sparsematrix_base, only: sparsematrix_malloc_ptr, assignment(=), DENSE_FULL, &
           matrices_null, allocate_matrices, deallocate_matrices
      use sparsematrix, only: uncompress_matrix2
      use module_bigdft_mpi
      use module_bigdft_output
      use module_bigdft_errors
      use module_bigdft_arrays
      use locregs
      use sparsematrix_types
      use chess_utils
      use bigpoly_io, only : bigpoly_write_submatrix
      !use matrix_operations, only: overlapPowerGeneral, check_taylor_order
      implicit none
      integer, intent(in) :: iproc,iformat,npsidim,mat_format
      !integer, intent(in) :: norb   !< number of orbitals, not basis functions
      type(atoms_data), intent(in) :: at
      type(orbitals_data), intent(in) :: orbs         !< orbs describing the basis functions
      type(local_zone_descriptors), intent(in) :: Lzd
      real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz
      real(wp), dimension(npsidim), intent(in) :: psi  ! Should be the real linear dimension and not the global
      character(len=*), intent(in) :: dir_output, filename
      type(fragmentInputParameters), intent(in) :: input_frag
      type(system_fragment), dimension(input_frag%nfrag_ref), intent(inout) :: ref_frags
      type(linear_matrices), intent(inout) :: linmat
      integer, intent(in) :: num_neighbours
      real(kind=8), intent(in) :: neighbour_cutoff
      !Local variables
      integer :: ncount1,ncount_rate,ncount_max,ncount2,iat
      integer :: isforb,isfat,ifrag,ifrag_ref,unitwf,ityp,ispin
      integer :: ntmb_frag_and_env,nelec_frag,ia,ja,io,jo,unitm
      integer, allocatable, dimension(:,:) :: map_frag_and_env, frag_map
      real(kind=8), allocatable, dimension(:,:,:) :: coeff_frag
      real(kind=8), allocatable, dimension(:,:) ::  rxyz_frag
      real(kind=8), allocatable, dimension(:) :: eval_frag
      real(kind=4) :: tr0,tr1
      real(kind=8) :: tel
      character(len=256) :: full_filename
      logical, allocatable, dimension(:) :: fragment_written
      logical :: binary
      !logical, parameter :: write_overlap=.true. ! want this to include rotation taking into account environment?  also do some check after restart? - not sure this is possible
      type(io_descriptor) :: descr

      type(matrices),dimension(1) :: ovrlp_half_
      logical :: calc_sks=.false.
      real(kind=8), dimension(:,:,:), pointer :: kernel_orthog

      logical :: write_dense, write_sparse

      if (iproc == 0) call yaml_map('Write wavefunctions to file', trim(filename)//'.*')
      !if (iproc == 0) write(*,"(1x,A,A,a)") "Write wavefunctions to file: ", trim(filename),'.*'

      if (iformat == WF_FORMAT_ETSF) then
          stop 'Linear scaling with ETSF writing not implemented yet'

          !    call write_waves_etsf(iproc,filename,orbs,n1,n2,n3,hx,hy,hz,at,rxyz,wfd,psi)
      else
         call cpu_time(tr0)
         call system_clock(ncount1,ncount_rate,ncount_max)

         ! Write the TMBs in the Plain BigDFT files.
         ! For now only output one (first) set per reference fragment

         ! array to check if we already outputted tmbs for this fragment type
         fragment_written=f_malloc((/input_frag%nfrag_ref/),id='fragment_written')
         fragment_written=.false.

         unitwf=99
         isforb=0
         isfat=0
         loop_ifrag: do ifrag=1,input_frag%nfrag
            ! find reference fragment this corresponds to and check if we already outputted tmbs for this reference fragment
            ifrag_ref=input_frag%frag_index(ifrag)

            if (fragment_written(ifrag_ref)) then
               isforb=isforb+ref_frags(ifrag_ref)%fbasis%forbs%norb
               isfat=isfat+ref_frags(ifrag_ref)%astruct_frg%nat
               cycle
            end if
            fragment_written(ifrag_ref)=.true.

            ! always have at least 1 tmb/atom so this is safe
            frag_map=f_malloc0((/ref_frags(ifrag_ref)%fbasis%forbs%norb,3/),id='frag_map')

            ! loop over orbitals of this fragment
            descr = dumpmywaves(trim(dir_output)//trim(input_frag%dirname(ifrag_ref))//trim(filename), &
                 (iformat == WF_FORMAT_BINARY), Lzd%Llr, &
                 orbs, ref_frags(ifrag_ref)%astruct_frg, rxyz, &
                 psi, isat = isfat, map = frag_map)
            !loop_iforb: do iforb=1,ref_frags(ifrag_ref)%fbasis%forbs%norb

            !debug
            !if (iproc==0) then
            !   do iorb=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
            !      if (iorb<=ref_frags(ifrag_ref)%astruct_frg%nat) then
            !         write(*,'(A,6(1x,I4),3(1x,F12.6))') 'iproc,ifrag,ifrag_ref',iproc,ifrag,ifrag_ref,frag_map(iorb,:),rxyz(:,frag_map(iorb,3))
            !      else
            !         write(*,'(A,6(1x,I4))') 'iproc,ifrag,ifrag_ref',iproc,ifrag,ifrag_ref,frag_map(iorb,:)
            !      end if
            !   end do
            !end if

            ! write environment coordinates
            ! want to avoid several MPI ranks writing to the same file
            ! and since the number of reference frags is expected to be relatively small as well as the file sizes,
            ! write all files from iproc=0
            ! make use of frag_map(onwhichatom_frag,3)=iiat (atom frag -> atom full)
            if (num_neighbours/=0) then
               ! column 1 tmbs->tmbs, column 2 tmbs->atoms, column 3 atoms->atoms (as for frag_map)
               ! size orbs%norb is overkill but we don't know norb for env yet
               map_frag_and_env = f_malloc((/orbs%norb,3/),id='map_frag_and_env')

               call find_neighbours(num_neighbours,at,rxyz,orbs,ref_frags(ifrag_ref),frag_map,&
                    ntmb_frag_and_env,map_frag_and_env,.false.,neighbour_cutoff,.true.)
               !full_filename=trim(dir_output)//trim(input_frag%dirname(ifrag_ref))//trim(filename)//'_env'
               full_filename=trim(dir_output)//trim(input_frag%label(ifrag_ref))//'_env'
               if (iproc==0) then
                  !open file to make sure we overwrite rather than append
                  open(99,file=trim(full_filename)//'.xyz',status='replace')
                  call astruct_dump_to_file(ref_frags(ifrag_ref)%astruct_env,full_filename,'# fragment environment',unit=99)
                  close(99)
               end if
            end if

            ! NEED to think about this - just make it diagonal for now? or random?  or truncate so they're not normalized?  or normalize after truncating?
            ! Or maybe don't write coeffs at all but assume we're always doing frag to frag and can use isolated frag coeffs?

            ! Now write the coefficients to file
            ! Must be careful, the orbs%norb is the number of basis functions
            ! while the norb is the number of orbitals.

            ! in from scratch case ref_frags(ifrag_ref)%nelec will be empty (zero)
            ! need to recalculate in this case, so may as well recalculate anyway
            ! probably also an easier way to do this...
            nelec_frag=0
            do iat=1,ref_frags(ifrag_ref)%astruct_frg%nat
               !ityp=ref_frags(ifrag_ref)%astruct_frg%iatype(iat)
               ityp=at%astruct%iatype(iat+isfat)
               nelec_frag=nelec_frag+at%nelpsp(ityp)
            end do

            if (nelec_frag/=ref_frags(ifrag_ref)%nelec .and. ref_frags(ifrag_ref)%nelec/=0) &
                 call f_err_throw('Problem with nelec in fragment output')

            ! order of tmbs not guaranteed so can't do this the simple way
            ! in fact it makes no sense to be cropping coeffs at all in this case as ks states not divided between fragments
            ! but don't want to keep all of them
            ! so either do cropped kernel (to be added) or support function 'charges' as diagonal part
            ! for the latter assume orthogonal and just take K_aa

            ! should eventually extract this from sparse?
            linmat%kernel_%matrix = sparsematrix_malloc_ptr(linmat%smat(3),iaction=DENSE_FULL,id='linmat%kernel_%matrix')

            call uncompress_matrix2(iproc, bigdft_mpi%nproc, bigdft_mpi%mpi_comm, &
                 linmat%smat(3), linmat%kernel_%matrix_compr, linmat%kernel_%matrix)

            !might be better to move this outside of the routine?
            !if (calc_sks) then
            !   ovrlp_half_(1) = matrices_null()
            !   call allocate_matrices(linmat%smat(3), allocate_full=.true., matname='ovrlp_half_(1)', mat=ovrlp_half_(1))

            !   !for case where tmbs aren't properly orthogonal, calculate S^1/2 K S^1/2
            !   call overlapPowerGeneral(iproc, bigdft_mpi%nproc, methTransformOverlap, 1, (/2/), &
            !        orthpar%blocksize_pdgemm, &
            !        imode=1, ovrlp_smat=linmat%smat(1), inv_ovrlp_smat=linmat%smat(3), &
            !        ovrlp_mat=linmat%ovrlp_, inv_ovrlp_mat=ovrlp_half_, &
            !        check_accur=.true., mean_error=mean_error, max_error=max_error)!!, &
            !   !if (iproc==0) call yaml_map('max error',max_error)
            !   !if (iproc==0) call yaml_map('mean error',mean_error)
            !   call check_taylor_order(mean_error, max_inversion_error, methTransformOverlap)

            !   call uncompress_matrix2(iproc, bigdft_mpi%nproc, linmat%smat(3), &
            !        ovrlp_half_(1)%matrix_compr, ovrlp_half_(1)%matrix)

            !   kernel_orthog=f_malloc_ptr(src_ptr=linmat%kernel_%matrix,id='kernel_orthog')

            !   ! do this with sparse instead... fix after testing
            !   call calculate_coeffMatcoeff(bigdft_mpi%nproc,linmat%kernel_%matrix,orbs,orbs,ovrlp_half_(1)%matrix,kernel_orthog)
            !else
               kernel_orthog => linmat%kernel_%matrix
            !end if

            !kernel_frag=f_malloc0((/ref_frags(ifrag_ref)%fbasis%forbs%norb,ref_frags(ifrag_ref)%fbasis%forbs%norb/),id='kernel_frag')
            !do io=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
            !   do jo=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
            !      kernel_frag(io,jo)=kernel_orthog(frag_map(io,1),frag_map(jo,1),1)!ispin)
            !   end do
            !end do

            if(iproc == 0) then
               ! want this to make sense also
               rxyz_frag=f_malloc0((/3,ref_frags(ifrag_ref)%astruct_frg%nat/),id='rxyz_frag')
               do ia=1,ref_frags(ifrag_ref)%astruct_frg%nat
                  rxyz_frag(:,ia)=rxyz(:,frag_map(ia,3))
               end do

               full_filename=trim(dir_output)//trim(input_frag%dirname(ifrag_ref))//trim(filename)
               call writeheader(descr, (mat_format/=WF_FORMAT_PLAIN), trim(full_filename), &
                    lzd%glr, orbs)

               coeff_frag=f_malloc0((/ref_frags(ifrag_ref)%fbasis%forbs%norb,ref_frags(ifrag_ref)%fbasis%forbs%norbu,&
                    ref_frags(ifrag_ref)%fbasis%forbs%nspin/),id='coeff_frag')
               do ispin=1,ref_frags(ifrag_ref)%fbasis%forbs%nspin
                  do io=1,ref_frags(ifrag_ref)%fbasis%forbs%norbu
                     coeff_frag(io,io,ispin)=kernel_orthog(frag_map(io,1),frag_map(io,1),ispin)
                  end do
               end do
               
               ! evals irrelevant so fill with fake value
               eval_frag=f_malloc0(ref_frags(ifrag_ref)%fbasis%forbs%norb,id='eval_frag')
               do io=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
                  eval_frag(io)=-0.5d0
               end do

               if (mat_format /= WF_FORMAT_PLAIN) then
                  call f_open_file(unitwf,file=trim(full_filename)//'_coeff.bin',&
                       binary=.true.)
               else
                  call f_open_file(unitwf,file=trim(full_filename)//'_coeff',&
                       binary=.false.)
               end if
               !if(iformat == WF_FORMAT_PLAIN) then
               !   open(unitwf, file=trim(full_filename)//'_coeff.bin', status='unknown',form='formatted')
               !else
               !   open(unitwf, file=trim(full_filename)//'_coeff.bin', status='unknown',form='unformatted')
               !end if

               ! Not sure whether this is correct for nspin=2...

               call writeLinearCoefficients(unitwf, (mat_format == WF_FORMAT_PLAIN), ref_frags(ifrag_ref)%astruct_frg%nat, &
                    rxyz_frag, ref_frags(ifrag_ref)%fbasis%forbs%norb, nelec_frag, &
                    ref_frags(ifrag_ref)%fbasis%forbs%norbu, ref_frags(ifrag_ref)%fbasis%forbs%nspin, coeff_frag, eval_frag)

               !call writeLinearCoefficients(unitwf,(mat_format == WF_FORMAT_PLAIN),ref_frags(ifrag_ref)%astruct_frg%nat,&
               !     rxyz(:,isfat+1:isfat+ref_frags(ifrag_ref)%astruct_frg%nat),ref_frags(ifrag_ref)%fbasis%forbs%norb,&
               !     nelec_frag,&
               !     ref_frags(ifrag_ref)%fbasis%forbs%norb, &
               !     coeff(isforb+1:isforb+ref_frags(ifrag_ref)%fbasis%forbs%norb,&
               !     isforb+1:isforb+ref_frags(ifrag_ref)%fbasis%forbs%norb),&
               !     orbs%eval(isforb+1:isforb+ref_frags(ifrag_ref)%fbasis%forbs%norb)) !-0.5d0
               call f_close(unitwf)

               call f_free(rxyz_frag)
               call f_free(eval_frag)
               call f_free(coeff_frag)
            end if
            !call f_free(kernel_frag)

            call io_deallocate_descriptor(descr)

            ! put this in a routine and reuse in write_linear_matrices
            ! FIX SPIN
            unitm=99
            binary=(mat_format /= WF_FORMAT_PLAIN)

            write_sparse = (1<=mat_format .and. mat_format<=9) .or. &
                     (21<=mat_format .and.  mat_format<=29)
            write_dense = (11<=mat_format .and. mat_format<=19) .or. &
                    (21<=mat_format .and.  mat_format<=29)

            if (write_dense) then
               if(iproc == 0) then
                  full_filename=trim(dir_output)//trim(input_frag%dirname(ifrag_ref))
                  call f_open_file(unitm, file=trim(full_filename)//'density_kernel.bin',&
                       binary=binary)

                  if (.not. binary) then
                      !!write(unitm,'(a,3i10,a)') '#  ', ref_frags(ifrag_ref)%fbasis%forbs%norb, &
                      !!    ref_frags(ifrag_ref)%astruct_frg%nat, linmat%smat(2)%nspin, &
                      !!    '    number of basis functions, number of atoms, number of spins'
                      write(unitm,'(3i10,a)') linmat%smat(2)%nspin, ref_frags(ifrag_ref)%fbasis%forbs%norb, &
                          ref_frags(ifrag_ref)%astruct_frg%nat,  &
                          '    number of basis functions, number of atoms, number of spins'
                  else
                      !!write(unitm) '#  ', ref_frags(ifrag_ref)%fbasis%forbs%norb, &
                      !!    ref_frags(ifrag_ref)%astruct_frg%nat, linmat%smat(2)%nspin, &
                      !!    '    number of basis functions, number of atoms, number of spins'
                      write(unitm) linmat%smat(2)%nspin, ref_frags(ifrag_ref)%fbasis%forbs%norb, &
                          ref_frags(ifrag_ref)%astruct_frg%nat,  &
                          '    number of basis functions, number of atoms, number of spins'
                  end if
                  !!do ia=1,ref_frags(ifrag_ref)%astruct_frg%nat
                  !!    if (.not. binary) then
                  !!        write(unitm,'(a,3es24.16)') '#  ',rxyz_frag(1:3,ia)
                  !!    else
                  !!        write(unitm) '#  ',rxyz_frag(1:3,ia)
                  !!    end if
                  !!end do

                  ! need to fix spin
                  !do ispin=1,linmat%smat(2)%nspin
                     do io=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
                        ia=frag_map(io,2)
                        do jo=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
                           ja=frag_map(jo,2)
                           if (.not. binary) then
                              write(unitm,'(2(i6,1x),e19.12,2(1x,i6))') io,jo,&
                                   kernel_orthog(frag_map(io,1),frag_map(jo,1),1),ia,ja
                           else
                              write(unitm) io,jo,kernel_orthog(frag_map(io,1),frag_map(jo,1),1),ia,ja
                           end if
                        end do
                     end do
                  !end do
                  call f_close(unitm)
               end if
            end if

            if (write_sparse) then

               full_filename=trim(dir_output) // &
                             trim(input_frag%dirname(ifrag_ref)) // &
                             trim('density_kernel_sparse')
               call bigpoly_write_submatrix(iproc, bigdft_mpi%nproc, &
                    bigdft_mpi%mpi_comm, linmat%smat(3), linmat%kernel_, &
                    full_filename, binary, 1, &
                    frag_map(1,1), &
                    frag_map(1,1)+ref_frags(ifrag_ref)%fbasis%forbs%norb-1, &
                    frag_map(1,1), &
                    frag_map(1,1)+ref_frags(ifrag_ref)%fbasis%forbs%norb-1)
                   
            end if 

            ! lr408: to also be implemented for sparse case
            ! lr408: for now not a priority as this feature is more or less deprecated
            ! also output 'environment' kernel
            if (ref_frags(ifrag_ref)%astruct_env%nat/=ref_frags(ifrag_ref)%astruct_frg%nat &
                 .and. num_neighbours/=0) then
               ! FIX SPIN
               unitm=99
               binary=(mat_format /= WF_FORMAT_PLAIN)
               if(iproc == 0) then
                  full_filename=trim(dir_output)//trim(input_frag%dirname(ifrag_ref))
                  call f_open_file(unitm, file=trim(full_filename)//'density_kernel_env.bin',&
                       binary=binary)

                  if (.not. binary) then
                      !!write(unitm,'(a,3i10,a)') '#  ', ntmb_frag_and_env, ref_frags(ifrag_ref)%astruct_env%nat, &
                      !!    linmat%smat(2)%nspin, '    number of basis functions, number of atoms, number of spins'
                      write(unitm,'(3i10,a)') linmat%smat(2)%nspin, ntmb_frag_and_env, ref_frags(ifrag_ref)%astruct_env%nat, &
                          '    number of basis functions, number of atoms, number of spins'
                  else
                      !!write(unitm) '#  ', ntmb_frag_and_env, ref_frags(ifrag_ref)%astruct_env%nat, &
                      !!    linmat%smat(2)%nspin, '    number of basis functions, number of atoms, number of spins'
                      write(unitm) linmat%smat(2)%nspin, ntmb_frag_and_env, ref_frags(ifrag_ref)%astruct_env%nat, &
                          '    number of basis functions, number of atoms, number of spins'
                  end if
                  !!do ia=1,ref_frags(ifrag_ref)%astruct_env%nat
                  !!    if (.not. binary) then
                  !!        write(unitm,'(a,3es24.16)') '#  ',ref_frags(ifrag_ref)%astruct_env%rxyz(1:3,ia)
                  !!    else
                  !!        write(unitm) '#  ',ref_frags(ifrag_ref)%astruct_env%rxyz(1:3,ia)
                  !!    end if
                  !!end do

                  ! need to fix spin
                  !do ispin=1,linmat%smat(2)%nspin
                     do io=1,ntmb_frag_and_env
                        ia=map_frag_and_env(io,2)
                        do jo=1,ntmb_frag_and_env
                           ja=map_frag_and_env(jo,2)
                           if (.not. binary) then
                              write(unitm,'(2(i6,1x),e19.12,2(1x,i6))') io,jo,&
                                   kernel_orthog(map_frag_and_env(io,1),map_frag_and_env(jo,1),1),ia,ja
                           else
                              write(unitm) io,jo,kernel_orthog(map_frag_and_env(io,1),map_frag_and_env(jo,1),1),ia,ja
                           end if
                        end do
                     end do
                  !end do
                  call f_close(unitm)
               end if
               call f_free(map_frag_and_env)

               ! deallocate/nullify here to be safe
               ! if there are ghost atoms we want to really deallocate atomnames, otherwise just nullify
               if (trim(ref_frags(ifrag_ref)%astruct_env%atomnames(ref_frags(ifrag_ref)%astruct_env%ntypes))/='X') then
                  nullify(ref_frags(ifrag_ref)%astruct_env%atomnames)
               end if
               call deallocate_atomic_structure(ref_frags(ifrag_ref)%astruct_env)
               call nullify_atomic_structure(ref_frags(ifrag_ref)%astruct_env)
            end if


            !pretty sure these should be moved out of fragment loop...
            call f_free_ptr(linmat%kernel_%matrix)
            if (calc_sks) then
               call deallocate_matrices(ovrlp_half_(1))
               call f_free_ptr(kernel_orthog)
            end if

            nullify(kernel_orthog)

            call cpu_time(tr1)
            call system_clock(ncount2,ncount_rate,ncount_max)
            tel=dble(ncount2-ncount1)/dble(ncount_rate)
            if (iproc == 0) then
               call yaml_sequence_open('Write Waves Time')
               call yaml_sequence(advance='no')
               call yaml_mapping_open(flow=.true.)
               call yaml_map('Process',iproc)
               call yaml_map('Timing',(/ real(tr1-tr0,kind=8),tel /),fmt='(1pe10.3)')
               call yaml_mapping_close()
               call yaml_sequence_close()
            end if

            call f_free(frag_map)

            isforb=isforb+ref_frags(ifrag_ref)%fbasis%forbs%norb
            isfat=isfat+ref_frags(ifrag_ref)%astruct_frg%nat
         end do loop_ifrag
      end if

      call f_free(fragment_written)


    END SUBROUTINE writemywaves_linear_fragments


    !might not be the best place for it - used above and in restart
    function dist_and_shift(periodic,alat,A,B)
      implicit none
      real(kind=8), intent(in) :: A
      real(kind=8), intent(inout) :: B
      real(kind=8) :: dist_and_shift
      real(kind=8), intent(in) :: alat
      logical, intent(in) :: periodic
      !local variables
      real(kind=8) :: shift

      !shift the B vector on its periodic image
      dist_and_shift = A - B
      if (periodic) then
         !periodic image, if distance is bigger than half of the box
         shift = 0.0d0
         if (dist_and_shift > 0.5d0*alat) then
           shift = alat
         else if (dist_and_shift < -0.5d0*alat) then
           shift = -alat
         end if
         !shift = real(floor(dist_and_shift+0.5d0),kind=8)
         dist_and_shift = dist_and_shift - shift
         B = B + shift
      end if

    end function dist_and_shift



   !> finds environment atoms and fills ref_frag%astruct_env accordingly
   !! also returns mapping array for fragment and environment
   subroutine find_neighbours(num_neighbours,at,rxyz,orbs,ref_frag,frag_map,ntmb_frag_and_env,map_frag_and_env,&
        closest_only,cutoff,check_for_ghosts)
      use module_fragments
      use module_types
      use module_atoms, only: deallocate_atomic_structure, nullify_atomic_structure, set_astruct_from_file
      use module_precisions
      use module_bigdft_mpi
      use yaml_strings, only: f_strcpy
      use rototranslations, only: frag_center
      use at_domain, only: domain_periodic_dims
      use module_bigdft_arrays
      implicit none
      integer, intent(in) :: num_neighbours
      type(atoms_data), intent(in) :: at
      real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz
      type(orbitals_data), intent(in) :: orbs
      type(system_fragment), intent(inout) :: ref_frag
      integer, dimension(ref_frag%fbasis%forbs%norb,3), intent(in) :: frag_map
      integer, intent(out) :: ntmb_frag_and_env
      integer, dimension(orbs%norb,3), intent(out) :: map_frag_and_env
      logical, intent(in) :: closest_only
      logical, intent(in) :: check_for_ghosts
      real(kind=8), intent(in) :: cutoff

      !local variables
      integer :: iatnf, iat, ityp, iatf, iatt, num_before, num_after, jat, num_neighbours_tot, iorb, ntypes, nat_not_frag
      integer, allocatable, dimension(:) :: map_not_frag, ipiv
      integer, allocatable, dimension(:) :: num_neighbours_type, atype_not_frag
      integer, pointer, dimension(:) :: iatype

      real(kind=8) :: tol
      real(kind=8), dimension(3) :: frag_centre
      real(kind=8), allocatable, dimension(:) :: dist
      real(kind=8), allocatable, dimension(:,:) :: rxyz_frag, rxyz_not_frag, rxyz_frag_and_env
      type(atomic_structure) :: astruct_ghost
      logical :: on_frag, perx, pery, perz, ghosts_exist
      logical, dimension(3) :: peri

      if (closest_only) then
         tol=0.1d0
      else
         tol=1.0e-2
      end if

      astruct_ghost%nat=0

      ! first need to check for and read in ghost atoms if required
      ! might be better to move this outside routine, and just pass in astruct_ghost to avoid re-reading file for each fragment?
      if (check_for_ghosts) then
         !ghost atoms in ghost.xyz - would be better if this was seed_ghost.xyz but since we don't have seed here at the moment come back to this point later
         call nullify_atomic_structure(astruct_ghost)
         astruct_ghost%nat=0

         ! first check if ghost file exists
         inquire(FILE = 'ghost.xyz', EXIST = ghosts_exist)

         if (ghosts_exist) then
            call set_astruct_from_file('ghost',bigdft_mpi%iproc,astruct_ghost)
         end if

      end if

      rxyz_frag = f_malloc((/ 3,ref_frag%astruct_frg%nat /),id='rxyz_frag')
      nat_not_frag = at%astruct%nat-ref_frag%astruct_frg%nat+astruct_ghost%nat
      rxyz_not_frag = f_malloc((/ 3,nat_not_frag /),id='rxyz_not_frag')
      map_not_frag = f_malloc(nat_not_frag,id='map_not_frag')

      ! we actually want num_neighbours of each atom type (if possible)
      ! figure out how many atoms that actually is by counting how many of each type are not in fragment

      ! ghost atoms are an additional atom type added to end of list
      if (astruct_ghost%nat>0) then
         ntypes=at%astruct%ntypes+1
      else
         ntypes=at%astruct%ntypes
      end if

      atype_not_frag=f_malloc0(ntypes,id='atype_not_frag')
      num_neighbours_type=f_malloc(ntypes,id='num_neighbours_type')

      iatnf=0
      do iat=1,at%astruct%nat
         on_frag=.false.
         do iatf=1,ref_frag%astruct_frg%nat
            if (frag_map(iatf,3)==iat) then
               on_frag=.true.
               rxyz_frag(:,iatf)=rxyz(:,iat)
               exit
            end if
         end do
         if (.not. on_frag) then
            iatnf=iatnf+1
            rxyz_not_frag(:,iatnf) = rxyz(:,iat)
            map_not_frag(iatnf)=iat
            atype_not_frag(at%astruct%iatype(iat)) = atype_not_frag(at%astruct%iatype(iat)) + 1
         end if
      end do

      ! if we have some ghost atoms, add them to the end of rxyz_not_frag array
      if (astruct_ghost%nat>0) then
         do iat=1,astruct_ghost%nat
            iatnf=iatnf+1
            rxyz_not_frag(:,iatnf) = astruct_ghost%rxyz(:,iat)
            map_not_frag(iatnf)=at%astruct%nat+iat
            atype_not_frag(at%astruct%ntypes+1) = atype_not_frag(at%astruct%ntypes+1) + 1
         end do
      end if

      ! in this case we don't care about atom types, we just want the closest neighbours
      if (closest_only) then
         num_neighbours_tot=num_neighbours
         num_neighbours_type=0
      else
         num_neighbours_tot=0
         do ityp=1,ntypes
            num_neighbours_type(ityp) = min(atype_not_frag(ityp),num_neighbours)
            num_neighbours_tot = num_neighbours_tot + num_neighbours_type(ityp)
            !write(*,'(a,6(i3,2x))') 'type',ifrag,ifrag_ref,ityp,at%astruct%ntypes,num_neighbours_type(ityp),num_neighbours_tot
            !write(*,'(a,5(i3,2x))') 'type',ityp,ntypes,num_neighbours_type(ityp),num_neighbours_tot,astruct_ghost%nat
         end do
      end if
      call f_free(atype_not_frag)


      if (astruct_ghost%nat>0) then
         iatype=f_malloc_ptr(at%astruct%nat+astruct_ghost%nat,id='iatype')
         call vcopy(at%astruct%nat,at%astruct%iatype(1),1,iatype(1),1)
         do iat=at%astruct%nat+1,at%astruct%nat+astruct_ghost%nat
            iatype(iat)=at%astruct%ntypes+1
         end do
      else
         iatype => at%astruct%iatype
      end if


      ! find distances from centre of fragment - not necessarily the best approach but fine if fragment has 1 atom (most likely case)
      frag_centre=frag_center(ref_frag%astruct_frg%nat,rxyz_frag)

      dist = f_malloc(nat_not_frag,id='dist')
      ipiv = f_malloc(nat_not_frag,id='ipiv')
      ! find distances from this atom BEFORE shifting
!!$      perx=(at%astruct%geocode /= 'F')
!!$      pery=(at%astruct%geocode == 'P')
!!$      perz=(at%astruct%geocode /= 'F')
      !peri=bc_periodic_dims(geocode_to_bc(at%astruct%geocode))
      peri=domain_periodic_dims(at%astruct%dom)
      perx=peri(1)
      pery=peri(2)
      perz=peri(3)

      ! if coordinates wrap around (in periodic), take this into account
      ! assume that the fragment and environment are written in free bc
      ! think about other periodic cases that might need fixing...
      ! no need to shift wrt centre as this is done when reading in
      do iat=1,nat_not_frag
         dist(iat) = dist_and_shift(perx,at%astruct%cell_dim(1),frag_centre(1),rxyz_not_frag(1,iat))**2
         dist(iat) = dist(iat) + dist_and_shift(pery,at%astruct%cell_dim(2),frag_centre(2),rxyz_not_frag(2,iat))**2
         dist(iat) = dist(iat) + dist_and_shift(perz,at%astruct%cell_dim(3),frag_centre(3),rxyz_not_frag(3,iat))**2

         dist(iat) = -dsqrt(dist(iat))
!!$         write(*,'(A,2(I3,2x),F12.6,3x,2(3(F12.6,1x),2x))') 'ifrag,iat,dist',ifrag,iat,dist(iat),&
!!$              at%astruct%cell_dim(:),rxyz_new_all(:,iat)

         !rxyz_not_frag(:,iat) = rxyz_new_all(:,iat)-frag_trans_frag(ifrag)%rot_center_new
      end do

      ! sort atoms into neighbour order
      call sort_positions(nat_not_frag,dist,ipiv)

      ! allocate this larger than needed in case we have to complete a 'shell' of neighbours
      rxyz_frag_and_env = f_malloc((/ 3,ref_frag%astruct_frg%nat+num_neighbours_tot /),id='rxyz_frag_and_env')
      !rxyz_frag_and_env = f_malloc((/ 3,nat_not_frag /),id='rxyz_frag_and_env')

      ! take fragment and closest neighbours (assume that environment atoms were originally the closest)
      do iat=1,ref_frag%astruct_frg%nat
         rxyz_frag_and_env(:,iat) = rxyz_frag(:,iat)
         map_frag_and_env(iat,3) = frag_map(iat,3)
      end do
      call f_free(rxyz_frag)

      iatf=0
      do ityp=1,ntypes
         iatt=0
         ! in this case no neighbours of that atomic species exist, so we can loop back around
         if (num_neighbours_type(ityp)==0 .and. (.not. closest_only)) cycle
         do iat=1,nat_not_frag
            !print*,ityp,iat,nat_not_frag,map_not_frag(ipiv(iat)),iatype(map_not_frag(ipiv(iat)))
            if (iatype(map_not_frag(ipiv(iat)))/=ityp .and. (.not. closest_only)) cycle
            !print*,ityp,iat,nat_not_frag
            ! first apply a distance cut-off so that all neighbours are ignored beyond some distance (needed e.g. for defects)
            if (abs(dist(ipiv(iat))) > cutoff) then
               ! subtract the neighbours that we won't be including
               !if (bigdft_mpi%iproc==0) write(*,'(a,5(i3,2x),2(F8.2,2x),2(i3,2x))') 'cut',ityp,iatt,num_neighbours_type(ityp),iatf,iat,dist(ipiv(iat)),cutoff,&
               !     num_neighbours_tot,num_neighbours_tot - (num_neighbours_type(ityp) - iatt)
               num_neighbours_tot = num_neighbours_tot - (num_neighbours_type(ityp) - iatt)
               num_neighbours_type(ityp) = iatt
               exit
            end if
            !print*,'iat',ityp,iatt,num_neighbours_type(ityp),iatf,iat,dist(ipiv),cutoff,num_neighbours_tot

            iatf=iatf+1
            iatt=iatt+1
            rxyz_frag_and_env(:,iatf+ref_frag%astruct_frg%nat) = rxyz_not_frag(:,ipiv(iat))
            map_frag_and_env(iatf+ref_frag%astruct_frg%nat,3) = map_not_frag(ipiv(iat))
            !print*,'iatt',ityp,iatt,num_neighbours_type(ityp),iatf,iat,nat_not_frag

            ! exit if we've reached the number for this species or the total if we're not looking at species
            if ((closest_only .and. iatt==num_neighbours_tot) &
                 .or. ((.not. closest_only) .and. iatt==num_neighbours_type(ityp))) exit
         end do

         !never cut off a shell - either include all at that distance or none (depending on if at least at minimum?)
         ! - check distances of next point to see...
         num_after=0
         do jat=iat+1,nat_not_frag
            if (iatype(map_not_frag(ipiv(jat)))/=ityp .and. (.not. closest_only)) cycle
            if (abs(dist(ipiv(jat))-dist(ipiv(iat))) < tol) then
               num_after=num_after+1
            else
               exit
            end if
         end do
         if (num_after==0) then
            if (.not. closest_only) then
               cycle
            else
               exit
            end if
         end if

         !also check before
         num_before=0
         do jat=iat-1,1,-1
            if (iatype(map_not_frag(ipiv(jat)))/=ityp .and. (.not. closest_only)) cycle
            if (abs(dist(ipiv(jat))-dist(ipiv(iat))) < tol) then
               num_before=num_before+1
            else
               exit
            end if
         end do
         !get rid of them (assuming we will still have at least one neighbour of this type left)
         if (((.not. closest_only) .and. num_neighbours_type(ityp)>num_before+1) &
              .or. (closest_only .and. num_neighbours_tot>num_before+1)) then
            num_neighbours_type(ityp)=num_neighbours_type(ityp)-num_before-1
            num_neighbours_tot=num_neighbours_tot-num_before-1
            iatf=iatf-num_before-1
         !add neighbours until shell is complete
         else
            num_neighbours_type(ityp)=num_neighbours_type(ityp)+num_after
            num_neighbours_tot=num_neighbours_tot+num_after
            do jat=iat+1,nat_not_frag
               if (iatype(map_not_frag(ipiv(jat)))/=ityp .and. (.not. closest_only)) cycle
               iatf=iatf+1
               iatt=iatt+1
               rxyz_frag_and_env(:,iatf+ref_frag%astruct_frg%nat) = rxyz_not_frag(:,ipiv(jat))
               map_frag_and_env(iatf+ref_frag%astruct_frg%nat,3) = map_not_frag(ipiv(jat))
               if ((closest_only .and. iatt==num_neighbours_tot) &
                    .or. ((.not. closest_only) .and. iatt==num_neighbours_type(ityp))) exit
            end do
         end if

         ! in this case we're ignoring atomic species so we should exit after first iteration
         if (closest_only) exit

      end do

      if (iatf/=num_neighbours_tot) then
          write(*,*) 'Error num_neighbours_tot/=iatf in find_neighbours',num_neighbours_tot, iatf
          stop
      end if
      call f_free(num_neighbours_type)
      call f_free(rxyz_not_frag)
      call f_free(map_not_frag)
      call f_free(dist)
      call f_free(ipiv)

      ! fill in map for tmbs
      ntmb_frag_and_env = 0
      do iat=1,ref_frag%astruct_frg%nat+num_neighbours_tot
         do iorb=1,orbs%norb
            if (orbs%onwhichatom(iorb)==map_frag_and_env(iat,3)) then
               ntmb_frag_and_env = ntmb_frag_and_env+1
               map_frag_and_env(ntmb_frag_and_env,1)=iorb
               map_frag_and_env(ntmb_frag_and_env,2)=iat
            end if
         end do
      end do

      ! put rxyz_env into astruct_env - first delete pre-existing structure
      if (ref_frag%astruct_env%nat/=0) then
         call deallocate_atomic_structure(ref_frag%astruct_env)
         call nullify_atomic_structure(ref_frag%astruct_env)
      end if

      ! copy some stuff from astruct_frg
      ref_frag%astruct_env%inputfile_format = ref_frag%astruct_frg%inputfile_format
      ref_frag%astruct_env%units = ref_frag%astruct_frg%units
      ref_frag%astruct_env%geocode = ref_frag%astruct_frg%geocode
      ref_frag%astruct_env%dom = ref_frag%astruct_frg%dom

      ref_frag%astruct_env%nat = ref_frag%astruct_frg%nat+num_neighbours_tot
      ref_frag%astruct_env%rxyz = f_malloc_ptr((/3,ref_frag%astruct_env%nat/),id='ref_frag%astruct_env%rxyz')
      call vcopy(3*ref_frag%astruct_env%nat,rxyz_frag_and_env(1,1),1,ref_frag%astruct_env%rxyz(1,1),1)
      call f_free(rxyz_frag_and_env)

      ! now deal with atom types
      ref_frag%astruct_env%ntypes = ntypes
      if (astruct_ghost%nat>0) then
         ref_frag%astruct_env%atomnames=f_malloc0_str_ptr(len(at%astruct%atomnames),ntypes,&
            id='ref_frag%astruct_env%atomnames')
         do iat=1,at%astruct%ntypes
            call f_strcpy(src=at%astruct%atomnames(iat),dest=ref_frag%astruct_env%atomnames(iat))
         end do
         ref_frag%astruct_env%atomnames(ntypes)=astruct_ghost%atomnames(1)
      else
         ref_frag%astruct_env%atomnames => at%astruct%atomnames
      end if

      ! can't just point to full version due to atom reordering
      ref_frag%astruct_env%iatype = f_malloc_ptr(ref_frag%astruct_env%nat,id='ref_frag%astruct_env%iatype')

      ! polarization etc is irrelevant
      ref_frag%astruct_env%input_polarization = f_malloc0_ptr(ref_frag%astruct_env%nat,&
           id='ref_frag%astruct_env%input_polarization')
      ref_frag%astruct_env%ifrztyp = f_malloc0_ptr(ref_frag%astruct_env%nat,id='ref_frag%astruct_env%ifrztyp')

      do iat=1,ref_frag%astruct_env%nat
         ref_frag%astruct_env%iatype(iat) = iatype(map_frag_and_env(iat,3))
         ref_frag%astruct_env%input_polarization(iat) = 100
         !if (iproc==0) print*,iat,trim(ref_frag%astruct_env%atomnames(ref_frag%astruct_env%iatype(iat)))
      end do

      if (astruct_ghost%nat>0) then
         call f_free_ptr(iatype)
      else
         nullify(iatype)
      end if

      if (check_for_ghosts) then
         call deallocate_atomic_structure(astruct_ghost)
         call nullify_atomic_structure(astruct_ghost)
      end if

   end subroutine find_neighbours

    subroutine io_read_descr_coeff(unitwf, formatted, norb_old, ntmb_old, nspin_old, &
           & lstat, error, nat, rxyz_old)
        use module_precisions
        use module_types
        !use internal_io
        implicit none
        integer, intent(in) :: unitwf
        logical, intent(in) :: formatted
        integer, intent(out) :: norb_old, ntmb_old, nspin_old
        logical, intent(out) :: lstat
        character(len =256), intent(out) :: error
        ! Optional arguments
        integer, intent(in), optional :: nat
        real(gp), dimension(:,:), intent(out), optional :: rxyz_old

        integer :: i, iat, i_stat, nat_
        real(gp) :: rxyz(3)

        lstat = .false.
        write(error, "(A)") "cannot read coeff description."
        if (formatted) then
           read(unitwf,*,iostat=i_stat) ntmb_old, norb_old, nspin_old
           if (i_stat /= 0) return
           !write(*,*) 'reading ',nat,' atomic positions'
           if (present(nat) .And. present(rxyz_old)) then
              read(unitwf,*,iostat=i_stat) nat_
              if (i_stat /= 0) return
              ! Sanity check
              if (size(rxyz_old, 2) /= nat) stop "Mismatch in coordinate array size."
              if (nat_ /= nat) stop "Mismatch in coordinate array size."
              do iat=1,nat
                 read(unitwf,*,iostat=i_stat) (rxyz_old(i,iat),i=1,3)
                 if (i_stat /= 0) return
              enddo
           else
              read(unitwf,*,iostat=i_stat) nat_
              if (i_stat /= 0) return
              do iat=1,nat_
                 read(unitwf,*,iostat=i_stat)
                 if (i_stat /= 0) return
              enddo
           end if
           !read(unitwf,*,iostat=i_stat) i, iat
           !if (i_stat /= 0) return
        else
           read(unitwf,iostat=i_stat) ntmb_old, norb_old, nspin_old
           if (i_stat /= 0) return
           if (present(nat) .And. present(rxyz_old)) then
              read(unitwf,iostat=i_stat) nat_
              if (i_stat /= 0) return
              ! Sanity check
              if (size(rxyz_old, 2) /= nat) stop "Mismatch in coordinate array size."
              if (nat_ /= nat) stop "Mismatch in coordinate array size."
              do iat=1,nat
                 read(unitwf,iostat=i_stat)(rxyz_old(i,iat),i=1,3)
                 if (i_stat /= 0) return
              enddo
           else
              read(unitwf,iostat=i_stat) nat_
              if (i_stat /= 0) return
              do iat=1,nat_
                 read(unitwf,iostat=i_stat) rxyz
                 if (i_stat /= 0) return
              enddo
           end if
           !read(unitwf,iostat=i_stat) i, iat
           !if (i_stat /= 0) return
        end if
        lstat = .true.
    END SUBROUTINE io_read_descr_coeff


    subroutine read_coeff_minbasis(unitwf,useFormattedInput,iproc,ntmb,norb_old,nfvctr,nspin,coeff,eval,nat,rxyz_old)
      use module_precisions
      use module_types
      !use internal_io
      use yaml_output
      implicit none
      logical, intent(in) :: useFormattedInput
      integer, intent(in) :: unitwf,iproc,ntmb,nfvctr,nspin
      integer, intent(out) :: norb_old
      real(wp), dimension(nfvctr,nfvctr,nspin), intent(out) :: coeff
      real(wp), dimension(ntmb), intent(out) :: eval
      integer, optional, intent(in) :: nat
      real(gp), dimension(:,:), optional, intent(out) :: rxyz_old

      !local variables
      character(len = 256) :: error
      logical :: lstat
      integer :: i_stat
      integer :: ntmb_old, i1, i2, i, j, iorb, iorb_old, nspin_old, ispin, is
      real(wp) :: tt

      call io_read_descr_coeff(unitwf, useFormattedInput, norb_old, ntmb_old, nspin_old, &
           & lstat, error, nat, rxyz_old)
      if (.not. lstat) call io_error(trim(error))

      if (ntmb_old /= nfvctr) then
         if (iproc == 0) write(error,"(A)") 'error in read coeffs, ntmb_old/=ntmb'
         call io_error(trim(error))
      end if

      if (nspin_old /= nspin .and. nspin_old == 2) then
         if (iproc == 0) write(error,"(A)") 'error in read coeffs, nspin_old/=nspin'
         call io_error(trim(error))
      end if

      ! read the eigenvalues
      if (useFormattedInput) then
         do ispin=1,nspin_old
            do iorb=1,nfvctr
               read(unitwf,*,iostat=i_stat) is,iorb_old,eval(iorb+(ispin-1)*nfvctr)
               if (nspin == 2 .and. nspin_old == 1) then
                  eval(iorb+nfvctr) = eval(iorb+(ispin-1)*nfvctr)
               end if
               if (iorb_old /= iorb) stop 'read_coeff_minbasis'
            end do
         end do
      else
         i_stat = 0
         do ispin=1,nspin_old
            do iorb=1,nfvctr
               read(unitwf,iostat=i_stat) is,iorb_old,eval(iorb+(ispin-1)*nfvctr)
               if (nspin == 2 .and. nspin_old == 1) then
                  eval(iorb+nfvctr) = eval(iorb+(ispin-1)*nfvctr)
               end if
               if (iorb_old /= iorb) stop 'read_coeff_minbasis'
            end do
         end do
         if (i_stat /= 0) stop 'Problem reading the eigenvalues'
      end if

      !if (iproc == 0) write(*,*) 'coefficients need NO reformatting'

      ! Now read the coefficients
      do ispin = 1, nspin_old
         do i = 1, nfvctr
            do j = 1,nfvctr
               if (useFormattedInput) then
                  read(unitwf,*,iostat=i_stat) is,i1,i2,tt
               else
                  read(unitwf,iostat=i_stat) is,i1,i2,tt
               end if
               if (i_stat /= 0) stop 'Problem reading the coefficients'
               coeff(j,i,ispin) = tt
               if (nspin == 2 .and. nspin_old == 1) then
                  coeff(j,i,2) = tt
               end if
            end do
         end do
      end do

      ! this is already done when writing coefficients...
      ! rescale so first significant element is +ve
      !do i = 1, ntmb
      !   do j = 1,nfvctr
      !      if (abs(coeff(j,i))>1.0e-1) then
      !         if (coeff(j,i)<0.0_gp) call dscal(ntmb,-1.0_gp,coeff(1,i),1)
      !         exit
      !      end if
      !   end do
      !   !if (j==ntmb+1) print*,'Error finding significant coefficient, coefficients not scaled to have +ve first element'
      !end do

    END SUBROUTINE read_coeff_minbasis

    !1subroutine read_kernel(unitwf,useFormattedInput,iproc,ntmb,norb_old,kernel,nat,rxyz_old)
    !1  use module_base
    !1  use module_types
    !1  !use internal_io
    !1  use yaml_output
    !1  implicit none
    !1  logical, intent(in) :: useFormattedInput
    !1  integer, intent(in) :: unitwf,iproc,ntmb
    !1  integer, intent(out) :: norb_old
    !1  real(wp), dimension(ntmb,ntmb), intent(out) :: kernel
    !1  !real(wp), dimension(ntmb), intent(out) :: eval !eliminate use of eval?
    !1  integer, optional, intent(in) :: nat
    !1  real(gp), dimension(:,:), optional, intent(out) :: rxyz_old
    !1
    !1  !local variables
    !1  character(len = 256) :: error
    !1  logical :: lstat
    !1  integer :: i_stat
    !1  integer :: ntmb_old, i1, i2,i,j,iorb,iorb_old
    !1  real(wp) :: tt
    !1
    !1  call io_read_descr_kernel(unitwf, useFormattedInput, norb_old, ntmb_old, &
    !1       & lstat, error, nat, rxyz_old)
    !1  if (.not. lstat) call io_error(trim(error))
    !1
    !1  if (ntmb_old /= ntmb) then
    !1     if (iproc == 0) write(error,"(A)") 'error in read coeffs, ntmb_old/=ntmb'
    !1     call io_error(trim(error))
    !1  end if
    !1
    !1  ! read the eigenvalues
    !1  if (useFormattedInput) then
    !1     do iorb=1,ntmb
    !1        read(unitwf,*,iostat=i_stat) iorb_old,eval(iorb)
    !1        if (iorb_old /= iorb) stop 'read_coeff_minbasis'
    !1     enddo
    !1  else
    !1     do iorb=1,ntmb
    !1        read(unitwf,iostat=i_stat) iorb_old,eval(iorb)
    !1        if (iorb_old /= iorb) stop 'read_coeff_minbasis'
    !1     enddo
    !1     if (i_stat /= 0) stop 'Problem reading the eigenvalues'
    !1  end if
    !1
    !1  !if (iproc == 0) write(*,*) 'coefficients need NO reformatting'
    !1
    !1  ! Now read the coefficients
    !1  do i = 1, ntmb
    !1     do j = 1,nfvctr
    !1        if (useFormattedInput) then
    !1           read(unitwf,*,iostat=i_stat) i1,i2,tt
    !1        else
    !1           read(unitwf,iostat=i_stat) i1,i2,tt
    !1        end if
    !1        if (i_stat /= 0) stop 'Problem reading the coefficients'
    !1        coeff(j,i) = tt
    !1     end do
    !1  end do
    !1
    !1  ! rescale so first significant element is +ve
    !1  do i = 1, ntmb
    !1     do j = 1,nfvctr
    !1        if (abs(coeff(j,i))>1.0e-1) then
    !1           if (coeff(j,i)<0.0_gp) call dscal(ntmb,-1.0_gp,coeff(1,i),1)
    !1           exit
    !1        end if
    !1     end do
    !1     !if (j==ntmb+1) print*,'Error finding significant coefficient, coefficients not scaled to have +ve first element'
    !1  end do
    !1
    !1END SUBROUTINE read_kernel


    subroutine io_read_descr(unitwf, formatted, lstat, error, &
         iorb, eval, lr, onwhichatom, confPotOrder, confPotprefac, locrad, &
         rxyz, gdom, nboxf)
        use module_precisions
        use locregs
        use at_domain
        use f_precisions
        implicit none

        integer, intent(in) :: unitwf
        logical, intent(in) :: formatted
        logical, intent(out) :: lstat
        character(len =256), intent(out) :: error
        ! Optional arguments
        type(locreg_descriptors), intent(inout), optional :: lr
        integer, intent(out), optional :: iorb
        real(wp), intent(out), optional :: eval
        real(gp), intent(out), optional :: locrad
        integer, intent(out), optional :: onwhichatom
        integer, intent(out), optional :: confPotOrder
        real(gp), intent(out), optional :: confPotprefac
        real(gp),dimension(:,:),intent(out),optional :: rxyz
        type(domain), intent(in), optional :: gdom
        integer, dimension(2,3), intent(in), optional :: nboxf

        type(domain) :: dom
        integer :: i, iat, i_stat, nat, n1, n2, n3, ival, ns1, ns2, ns3
        integer, dimension(2,3) :: nbox
        real(wp) :: wval
        real(gp) :: rxyz_(3), hgrids(3), centre(3), rad, fac
        character(len = 256) :: line

        lstat = .false.
        write(error, "(A)") "cannot read psi description."
        !if (bigdft_mpi%iproc==0) print*,'formatted: ',formatted
        if (formatted) then
           read(unitwf,*,iostat=i_stat) ival, wval
           write(error, "(A)") "cannot read orbital id and eigen value."
           if (i_stat /= 0) return
           if (present(iorb)) iorb = ival
           if (present(eval)) eval = wval
           read(unitwf,*,iostat=i_stat) hgrids(1),hgrids(2),hgrids(3)
           write(error, "(A)") "cannot read hgrid values."
           if (i_stat /= 0) return
           write(error, "(A)") "cannot read grid values."
           read(unitwf,*,iostat=i_stat) n1, n2, n3
           if (i_stat /= 0) return
           read(unitwf, "(A)",iostat=i_stat) line
           if (i_stat /= 0) return
           write(error, "(A)") "cannot read grid starting offsets."
           read(line,*,iostat=i_stat) ns1,ns2,ns3
           if (i_stat == 0) then
              write(error, "(A)") "cannot read localisation region parameters."
              read(unitwf,*,iostat=i_stat) (centre(i),i=1,3),iat,rad,ival,fac
              if (i_stat /= 0) return
              write(error, "(A)") "cannot read atom number."
              read(unitwf,*,iostat=i_stat) nat
              if (i_stat /= 0) return
           else
              ! Legacy format used by the cubic code
              ns1 = 0
              ns2 = 0
              ns3 = 0
              centre = UNINITIALIZED(1._gp)
              rad = UNINITIALIZED(rad)
              iat = UNINITIALIZED(iat)
              ival = UNINITIALIZED(ival)
              fac = UNINITIALIZED(fac)
              read(line,*,iostat=i_stat) nat
              if (i_stat /= 0) return
           end if
           if (present(onwhichatom)) onwhichatom = iat
           if (present(locrad)) locrad = rad
           if (present(confPotOrder)) confPotOrder = ival
           if (present(confPotprefac)) confPotprefac = fac
           if (present(rxyz)) then
              ! Sanity check
              if (size(rxyz, 2) /= nat) stop "Mismatch in coordinate array size."
              do iat=1,nat
                 read(unitwf,*,iostat=i_stat) (rxyz(i,iat),i=1,3)
                 if (i_stat /= 0) return
              enddo
           else
              do iat=1,nat
                 write(error, "(A)") "cannot read atom positions."
                 read(unitwf,*,iostat=i_stat)
                 if (i_stat /= 0) return
              enddo
           end if
        else
           read(unitwf,iostat=i_stat) ival, wval
           if (i_stat /= 0) return
           if (present(iorb)) iorb = ival
           if (present(eval)) eval = wval
           read(unitwf,iostat=i_stat) hgrids(1),hgrids(2),hgrids(3)
           if (i_stat /= 0) return
           read(unitwf,iostat=i_stat) n1, n2, n3
           if (i_stat /= 0) return
           read(unitwf,iostat=i_stat) ns1,ns2,ns3
           if (i_stat /= 0) return
           read(unitwf,iostat=i_stat) (centre(i),i=1,3),iat,rad,ival,fac
           if (i_stat /= 0) return
           if (present(onwhichatom)) onwhichatom = iat
           if (present(locrad)) locrad = rad
           if (present(confPotOrder)) confPotOrder = ival
           if (present(confPotprefac)) confPotprefac = fac
           read(unitwf,iostat=i_stat) nat
           if (i_stat /= 0) return
           if (present(rxyz)) then
              ! Sanity check
              if (size(rxyz, 2) /= nat) stop "Mismatch in coordinate array size."
              do iat=1,nat
                 read(unitwf,iostat=i_stat) (rxyz(i,iat),i=1,3)
                 if (i_stat /= 0) return
              enddo
           else
              do iat=1,nat
                 read(unitwf,iostat=i_stat) rxyz_
                 if (i_stat /= 0) return
              enddo
           end if
        end if
        if (present(lr)) then
           if (present(gdom) .and. rad == UNINITIALIZED(rad)) then
              dom = gdom
           else if (present(gdom)) then
              dom = change_domain_BC(gdom, geocode = 'F')
           else
              dom = domain_new(ATOMIC_UNITS, (/ FREE_BC, FREE_BC, FREE_BC /), &
                   acell = hgrids * (/ n1+1, n2+1, n3+1 /))
           end if
           nbox(1,1) = ns1
           nbox(1,2) = ns2
           nbox(1,3) = ns3
           nbox(2,1) = ns1 + n1
           nbox(2,2) = ns2 + n2
           nbox(2,3) = ns3 + n3
           if (present(nboxf)) then
              call init_lr(lr, dom, 0.5_gp * hgrids, nbox, nboxf, .false., gdom)
           else
              call init_lr(lr, dom, 0.5_gp * hgrids, nbox, nbox, .false., gdom)
           end if
           lr%locregCenter = centre
           lr%locrad = rad
        end if
        if (rad /= UNINITIALIZED(rad)) then
           if (formatted) then
              call read_lr_txt(unitwf, lr)
           else
              call read_lr_bin(unitwf, lr)
           end if
        else
           ! Legacy format
           if (formatted) then
              read(unitwf,*,iostat=i_stat) n1, n2
           else
              read(unitwf,iostat=i_stat) n1, n2
           end if
           if (i_stat /= 0) return
           if (present(lr)) call init_sized(lr, n1, n2)
        end if
        lstat = .true.

    END SUBROUTINE io_read_descr

    subroutine io_error(error)
      use module_bigdft_mpi

      implicit none

      character(len = *), intent(in) :: error
      integer :: ierr

      call io_warning(error)
      call MPI_ABORT(bigdft_mpi%mpi_comm, ierr)
    END SUBROUTINE io_error


    subroutine io_warning(error)
      implicit none

      character(len = *), intent(in) :: error

      write(0,"(2A)") "WARNING! ", trim(error)
    END SUBROUTINE io_warning


    subroutine io_open(unitwf, filename, formatted)
      use f_utils, only: f_open_file
      implicit none
      character(len = *), intent(in) :: filename
      logical, intent(in) :: formatted
      integer, intent(out) :: unitwf

      ! We open the Fortran file
      unitwf = 99
      call f_open_file(unitwf,file=trim(filename),binary=.not. formatted)
  !!$    if (.not. formatted) then
  !!$       open(unit=unitwf,file=trim(filename),status='unknown',form="unformatted", iostat=i_stat)
  !!$    else
  !!$       open(unit=unitwf,file=trim(filename),status='unknown', iostat=i_stat)
  !!$    end if
  !!$    if (i_stat /= 0) then
  !!$       call io_warning("Cannot open file '" // trim(filename) // "'.")
  !!$       unitwf = -1
  !!$       return
  !!$    end if
    END SUBROUTINE io_open

    !eventually have the same header information as write_sparse?
    !differs in behaviour though as assumes all matrices are alrady allocated 
    ! to correct size - think more about whether this is the best approach...
    subroutine read_matrix_local(filename, iformat, nspin, ntmb, mat, comm)
      use module_bigdft_errors
      use module_types
      use f_utils
      use module_bigdft_arrays
      use bigpoly, only : has_ntpoly
      use bigpoly_io, only : bigpoly_read_to_dense
      use module_bigdft_profiling
      implicit none

      ! Calling arguments
      character(len=*),intent(in) :: filename
      integer, intent(in) :: iformat
      integer, intent(in) :: ntmb, nspin
      real(kind=8),dimension(ntmb,ntmb,nspin),intent(inout) :: mat
      integer, intent(in) :: comm

      ! Local variables
      integer :: iunit, dummy_int, ispin, iorb, jorb, ntmb_old, nspin_old
      logical :: binary
      logical :: sparse

      call f_routine(id='read_matrix_local')

      sparse = (1<=iformat .and. iformat<=9) .or. &
               (21<=iformat .and. iformat<=29)
      binary= (mod(iformat,10) /= WF_FORMAT_PLAIN)

      if (has_ntpoly .and. sparse) then
         if (nspin == 1) then
            call bigpoly_read_to_dense(mat(:,:,1), &
                trim(filename)//'_sparse.mtx', comm, binary)
         else
            call bigpoly_read_to_dense(mat(:,:,1), &
                trim(filename)//'_sparse_alpha.mtx', comm, binary)
            call bigpoly_read_to_dense(mat(:,:,2), &
                trim(filename)//'_sparse_beta.mtx', comm, binary)
         end if
      else if (.not. sparse) then ! built in read
         iunit = 99
         call f_open_file(iunit, file=trim(filename)//'.bin', binary=binary)

         !check heading information is consistent
         if (.not. binary) then
             read(iunit,*) nspin_old, ntmb_old
         else
             read(iunit) nspin_old, ntmb_old
         end if
         if (ntmb_old/=ntmb) then
            call f_err_throw("Number of tmbs incorrect in read_matrix_local", &
                err_name='BIGDFT_RUNTIME_ERROR')
         end if
         if (nspin_old/=nspin .and. nspin_old==2) then
            call f_err_throw("Number of spins incorrect in read_matrix_local", &
                err_name='BIGDFT_RUNTIME_ERROR')
         end if

         do ispin=1,nspin_old
            do iorb=1,ntmb
               do jorb=1,ntmb
                  if (.not. binary) then
                     read(iunit,*) dummy_int,dummy_int,mat(iorb,jorb,ispin)
                  else
                     read(iunit) dummy_int,dummy_int,mat(iorb,jorb,ispin)
                  end if
                  if (nspin_old == 1 .and. nspin == 2) then
                     mat(iorb,jorb,2) = mat(iorb,jorb,1)
                  end if
               end do
            end do
         end do

         call f_close(iunit)
      else
         call f_err_throw("Sparse matrix read only supported with ntpoly") 
      end if

      call f_release_routine()

    end subroutine read_matrix_local


    !> Write Hamiltonian, overlap and kernel matrices in tmb basis
    subroutine write_linear_matrices(iproc, nproc, comm, imethod_overlap, &
               filename, iformat, tmb, at, rxyz, norder_taylor, &
               calculate_onsite_overlap, write_cr2_matrices, write_matmul)
      use module_types
      use module_precisions
      use bigpoly, only : has_ntpoly
      use sparsematrix_base, only: sparsematrix_malloc_ptr, sparsematrix_malloc0_ptr, matrices_null, &
                                   DENSE_FULL, SPARSE_TASKGROUP, assignment(=), &
                                   deallocate_matrices
      use sparsematrix, only: uncompress_matrix2, transform_sparse_matrix, matrix_matrix_mult_wrapper
      use sparsematrix_io, only: write_sparse_matrix_metadata
      use matrix_operations, only: overlapPowerGeneral
      use at_domain, only: domain_geocode
      use module_bigdft_mpi
      use module_bigdft_output
      use module_bigdft_arrays
      use module_bigdft_profiling
      use sparsematrix_types
      implicit none
      integer, intent(in) :: iproc,nproc,comm,imethod_overlap,norder_taylor
      integer,intent(in) :: iformat !< 1: plain sparse, 11: plain dense, 21: plain both (later extend to other than plain formats...)
      character(len=*), intent(in) :: filename
      type(DFT_wavefunction), intent(inout) :: tmb
      type(atoms_data), intent(in) :: at
      real(gp),dimension(3,at%astruct%nat),intent(in) :: rxyz
      logical,intent(in) :: calculate_onsite_overlap, write_cr2_matrices, write_matmul
      !local variables
      logical :: binary
      integer :: ispin, iorb, jorb, iat, jat,unitm
      !!integer :: i_stat, i_all
      character(len=*),parameter :: subname='write_linear_matrices'
      logical :: write_sparse, write_dense
      type(matrices), dimension(1) :: sinvhmat, kxsmat
      real(kind=8),dimension(:),pointer :: ham_large, tmp_large, ovlp_large, kernel_large
      integer, dimension(1) :: power
      integer :: iato, jato

      call f_routine(id='write_linear_matrices')

      write_sparse = (1<=iformat .and. iformat<=9) .or. &
                     (21<=iformat .and.  iformat<=29)
      write_dense = (11<=iformat .and. iformat<=19) .or. &
                    (21<=iformat .and.  iformat<=29)

      ! Write out I/O parameters
      if (iproc == 0) then
         call yaml_mapping_open('Matrix IO')
         call yaml_map("write_sparse", write_sparse)
         call yaml_map("write_dense", write_dense)
         call yaml_map("has_ntpoly", has_ntpoly)
      end if

      unitm=99
      binary=(mod(iformat,10) /= WF_FORMAT_PLAIN)

      if (write_sparse) then
          call write_sparse_matrix_metadata(iproc, tmb%linmat%smat(2)%nfvctr, &
               at%astruct%nat, at%astruct%ntypes, at%astruct%units, &
               domain_geocode(at%astruct%dom), at%astruct%cell_dim, &
               at%astruct%shift, at%astruct%iatype, at%astruct%rxyz, &
               at%nzatom, at%nelpsp, at%astruct%atomnames, &
               tmb%orbs%onwhichatom, &
               trim(filename)//'sparsematrix_metadata.dat')
      end if

      call write_linear_selector(iproc, nproc, comm, binary, write_dense, &
               write_sparse, write_matmul, iformat, tmb%linmat%smat(2), &
               tmb%linmat%ham_, filename, "hamiltonian", &
               tmb%linmat%smat(2)%nspin)

      call write_linear_selector(iproc, nproc, comm, binary, write_dense, &
               write_sparse, write_matmul, iformat, tmb%linmat%smat(1), &
               tmb%linmat%ovrlp_, filename, "overlap", 1)

      call write_linear_selector(iproc, nproc, comm, binary, write_dense, &
               write_sparse, write_matmul, iformat, tmb%linmat%smat(3), &
               tmb%linmat%kernel_, filename, "density_kernel", &
               tmb%linmat%smat(3)%nspin)


      ! write the matrices which are useful for complexity reduction analysis
      if (write_cr2_matrices) then
          sinvhmat(1) = matrices_null()
          kxsmat(1) = matrices_null()
          sinvhmat(1)%matrix_compr = sparsematrix_malloc0_ptr( &
              tmb%linmat%smat(3), iaction=SPARSE_TASKGROUP, &
              id='sinvhmat%matrix_compr')
          kxsmat(1)%matrix_compr = sparsematrix_malloc0_ptr( &
              tmb%linmat%smat(3), iaction=SPARSE_TASKGROUP, &
              id='kxsmat%matrix_compr')

          ham_large = sparsematrix_malloc0_ptr(tmb%linmat%smat(3), &
              iaction=SPARSE_TASKGROUP, id='ham_large')
          ovlp_large = sparsematrix_malloc0_ptr(tmb%linmat%smat(3), &
              iaction=SPARSE_TASKGROUP, id='ovlp_large')
          kernel_large = sparsematrix_malloc0_ptr(tmb%linmat%smat(3), &
              iaction=SPARSE_TASKGROUP, id='kernel_large')
          tmp_large = sparsematrix_malloc0_ptr(tmb%linmat%smat(3), &
              iaction=SPARSE_TASKGROUP, id='tmp_large')

          call transform_sparse_matrix(iproc, tmb%linmat%smat(2), &
              tmb%linmat%smat(3), SPARSE_TASKGROUP, 'small_to_large', &
              smat_in=tmb%linmat%ham_%matrix_compr, lmat_out=ham_large)
          call transform_sparse_matrix(iproc, tmb%linmat%smat(1), &
              tmb%linmat%smat(3), SPARSE_TASKGROUP, 'small_to_large', &
              smat_in=tmb%linmat%ovrlp_%matrix_compr, lmat_out=ovlp_large)
          call transform_sparse_matrix(iproc, tmb%linmat%smat(3), &
              tmb%linmat%smat(3), SPARSE_TASKGROUP, 'small_to_large', &
              smat_in=tmb%linmat%kernel_%matrix_compr, lmat_out=kernel_large)

          ! calculate S^-1
          power=1
          call overlapPowerGeneral(iproc, nproc, bigdft_mpi%mpi_comm, &
               norder_taylor, 1, power, -1, &
               imode=1, ovrlp_smat=tmb%linmat%smat(1), &
               inv_ovrlp_smat=tmb%linmat%smat(3), &
               ovrlp_mat=tmb%linmat%ovrlp_, inv_ovrlp_mat=sinvhmat, &
               check_accur=.false., ice_obj=tmb%ice_obj)

          ! Calculate S^-1 * H
          call f_memcpy(src=sinvhmat(1)%matrix_compr,dest=tmp_large)
          call matrix_matrix_mult_wrapper(iproc, nproc, tmb%linmat%smat(3), &
                                          tmp_large, ham_large, &
                                          sinvhmat(1)%matrix_compr)

          ! Write out S^-1 * H
          call write_linear_selector(iproc, nproc, comm, binary, write_dense, &
               write_sparse, write_matmul, iformat, tmb%linmat%smat(3), &
               sinvhmat(1), filename, "sinvh", 1)

          ! Calculate K * S
          call matrix_matrix_mult_wrapper(iproc, nproc, tmb%linmat%smat(3), &
                                          kernel_large, ovlp_large, &
                                          kxsmat(1)%matrix_compr)

          ! Write out K * S
          call write_linear_selector(iproc, nproc, comm, binary, write_dense, &
               write_sparse, write_matmul, iformat, tmb%linmat%smat(3), &
               kxsmat(1), filename, "kxs", tmb%linmat%smat(3)%nspin)

          ! calculate S^-1/2 and S^1/2
          power=-2
          call overlapPowerGeneral(iproc, nproc, bigdft_mpi%mpi_comm, &
               norder_taylor, 1, power, -1, &
               imode=1, ovrlp_smat=tmb%linmat%smat(1), &
               inv_ovrlp_smat=tmb%linmat%smat(3), &
               ovrlp_mat=tmb%linmat%ovrlp_, inv_ovrlp_mat=sinvhmat, &
               check_accur=.false., ice_obj=tmb%ice_obj)
          call write_linear_selector(iproc, nproc, comm, binary, write_dense, &
               write_sparse, write_matmul, iformat, tmb%linmat%smat(3), &
               sinvhmat(1), filename, "sinv_sqrt", 1)

          power=2
          call overlapPowerGeneral(iproc, nproc, bigdft_mpi%mpi_comm, &
               norder_taylor, 1, power, -1, &
               imode=1, ovrlp_smat=tmb%linmat%smat(1), &
               inv_ovrlp_smat=tmb%linmat%smat(3), &
               ovrlp_mat=tmb%linmat%ovrlp_, inv_ovrlp_mat=sinvhmat, &
               check_accur=.false., ice_obj=tmb%ice_obj)
          call write_linear_selector(iproc, nproc, comm, binary, write_dense, &
               write_sparse, write_matmul, iformat, tmb%linmat%smat(3), &
               sinvhmat(1), filename, "s_sqrt", 1)

          ! Cleanup
          call f_free_ptr(ovlp_large)
          call f_free_ptr(kernel_large)
          call f_free_ptr(tmp_large)
          call f_free_ptr(ham_large)
          call deallocate_matrices(sinvhmat(1))
          call deallocate_matrices(kxsmat(1))
      end if

      ! Close the I/O block of the output file.
      if (iproc == 0) then
         call yaml_mapping_close()
      end if

      if (calculate_onsite_overlap) then
          ! calculate 'onsite' overlap matrix as well - needs double checking

          if (write_dense) then
              tmb%linmat%ovrlp_%matrix = sparsematrix_malloc_ptr(tmb%linmat%smat(1), iaction=DENSE_FULL, &
                                         id='tmb%linmat%ovrlp_%matrix')

              call tmb_overlap_onsite(iproc, nproc, imethod_overlap, at, tmb, rxyz)
              !call tmb_overlap_onsite_rotate(iproc, nproc, at, tmb, rxyz)

              if (iproc==0) then
                 !if(iformat == WF_FORMAT_PLAIN) then
                 call f_open_file(unitm,file=filename//'overlap_onsite.bin',&
                      binary=binary)
                 !else
                 !open(99, file=filename//'overlap_onsite.bin', status='unknown',form='unformatted')
                 !end if

                 if (.not. binary) then
                     write(unitm,'(a,2i10,a)') '#  ',tmb%linmat%smat(2)%nfvctr, at%astruct%nat, &
                         '    number of basis functions, number of atoms'
                 else
                     write(unitm) '#  ',tmb%linmat%smat(2)%nfvctr, at%astruct%nat, &
                         '    number of basis functions, number of atoms'
                 end if
                 do iat=1,at%astruct%nat
                     if (.not. binary) then
                         write(unitm,'(a,3es24.16)') '#  ',rxyz(1:3,iat)
                     else
                         write(unitm) '#  ',rxyz(1:3,iat)
                     end if
                 end do

                 do ispin=1,tmb%linmat%smat(3)%nspin
                    do iorb=1,tmb%linmat%smat(3)%nfvctr
                       iat=tmb%orbs%onwhichatom(iorb)
                       do jorb=1,tmb%linmat%smat(3)%nfvctr
                          jat=tmb%orbs%onwhichatom(jorb)
                          if (.not. binary) then
                             write(unitm,'(2(i6,1x),e19.12,2(1x,i6))') iorb,jorb,tmb%linmat%ovrlp_%matrix(iorb,jorb,ispin),iat,jat
                          else
                             write(unitm) iorb,jorb,tmb%linmat%ovrlp_%matrix(iorb,jorb,ispin),iat,jat
                          end if
                       end do
                    end do
                 end do

                 !DEBUG atom ordering
                 do ispin=1,tmb%linmat%smat(3)%nspin
                    do iato=1,at%astruct%nat
                       do iorb=1,tmb%linmat%smat(3)%nfvctr
                          iat=tmb%orbs%onwhichatom(iorb)
                          if (iat/=iato) cycle

                          do jato=1,at%astruct%nat
                             do jorb=1,tmb%linmat%smat(3)%nfvctr
                                jat=tmb%orbs%onwhichatom(jorb)
                                if (jat/=jato) cycle

                                write(27,'(2(i6,1x),e19.12,2(1x,i6),3(1x,f12.6))') &
                                     iorb,jorb,tmb%linmat%ovrlp_%matrix(iorb,jorb,ispin),&
                                     iat,jat,rxyz(1:3,jat)
                             end do
                          end do
                       end do
                    end do
                 end do
                 !DEBUG atom ordering
                 call f_close(unitm)
              end if

              call f_free_ptr(tmb%linmat%ovrlp_%matrix)

          end if

      end if


      call f_release_routine()

   end subroutine write_linear_matrices

    
    ! This wraps up the selection of matrix format for writing out to disk.
    subroutine write_linear_selector(iproc, nproc, comm, binary, &
               write_dense, write_sparse, write_matmul, iformat, &
               smat, mat, filename, matname, nspin)
      use sparsematrix_base, only : matrices, sparse_matrix
      use sparsematrix_io, only: write_dense_matrix, write_sparse_matrix
      use bigpoly_io, only : bigpoly_write_matrix
      use bigpoly, only : has_ntpoly
      use module_bigdft_arrays
      use module_bigdft_profiling
      implicit none
      integer, intent(in) :: iproc,nproc,comm
      logical, intent(in) :: binary, write_dense, write_sparse, write_matmul
      integer, intent(in) :: iformat
      type(sparse_matrix), intent(inout) :: smat
      type(matrices), intent(inout) :: mat
      character(len=*), intent(in) :: filename, matname
      integer, intent(in) :: nspin
      character(len=128) :: sparse_format

      call f_routine(id='write_linear_selector')

      if (write_dense) then
         call write_dense_matrix(iproc, nproc, comm, smat, mat, &
              uncompress=.true., &
              filename=trim(filename)//trim(matname)//'.bin', &
              binary=binary)
      end if

      if (write_sparse) then
          if (.not. has_ntpoly) then
              call get_sparse_matrix_format(iformat, sparse_format)
              call write_sparse_matrix(sparse_format, iproc, nproc, &
                   comm, smat, mat, &
                   trim(filename)//trim(matname)//'_sparse', write_matmul)
          else
              call bigpoly_write_matrix(iproc, nproc, comm, smat, mat, &
                   trim(filename)//trim(matname)//'_sparse', binary, nspin)
          end if
      end if

      call f_release_routine()

    end subroutine


    subroutine writeLinearCoefficients(unitwf,useFormattedOutput,nat,rxyz,&
               ntmb,nelec,nfvctr,nspin,coeff,eval)
      use module_precisions
      use module_bigdft_mpi
      use module_bigdft_output
      implicit none
      logical, intent(in) :: useFormattedOutput
      integer, intent(in) :: unitwf,nat,ntmb,nelec,nfvctr,nspin
      real(wp), dimension(nfvctr,nfvctr,nspin), intent(in) :: coeff
      real(wp), dimension(ntmb), intent(in) :: eval
      real(gp), dimension(3,nat), intent(in) :: rxyz
      !local variables
      integer :: iat,i,j,iorb,is,ispin
      real(wp) :: tt

      ! Write the Header
      if (useFormattedOutput) then
         write(unitwf,*) nfvctr, nelec, nspin
         write(unitwf,*) nat
         do iat=1,nat
           write(unitwf,'(3(1x,e24.17))') (rxyz(j,iat),j=1,3)
         enddo
         ispin=1
         do iorb=1,ntmb
           if (iorb<=nfvctr) then
             ispin=1
           else
             ispin=2
           end if
           write(unitwf,*) ispin,iorb-(ispin-1)*nfvctr,eval(iorb)
         enddo
      else
         write(unitwf) nfvctr, nelec, nspin
         write(unitwf) nat
         do iat=1,nat
         write(unitwf) (rxyz(j,iat),j=1,3)
         enddo
         ispin=1
         do iorb=1,ntmb
           if (iorb<=nfvctr) then
             ispin=1
           else
             ispin=2
           end if
         write(unitwf) ispin,iorb-(ispin-1)*nfvctr,eval(iorb)
         enddo
      end if

      ! Now write the coefficients
      do is = 1, nspin
        do i = 1, nfvctr
           ! first element always positive, for consistency when using for transfer integrals
           ! unless 1st element below some threshold, in which case first significant element
           do j=1,nfvctr
              if (abs(coeff(j,i,is))>1.0e-1) then
                 if (coeff(j,i,is)<0.0_gp) call dscal(nfvctr,-1.0_gp,coeff(1,i,is),1)
                 exit
              end if
           end do
           !if (j==ntmb+1)print*,'Error finding significant coefficient, coefficients not scaled to have +ve first element'

           do j = 1,nfvctr
                tt = coeff(j,i,is)
                if (useFormattedOutput) then
                   write(unitwf,'(3(i6,1x),e19.12)') is,i,j,tt
                else
                   write(unitwf) is,i,j,tt
                end if
           end do
        end do
      end do
      if (get_verbose_level() >= 2 .and. bigdft_mpi%iproc==0) call yaml_map('Wavefunction coefficients written',.true.)

    END SUBROUTINE writeLinearCoefficients



    subroutine write_partial_charges(atoms, charge_per_atom, write_gnuplot)
      use module_types
      use module_bigdft_output
      ! Calling arguments
      type(atoms_data),intent(in) :: atoms
      real(kind=8),dimension(atoms%astruct%nat),intent(in) :: charge_per_atom
      logical,intent(in) :: write_gnuplot
      ! Local variables
      integer :: iat, itypes, iitype, nntype, intype, iunit
      real(kind=8) :: total_charge, total_net_charge, frac_charge, range_min, range_max
      character(len=20) :: atomname, colorname
      real(kind=8),dimension(2) :: charges
      character(len=128) :: output
      character(len=2) :: backslash
      integer,parameter :: ncolors = 12 !7
      !character(len=20),dimension(ncolors),parameter :: colors=(/'violet', &
      !                                                           'blue  ', &
      !                                                           'cyan  ', &
      !                                                           'green ', &
      !                                                           'yellow', &
      !                                                           'orange', &
      !                                                           'red   '/)
      ! Presumably well suited colorschemes from colorbrewer2.org
      character(len=20),dimension(ncolors),parameter :: colors=(/'#a6cee3', &
                                                                 '#1f78b4', &
                                                                 '#b2df8a', &
                                                                 '#33a02c', &
                                                                 '#fb9a99', &
                                                                 '#e31a1c', &
                                                                 '#fdbf6f', &
                                                                 '#ff7f00', &
                                                                 '#cab2d6', &
                                                                 '#6a3d9a', &
                                                                 '#ffff99', &
                                                                 '#b15928'/)

      call yaml_sequence_open('Charge analysis (charge / net charge)')
      total_charge=0.d0
      total_net_charge=0.d0
      do iat=1,atoms%astruct%nat
          call yaml_sequence(advance='no')
          call yaml_mapping_open(flow=.true.)
          atomname=atoms%astruct%atomnames(atoms%astruct%iatype(iat))
          charges(1)=-charge_per_atom(iat)
          charges(2)=-(charge_per_atom(iat)-real(atoms%nelpsp(atoms%astruct%iatype(iat)),kind=8))
          total_charge = total_charge + charges(1)
          total_net_charge = total_net_charge + charges(2)
          call yaml_map(trim(atomname),charges,fmt='(1es20.12)')
          call yaml_mapping_close(advance='no')
          call yaml_comment(trim(yaml_toa(iat,fmt='(i4.4)')))
      end do
      call yaml_sequence(advance='no')
      call yaml_map('total charge',total_charge,fmt='(es16.8)')
      call yaml_sequence(advance='no')
      call yaml_map('total net charge',total_net_charge,fmt='(es16.8)')
      call yaml_sequence_close()

      if (write_gnuplot) then
          output='chargeanalysis.gp'
          call yaml_map('output file',trim(output))
          iunit=100
          call f_open_file(iunit, file=trim(output), binary=.false.)
          write(iunit,'(a)') '# plot the fractional charge as a normalized sum of Gaussians'
          write(iunit,'(a)') 'set samples 1000'
          range_min = minval(-(charge_per_atom(:)-real(atoms%nelpsp(atoms%astruct%iatype(:)),kind=8))) - 0.1d0
          range_max = maxval(-(charge_per_atom(:)-real(atoms%nelpsp(atoms%astruct%iatype(:)),kind=8))) + 0.1d0
          write(iunit,'(a,2(es12.5,a))') 'set xrange[',range_min,':',range_max,']'
          write(iunit,'(a)') 'sigma=0.005'
          write(backslash,'(a)') '\ '
          do itypes=1,atoms%astruct%ntypes
              nntype = 0
              do iat=1,atoms%astruct%nat
                  iitype = (atoms%astruct%iatype(iat))
                  if (iitype==itypes) then
                      nntype = nntype + 1
                  end if
              end do
              write(iunit,'(a,i0,a,i0,2a)') 'f',itypes,'(x) = 1/',nntype,'.0*( '//trim(backslash)
              intype = 0
              do iat=1,atoms%astruct%nat
                  iitype = (atoms%astruct%iatype(iat))
                  if (iitype==itypes) then
                      intype = intype + 1
                      frac_charge = -(charge_per_atom(iat)-real(atoms%nelpsp(atoms%astruct%iatype(iat)),kind=8))
                      if (intype<nntype) then
                          write(iunit,'(a,es16.9,a)') '  1.0*exp(-(x-',frac_charge,')**2/(2*sigma**2)) + '//trim(backslash)
                      else
                          write(iunit,'(a,es16.9,a)') '  1.0*exp(-(x-',frac_charge,')**2/(2*sigma**2)))'
                      end if
                  end if
              end do
              atomname=atoms%astruct%atomnames(itypes)
              if (itypes<ncolors) then
                  colorname = colors(itypes)
              else
                  colorname = 'color'
              end if
              if (itypes==1) then
                  write(iunit,'(a,i0,5a)') "plot f",itypes,"(x) lc rgb '",trim(colorname), &
                      "' lt 1 lw 2 w l title '",trim(atomname),"'"
              else
                  write(iunit,'(a,i0,5a)') "replot f",itypes,"(x) lc rgb '",trim(colorname), &
                      "' lt 1 lw 2 w l title '",trim(atomname),"'"
              end if
          end do
      end if
    end subroutine write_partial_charges


    subroutine plot_density(iproc,filename,at,rxyz,kernel,nspin,rho,ixyz0)
      use module_precisions
      use Poisson_Solver, only: coulomb_operator
      use IObox, only: dump_field
      use PSbox, only: PS_gather
      use module_atoms, only: atoms_data
      use module_bigdft_arrays
      use module_bigdft_profiling
      implicit none
      integer, intent(in) :: iproc,nspin
      type(atoms_data), intent(in) :: at
      type(coulomb_operator), intent(in) :: kernel
      character(len=*), intent(in) :: filename
      real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz
      real(dp), dimension(*) :: rho !< intent(in)
      integer, dimension(3), intent(in), optional ::  ixyz0 !< points that have to be plot as lines
      !local variables
      real(dp), dimension(:,:,:,:), allocatable :: pot_ion

      call f_routine(id='plot_density')

      pot_ion = &
           f_malloc([kernel%mesh%ndims(1),kernel%mesh%ndims(2),kernel%mesh%ndims(3), nspin],id='pot_ion')

      call PS_gather(src=rho,dest=pot_ion,grid=kernel%grid,&
                     mpi_env=kernel%mpi_env,nsrc=nspin)

      if (iproc==0) then
         call dump_field(filename,kernel%mesh,nspin,pot_ion,&
              rxyz,at%astruct%iatype,at%nzatom,at%nelpsp,ixyz0=ixyz0)
      end if

      call f_free(pot_ion)

      call f_release_routine()

    END SUBROUTINE plot_density


    subroutine plot_locreg_grids(iproc, nspin, orbitalNumber, llr, glr, atoms, rxyz, hx, hy, hz)
      use module_types
      use module_precisions
      use locregs
      use liborbs_functions
      use module_atoms, only: write_xyz_bc
      use at_domain, only: domain_geocode
      use f_utils
      use module_bigdft_arrays
      use module_bigdft_profiling
      use compression
      implicit none

      ! Calling arguments
      integer, intent(in) :: iproc, nspin, orbitalNumber
      type(locreg_descriptors), intent(in) :: llr, glr
      type(atoms_data), intent(in) ::atoms
      real(kind=8), dimension(3,atoms%astruct%nat), intent(in) :: rxyz
      real(kind=8), intent(in) :: hx, hy, hz

      ! Local variables
      integer :: iseg, jj, i3, i2, i0, i1, i, ishift, iat, jjj, iunit
      character(len=10) :: num
      character(len=20) :: filename
      type(wvf_manager) :: manager
      type(wvf_daub_view) :: phi
      real(kind=8), dimension(:), allocatable :: lphi

      call f_routine(id='plot_locreg_grids')

      manager = wvf_manager_new()

      lphi = f_malloc(array_dim(llr),id='lphi')
      lphi = 1.d0
      phi = wvf_view_on_daub_glr(manager, glr, llr, lphi(1))
      call f_free(lphi)

      write(num,'(i6.6)') orbitalNumber
      filename='grid_'//trim(num)

      !open(unit=2000+iproc,file=trim(filename)//'.xyz',status='unknown')
      iunit = 2000+iproc
      call f_open_file(iunit, file=trim(filename)//'.xyz', binary=.false.)

      !write(2000+iproc,*) llr%wfd%nvctr_c+llr%wfd%nvctr_f+atoms%astruct%nat,' atomic'
      write(2000+iproc,*) lr_n_c_points(glr)+lr_n_f_points(glr)+ &
           lr_n_c_points(llr)+lr_n_f_points(llr)+atoms%astruct%nat,' atomic'
      call write_xyz_bc(2000+iproc,domain_geocode(atoms%astruct%dom),1.0_gp,atoms%astruct%cell_dim)

!!$      if (atoms%astruct%geocode=='F') then
!!$         write(2000+iproc,*)'complete simulation grid with low and high resolution points'
!!$      else if (atoms%astruct%geocode =='S') then
!!$         write(2000+iproc,'(a,2x,3(1x,1pe24.17))')'surface',atoms%astruct%cell_dim(1),atoms%astruct%cell_dim(2),&
!!$              atoms%astruct%cell_dim(3)
!!$      else if (atoms%astruct%geocode =='P') then
!!$         write(2000+iproc,'(a,2x,3(1x,1pe24.17))')'periodic',atoms%astruct%cell_dim(1),atoms%astruct%cell_dim(2),&
!!$              atoms%astruct%cell_dim(3)
!!$      else if (atoms%astruct%geocode =='W') then
!!$         call f_err_throw("Wires bc has to be implemented here", &
!!$              err_name='BIGDFT_RUNTIME_ERROR')
!!$      end if

      do iat=1,atoms%astruct%nat
        write(2000+iproc,'(a6,2x,3(1x,e12.5),3x)') trim(atoms%astruct%atomnames(atoms%astruct%iatype(iat))),&
             rxyz(1,iat),rxyz(2,iat),rxyz(3,iat)
      end do


      jjj=0
      do iseg=1,glr%wfd%nseg_c
         call local_segment_to_grid(glr%wfd, iseg, glr%mesh_coarse%ndims(1), &
              glr%mesh_coarse%ndims(1)*glr%mesh_coarse%ndims(2), i0, i1, i2, i3, jj)
         do i=i0,i1
             jjj=jjj+1
             if(phi%c%mem(jjj,1)==1.d0) write(2000+iproc,'(a4,2x,3(1x,e10.3))') '  lg ',&
                  real(i,kind=8)*hx,real(i2,kind=8)*hy,real(i3,kind=8)*hz
             write(2000+iproc,'(a4,2x,3(1x,e10.3))') '  g ',real(i,kind=8)*hx,&
                  real(i2,kind=8)*hy,real(i3,kind=8)*hz
         enddo
      enddo

      ishift=glr%wfd%nseg_c
      ! fine part
      do iseg=1,glr%wfd%nseg_f
         call local_segment_to_grid(glr%wfd, ishift+iseg, glr%mesh_coarse%ndims(1), &
              glr%mesh_coarse%ndims(1)*glr%mesh_coarse%ndims(2), i0, i1, i2, i3, jj)
         do i=i0,i1
            jjj=jjj+1
            if(phi%c%mem(jjj,1)==1.d0) write(2000+iproc,'(a4,2x,3(1x,e10.3))') &
                '  lG ',real(i,kind=8)*hx,real(i2,kind=8)*hy,real(i3,kind=8)*hz
            write(2000+iproc,'(a4,2x,3(1x,e10.3))') &
                '  G ',real(i,kind=8)*hx,real(i2,kind=8)*hy,real(i3,kind=8)*hz
            jjj=jjj+6
         enddo
      enddo

      call wvf_deallocate_manager(manager)

      !close(unit=2000+iproc)
      call f_close(iunit)

      call f_release_routine()

    end subroutine plot_locreg_grids

   subroutine get_sparse_matrix_format(iformat, sparse_format)
     use module_bigdft_errors
     implicit none
     integer,intent(in) :: iformat
     character(len=*),intent(out) :: sparse_format

     if (mod(iformat,10) == MATRIX_FORMAT_PLAIN) then
         sparse_format = 'serial_text'
     else if (mod(iformat,10) == MATRIX_FORMAT_MPI_NATIVE) then
         sparse_format = 'parallel_mpi-native'
     else
         call f_err_throw("unsupported value for 'iformat'")
     end if
   end subroutine get_sparse_matrix_format


   subroutine yaml_sequence_lr(lr, label, unit, export, lbin)
     use yaml_output
     use locregs
     use dictionaries
     implicit none
     type(locreg_descriptors), intent(in) :: lr
     character(len = *), intent(in), optional :: label
     integer, intent(in), optional :: unit
     character(len = *), intent(in), optional :: export
     logical, intent(in), optional :: lbin

     type(dictionary), pointer :: dlr

     call dict_init(dlr)
     call lr_merge_to_dict(dlr, lr, export, lbin)
     call yaml_sequence(advance = "NO", unit = unit)
     call yaml_mapping_open(label = label, unit = unit)
     call yaml_dict_dump(dlr, unit = unit)
     call yaml_mapping_close(unit = unit)
     call dict_free(dlr)
   end subroutine yaml_sequence_lr

   subroutine writeheader(descr, lbin, filename, glr, orbs, &
        exportGlr)
     use module_precisions
     use locregs
     use module_types
     use yaml_output
     use yaml_strings
     use module_atoms
     use dictionaries
     use f_utils
     use at_domain
     use liborbs_io, only: io_descr_funcs_to_dict
     use module_bigdft_arrays
     implicit none
     type(io_descriptor), intent(in) :: descr
     logical, intent(in) :: lbin
     character(len = *), intent(in) :: filename
     type(locreg_descriptors), intent(in) :: glr
     type(orbitals_data), intent(in) :: orbs
     character(len = *), intent(in), optional :: exportGlr

     logical :: withLocregs, withKpts
     integer :: iunit, ierr, idefault
     integer :: ilr, ikpt, nlr
     type(dictionary), pointer :: at, func
     logical, dimension(:), allocatable :: lrs

     call yaml_get_default_stream(idefault)

     call f_delete_file(filename // ".yaml")
     
     iunit = 99
     call yaml_set_stream(unit = iunit, filename = filename // ".yaml")
     call yaml_new_document(iunit)

     call dict_init(at)
     call astruct_merge_to_dict(at, descr%astruct, descr%astruct%rxyz)
     call yaml_map(ATOM_STRUCT, at, label = TAG_ASTRUCT, unit = iunit)
     call dict_free(at)

     withLocregs = any(orbs%inwhichlocreg > 1)
     withKpts = orbs%nkpts > 1 .or. any(orbs%kpts(:, 1) /= 0._gp)

     at => io_descr_funcs_to_dict(descr%parent)

     call yaml_sequence_open(LREGS, unit = iunit)
     call yaml_sequence_lr(glr, label = TAG_GLOBAL, unit = iunit, &
          export = exportGlr, lbin = lbin)

     nlr = 0
     func => dict_iter(at)
     do while (associated(func))
        if (LOCREG .in. func) then
           if (IND .in. func // LOCREG) then
              ilr = func // LOCREG // IND
              nlr = max(nlr, ilr)
           end if
        end if
        if (.not. withLocregs) then
           call dict_remove(func, LOCREG)
        end if
        if (.not. withKpts) then
           call dict_remove(func, KPT)
        end if
        func => dict_next(func)
     end do

     if (nlr > 0) then
        lrs = f_malloc0(nlr, id = "lrs")
        func => dict_iter(at)
        do while (associated(func))
           if (LOCREG .in. func) then
              if (IND .in. func // LOCREG) then
                 ilr = func // LOCREG // IND
                 if (.not. lrs(ilr)) then
                    call dict_remove(func // LOCREG, IND)
                    call yaml_sequence(advance = "NO", unit = iunit)
                    call yaml_mapping_open(label = "LOCREG" // &
                         adjustl(yaml_toa(ilr, fmt = "(I4.4)")), unit = iunit)
                    call yaml_dict_dump(func // LOCREG, unit = iunit)
                    call yaml_mapping_close(unit = iunit)
                    lrs(ilr) = .true.
                 end if
                 call set(func // LOCREG, "*LOCREG" // &
                      adjustl(yaml_toa(ilr, fmt = "(I4.4)")))
              end if
           end if
           func => dict_next(func)
        end do
        call f_free(lrs)
     end if
     call yaml_sequence_close(unit = iunit)

     if (withKpts) then
        call yaml_sequence_open(KPTS, unit = iunit)
        do ikpt = 1, orbs%nkpts
           call yaml_sequence(advance = "NO", unit = iunit)
           call yaml_mapping_open(label = "KPT" // adjustl(yaml_toa(ikpt, fmt = "(I3.3)")), unit = iunit)
           call yaml_map("coordinates", orbs%kpts(:, ikpt), unit = iunit)
           call yaml_map("weight", orbs%kwgts(ikpt), unit = iunit)
           call yaml_map(IND, ikpt, unit = iunit)
           call yaml_mapping_close(unit = iunit)
        end do
        call yaml_sequence_close(unit = iunit)
     end if

     call yaml_map(FUNCS, at, unit = iunit)

     call dict_free(at)

     call yaml_close_stream(iunit, ierr)

     call yaml_set_default_stream(idefault, ierr)
   end subroutine writeheader

   subroutine writerhoij(unitf, lbin, nat, pawrhoij)
     use m_pawrhoij
     implicit none
     integer, intent(in) :: unitf, nat
     logical, intent(in) :: lbin
     type(pawrhoij_type), dimension(nat), intent(in) :: pawrhoij

     integer :: i

     if (lbin) then
        do i = 1, nat
           write(unitf) i, size(pawrhoij(i)%rhoijp, 1), size(pawrhoij(i)%rhoijp, 2)
           write(unitf) pawrhoij(i)%rhoijp
        end do
     else
        do i = 1, nat
           write(unitf, *) i, size(pawrhoij(i)%rhoijp, 1), size(pawrhoij(i)%rhoijp, 2)
           write(unitf, "(4(1x,e17.10))") pawrhoij(i)%rhoijp
        end do
     end if
   END SUBROUTINE writerhoij

   subroutine io_descr_add_from_old_files(descr, filenames, lbin, parse, lstat, gdom)
     use module_precisions
     use dictionaries
     use f_utils
     use at_domain
     use locregs
     use f_enums
     use f_precisions, only: UNINITIALIZED
     use liborbs_io, only: io_descr_add_func
     implicit none
     type(io_descriptor), intent(inout) :: descr
     character(max_field_length), dimension(:), intent(in) :: filenames
     logical, intent(in) :: lbin, parse
     logical, intent(out) :: lstat
     type(domain), intent(in), optional :: gdom

     integer :: ispinor, iunit
     type(locreg_descriptors) :: lr
     character(len =256) :: error
     real(wp) :: eig

     lstat=.true.
     if (parse) then
        eig = UNINITIALIZED(eig)
        do ispinor = 1, size(filenames)
           call f_file_exists(trim(descr%parent%path) // trim(filenames(ispinor)), exists = lstat)
           if (.not. lstat) return
           iunit = 99
           call f_open_file(iunit, file = trim(descr%parent%path) // trim(filenames(ispinor)), binary = lbin)
           if (ispinor == 1) then
              call io_read_descr(iunit, .not. lbin, lstat, error, &
                   lr = lr, rxyz = descr%astruct%rxyz, gdom = gdom, eval = eig)
           else
              call io_read_descr(iunit, .not. lbin, lstat, error)
           end if
           call f_close(iunit)
           if (.not. lstat) return
        end do
        call io_descr_add_func(descr%parent, filenames, .false., eigenvalue = eig, lr = lr, withLegacyHeader = .true.)
        call deallocate_locreg_descriptors(lr)
     else
        call io_descr_add_func(descr%parent, filenames, .false., withLegacyHeader = .true.)
     end if
   end subroutine io_descr_add_from_old_files

   function io_descr_from_old_file(lbin, filename, nat) result(descr)
     use module_precisions
     use module_bigdft_arrays
     use module_input_keys, only: wave_format_from_filename
     use locregs
     use f_utils
     use dictionaries
     use at_domain
     use liborbs_io, only: io_descr_invalidate
     implicit none
     logical, intent(in) :: lbin
     character(len = *), intent(in) :: filename
     integer, intent(in) :: nat
     type(io_descriptor) :: descr

     character(max_field_length), dimension(1) :: filenames
     logical :: lstat

     descr = io_init_description(filename)

     ! Backward compatibility with one old wavefunction file.
     descr%astruct%nat = nat
     descr%astruct%rxyz = f_malloc_ptr((/ 3, nat /), id = "rxyz")

     filenames(1) = filename
     call io_descr_add_from_old_files(descr, filenames, lbin, .true., lstat)
     if (.not. lstat) call io_descr_invalidate(descr%parent)
   end function io_descr_from_old_file

   function io_descr_from_old_files(lbin, filename, orbs, nat, gdom) result(descr)
     use module_precisions
     use module_bigdft_arrays
     use locregs
     use f_utils
     use at_domain
     use dictionaries
     use module_types
     use liborbs_io, only: io_descr_invalidate
     implicit none
     logical, intent(in) :: lbin
     character(len = *), intent(in) :: filename
     type(io_descriptor) :: descr
     type(orbitals_data), intent(in) :: orbs
     integer, intent(in), optional :: nat
     type(domain), intent(in), optional :: gdom

     logical :: parse, lstat
     integer :: islash, ikpt, iorb, ispinor
     character(max_field_length), dimension(4) :: filenames

     descr = io_init_description(filename)

     if (present(nat)) then
        descr%astruct%nat = nat
        descr%astruct%rxyz = f_malloc_ptr((/ 3, nat /), id = "rxyz")
     end if

     islash = index(filename, "/", back = .true.)
     do ikpt = 1, orbs%nkpts
        do iorb = 1, orbs%norb
           do ispinor = 1, orbs%nspinor
              call filename_of_iorb(lbin, filename(islash+1:), &
                   orbs, ikpt, iorb, ispinor, filenames(ispinor))
           end do
           parse = iorb + (ikpt-1)*orbs%norb > orbs%isorb .and. &
                iorb + (ikpt-1)*orbs%norb <= orbs%isorb + orbs%norbp
           call io_descr_add_from_old_files(descr, filenames(1:orbs%nspinor), lbin, &
                parse, lstat, gdom)
           if (.not. lstat) then
              call io_descr_invalidate(descr%parent)
              return
           end if
        end do
     end do
   end function io_descr_from_old_files

   function io_read_description(filename, nat, orbs, gdom, mpi_env) result(descr)
     use module_bigdft_mpi
     use module_types
     use at_domain
     use f_utils
     use public_enums
     use dictionaries
     use liborbs_io, only: io_descr_invalidate
     implicit none
     character(len = *), intent(in) :: filename
     integer, intent(in), optional :: nat
     type(orbitals_data), intent(in), optional :: orbs
     type(domain), intent(in), optional :: gdom
     type(mpi_environment), intent(in), optional :: mpi_env
     type(io_descriptor) :: descr

     logical :: ok, test
     integer :: iformat
     character(max_field_length) :: out, ftest

     iformat = WF_FORMAT_NONE
     out = filename
     test = .true.
     if (present(mpi_env)) test = mpi_env%iproc == 0
     if (test) then
        if (index(filename, ".yaml", back = .true.) == (len(filename) - 4)) then
           call f_file_exists(file = filename, exists = ok)
        else
           call f_file_exists(file = filename // ".yaml", exists = ok)
           if (ok) out = filename // ".yaml"
        end if
        if (ok) then
           iformat = WF_FORMAT_YAML
        else
           if (index(filename, ".etsf", back = .true.) == (len(filename) - 4)) then
              call f_file_exists(file = filename, exists = ok)
           else
              call f_file_exists(file = filename // ".etsf", exists = ok)
              if (ok) out = filename // ".etsf"
           end if
           if (ok) then
              iformat = WF_FORMAT_ETSF
           else
              call f_file_exists(file = filename, exists = ok)
              if (ok) then
                 if (index(filename, ".bin") > 0) then
                    iformat = WF_FORMAT_BINARY
                 else
                    iformat = WF_FORMAT_PLAIN
                 end if
              else if (present(orbs)) then
                 call filename_of_iorb(.true., filename, orbs, 1, 1, 1, ftest)
                 call f_file_exists(file = trim(ftest), exists = ok)
                 if (ok) then
                    iformat = WF_FORMAT_BINARY
                 else
                    call filename_of_iorb(.false., filename, orbs, 1, 1, 1, ftest)
                    call f_file_exists(file = trim(ftest), exists = ok)
                    if (ok) iformat = WF_FORMAT_PLAIN
                 end if
              end if
           end if
        end if
     end if
     if (present(mpi_env)) call fmpi_bcast(iformat, 1, 0, mpi_env%mpi_comm)
     select case (iformat)
     case (WF_FORMAT_YAML)
        descr = io_yaml_description(trim(out), bigdft_mpi)
     case (WF_FORMAT_ETSF)
        descr = io_etsf_description(trim(out))
     case (WF_FORMAT_NONE)
        ! File not found, return an invalid descriptor.
        descr = io_init_description(trim(out))
        call io_descr_invalidate(descr%parent)
     case default
        if (present(orbs)) then
           descr = io_descr_from_old_files(iformat == WF_FORMAT_BINARY, trim(out), &
                orbs, nat, gdom)
        else if (present(nat)) then
           descr = io_descr_from_old_file(iformat == WF_FORMAT_BINARY, filename, nat)
        end if
     end select
   end function io_read_description

   function io_init_description(filename, allwaves, tags) result(descr)
     use dictionaries
     use module_atoms
     use liborbs_io, only: io_descriptor_from_dict
     implicit none
     character(len = *), intent(in) :: filename
     type(dictionary), pointer, optional :: allwaves, tags
     type(io_descriptor) :: descr

     integer :: islash
     type(dictionary), pointer :: w, glr

     nullify(w)
     if (present(allwaves)) w => allwaves
     nullify(glr)
     if (present(tags)) then
       if (dict_isdict(tags // TAG_GLOBAL)) then
          glr => tags // TAG_GLOBAL
       end if
     end if
     
     islash = index(filename, "/", back = .true.)
     if (islash > 0) then
        descr%parent = io_descriptor_from_dict(w, glr, filename(1:islash))
     else
        descr%parent = io_descriptor_from_dict(w, glr)
     end if

     descr%astruct = atomic_structure_null()
   end function io_init_description

   function io_yaml_description(filename, mpi_env) result(descr)
     use f_precisions, only: UNINITIALIZED
     use module_precisions
     use yaml_parse
     use dictionaries
     use module_bigdft_arrays
     use module_bigdft_mpi
     use module_atoms
     use at_domain
     use locregs
     use f_enums
     use f_utils
     use liborbs_io, only: io_descriptor_from_dict
     implicit none
     character(len = *), intent(in) :: filename
     type(mpi_environment), intent(in), optional :: mpi_env
     type(io_descriptor) :: descr

     type(dictionary), pointer :: allwaves, tags, glr, w
     integer :: islash

     nullify(allwaves)
     call dict_init(tags)
     call set(tags // TAG_GLOBAL, "")
     call set(tags // TAG_ASTRUCT, "")
     call yaml_parse_from_file(allwaves, filename, tags, mpi_env)

     nullify(glr)
     if (dict_isdict(tags // TAG_GLOBAL)) then
        glr => tags // TAG_GLOBAL
     end if
     nullify(w)
     if (associated(allwaves)) then
        if (dict_islist(allwaves)) then
           if (FUNCS .in. allwaves // 0) then
              w => allwaves // 0 // FUNCS
           end if
        end if
     end if

     islash = index(filename, "/", back = .true.)
     if (islash > 0) then
        descr%parent = io_descriptor_from_dict(w, glr, filename(1:islash))
     else
        descr%parent = io_descriptor_from_dict(w, glr)
     end if

     call dict_free(tags)

     descr%astruct = atomic_structure_null()
     if (associated(allwaves)) then
        if (ATOM_STRUCT .in. allwaves // 0) then
           call astruct_set_from_dict(allwaves // 0 // ATOM_STRUCT, descr%astruct)
        end if

        call dict_free(allwaves)
     end if
   end function io_yaml_description

   function io_etsf_description(filename) result(descr)
     use module_precisions
     use module_bigdft_arrays
     use module_bigdft_mpi
     use at_domain
     use module_atoms
     use locregs
     use f_enums
     use dictionaries
     use liborbs_io, only: io_descr_invalidate
     implicit none
     character(len = *), intent(in) :: filename
     type(io_descriptor) :: descr

     descr = io_init_description(filename)
     call io_descr_invalidate(descr%parent)
     ! TODO: implement ETSF mapping to our internal dictionary
   end function io_etsf_description

   subroutine io_deallocate_descriptor(descr)
     use module_atoms
     use liborbs_io, only: liborbs_io_deallocate_descriptor => io_deallocate_descriptor
     implicit none
     type(io_descriptor), intent(inout) :: descr

     call liborbs_io_deallocate_descriptor(descr%parent)
     call deallocate_atomic_structure(descr%astruct)
   end subroutine io_deallocate_descriptor

   function io_descr_files_exist(descr, mpi_env) result(ok)
     use module_bigdft_mpi
     use liborbs_io, only: liborbs_io_descr_files_exist => io_descr_files_exist
     implicit none
     type(io_descriptor), intent(in) :: descr
     type(mpi_environment), intent(in), optional :: mpi_env
     logical :: ok, test

     integer :: iok

     test = .true.
     if (present(mpi_env)) test = mpi_env%iproc == 0

     ok = .false.
     if (test) then
        ok = liborbs_io_descr_files_exist(descr%parent)
     end if

     if (present(mpi_env)) then
        iok = 0
        if (ok) iok = 1
        call fmpi_bcast(iok, 1, 0, mpi_env%mpi_comm)
        ok = .true.
        if (iok == 0) ok = .false.
     end if
   end function io_descr_files_exist

   function io_descr_rxyz(descr) result(rxyz)
     use module_precisions
     use dictionaries
     use module_bigdft_arrays
     implicit none
     type(io_descriptor), intent(in) :: descr
     real(gp), dimension(:,:), pointer :: rxyz

     if (associated(descr%astruct%rxyz)) then
        rxyz = f_malloc_ptr(descr%astruct%rxyz, id = "rxyz")
     else
        nullify(rxyz)
     end if
   end function io_descr_rxyz

   subroutine readrhoij(unitf, lbin, nat, pawrhoij)
     use m_pawrhoij
     use dictionaries
     implicit none
     integer, intent(in) :: unitf, nat
     logical, intent(in) :: lbin
     type(pawrhoij_type), dimension(nat), intent(inout) :: pawrhoij

     integer :: i, iat, s1, s2

     if (lbin) then
        do i = 1, nat
           read(unitf) iat, s1, s2
           if (f_err_raise(s1 /= size(pawrhoij(i)%rhoijp, 1), &
                & 'wrong read size for rhoij', &
                & err_name='BIGDFT_RUNTIME_ERROR')) return
           if (f_err_raise(s2 /= size(pawrhoij(i)%rhoijp, 2), &
                & 'wrong read size for rhoij', &
                & err_name='BIGDFT_RUNTIME_ERROR')) return
           if (f_err_raise(i < 1 .or. i > nat, &
                & 'wrong atomic id', &
                & err_name='BIGDFT_RUNTIME_ERROR')) return
           read(unitf) pawrhoij(iat)%rhoijp
        end do
     else
        do i = 1, nat
           read(unitf, *) iat, s1, s2
           if (f_err_raise(s1 /= size(pawrhoij(i)%rhoijp, 1), &
                & 'wrong read size for rhoij', &
                & err_name='BIGDFT_RUNTIME_ERROR')) return
           if (f_err_raise(s2 /= size(pawrhoij(i)%rhoijp, 2), &
                & 'wrong read size for rhoij', &
                & err_name='BIGDFT_RUNTIME_ERROR')) return
           if (f_err_raise(i < 1 .or. i > nat, &
                & 'wrong atomic id', &
                & err_name='BIGDFT_RUNTIME_ERROR')) return
           read(unitf, "(4(1x,e17.10))") pawrhoij(iat)%rhoijp
        end do
     end if
   END SUBROUTINE readrhoij


   !> Reads wavefunction from file and transforms it properly if hgrid or size of simulation cell
   !!  have changed
   subroutine readmywaves(allwaves,orbs,glr,at,rxyz,  &
        psi,rxyz_old,mask,log)
     use module_precisions
     use module_types
     use yaml_output
     use public_enums
     use locregs
     use m_pawrhoij
     use at_domain, only: domain_periodic_dims
     use dictionaries
     use module_bigdft_arrays
     use module_bigdft_mpi
     use at_domain
     use liborbs_io, only: io_descr_read_phi
     implicit none
     type(io_descriptor), intent(in) :: allwaves
     type(locreg_descriptors), intent(in) :: glr
     type(orbitals_data), intent(inout) :: orbs
     type(atoms_data), intent(in) :: at
     real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz
     real(wp), dimension(orbs%npsidim_orbs), intent(out) :: psi
     real(gp), dimension(3,at%astruct%nat), intent(out), optional :: rxyz_old
     logical, dimension(orbs%norbp), intent(out), optional :: mask
     logical, intent(in), optional :: log
     !Local variables
     character(len=*), parameter :: subname='readmywaves'
     logical :: ok, do_log
     integer :: ncount1,ncount_rate,ncount_max,iorb,ncount2
     integer :: ishift, iat
     real(kind=4) :: tr0,tr1
     real(gp) :: tel, displ
     real(gp), dimension(3) :: dxyz
     real(wp), dimension(:), allocatable :: psifscf
     real(gp), dimension(:,:), pointer :: frxyz
     real(wp), dimension(:), pointer :: subpsi
     !integer, dimension(orbs%norb) :: orblist2

     call cpu_time(tr0)
     call system_clock(ncount1,ncount_rate,ncount_max)

     frxyz => io_descr_rxyz(allwaves)
     if (f_err_raise(size(frxyz, 2) /= at%astruct%nat, &
          "not the same number of atoms", err_name = "BIGDFT_RUNTIME_ERROR")) return

     displ= 0._gp
     dxyz = 0._gp
     do iat=1,at%astruct%nat
        displ=displ+distance(at%astruct%dom,rxyz(:,iat),frxyz(:,iat))**2
        dxyz=dxyz+closest_r(at%astruct%dom,rxyz(:,iat),center=frxyz(:,iat))
     enddo
     displ=sqrt(displ)
     dxyz=dxyz/real(at%astruct%nat,gp)

     psifscf = f_malloc(glr%mesh_fine%ndim,id='psifscf')
     ishift = 1
     do iorb = 1, orbs%norbp
        subpsi => f_subptr(psi, from = ishift, size = array_dim(glr) * orbs%nspinor)
        call io_descr_read_phi(allwaves%parent, subpsi, orbs%eval(orbs%isorb + iorb), orbs%isorb + iorb, &
             glr, ok, displ, dxyz, psifscf)
        if (present(mask)) mask(iorb) = ok
        ishift = ishift + array_dim(glr) * orbs%nspinor
     end do
     call f_free(psifscf)

     call cpu_time(tr1)
     call system_clock(ncount2,ncount_rate,ncount_max)
     tel=dble(ncount2-ncount1)/dble(ncount_rate)

     if (present(rxyz_old)) then
        call f_memcpy(dest = rxyz_old, src = frxyz)
     end if
     call f_free_ptr(frxyz)

     do_log = bigdft_mpi%iproc == 0
     if (present(log)) do_log = log
     if (do_log) then
        call yaml_sequence_open('Reading Waves Time')
        call yaml_sequence(advance='no')
        call yaml_mapping_open(flow=.true.)
        call yaml_map('Process',bigdft_mpi%iproc)
        call yaml_map('Timing',(/ real(tr1-tr0,kind=8),tel /),fmt='(1pe10.3)')
        call yaml_mapping_close()
        call yaml_sequence_close()
     end if
     !write(*,'(a,i4,2(1x,1pe10.3))') '- READING WAVES TIME',iproc,tr1-tr0,tel
   END SUBROUTINE readmywaves

   !> Associate to the absolute value of orbital a filename which depends of the k-point and
   !! of the spin sign
   subroutine filename_of_iorb(lbin,filename,orbs,ikpt,iorb,ispinor,filename_out)
     use module_precisions
     use module_types
     implicit none
     !Arguments
     logical, intent(in) :: lbin
     character(len=*), intent(in) :: filename
     integer, intent(in) :: ikpt,iorb,ispinor
     type(orbitals_data), intent(in) :: orbs
     character(len=*), intent(out) :: filename_out
     !local variables
     character(len=1) :: spintype,realimag
     character(len=4) :: f3
     character(len=7) :: f6
     character(len=8) :: completename
     real(gp) :: spins
     integer :: iband

     !see if the wavefunction is real or imaginary
     realimag=merge('I','R',modulo(ispinor,2)==0)

     write(f3,'(a1,i3.3)') "k", ikpt !not more than 999 kpts

     !calculate the spin sector
     spins=orbs%spinsgn(iorb)
     if(orbs%nspinor == 4) then
        spintype=merge('A','B',ispinor <=2)
     else
        spintype=merge('U','D',spins==1.0_gp)
     end if
     !no spin polarization if nspin=1
     if (orbs%nspin==1) spintype='N'

     iband = iorb
     !purge the value from the spin sign
     if (spins==-1.0_gp) iband=iband-orbs%norbu

     !value of the orbital
     write(f6,'(a1,i6.6)') "b", iband

     !complete the information in the name of the orbital
     completename='-'//f3//'-'//spintype//realimag
     if (lbin) then
        filename_out = trim(filename)//completename//".bin."//f6
        !print *,'complete name <',trim(filename_out),'> end'
     else
        filename_out = trim(filename)//completename//"."//f6
        !print *,'complete name <',trim(filename_out),'> end'
     end if

     !print *,'filename: ',filename_out
   end subroutine filename_of_iorb
   
   !> Associate to the absolute value of orbital a filename which depends of the k-point and
   !! of the spin sign
   subroutine filename_of_iorbp(lbin,filename,orbs,iorbp,ispinor,filename_out,iorb_shift)
     use module_precisions
     use module_types
     implicit none
     !Arguments
     character(len=*), intent(in) :: filename
     logical, intent(in) :: lbin
     integer, intent(in) :: iorbp,ispinor
     type(orbitals_data), intent(in) :: orbs
     character(len=*), intent(out) :: filename_out
     integer, intent(in), optional :: iorb_shift

     integer :: shift

     shift = 0
     if (present(iorb_shift)) shift = iorb_shift
     call filename_of_iorb(lbin, filename, orbs, orbs%iokpt(iorbp), &
          orbs%isorb + iorbp - (orbs%iokpt(iorbp)-1)*orbs%norb + shift, &
          ispinor, filename_out)
   end subroutine filename_of_iorbp

   !> Reads wavefunction from file and transforms it properly if hgrid or size of simulation cell
   !! have changed
   subroutine readmywaves_linear_new(iproc,nproc,dir_output,filename,&
        imatformat,at,tmb,rxyz,ref_frags,input_frag,frag_calc,read_kernel,read_coeffs,&
        max_nbasis_env,frag_env_mapping,orblist)
     use module_precisions
     use module_types
     use module_bigdft_output
     use module_bigdft_mpi
     use module_bigdft_errors
     use module_fragments
     use module_interfaces, only: reformat_supportfunctions, plot_wf
     use public_enums
     use rototranslations
     use reformatting
     use locregs
     use at_domain, only: domain,change_domain_BC,domain_geocode
     use f_utils
     use module_bigdft_arrays
     use liborbs_io, only: io_descr_phig
     implicit none
     integer, intent(in) :: iproc, nproc
     integer, intent(in) :: imatformat
     type(atoms_data), intent(in) :: at
     type(DFT_wavefunction), intent(inout) :: tmb
     real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz
     !real(gp), dimension(3,at%astruct%nat), intent(out) :: rxyz_old
     character(len=*), intent(in) :: dir_output, filename
     type(fragmentInputParameters), intent(in) :: input_frag
     type(system_fragment), dimension(:), pointer :: ref_frags
     logical, intent(in) :: frag_calc, read_kernel, read_coeffs
     integer, intent(in) :: max_nbasis_env
     integer, dimension(input_frag%nfrag,max_nbasis_env,3), intent(inout) :: frag_env_mapping
     integer, dimension(tmb%orbs%norb), intent(in), optional :: orblist
     !Local variables
     real(gp), parameter :: W_tol=1.e-3_gp !< wahba's tolerance
     integer :: ncount1,ncount_rate,ncount_max,ncount2
     integer :: ispinor,ilr
     integer :: unitwf,itoo_big
!!$ real(gp), dimension(3) :: mol_centre, mol_centre_new
     real(kind=4) :: tr0,tr1
     real(kind=8) :: tel
     character(len=256) :: full_filename
     character(len=*), parameter :: subname='readmywaves_linear_new'
     ! to eventually be part of the fragment structure?
     integer :: ndim_old, iiorb, ifrag, ifrag_ref, isfat, iorbp, iforb, isforb, iiat, iat, i, forb
     type(local_zone_descriptors) :: lzd_old
     real(wp), dimension(:), pointer :: psi_old
     type(phi_array), dimension(:), pointer :: phi_array_old
     type(rototranslation), dimension(:), pointer :: frag_trans_orb, frag_trans_frag
     integer, dimension(:), allocatable :: ipiv
     real(gp), dimension(:,:), allocatable :: rxyz_new, rxyz4_ref, rxyz4_new, rxyz_ref
     real(gp), dimension(:,:), allocatable :: rxyz_old !<this is read from the disk and not needed
     real(kind=gp), dimension(:), allocatable :: dist
     real(gp) :: max_shift, max_wahba, av_wahba
     logical :: output_wahba !< output all information relating to rototranslations, only relevant if nfrag>1
     logical, dimension(:,:), allocatable :: mpi_has_frag
     logical :: skip
     integer :: imatformat_local
     integer :: stat(fmpi_status_size)
!!$ integer :: ierr
     logical :: plain
     type(io_descriptor) :: mywaves

     ! DEBUG
     ! character(len=12) :: orbname
     !real(wp), dimension(:), allocatable :: gpsi

     call cpu_time(tr0)
     call system_clock(ncount1,ncount_rate,ncount_max)

     ! this should probably be an input variable.,,
     output_wahba = .true.

     rxyz_old=f_malloc([3,at%astruct%nat],id='rxyz_old')

     ! to be fixed
     if (present(orblist)) then
        call f_err_throw('orblist no longer functional in initialize_linear_from_file due to addition of fragment calculation')
     end if

     ! lzd_old => ref_frags(onwhichfrag)%frag_basis%lzd
     ! orbs_old -> ref_frags(onwhichfrag)%frag_basis%forbs ! <- BUT problem with it not being same type
     ! phi_array_old => ref_frags(onwhichfrag)%frag_basis%phi

     ! change parallelization later, for now all procs read the same number of tmbs as before
     ! initialize fragment lzd and phi_array_old for fragment, then allocate lzd_old which points to appropriate fragment entry
     ! for now directly using lzd_old etc - less efficient if fragments are used multiple times

     ! use above information to generate lzds
     call nullify_local_zone_descriptors(lzd_old)
     call nullify_locreg_descriptors(lzd_old%glr)
     lzd_old%nlr=tmb%orbs%norb
     allocate(lzd_old%Llr(lzd_old%nlr))
     do ilr=1,lzd_old%nlr
        call nullify_locreg_descriptors(lzd_old%llr(ilr))
     end do

     ! has size of new orbs, will possibly point towards the same tmb multiple times
     allocate(phi_array_old(tmb%orbs%norbp))
     do iorbp=1,tmb%orbs%norbp
        nullify(phi_array_old(iorbp)%psig)
     end do

     !allocate(frag_trans_orb(tmb%orbs%norbp))

     call timing(iproc,'readtmbfiles','ON')
     unitwf=99
     isforb=0
     isfat=0
     do ifrag=1,input_frag%nfrag
        ! find reference fragment this corresponds to
        ifrag_ref=input_frag%frag_index(ifrag)

        full_filename = trim(dir_output)//trim(input_frag%dirname(ifrag_ref))//trim(filename)
        mywaves = io_read_description(trim(full_filename), &
             at%astruct%nat, tmb%orbs, tmb%lzd%glr%mesh_coarse%dom, bigdft_mpi)

        ! loop over orbitals of this fragment
        loop_iforb: do iforb=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
           forb = iforb+isforb
           if (ref_frags(ifrag_ref)%fbasis%forbs%spinsgn(iforb) == -1.0d0) then
              forb = forb + tmb%orbs%norbu - ref_frags(ifrag_ref)%fbasis%forbs%norbu
           end if
           ilr=tmb%orbs%inwhichlocreg(forb)
           iorbp = forb - tmb%orbs%isorb
           if (iorbp < 1 .or. iorbp > tmb%orbs%norbp) cycle

           do ispinor=1,tmb%orbs%nspinor
              ! in general this might point to a different tmb
              phi_array_old(iorbp)%psig => io_descr_phig(mywaves%parent, iforb, ispinor, &
                   Lzd_old%Llr(ilr), gdom = tmb%lzd%glr%mesh%dom)
           end do
           call f_memcpy(rxyz_old(1, isfat+1), mywaves%astruct%rxyz(1,1), &
                3 * ref_frags(ifrag_ref)%astruct_frg%nat)
        end do loop_iforb
        isforb=isforb+ref_frags(ifrag_ref)%fbasis%forbs%norbu
        isfat=isfat+ref_frags(ifrag_ref)%astruct_frg%nat

        call io_deallocate_descriptor(mywaves)
     end do
     Lzd_old%hgrids = Lzd_old%Llr(1)%mesh_coarse%hgrids
     call timing(iproc,'readtmbfiles','OF')

     call timing(iproc,'tmbrestart','ON')

     ! reformat fragments

     !if genuine fragment calculation do this, otherwise find 3 nearest neighbours (use sort in time.f90) and send rxyz arrays with 4 atoms
     itoo_big=0
     av_wahba = 0.0d0
     max_wahba = 0.0d0
     fragment_if: if (frag_calc) then
        ! Find fragment transformations for each fragment, then put in frag_trans array for each orb
        allocate(frag_trans_frag(input_frag%nfrag))

        ! needed for sharing frag_trans info
        mpi_has_frag=f_malloc0((/ 0.to.bigdft_mpi%nproc-1,1.to.input_frag%nfrag /),id='mpi_has_frag')
        isfat=0
        isforb=0
        do ifrag=1,input_frag%nfrag
           ! find reference fragment this corresponds to
           ifrag_ref=input_frag%frag_index(ifrag)

           ! check if we need this fragment transformation on this proc
           ! if this is an environment calculation we need mapping on all mpi, so easier to just calculate all transformations on all procs
           if (ref_frags(ifrag_ref)%astruct_env%nat/=0) then
              skip=.false.
           else
              skip=.true.
              do iforb=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
                 do iorbp=1,tmb%orbs%norbp
                    iiorb=iorbp+tmb%orbs%isorb
                    ! check if this ref frag orbital corresponds to the orbital we want
                    forb = iforb+isforb
                    if (ref_frags(ifrag_ref)%fbasis%forbs%spinsgn(iforb) == -1.0d0) then
                       forb = forb + tmb%orbs%norbu - ref_frags(ifrag_ref)%fbasis%forbs%norbu
                    end if
                    ! check if this ref frag orbital corresponds to the orbital we want
                    if (iiorb==forb) then
                       skip=.false.
                       mpi_has_frag(iproc,ifrag)=.true.
                       exit
                    end if
                 end do
              end do
           end if

           if (skip) then
              isfat=isfat+ref_frags(ifrag_ref)%astruct_frg%nat
              isforb=isforb+ref_frags(ifrag_ref)%fbasis%forbs%norbu
              cycle
           end if

           if (ref_frags(ifrag_ref)%astruct_env%nat==0) then
              rxyz_ref = f_malloc((/ 3, ref_frags(ifrag_ref)%astruct_frg%nat /),id='rxyz_ref')
              rxyz_new = f_malloc((/ 3, ref_frags(ifrag_ref)%astruct_frg%nat /),id='rxyz_new')

              do iat=1,ref_frags(ifrag_ref)%astruct_frg%nat
                 rxyz_new(:,iat)=rxyz(:,isfat+iat)
                 rxyz_ref(:,iat)=rxyz_old(:,isfat+iat)
              end do

!!$           ! use center of fragment for now, could later change to center of symmetry
!!$           frag_trans_frag(ifrag)%rot_center=frag_center(ref_frags(ifrag_ref)%astruct_frg%nat,rxyz_ref)
!!$           frag_trans_frag(ifrag)%rot_center_new=frag_center(ref_frags(ifrag_ref)%astruct_frg%nat,rxyz_new)
!!$
!!$           ! shift rxyz wrt center of rotation
!!$           do iat=1,ref_frags(ifrag_ref)%astruct_frg%nat
!!$              rxyz_ref(:,iat)=rxyz_ref(:,iat)-frag_trans_frag(ifrag)%rot_center
!!$              rxyz_new(:,iat)=rxyz_new(:,iat)-frag_trans_frag(ifrag)%rot_center_new
!!$           end do
!!$
!!$           call find_frag_trans(ref_frags(ifrag_ref)%astruct_frg%nat,rxyz_ref,rxyz_new,frag_trans_frag(ifrag))

              frag_trans_frag(ifrag) = rototranslation_new(ref_frags(ifrag_ref)%astruct_frg%nat,&
                src=rxyz_ref,dest=rxyz_new)
              call f_free(rxyz_ref)
              call f_free(rxyz_new)

              ! take into account environment coordinates
           else
              call match_environment_atoms(isfat,at,rxyz,tmb%orbs,ref_frags(ifrag_ref),&
                   max_nbasis_env,frag_env_mapping(ifrag,:,:),frag_trans_frag(ifrag),.false.)
           end if

           ! in environment case we're calculating all transformations on each MPI, so no need to incrememnt on each
           if (frag_trans_frag(ifrag)%Werror > W_tol .and. ((ref_frags(ifrag_ref)%astruct_env%nat/=0 .and. iproc==0) &
                .or. ref_frags(ifrag_ref)%astruct_env%nat==0))then
              call f_increment(itoo_big)
           end if

           max_wahba = max(max_wahba,frag_trans_frag(ifrag)%Werror)

           ! useful for identifying which fragments are problematic
           if (iproc==0 .and. frag_trans_frag(ifrag)%Werror>W_tol) then
              write(*,'(A,1x,I3,1x,I3,1x,3(F12.6,1x),2(F12.6,1x),2(I8,1x))') 'ifrag,ifrag_ref,rot_axis,theta,error',&
                   ifrag,ifrag_ref,frag_trans_frag(ifrag)%rot_axis,frag_trans_frag(ifrag)%theta/(4.0_gp*atan(1.d0)/180.0_gp),&
                   frag_trans_frag(ifrag)%Werror,itoo_big,iproc
              write(*,*) ''
           end if

           isfat=isfat+ref_frags(ifrag_ref)%astruct_frg%nat
           isforb=isforb+ref_frags(ifrag_ref)%fbasis%forbs%norbu
        end do


        if (output_wahba) then
           ! need to fetch quantities from other procs
           if (bigdft_mpi%nproc > 1) then
              !call fmpi_allreduce(mpi_has_frag(0,1),bigdft_mpi%nproc*input_frag%nfrag,op=FMPI_LOR,comm=bigdft_mpi%mpi_comm)
              call fmpi_allreduce(mpi_has_frag,op=FMPI_LOR,comm=bigdft_mpi%mpi_comm)

              do ifrag=1,input_frag%nfrag
                 ! if iproc=0 already has frag, no need to do anything, otherwise need to look for it
                 if (mpi_has_frag(0,ifrag)) cycle
                 do i=0,bigdft_mpi%nproc-1
                    if (mpi_has_frag(i,ifrag)) then
                       ! send from proc i to proc 0
                       !if (iproc==0) print*,'need to send from ',i
                       if(iproc==i) call fmpi_send(frag_trans_frag(ifrag)%theta, count=1, dest=0, tag=10*ifrag, &
                            comm=bigdft_mpi%mpi_comm)
                       if(iproc==0) call fmpi_recv(frag_trans_frag(ifrag)%theta, count=1, source=i, tag=10*ifrag, &
                            comm=bigdft_mpi%mpi_comm, status=stat)
                       if(iproc==i) call fmpi_send(frag_trans_frag(ifrag)%rot_axis(1), count=3, dest=0, tag=10*ifrag+1, &
                            comm=bigdft_mpi%mpi_comm)
                       if(iproc==0) call fmpi_recv(frag_trans_frag(ifrag)%rot_axis(1), count=3, source=i, tag=10*ifrag+1, &
                            comm=bigdft_mpi%mpi_comm, status=stat)
                       if(iproc==i) call fmpi_send(frag_trans_frag(ifrag)%Werror, count=1, dest=0, tag=10*ifrag+2, &
                            comm=bigdft_mpi%mpi_comm)
                       if(iproc==0) call fmpi_recv(frag_trans_frag(ifrag)%Werror, count=1, source=i, tag=10*ifrag+2, &
                            comm=bigdft_mpi%mpi_comm, status=stat)
                       ! if(iproc==i) call mpi_send(frag_trans_frag(ifrag)%theta, 1, mpi_double_precision, 0, 10*ifrag, &
                       !      bigdft_mpi%mpi_comm, ierr)
                       ! if(iproc==0) call mpi_recv(frag_trans_frag(ifrag)%theta, 1, mpi_double_precision, i, 10*ifrag, &
                       !      bigdft_mpi%mpi_comm, stat, ierr)
                       ! if(iproc==i) call mpi_send(frag_trans_frag(ifrag)%rot_axis(1), 3, mpi_double_precision, 0, 10*ifrag+1, &
                       !      bigdft_mpi%mpi_comm, ierr)
                       ! if(iproc==0) call mpi_recv(frag_trans_frag(ifrag)%rot_axis(1), 3, mpi_double_precision, i, 10*ifrag+1, &
                       !      bigdft_mpi%mpi_comm, stat, ierr)
                       ! if(iproc==i) call mpi_send(frag_trans_frag(ifrag)%Werror, 1, mpi_double_precision, 0, 10*ifrag+2, &
                       !      bigdft_mpi%mpi_comm, ierr)
                       ! if(iproc==0) call mpi_recv(frag_trans_frag(ifrag)%Werror, 1, mpi_double_precision, i, 10*ifrag+2, &
                       !      bigdft_mpi%mpi_comm, stat, ierr)

                       exit
                    end if
                 end do
              end do
           end if

           if (iproc==0) then
              !call yaml_mapping_open('Information about the rototranslations')
              call yaml_sequence_open('Fragment transformations')
              do ifrag=1,input_frag%nfrag
                 av_wahba = av_wahba + frag_trans_frag(ifrag)%Werror
                 ifrag_ref=input_frag%frag_index(ifrag)
                 call yaml_sequence(advance='no')
                 call yaml_map('Fragment name',trim(input_frag%label(ifrag_ref))) ! change this to actual name
                 call yaml_map('Angle (degrees)',frag_trans_frag(ifrag)%theta/(4.0_gp*atan(1.d0)/180.0_gp),fmt='(f12.6)')
                 call yaml_map('Axis',frag_trans_frag(ifrag)%rot_axis,fmt='(3f10.6)')
                 call yaml_map('Wahba cost function',frag_trans_frag(ifrag)%Werror,fmt='(1es13.6)')
              end do
              call yaml_sequence_close()
              !call yaml_mapping_close()
              call yaml_flush_document()
              av_wahba = av_wahba / input_frag%nfrag
           end if
        end if
        call f_free(mpi_has_frag)

        !if (bigdft_mpi%nproc > 1) then
        !   call fmpi_allreduce(frag_env_mapping, FMPI_SUM, comm=bigdft_mpi%mpi_comm)
        !end if

        allocate(frag_trans_orb(tmb%orbs%norbp))

        isforb=0
        isfat=0
        do ifrag=1,input_frag%nfrag
           ! find reference fragment this corresponds to
           ifrag_ref=input_frag%frag_index(ifrag)
           ! loop over orbitals of this fragment
           do iforb=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
              do iorbp=1,tmb%orbs%norbp
                 iiorb=iorbp+tmb%orbs%isorb
                 ! check if this ref frag orbital corresponds to the orbital we want
                 forb = iforb+isforb
                 if (ref_frags(ifrag_ref)%fbasis%forbs%spinsgn(iforb) == -1.0d0) then
                    forb = forb + tmb%orbs%norbu - ref_frags(ifrag_ref)%fbasis%forbs%norbu
                 end if
                 ! check if this ref frag orbital corresponds to the orbital we want
                 if (iiorb/=forb) cycle

!!$              frag_trans_orb(iorbp)%rot_center=frag_trans_frag(ifrag)%rot_center
!!$              frag_trans_orb(iorbp)%rot_center_new=frag_trans_frag(ifrag)%rot_center_new

!!!!!!!!!!!!!
                 iiat=tmb%orbs%onwhichatom(iiorb)

!!$              frag_trans_orb(iorbp)%rot_axis=(frag_trans_frag(ifrag)%rot_axis)
!!$              frag_trans_orb(iorbp)%theta=frag_trans_frag(ifrag)%theta
!!$              frag_trans_orb(iorbp)%Rmat=frag_trans_frag(ifrag)%Rmat

                 frag_trans_orb(iorbp)=frag_trans_frag(ifrag)
                 ! use atom position
                 call set_translation(frag_trans_orb(iorbp),&
                      src=rxyz_old(:,iiat),dest=rxyz(:,iiat))
!!$              frag_trans_orb(iorbp)%rot_center=rxyz_old(:,iiat)
!!$              frag_trans_orb(iorbp)%rot_center_new=rxyz(:,iiat)
!!!!!!!!!!!!!


                 !write(*,'(a,x,2(i2,x),4(f5.2,x),6(f7.3,x))'),'trans2',ifrag,iiorb,frag_trans_orb(iorbp)%theta,&
                 !     frag_trans_orb(iorbp)%rot_axis, &
                 !     frag_trans_orb(iorbp)%rot_center,frag_trans_orb(iorbp)%rot_center_new
              end do
           end do
           isforb=isforb+ref_frags(ifrag_ref)%fbasis%forbs%norbu
           isfat=isfat+ref_frags(ifrag_ref)%astruct_frg%nat
        end do

        deallocate(frag_trans_frag)
     else
        ! only 1 'fragment', calculate rotation/shift atom wise, using nearest neighbours
        allocate(frag_trans_orb(tmb%orbs%norbp))

        rxyz4_ref = f_malloc((/ 3, min(4, ref_frags(ifrag_ref)%astruct_frg%nat) /),id='rxyz4_ref')
        rxyz4_new = f_malloc((/ 3, min(4, ref_frags(ifrag_ref)%astruct_frg%nat) /),id='rxyz4_new')

        isforb=0
        isfat=0
        do ifrag=1,input_frag%nfrag
           ! find reference fragment this corresponds to
           ifrag_ref=input_frag%frag_index(ifrag)

           rxyz_ref = f_malloc((/ 3, ref_frags(ifrag_ref)%astruct_frg%nat /),id='rxyz_ref')
           rxyz_new = f_malloc((/ 3, ref_frags(ifrag_ref)%astruct_frg%nat /),id='rxyz_new')
           dist = f_malloc(ref_frags(ifrag_ref)%astruct_frg%nat,id='dist')
           ipiv = f_malloc(ref_frags(ifrag_ref)%astruct_frg%nat,id='ipiv')

           ! loop over orbitals of this fragment
           do iforb=1,ref_frags(ifrag_ref)%fbasis%forbs%norb
              do iorbp=1,tmb%orbs%norbp
                 iiorb=iorbp+tmb%orbs%isorb
                 ! check if this ref frag orbital corresponds to the orbital we want
                 forb = iforb+isforb
                 if (ref_frags(ifrag_ref)%fbasis%forbs%spinsgn(iforb) == -1.0d0) then
                    forb = forb + tmb%orbs%norbu - ref_frags(ifrag_ref)%fbasis%forbs%norbu
                 end if
                 ! check if this ref frag orbital corresponds to the orbital we want
                 if (iiorb/=forb) cycle

                 do iat=1,ref_frags(ifrag_ref)%astruct_frg%nat
                    rxyz_new(:,iat)=rxyz(:,isfat+iat)
                    rxyz_ref(:,iat)=rxyz_old(:,isfat+iat)
                 end do

                 iiat=tmb%orbs%onwhichatom(iiorb)

                 ! use atom position
!!$              frag_trans_orb(iorbp)%rot_center=rxyz_old(:,iiat)
!!$              frag_trans_orb(iorbp)%rot_center_new=rxyz(:,iiat)
                 call set_translation(frag_trans_orb(iorbp),src=rxyz_old(:,iiat),&
                      dest=rxyz(:,iiat))

                 ! shift rxyz wrt center of rotation
                 do iat=1,ref_frags(ifrag_ref)%astruct_frg%nat
                    rxyz_ref(:,iat)=rxyz_ref(:,iat)-rxyz_old(:,iiat)!frag_trans_orb(iorbp)%rot_center
                    rxyz_new(:,iat)=rxyz_new(:,iat)-rxyz(:,iiat)!frag_trans_orb(iorbp)%rot_center_new
                 end do

                 ! find distances from this atom
                 do iat=1,ref_frags(ifrag_ref)%astruct_frg%nat
                    dist(iat)=-dsqrt(rxyz_ref(1,iat)**2+rxyz_ref(2,iat)**2+rxyz_ref(3,iat)**2)
                 end do

                 ! sort atoms into neighbour order
                 call sort_positions(ref_frags(ifrag_ref)%astruct_frg%nat,dist,ipiv)

                 ! take atom and 3 nearest neighbours
                 do iat=1,min(4,ref_frags(ifrag_ref)%astruct_frg%nat)
                    rxyz4_ref(:,iat)=rxyz_ref(:,ipiv(iat))
                    rxyz4_new(:,iat)=rxyz_new(:,ipiv(iat))
                 end do

                 call find_and_set_rotation(frag_trans_orb(iorbp),&
                      min(4,ref_frags(ifrag_ref)%astruct_frg%nat),src=rxyz4_ref,dest=rxyz4_new)

!!$              call find_frag_trans(min(4,ref_frags(ifrag_ref)%astruct_frg%nat),rxyz4_ref,rxyz4_new,&
!!$                   frag_trans_orb(iorbp))
                 if (frag_trans_orb(iorbp)%Werror > W_tol) call f_increment(itoo_big)

                 max_wahba = max(max_wahba,frag_trans_orb(iorbp)%Werror)
                 ! can't easily calculate the average as some orbitals are on more
                 ! than one proc
                 !av_wahba = av_wahba + frag_trans_orb(iorbp)%Werror/tmb%orbs%norb

!!$              print *,'transformation of the fragment, iforb',iforb
!!$              write(*,'(A,I3,1x,I3,1x,3(F12.6,1x),F12.6)') 'ifrag,iorb,rot_axis,theta',&
!!$                   ifrag,iiorb,frag_trans_orb(iorbp)%rot_axis,frag_trans_orb(iorbp)%theta/(4.0_gp*atan(1.d0)/180.0_gp)

              end do
           end do
           isforb=isforb+ref_frags(ifrag_ref)%fbasis%forbs%norbu
           isfat=isfat+ref_frags(ifrag_ref)%astruct_frg%nat

           call f_free(ipiv)
           call f_free(dist)
           call f_free(rxyz_ref)
           call f_free(rxyz_new)

        end do

        call f_free(rxyz4_ref)
        call f_free(rxyz4_new)

     end if fragment_if

     !reduce the number of warnings
     if (nproc >1) call fmpi_allreduce(itoo_big,count=1,op=FMPI_SUM,comm=bigdft_mpi%mpi_comm)
     if (nproc >1) call fmpi_allreduce(max_wahba,count=1,op=FMPI_MAX,comm=bigdft_mpi%mpi_comm)


     if (itoo_big > 0 .and. iproc==0) call yaml_warning('Found '//itoo_big//' warning of high Wahba cost functions')
     ! if this isn't an actual fragment calculation then we don't have access to the average value
     if (iproc==0 .and. output_wahba .and. frag_calc) call yaml_map('Average Wahba cost function value',av_wahba,fmt='(es9.2)')
     if (iproc==0) call yaml_map('Maximum Wahba cost function value',max_wahba,fmt='(es9.2)')


!!!debug - check calculated transformations
     !!if (iproc==0) then
     !!   open(109)
     !!   do ifrag=1,input_frag%nfrag
     !!      ! find reference fragment this corresponds to
     !!      ifrag_ref=input_frag%frag_index(ifrag)
     !!      num_env=ref_frags(ifrag_ref)%astruct_env%nat-ref_frags(ifrag_ref)%astruct_frg%nat
     !!      write(109,'((a,1x,I5,1x),F12.6,2x,3(F12.6,1x),6(1x,F18.6),2x,F6.2,2(2x,I5))') &
     !!           trim(input_frag%label(ifrag_ref)),ifrag,&
     !!           frag_trans_frag(ifrag)%theta/(4.0_gp*atan(1.d0)/180.0_gp),frag_trans_frag(ifrag)%rot_axis,&
     !!           frag_trans_frag(ifrag)%rot_center,frag_trans_frag(ifrag)%rot_center_new,&
     !!           Werror(ifrag),num_env,ifrag_ref
     !!   end do
     !!   close(109)
     !!end if


     ! hack to make reformatting work for case when hgrid changes, here the ndims is meaningless
!!$  Lzd_old%glr%mesh_coarse=&
!!$       cell_new(tmb%lzd%glr%geocode,tmb%lzd%glr%mesh_coarse%ndims,lzd_old%hgrids)
     Lzd_old%glr%mesh_coarse = tmb%lzd%glr%mesh_coarse

     nullify(psi_old)
     call timing(iproc,'tmbrestart','OF')
     call reformat_supportfunctions(iproc,nproc,&
          at,rxyz_old,rxyz,.false.,tmb,ndim_old,lzd_old,frag_trans_orb,&
          psi_old,trim(dir_output),input_frag,ref_frags,max_shift,phi_array_old)
     call timing(iproc,'tmbrestart','ON')

     deallocate(frag_trans_orb)

     do iorbp=1,tmb%orbs%norbp
        !nullify/deallocate here as appropriate, in future may keep
        call f_free_ptr(phi_array_old(iorbp)%psig)
     end do

     deallocate(phi_array_old)
     call f_free(rxyz_old)
     call deallocate_local_zone_descriptors(lzd_old)

     !! DEBUG - plot in global box - CHECK WITH REFORMAT ETC IN LRs
     !ind=1
     !gpsi=f_malloc(array_dim(tmb%Lzd%glr),id='gpsi')
     !do iorbp=1,tmb%orbs%norbp
     !   iiorb=iorbp+tmb%orbs%isorb
     !   ilr = tmb%orbs%inwhichlocreg(iiorb)
     !   call f_zero(gpsi)
     !   call Lpsi_to_global2(iproc, array_dim(tmb%Lzd%Llr(ilr)), &
     !        array_dim(tmb%Lzd%glr), &
     !        1, 1, 1, tmb%Lzd%glr, tmb%Lzd%Llr(ilr), &
     !        tmb%psi(ind:ind+array_dim(tmb%Lzd%Llr(ilr))), gpsi)
     !   call plot_wf(.false.,trim(dir_output)//trim(adjustl(yaml_toa(iiorb))),1,at,1.0_dp,tmb%Lzd%glr,&
     !        tmb%Lzd%hgrids(1),tmb%Lzd%hgrids(2),tmb%Lzd%hgrids(3),rxyz,gpsi)
     !   ind = ind + array_dim(tmb%Lzd%Llr(ilr))
     !end do
     !call f_free(gpsi)
     !! END DEBUG

     ! Read the coefficient file for each fragment and assemble total coeffs
     ! coeffs should eventually go into ref_frag array and then point? or be copied to (probably copied as will deallocate frag)
     unitwf=99
     isforb=0
     ! can directly loop over reference fragments here
     !do ifrag=1,input_frag%nfrag
     do ifrag_ref=1,input_frag%nfrag_ref
        ! find reference fragment this corresponds to
        !ifrag_ref=input_frag%frag_index(ifrag)

        ! read coeffs/kernel
        if (read_kernel) then
           full_filename=trim(dir_output)//trim(input_frag%dirname(ifrag_ref))//'density_kernel'
           !should fragments have some knowledge of spin?

           ! LR: imatformat is based on the format we want to write the matrices in
           ! so if we're not writing them, we will instead default to assuming that we want to use ntpoly format (i.e. sparse)
           if (imatformat == 0) then
              ! in this case, we default to ntpoly plain text format
              imatformat_local = 1
           else
              imatformat_local = imatformat
           end if
           call read_matrix_local(full_filename, imatformat_local, &
                ref_frags(ifrag_ref)%fbasis%forbs%nspin, &
                ref_frags(ifrag_ref)%fbasis%forbs%norbu, &
                ref_frags(ifrag_ref)%kernel, bigdft_mpi%mpi_comm)

           if (ref_frags(ifrag_ref)%nbasis_env/=0) then
              full_filename=trim(dir_output)//trim(input_frag%dirname(ifrag_ref))//'density_kernel_env'
              !should fragments have some knowledge of spin?
              call read_matrix_local(full_filename, imatformat_local, &
                   tmb%orbs%nspinor, ref_frags(ifrag_ref)%nbasis_env, &
                   ref_frags(ifrag_ref)%kernel_env, bigdft_mpi%mpi_comm)
           end if

           !!if (iproc==0) then
           !!   open(32)
           !!   do itmb=1,tmb%orbs%norb
           !!      do jtmb=1,tmb%orbs%norb
           !!         write(32,*) itmb,jtmb,tmb%coeff(itmb,jtmb),ref_frags(ifrag_ref)%kernel(itmb,jtmb,1)
           !!      end do
           !!   end do
           !!   write(32,*) ''
           !!   close(32)
           !!end if

        end if
        if (read_coeffs) then 
           ! should eventually switch this to using the sparsematrix routines
           ! but need the number of electrons of the fragment which isn't stored in that case
           full_filename=trim(dir_output)//trim(input_frag%dirname(ifrag_ref))//trim(filename)//'_coeff'
           call f_file_exists(trim(full_filename), exists = plain)
           if (plain) then
           !full_filename=trim(dir_output)//trim(input_frag%dirname(ifrag_ref))//'KS_coeffs.bin'
              call f_open_file(unitwf,file=trim(full_filename),binary=.false.)
           else
              call f_open_file(unitwf,file=trim(full_filename)//".bin",binary=.true.)
           end if
           call read_coeff_minbasis(unitwf,plain,iproc,ref_frags(ifrag_ref)%fbasis%forbs%norb,&
                ref_frags(ifrag_ref)%nelec,ref_frags(ifrag_ref)%fbasis%forbs%norbu,ref_frags(ifrag_ref)%fbasis%forbs%nspin,&
                ref_frags(ifrag_ref)%coeff,ref_frags(ifrag_ref)%eval)
           !call read_linear_coefficients(mode, bigdft_mpi%iproc, bigdft_mpi%nproc, bigdft_mpi%mpi_comm,&
           !     full_filename, input%nspin, ref_frags(ifrag_ref)%fbasis%forbs%norb, ref_frags(ifrag_ref)%fbasis%forbs%norb,&
           !     coeff, eval)
           call f_close(unitwf)
        end if

        !isforb=isforb+ref_frags(ifrag_ref)%fbasis%forbs%norb
     end do

     call cpu_time(tr1)
     call system_clock(ncount2,ncount_rate,ncount_max)
     tel=dble(ncount2-ncount1)/dble(ncount_rate)

     if (iproc == 0) then
        call yaml_sequence_open('Reading Waves Time')
        call yaml_sequence(advance='no')
        call yaml_mapping_open(flow=.true.)
        call yaml_map('Process',iproc)
        call yaml_map('Timing',(/ real(tr1-tr0,kind=8),tel /),fmt='(1pe10.3)')
        call yaml_mapping_close()
        call yaml_sequence_close()
     end if
     !write(*,'(a,i4,2(1x,1pe10.3))') '- READING WAVES TIME',iproc,tr1-tr0,tel
     call timing(iproc,'tmbrestart','OF')


   END SUBROUTINE readmywaves_linear_new


   !> Matches neighbouring atoms from environment file to those in full system
   !! returns atom mapping information and 'best' fragment transformation and corresponding Wahba error
   subroutine match_environment_atoms(isfat,at,rxyz,orbs,ref_frag,max_nbasis_env,frag_env_mapping,frag_trans,ignore_species)
      use module_precisions
      use module_types
      use module_fragments
      use module_atoms, only: deallocate_atomic_structure, nullify_atomic_structure, set_astruct_from_file
      use rototranslations
      use at_domain, only: domain_periodic_dims
      use module_bigdft_arrays
      use module_bigdft_mpi
      implicit none
      integer, intent(in) :: isfat
      type(atoms_data), intent(in) :: at
      real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz
      type(orbitals_data), intent(in) :: orbs
      type(system_fragment), intent(in) :: ref_frag
      integer, intent(in) :: max_nbasis_env
      integer, dimension(max_nbasis_env,3), intent(out) :: frag_env_mapping
      type(rototranslation), intent(out) :: frag_trans
      logical, intent(in) :: ignore_species

      !local variables
      integer :: iat, ityp, ipiv_shift, iatt, iatf, num_env, np, c, itmb, iorb, i, minperm, num_neighbours
      integer :: ntypes, nat_not_frag, ia
      integer, allocatable, dimension(:) :: ipiv, array_tmp, num_neighbours_type
      integer, dimension(:,:), allocatable :: permutations
      integer, pointer, dimension(:) :: iatype

      real(kind=gp) :: minerror, mintheta, err_tol, rot_tol
      real(kind=gp), allocatable, dimension(:) :: dist
      real(kind=gp), dimension(:,:), allocatable :: rxyz_new_all, rxyz_frg_new, rxyz_new_trial, rxyz_ref, rxyz_new

      type(atomic_structure) :: astruct_ghost

      logical :: perx, pery, perz, wrong_atom, check_for_ghosts, ghosts_exist

      character(len=2) :: atom_ref, atom_trial
      logical, dimension(3) :: peri

      ! we only want to check for ghost atoms if they appear in the enviornment coordinates
      ! AND if we aren't ignoring species
      check_for_ghosts=.false.
      if (.not. ignore_species) then
         do ityp=1,ref_frag%astruct_env%ntypes
            if (trim(ref_frag%astruct_env%atomnames(ityp))=='X') then
               check_for_ghosts=.true.
               exit
            end if
        end do
      end if

      astruct_ghost%nat=0

      !first need to check for and read in ghost atoms if required
      !might be better to move this outside routine, and just pass in astruct_ghost to avoid re-reading file for each fragment?
      if (check_for_ghosts) then
         !ghost atoms in ghost.xyz - would be better if this was seed_ghost.xyz but since we don't have seed here at the moment come back to this point later
         call nullify_atomic_structure(astruct_ghost)

         !first check if ghost file exists
         inquire(FILE = 'ghost.xyz', EXIST = ghosts_exist)

         if (ghosts_exist) then
            call set_astruct_from_file('ghost',bigdft_mpi%iproc,astruct_ghost)
         else
            !could ignore all environment ghost atoms but easier to flag as an error
            stop 'Error missing ghost atom file in match_environment_atoms'
         end if

      end if


      nat_not_frag = at%astruct%nat-ref_frag%astruct_frg%nat + astruct_ghost%nat

      !from _env file - includes fragment and environment
      rxyz_ref = f_malloc((/ 3,ref_frag%astruct_env%nat /),id='rxyz_ref')
      !all coordinates in new system, except those in fragment
      rxyz_new_all = f_malloc((/ 3,nat_not_frag /),id='rxyz_new_all')
      dist = f_malloc(nat_not_frag,id='dist')
      ipiv = f_malloc(nat_not_frag,id='ipiv')
      !just the fragment in the new system
      rxyz_frg_new = f_malloc((/ 3,ref_frag%astruct_frg%nat /),id='rxyz_frg_new')

      !just a straightforward copy
      do iat=1,ref_frag%astruct_env%nat
         rxyz_ref(:,iat)=ref_frag%astruct_env%rxyz(:,iat)
      end do


      !also add up how many atoms of each type (don't include fragment itself)
      if (astruct_ghost%nat>0) then
         ntypes=at%astruct%ntypes+1
      else
         ntypes=at%astruct%ntypes
      end if
     
      if (astruct_ghost%nat>0) then
         iatype=f_malloc_ptr(at%astruct%nat+astruct_ghost%nat,id='iatype')
         call vcopy(at%astruct%nat,at%astruct%iatype(1),1,iatype(1),1)
         do iat=at%astruct%nat+1,at%astruct%nat+astruct_ghost%nat
            iatype(iat)=at%astruct%ntypes+1
         end do
      else
         iatype => at%astruct%iatype
      end if


      num_neighbours_type=f_malloc0(ntypes,id='num_neighbours_type')
      do iat=ref_frag%astruct_frg%nat+1,ref_frag%astruct_env%nat

         !if it's a ghost atom the atom won't appear in the main atomnames file
         if (trim(ref_frag%astruct_env%atomnames(ref_frag%astruct_env%iatype(iat)))=='X') then
            num_neighbours_type(ntypes) = num_neighbours_type(ntypes)+1
         else 
            !be careful here as atom types not necessarily in same order in env file as in main file (or even same number thereof)
            do ityp=1,at%astruct%ntypes
               if (trim(at%astruct%atomnames(ityp))==trim(ref_frag%astruct_env%atomnames(ref_frag%astruct_env%iatype(iat)))) then
                  num_neighbours_type(ityp) = num_neighbours_type(ityp)+1
                  exit
               end if
            end do
         end if
      end do
      num_neighbours=sum(num_neighbours_type)
      if (sum(num_neighbours_type)/=ref_frag%astruct_env%nat-ref_frag%astruct_frg%nat) &
           stop 'Error with num_neighbours_type in fragment environment restart'
      !print*,ifrag,ifrag_ref,sum(num_neighbours_type),num_neighbours_type

      !take all atoms not in this fragment (might be overcomplicating this...)
      do iat=1,isfat
         rxyz_new_all(:,iat)=rxyz(:,iat)
      end do

      do iat=isfat+ref_frag%astruct_frg%nat+1,at%astruct%nat
         rxyz_new_all(:,iat-ref_frag%astruct_frg%nat)=rxyz(:,iat)
      end do

      !also add ghost atoms if necessary
      do iat=1,astruct_ghost%nat
         rxyz_new_all(:,at%astruct%nat-ref_frag%astruct_frg%nat+iat)=astruct_ghost%rxyz(:,iat)
      end do


      !just take those in the fragment
      do iat=1,ref_frag%astruct_frg%nat
         rxyz_frg_new(:,iat)=rxyz(:,isfat+iat)
      end do

      !this should just be the fragment centre - fragment xyz comes first in rxyz_env so this should be ok
      call set_translation(frag_trans,&
           src=frag_center(ref_frag%astruct_frg%nat,rxyz_ref(:,1:ref_frag%astruct_frg%nat)),&
           dest=frag_center(ref_frag%astruct_frg%nat,rxyz_frg_new))
!!$      frag_trans%rot_center=frag_center(ref_frag%astruct_frg%nat,rxyz_ref(:,1:ref_frag%astruct_frg%nat))
!!$      !the fragment centre in new coordinates
!!$      frag_trans%rot_center_new=frag_center(ref_frag%astruct_frg%nat,rxyz_frg_new)

      ! shift rxyz wrt center of rotation
      do iat=1,ref_frag%astruct_env%nat
         rxyz_ref(:,iat)=rxyz_ref(:,iat)-frag_trans%rot_center_src
      end do

      ! find distances from this atom BEFORE shifting
!!$      perx=(at%astruct%geocode /= 'F')
!!$      pery=(at%astruct%geocode == 'P')
!!$      perz=(at%astruct%geocode /= 'F')
      !peri=bc_periodic_dims(geocode_to_bc(at%astruct%geocode))
      peri=domain_periodic_dims(at%astruct%dom)
      perx=peri(1)
      pery=peri(2)
      perz=peri(3)

      !if coordinates wrap around (in periodic), correct before shifting
      !assume that the fragment itself doesn't, just the environment...
      !think about other periodic cases that might need fixing...
      do iat=1,nat_not_frag
         dist(iat) = dist_and_shift(perx,at%astruct%cell_dim(1),frag_trans%rot_center_dest(1),rxyz_new_all(1,iat))**2
         dist(iat) = dist(iat) + dist_and_shift(pery,at%astruct%cell_dim(2),frag_trans%rot_center_dest(2),rxyz_new_all(2,iat))**2
         dist(iat) = dist(iat) + dist_and_shift(perz,at%astruct%cell_dim(3),frag_trans%rot_center_dest(3),rxyz_new_all(3,iat))**2
         dist(iat) = -dsqrt(dist(iat))
!!$         write(*,'(A,2(I3,2x),F12.6,3x,2(3(F12.6,1x),2x))') 'ifrag,iat,dist',ifrag,iat,dist(iat),&
!!$              at%astruct%cell_dim(:),rxyz_new_all(:,iat)

         rxyz_new_all(:,iat) = rxyz_new_all(:,iat)-frag_trans%rot_center_dest
      end do

      do iat=1,ref_frag%astruct_frg%nat
         rxyz_frg_new(:,iat)=rxyz_frg_new(:,iat)-frag_trans%rot_center_dest
      end do

      ! sort atoms into neighbour order
      call sort_positions(nat_not_frag,dist,ipiv)

      rxyz_new = f_malloc((/ 3,ref_frag%astruct_env%nat /),id='rxyz_new')

      ! take fragment and closest neighbours (assume that environment atoms were originally the closest)
      ! put mapping in column 2 to avoid overwriting later
      do iat=1,ref_frag%astruct_frg%nat
         rxyz_new(:,iat)=rxyz_frg_new(:,iat)
         frag_env_mapping(iat,2) = isfat+iat
      end do

      iatf=0
      do ityp=1,ntypes
         iatt=0
         if (num_neighbours_type(ityp)==0 .and. (.not. ignore_species)) cycle
         do iat=1,nat_not_frag
            !ipiv_shift needed for quantities which reference full rxyz, not rxyz_new_all which has already eliminated frag atoms
            if (ipiv(iat)<=isfat) then
               ipiv_shift=ipiv(iat)
            else
               ipiv_shift=ipiv(iat)+ref_frag%astruct_frg%nat
            end if
            if (iatype(ipiv_shift)/=ityp .and. (.not. ignore_species)) cycle
            iatf=iatf+1
            iatt=iatt+1
            rxyz_new(:,iatf+ref_frag%astruct_frg%nat)=rxyz_new_all(:,ipiv(iat))
            frag_env_mapping(iatf+ref_frag%astruct_frg%nat,2) = ipiv_shift
            if ((ignore_species.and.iatt==num_neighbours) &
                 .or. ((.not. ignore_species) .and. iatt==num_neighbours_type(ityp))) exit
         end do
         !write(*,'(a,5(i3,2x))')'ityp',ityp,ntypes,iatt,iatf,num_neighbours_type(ityp)
         if (ignore_species) exit
      end do

      !print*,'iatf,sum',iatf,sum(num_neighbours_type),num_neighbours,ignore_species
      if (((.not. ignore_species) .and. iatf/=sum(num_neighbours_type)) &
           .or. (ignore_species .and. iatf/=num_neighbours)) stop 'Error num_neighbours/=iatf in match_environment_atoms'

      call f_free(num_neighbours_type)
      call f_free(dist)
      call f_free(ipiv)
      call f_free(rxyz_frg_new)
      call f_free(rxyz_new_all)

      !# don't sort rxyz_ref - just check Wahba permutations for all atoms
      !# assuming small number of neighbours so saves generalizing things and makes it easier for mapping env -> full

!!$      do iat=1,ref_frag%astruct_env%nat
!!$         write(*,'(A,3(I3,2x),2x,2(3(F12.6,1x),2x))') 'ifrag,ifrag_ref,iat,rxyz_ref,rxyz_new',&
!!$              ifrag,ifrag_ref,iat,rxyz_ref_sorted(:,iat),rxyz_new(:,iat)
!!$      end do

      !ADD CHECKING OF ATOM TYPE TO ABOVE SORTING PROCEDURE, for the moment assuming identical atom types
      !if error is above some threshold and we have some degenerate distances
      !then try to find ordering which gives lowest Wahba error
      !also give preference to zero rotation
      !write(*,'(A)') 'Problem matching environment atoms to new coordinates, attempting to find correct order'
      !write(*,'(A)') 'Checking for ordering giving a more accurate transformation/no rotation'

      num_env=ref_frag%astruct_env%nat-ref_frag%astruct_frg%nat

      !assume that we have only a small number of identical distances, or this would become expensive...
      array_tmp=f_malloc(num_env,id='array_tmp')
      do i=1,num_env
         array_tmp(i)=i
      end do

      np=fact(num_env)
      permutations=f_malloc((/num_env,np/),id='permutations')
      c=0
      call reorder(num_env,num_env,c,np,array_tmp,permutations)
      call f_free(array_tmp)

      rxyz_new_trial = f_malloc((/ 3,ref_frag%astruct_env%nat /),id='rxyz_new_trial')

      !the fragment part doesn't change
      do iat=1,ref_frag%astruct_frg%nat
         rxyz_new_trial(:,iat) = rxyz_new(:,iat)
      end do

      minerror=1.d100
      minperm=-1
      mintheta=-1

      !test each permutation
      do i=1,np
         wrong_atom=.false.
         !first check that the atom types are coherent - if not reject this transformation
         do iat=ref_frag%astruct_frg%nat+1,ref_frag%astruct_env%nat
            atom_ref = trim(ref_frag%astruct_env%atomnames(ref_frag%astruct_env%iatype(iat)))
            ia=frag_env_mapping(permutations(iat-ref_frag%astruct_frg%nat,i)+ref_frag%astruct_frg%nat,2)
            if (iatype(ia)==ntypes .and. astruct_ghost%nat>0) then
               atom_trial='X'
            else
               atom_trial = trim(at%astruct%atomnames(iatype(ia)))
            end if
            !write(*,'(a,4(i3,2x),2(a2,2x),3(i3,2x))') 'i,np,iat,nat,atom_ref,atom_trial',i,np,iat,ref_frag%astruct_env%nat,&
            !      trim(atom_ref),trim(atom_trial),&
            !      frag_env_mapping(iat,2),permutations(iat-ref_frag%astruct_frg%nat,i),&
            !      frag_env_mapping(permutations(iat-ref_frag%astruct_frg%nat,i)+ref_frag%astruct_frg%nat,2)
            if (trim(atom_ref)/=trim(atom_trial) .and. (.not. ignore_species)) then
               wrong_atom=.true.
               exit
            end if
         end do
         if (wrong_atom) cycle

         do iat=ref_frag%astruct_frg%nat+1,ref_frag%astruct_env%nat
            rxyz_new_trial(:,iat) &
                 = rxyz_new(:,ref_frag%astruct_frg%nat &
                 + permutations(iat-ref_frag%astruct_frg%nat,i))
         end do
         call find_and_set_rotation(frag_trans,ref_frag%astruct_env%nat,src=rxyz_ref,&
              dest=rxyz_new_trial)
         !call find_frag_trans(ref_frag%astruct_env%nat,rxyz_ref,&
         !     rxyz_new_trial,frag_trans)
         !if (frag_trans%Werror > W_tol) call f_increment(itoo_big)

         !do iat=1,ref_frag%astruct_env%nat
         !   write(*,'(A,3(I3,2x),2x,2(3(F12.6,1x),2x))') 'ifrag,ifrag_ref,iat,rxyz_new,rxyz_ref',&
         !        ifrag,ifrag_ref,iat,rxyz_new_trial(:,iat),rxyz_ref(:,iat)
         !end do
         !write(*,'(A,I3,2x,3(I3,1x),1x,F12.6)') 'i,perms,error: ',i,permutations(:,i),frag_trans%Werror
         !prioritize no rotation, and if not possible 180 degrees
         !could improve logic/efficiency here, i.e. stop checking once below some threshold
         !if ((frag_trans%Werror < minerror .and. (mintheta/=0 .or. minerror-frag_trans%Werror>1e-6)) &
         !     .or. (frag_trans%Werror-minerror<1e-6.and.frag_trans%theta==0.0d0) then

         err_tol = 1e-3 !1e-6
         rot_tol = 1e-3 !1e-6
         ! less than minerror by more than some tol
         ! or ~same error and zero rotation (wrt tol)
         ! or ~same error, 180 rotation (wrt tol) and not already zero rotation
         if ( (frag_trans%Werror < minerror - err_tol) &
              .or. (abs(frag_trans%Werror - minerror) < err_tol .and. abs(frag_trans%theta - 0.0d0) < rot_tol)) then ! &
!              .or. (abs(frag_trans%Werror - minerror) < err_tol .and. mintheta /= 0.0d0 &
!                   .and. abs(frag_trans%theta - 4.0_gp*atan(1.d0)) < rot_tol) ) then

            mintheta = frag_trans%theta
            minerror = frag_trans%Werror
            minperm = i
         end if
      end do

      ! use this as final transformation
      if (minperm/=-1) then
         !LG: commented it out, maybe it might be useful for debugging
         !write(*,'(A,I3,2x,2(F12.6,2x))') 'Final value of cost function:',&
         !     minperm,minerror,mintheta/(4.0_gp*atan(1.d0)/180.0_gp)
         do iat=ref_frag%astruct_frg%nat+1,ref_frag%astruct_env%nat
            rxyz_new_trial(:,iat) = rxyz_new(:,ref_frag%astruct_frg%nat + permutations(iat-ref_frag%astruct_frg%nat,minperm))
         end do
         call find_and_set_rotation(frag_trans,ref_frag%astruct_env%nat,src=rxyz_ref,&
              dest=rxyz_new_trial)
         !call find_frag_trans(ref_frag%astruct_env%nat,rxyz_ref,rxyz_new_trial,frag_trans)
         !if (frag_trans%Werror > W_tol) call f_increment(itoo_big)

         do iat=1,ref_frag%astruct_frg%nat
            frag_env_mapping(iat,3) = frag_env_mapping(iat,2)
         end do
         do iat=ref_frag%astruct_frg%nat+1,ref_frag%astruct_env%nat
            frag_env_mapping(iat,3) = frag_env_mapping(ref_frag%astruct_frg%nat &
                 + permutations(iat-ref_frag%astruct_frg%nat,minperm),2)
         end do

         ! fill in 1st and 2nd columns of env_mapping
         itmb = 0
         do iat=1,ref_frag%astruct_env%nat
            do iorb=1,orbs%norb
               if (orbs%onwhichatom(iorb) == frag_env_mapping(iat,3)) then
                  itmb = itmb+1
                  frag_env_mapping(itmb,1) = iorb
                  frag_env_mapping(itmb,2) = iat
               end if
           end do
         end do
         if (itmb /= ref_frag%nbasis_env) stop 'Error with nbasis_env'

         !do iorb=1,ref_frag%nbasis_env
         !   write(*,'(A,5(1x,I4))') 'mapping: ',ifrag,ifrag_ref,frag_env_mapping(iorb,:)
         !end do

         !debug
         !do iat=1,ref_frag%astruct_env%nat
         !   write(*,'(a,4(i3,2x),a,2(3(f8.2,1x),4x))') 'if,ifr,ia,m,t',ifrag,ifrag_ref,iat,frag_env_mapping(iat,3),&
         !        trim(at%astruct%atomnames(at%astruct%iatype(frag_env_mapping(iat,3)))),rxyz_new_trial(:,iat),rxyz_ref(:,iat)
         !end do

      else
         stop 'Error finding environment transformation'
      end if

      call f_free(rxyz_new_trial)
      call f_free(permutations)
      call f_free(rxyz_ref)
      call f_free(rxyz_new)

      if (astruct_ghost%nat>0) then
         call f_free_ptr(iatype)
      else
         nullify(iatype)
      end if

      if (check_for_ghosts) then
         call deallocate_atomic_structure(astruct_ghost)
         call nullify_atomic_structure(astruct_ghost)
      end if


   contains

      recursive subroutine reorder(nf,n,c,np,array_in,permutations)
        implicit none

        integer, intent(in) :: nf,n,np
        integer, intent(inout) :: c
        integer, dimension(1:nf), intent(inout) :: array_in
        integer, dimension(1:nf,1:np), intent(inout) :: permutations

        integer :: i, tmp
        integer, dimension(1:nf) :: array_out

        if (n>1) then
           do i=n,1,-1
              array_out=array_in
              tmp=array_in(n)
              array_out(n)=array_in(i)
              array_out(i)=tmp
              !print*,'i',i,n,'in',array_in,'out',array_out
              call reorder(nf,n-1,c,np,array_out,permutations)
           end do
        else
           c=c+1
           !print*,c,array_in
           permutations(:,c)=array_in(:)
           return
        end if

      end subroutine reorder

      function fact(n)
        implicit none

        integer, intent(in) :: n
        integer :: fact

        integer :: i

        fact=1
        do i=1,n
           fact = fact * i
        end do

      end function

   end subroutine match_environment_atoms
end module io
