!> module defining the major operations which can be intentionally wrapped
!! from CheSS
module chess_utils
  use sparsematrix_types
  implicit none

  private

  integer, parameter, public :: KERNEL_=3,HAMILTONIAN_=2,OVERLAP_=1

  type, public :: matrixindex_lookup
      integer,dimension(:),pointer :: ind_compr => null()
  end type matrixindex_lookup

  type, public :: matrixindex_in_compressed_fortransposed2
      type(matrixindex_lookup),dimension(-1:1) :: section !< One section for the "negative" and one for the "positive" part (the 0 in the middle is unavoidable)
      integer :: offset_compr=0
  end type matrixindex_in_compressed_fortransposed2

  !!type,public :: matrixindex_in_compressed_fortransposed
  !!    integer,dimension(:),pointer :: ind_compr !< lookup arrays for transposed operations
  !!    integer :: offset_compr
  !!end type matrixindex_in_compressed_fortransposed

  type, public :: linmat_auxiliary
      !!type(matrixindex_in_compressed_fortransposed),dimension(:),pointer :: mat_ind_compr
      type(matrixindex_in_compressed_fortransposed2),dimension(:),pointer :: mat_ind_compr2 => null()
  end type linmat_auxiliary

  type, public :: chess_matrix
     type(sparse_matrix), pointer :: metadata => null()
     type(matrices), pointer :: data => null()
     type(linmat_auxiliary), pointer :: aux => null()
  end type chess_matrix

  type, public :: linear_matrices
    type(sparse_matrix),dimension(3) :: smat !< small: sparsity pattern given by support function cutoff
                                             !! medium: sparsity pattern given by SHAMOP cutoff
                                             !! medium: sparsity pattern given by kernel cutoff

    type(sparse_matrix),dimension(:),pointer :: ks !< sparsity pattern for the KS orbitals (i.e. dense); spin up and down
    type(sparse_matrix),dimension(:),pointer :: ks_e !< sparsity pattern for the KS orbitals including extra stated (i.e. dense); spin up and down
    type(sparse_matrix_metadata) :: smmd !< metadata of the sparse matrices
    type(matrices) :: ham_, ovrlp_, kernel_
    type(matrices),dimension(3) :: ovrlppowers_
    type(linmat_auxiliary) :: auxs
    type(linmat_auxiliary) :: auxm
    type(linmat_auxiliary) :: auxl
  end type linear_matrices


  public :: linear_matrices_null, linmat_auxiliary_null, deallocate_linmat_auxiliary
  public :: deallocate_linear_matrices, nullify_linear_matrices
  !public :: matrixindex_in_compressed_fortransposed_null
  public :: matrixindex_in_compressed_fortransposed2_null
  public :: sparsematrix_axpy, sparsematrix_traceAB
  public :: chess_matrix_map, release_chess_matrix

  contains

  function matrixindex_in_compressed_fortransposed2_null() result (mat_ind_compr)
    implicit none
    type(matrixindex_in_compressed_fortransposed2) :: mat_ind_compr
    nullify(mat_ind_compr%section(-1)%ind_compr)
    nullify(mat_ind_compr%section(0)%ind_compr)
    nullify(mat_ind_compr%section(1)%ind_compr)
    mat_ind_compr%offset_compr = 0
  end function matrixindex_in_compressed_fortransposed2_null

  !function matrixindex_in_compressed_fortransposed_null() result (mat_ind_compr)
  !  implicit none
  !  type(matrixindex_in_compressed_fortransposed) :: mat_ind_compr
  !  nullify(mat_ind_compr%ind_compr)
  !  mat_ind_compr%offset_compr = 0
  !end function matrixindex_in_compressed_fortransposed_null

  pure subroutine nullify_linmat_auxiliary(aux)
    implicit none
    type(linmat_auxiliary), intent(out) :: aux
    !nullify(aux%mat_ind_compr)
    !aux%mat_ind_compr2 = matrixindex_in_compressed_fortransposed2_null()
    nullify(aux%mat_ind_compr2)
  end subroutine nullify_linmat_auxiliary

  pure function linmat_auxiliary_null() result (aux)
    implicit none
    type(linmat_auxiliary) :: aux
    call nullify_linmat_auxiliary(aux)
  end function linmat_auxiliary_null

  pure subroutine nullify_linear_matrices(linmat)
    use sparsematrix_memory, only: nullify_sparse_matrix_metadata, &
         & nullify_sparse_matrix, nullify_matrices
    implicit none
    type(linear_matrices), intent(out) :: linmat
    integer :: i
    call nullify_sparse_matrix_metadata(linmat%smmd)
    call nullify_sparse_matrix(linmat%smat(1))
    call nullify_sparse_matrix(linmat%smat(2))
    call nullify_sparse_matrix(linmat%smat(3))
    nullify(linmat%ks)
    nullify(linmat%ks_e)
    call nullify_matrices(linmat%ovrlp_)
    call nullify_matrices(linmat%ham_)
    call nullify_matrices(linmat%kernel_)
    do i=1,size(linmat%ovrlppowers_)
       call nullify_matrices(linmat%ovrlppowers_(i))
    end do
    call nullify_linmat_auxiliary(linmat%auxs)
    call nullify_linmat_auxiliary(linmat%auxm)
    call nullify_linmat_auxiliary(linmat%auxl)
  end subroutine nullify_linear_matrices

  pure function linear_matrices_null() result(linmat)
    implicit none
    type(linear_matrices) :: linmat
    call nullify_linear_matrices(linmat)
  end function linear_matrices_null

  !subroutine deallocate_matrixindex_in_compressed_fortransposed(mat_ind_compr)
  !  implicit none
  !  type(matrixindex_in_compressed_fortransposed),intent(inout) :: mat_ind_compr
  !  call f_free_ptr(mat_ind_compr%ind_compr)
  !end subroutine deallocate_matrixindex_in_compressed_fortransposed

  subroutine deallocate_matrixindex_in_compressed_fortransposed2(mat_ind_compr)
    use module_bigdft_arrays
    implicit none
    type(matrixindex_in_compressed_fortransposed2),intent(inout) :: mat_ind_compr
    call f_free_ptr(mat_ind_compr%section(-1)%ind_compr)
    call f_free_ptr(mat_ind_compr%section(0)%ind_compr)
    call f_free_ptr(mat_ind_compr%section(1)%ind_compr)
  end subroutine deallocate_matrixindex_in_compressed_fortransposed2

  subroutine deallocate_linmat_auxiliary(aux)
    implicit none
    type(linmat_auxiliary),intent(inout) :: aux
    integer :: i
    !!do i=lbound(aux%mat_ind_compr,1),ubound(aux%mat_ind_compr,1)
    !!    call deallocate_matrixindex_in_compressed_fortransposed(aux%mat_ind_compr(i))
    !!end do
    !!deallocate(aux%mat_ind_compr)

    do i=lbound(aux%mat_ind_compr2,1),ubound(aux%mat_ind_compr2,1)
        call deallocate_matrixindex_in_compressed_fortransposed2(aux%mat_ind_compr2(i))
    end do
    deallocate(aux%mat_ind_compr2)
  end subroutine deallocate_linmat_auxiliary

  subroutine deallocate_linear_matrices(linmat)
    use sparsematrix_memory, only: deallocate_sparse_matrix_metadata, &
                                   deallocate_sparse_matrix, &
                                   deallocate_matrices
    implicit none
    type(linear_matrices),intent(inout) :: linmat
    integer :: i, ispin
    call deallocate_sparse_matrix_metadata(linmat%smmd)
    call deallocate_sparse_matrix(linmat%smat(1))
    call deallocate_sparse_matrix(linmat%smat(2))
    call deallocate_sparse_matrix(linmat%smat(3))
    call deallocate_matrices(linmat%ovrlp_)
    call deallocate_matrices(linmat%ham_)
    call deallocate_matrices(linmat%kernel_)
    do i=1,size(linmat%ovrlppowers_)
        call deallocate_matrices(linmat%ovrlppowers_(i))
    end do
    if (associated(linmat%ks)) then
        do ispin=lbound(linmat%ks,1),ubound(linmat%ks,1)
            call deallocate_sparse_matrix(linmat%ks(ispin))
        end do
        deallocate(linmat%ks)
    end if
    if (associated(linmat%ks_e)) then
        do ispin=lbound(linmat%ks_e,1),ubound(linmat%ks_e,1)
            call deallocate_sparse_matrix(linmat%ks_e(ispin))
        end do
        deallocate(linmat%ks_e)
    end if
    call deallocate_linmat_auxiliary(linmat%auxs)
    call deallocate_linmat_auxiliary(linmat%auxm)
    call deallocate_linmat_auxiliary(linmat%auxl)
  end subroutine deallocate_linear_matrices

  function chess_matrix_map(data, metadata, aux, from, source) result(mat)
     implicit none
     integer, intent(in), optional :: source
     type(sparse_matrix), intent(in), target, optional :: metadata
     type(matrices), intent(in), target, optional :: data
     type(linmat_auxiliary), intent(in), target, optional :: aux
     type(linear_matrices), intent(in), target, optional :: from
     type(chess_matrix) :: mat

     if (present(from)) then
  		mat%metadata => from%smat(source)
     	select case(source)
     	case(OVERLAP_)
  			mat%data => from%ovrlp_
  			mat%aux => from%auxs
  		case(HAMILTONIAN_)
  			mat%data => from%ham_
  			mat%aux => from%auxm
  		case(KERNEL_)
  			mat%data => from%kernel_
  			mat%aux => from%auxl
  		end select
     end if
     if (present(data)) mat%data => data
     if (present(metadata)) mat%metadata => metadata
     if (present(aux)) mat%aux => aux
  end function chess_matrix_map

  subroutine release_chess_matrix(mat)
   implicit none
   type(chess_matrix), intent(inout) :: mat
   nullify(mat%data)
   nullify(mat%metadata)
   nullify(mat%aux)
  end subroutine release_chess_matrix

    subroutine sparsematrix_axpy(a, X, Y)
       implicit none
       real(mp), intent(in) :: a
       type(chess_matrix), intent(in) :: X
       type(chess_matrix), intent(inout) :: Y

       !we should write it in a way such as to be compliant with the sparsity pattern of Y
       call daxpy(Y%metadata%nvctr*Y%metadata%nspin,a,&
                  X%data%matrix_compr,1,Y%data%matrix_compr,1)

    end subroutine sparsematrix_axpy

    function sparsematrix_traceAB(iproc, nproc, comm, A, B) result(energy)
     use sparsematrix, only: trace_AB
     use sparsematrix_base
     use time_profiling
     implicit none
     integer, intent(in) :: iproc, nproc, comm
     type(chess_matrix), intent(in) :: A, B
     real(mp) :: energy
     !local variables
     integer :: ispin
        
      call f_timing(TCAT_SMAT_MULTIPLICATION,'ON')
      energy=0.0_mp
      do ispin=1,B%metadata%nspin
          energy = energy + trace_AB(iproc, nproc, comm, A%metadata, B%metadata,&
                                     A%data, B%data, ispin)
      end do
      call f_timing(TCAT_SMAT_MULTIPLICATION,'OF')
    end function


end module chess_utils