#ifndef OPENCL_WRAPPERS_H
#define OPENCL_WRAPPERS_H
#define CL_USE_DEPRECATED_OPENCL_1_0_APIS
#define CL_USE_DEPRECATED_OPENCL_1_1_APIS
#include <stdio.h>
#include <stdlib.h>
#include <config.h>
#include <math.h>
#include <assert.h>
#include <liborbs_utils.h>
#include <liborbs_ocl.h>
//#include <Tool.h>
//#include <time.h>

/** @file OpenCL_wrappers.h
 *  @brief Contains global declarations and fortran bindings for OpenCL convolutions.
 *  Warning : every fortran visible procedure is passing its argument per address.
 *  Warning : every floating point data is double precision.
 *  Warning : aliasing of buffer is not supported.
 */

/** Activate debugging info. */
#define DEBUG 0
/** Activate profiling info. */
#define PROFILING 0

struct bigdft_kernels {
  struct liborbs_kernels parent;
  cl_kernel c_initialize_kernel_d;
  cl_kernel p_initialize_kernel_d;
  cl_kernel reduction_dot_kernel_d;
  cl_kernel void_kernel;
  cl_kernel gemm_kernel_d_tb;
  cl_kernel gemm_kernel_d_ta;
  cl_kernel gemm_kernel_d_tatb;
  cl_kernel gemm_kernel_z;
  cl_kernel gemm_kernel_z_tb;
  cl_kernel gemm_kernel_z_cb;
  cl_kernel gemm_kernel_z_ta;
  cl_kernel gemm_kernel_z_ca;
  cl_kernel gemm_kernel_z_tatb;
  cl_kernel gemm_kernel_z_catb;
  cl_kernel gemm_kernel_z_tacb;
  cl_kernel gemm_kernel_z_cacb;
  cl_kernel benchmark_flops_kernel_d;
  cl_kernel benchmark_mops_kernel_d;
  cl_kernel transpose_kernel_d;
  cl_kernel notranspose_kernel_d;
  cl_kernel fft_kernel_d0_d;
  cl_kernel fft_kernel_d1_d;
  cl_kernel fft_kernel_d2_d;
  cl_kernel fft_kernel_d0_r2c_d;
  cl_kernel fft_kernel_d1_r2c_d;
  cl_kernel fft_kernel_d2_r2c_d;
  cl_kernel fft_kernel_d0_r_d;
  cl_kernel fft_kernel_d1_r_d;
  cl_kernel fft_kernel_d2_r_d;
  cl_kernel fft_kernel_d0_r_c2r_d;
  cl_kernel fft_kernel_d1_r_c2r_d;
  cl_kernel fft_kernel_d2_r_c2r_d;
  cl_kernel fft_kernel_k_d0_d;
  cl_kernel fft_kernel_k_d1_d;
  cl_kernel fft_kernel_k_d2_d;
  cl_kernel fft_kernel_d0_f_d;
  cl_kernel fft_kernel_d1_f_d;
  cl_kernel fft_kernel_d2_f_d;
  cl_kernel fft_kernel_d0_r2c_f_d;
  cl_kernel fft_kernel_d1_r2c_f_d;
  cl_kernel fft_kernel_d2_r2c_f_d;
  cl_kernel fft_kernel_d0_r_f_d;
  cl_kernel fft_kernel_d1_r_f_d;
  cl_kernel fft_kernel_d2_r_f_d;
  cl_kernel fft_kernel_d0_r_c2r_f_d;
  cl_kernel fft_kernel_d1_r_c2r_f_d;
  cl_kernel fft_kernel_d2_r_c2r_f_d;
  cl_kernel fft_kernel_k_d0_f_d;
  cl_kernel fft_kernel_k_d1_f_d;
  cl_kernel fft_kernel_k_d2_f_d;
};

/** Structure associating an OpenCL event with a comment, for profiling purpose. */
typedef struct {
	cl_event e;
	char *comment;
} event;

struct _bigdft_context {
  liborbs_context parent;
  cl_program benchmarkProgram;
  cl_program fftProgramd0;
  cl_program fftProgramd1;
  cl_program fftProgramd2;
  cl_program dgemmProgram;
  cl_program synProgram;
  cl_mem cossind0;
  cl_mem cossind1;
  cl_mem cossind2;
  cl_uint fft_size[3];
  event * event_list;
  size_t event_number;
  size_t event_allocated;
};

typedef struct _bigdft_context * bigdft_context;

struct _bigdft_command_queue {
  liborbs_command_queue parent;
  bigdft_context context;
  struct bigdft_kernels kernels;
};

typedef struct _bigdft_command_queue * bigdft_command_queue;

void FC_FUNC_(customize_fft,CUSTOMIZE_FFT)(bigdft_context * context, cl_uint *dimensions);

/** Recovers device info used by BigDFT code generator. */
/** Creates all bigdft kernels*/
void create_kernels(bigdft_context * context, struct bigdft_kernels *kernels);
/** Creates magicfilter kernels. to be called after building the magicfilter programs. */
void create_benchmark_kernels(bigdft_context * context, struct bigdft_kernels * kernels);
void create_initialize_kernels(bigdft_context * context, struct bigdft_kernels * kernels);
void create_fft_kernels(bigdft_context * context, struct bigdft_kernels * kernels);
/** Compiles magicfilter programs in the given context. */
void build_benchmark_programs(bigdft_context * context);
void build_fft_programs(bigdft_context * context);
/** Releases magicfilter kernels. */
void clean_benchmark_kernels(struct bigdft_kernels * kernels);
void clean_initialize_kernels(struct bigdft_kernels * kernels);
void clean_fft_kernels(bigdft_context * context, struct bigdft_kernels * kernels);
/** Releases magicfilter programs. */
void clean_benchmark_programs(bigdft_context * context);
void clean_fft_programs(bigdft_context * context);

/** Adds an event to the event list. */
int addToEventList(bigdft_context * context, event ev);

/** Reads the processor time stamp counter. */
void FC_FUNC_(rdtsc,RDTSC)(cl_ulong * t);
/** Return the real-time clock time in nanosecond since the epoch. */
void FC_FUNC_(nanosec,NANOSEC)(cl_ulong * t);

/** Initializes the event list. For profiling purpose. */
void FC_FUNC_(init_event_list,INIT_EVENT_LIST)(bigdft_context * context);
/** Prints the event list. */
void FC_FUNC_(print_event_list,PRINT_EVENT_LIST)(bigdft_context * context);
/** Buids and create the OpenCL kernel int the given context. */
void FC_FUNC_(ocl_build_programs,OCL_BUILD_PROGRAMS)(bigdft_context * context);
/** Creates a context containing devices of the type specified from the chosen platform*/
void FC_FUNC_(ocl_create_context,OCL_CREATE_CONTEXT)(bigdft_context * context, const char * platform, const char * devices, cl_int *device_type, cl_uint *device_number);
/** Creates a context containing all GPUs from the default platform */
void FC_FUNC_(ocl_create_gpu_context,OCL_CREATE_GPU_CONTEXT)(bigdft_context * context, cl_uint *device_number);
/** Creates a context containing all CPUs from the default platform */
void FC_FUNC_(ocl_create_cpu_context,OCL_CREATE_CPU_CONTEXT)(bigdft_context * context);
void FC_FUNC_(ocl_create_read_buffer_and_copy,OCL_CREATE_READ_BUFFER_AND_COPY)(bigdft_context *context, cl_uint *size, void *host_ptr, cl_mem *buff_ptr);
/** Creates a OpenCL write only buffer.
 *  @param context where the buffer is created.
 *  @param size of the buffer.
 *  @param buff_ptr return value : a buffer object reference.
 */
void FC_FUNC_(ocl_create_write_buffer,OCL_CREATE_WRITE_BUFFER)(bigdft_context *context, cl_uint *size, cl_mem *buff_ptr);
/** Creates a command queue in the given context, associating it to the first device in the context */
void FC_FUNC_(ocl_create_command_queue,OCL_CREATE_COMMAND_QUEUE)(bigdft_command_queue *command_queue, bigdft_context *context);
/** Creates a command queue in the given context, associating it to the device specified by index modulo the number of device. */
void FC_FUNC_(ocl_create_command_queue_id,OCL_CREATE_COMMAND_QUEUE_ID)(bigdft_command_queue *command_queue, bigdft_context *context, cl_uint *index);
/** Waits for all commands in a queue to complete. */
void FC_FUNC_(ocl_finish,OCL_FINISH)(bigdft_command_queue *command_queue);
/** Enqueues a barrier in a queue. Commands enqueued after the barrier will wait
 *  for commands enqueued before the barrier to be processed before being sent to the device. */
void FC_FUNC_(ocl_enqueue_barrier,OCL_ENQUEUE_BARRIER)(bigdft_command_queue *command_queue);
/** Releases a command queue and the associated kernels. */
void FC_FUNC_(ocl_clean_command_queue,OCL_CLEAN_COMMAND_QUEUE)(bigdft_command_queue *command_queue);
/** Releases the context, and beforehand releases the programs. */
void FC_FUNC_(ocl_clean,OCL_CLEAN)(bigdft_context *context);


/** Benchmark to evaluate the throughput of the copy mechanism used in the convolutions.
 *  @param command_queue used to process the convolution.
 *  @param n size of the first dimension.
 *  @param ndat size of the second dimension.
 *  @param psi input buffer of size ndat * n * sizeof(double), stored in column major order.
 *  @param out output buffer of size ndat * n * sizeof(double), stored in column major order.
 */
void FC_FUNC_(notranspose_d,NOTRANSPOSE_D)(bigdft_command_queue *command_queue, cl_uint *n,cl_uint *ndat,cl_mem *psi,cl_mem *out);
/** Benchmark to evaluate the throughput of the transposition mechanism used in the convolutions.
 *  @param command_queue used to process the convolution.
 *  @param n size of the first dimension.
 *  @param ndat size of the second dimension.
 *  @param psi input buffer of size ndat * n * sizeof(double), stored in column major order.
 *  @param out output buffer of size n * ndat * sizeof(double), stored in column major order.
 */
void FC_FUNC_(transpose_d,TRANSPOSE_D)(bigdft_command_queue *command_queue, cl_uint *n,cl_uint *ndat,cl_mem *psi,cl_mem *out);
/** Benchmark to evaluate the throughput of the OpenCL device in FLOPS, each element processed generates 4096 FLOP.
 *  @param command_queue used to process the convolution.
 *  @param n number of elements.
 *  @param in input buffer of size n * sizeof(double), stored in column major order.
 *  @param out output buffer of size n * sizeof(double), stored in column major order.
 */
void FC_FUNC_(benchmark_flops_d,BENCHMARK_FLOPS_D)(bigdft_command_queue *command_queue, cl_uint *n, cl_mem *in, cl_mem *out);
/** Benchmark to evaluate the throughput of the OpenCL device global memory in MOPS, each element processed generates 8 read and 8 write, which are coalesced.
 *  @param command_queue used to process the convolution.
 *  @param n number of elements.
 *  @param in input buffer of size n * sizeof(double), stored in column major order.
 *  @param out output buffer of size n * sizeof(double), stored in column major order.
 */
void FC_FUNC_(benchmark_mops_d,BENCHMARK_MOPS_D)(bigdft_command_queue *command_queue, cl_uint *n, cl_mem *in, cl_mem *out);


void FC_FUNC_(fft1d_d,FFT1D_D)(bigdft_command_queue *command_queue, cl_uint *n,cl_uint *ndat,cl_mem *psi,cl_mem *out);
void FC_FUNC_(fft1d_r_d,FFT1D_R_D)(bigdft_command_queue *command_queue, cl_uint *n,cl_uint *ndat,cl_mem *psi,cl_mem *out);
void FC_FUNC_(fft3d_d,FFT3D_D)(bigdft_command_queue *command_queue, cl_uint *dimensions,cl_mem *psi,cl_mem *out,cl_mem *tmp);
void FC_FUNC_(fft3d_k_r2c_d,FFT3D_K_R2C_D)(bigdft_command_queue *command_queue, cl_uint *dimensions,cl_mem *psi,cl_mem *out,cl_mem *tmp,cl_mem *k);
void FC_FUNC_(fft3d_r_d,FFT3D_R_D)(bigdft_command_queue *command_queue, cl_uint *dimensions,cl_mem *psi,cl_mem *out,cl_mem *tmp);
void FC_FUNC_(fft3d_r_c2r_d,FFT1D_R_C2R_D)(bigdft_command_queue *command_queue, cl_uint *dimensions,cl_mem *psi,cl_mem *out,cl_mem *tmp);
void FC_FUNC_(fft3d_k_r2c_d_generic,FFT1D_K_R2C_D_GENERIC)(bigdft_command_queue *command_queue, cl_uint *dimensions, cl_uint *periodic, cl_mem *psi,cl_mem *out,cl_mem *tmp,cl_mem *k);
void FC_FUNC_(fft3d_r_c2r_d_generic,FFT1D_R_C2R_D_GENERIC)(bigdft_command_queue *command_queue, cl_uint *dimensions, cl_uint *periodic, cl_mem *psi,cl_mem *out,cl_mem *tmp);
void FC_FUNC_(ocl_create_read_buffer_host,OCL_CREATE_READ_BUFFER_HOST)(bigdft_context *context, cl_uint *size, void *host_ptr, cl_mem *buff_ptr );
void FC_FUNC_(ocl_create_write_buffer_host,OCL_CREATE_WRITE_BUFFER_HOST)(bigdft_context *context, cl_uint *size, void *host_ptr, cl_mem *buff_ptr );
void FC_FUNC_(ocl_create_read_write_buffer_host,OCL_CREATE_READ_WRITE_BUFFER_HOST)(bigdft_context *context, cl_uint *size, void *host_ptr, cl_mem *buff_ptr );
void FC_FUNC_(ocl_map_read_buffer,OCL_MAP_READ_BUFFER)(bigdft_command_queue *command_queue, cl_mem *buff_ptr, cl_uint *offset, cl_uint *size);
void FC_FUNC_(ocl_map_write_buffer,OCL_MAP_WRITE_BUFFER)(bigdft_command_queue *command_queue, cl_mem *buff_ptr, cl_uint *offset, cl_uint *size);
void FC_FUNC_(ocl_map_read_write_buffer,OCL_MAP_READ_WRITE_BUFFER)(bigdft_command_queue *command_queue, cl_mem *buff_ptr, cl_uint *offset, cl_uint *size);
void FC_FUNC_(ocl_map_read_write_buffer_async,OCL_MAP_READ_WRITE_BUFFER_ASYNC)(bigdft_command_queue *command_queue, cl_mem *buff_ptr, cl_uint *offset, cl_uint *size);
void FC_FUNC_(ocl_pin_read_write_buffer,OCL_PIN_READ_WRITE_BUFFER)(bigdft_context *context, bigdft_command_queue *command_queue, cl_uint *size, void *host_ptr, cl_mem *buff_ptr );
void FC_FUNC_(ocl_pin_write_buffer,OCL_PIN_WRITE_BUFFER)(bigdft_context *context, bigdft_command_queue *command_queue, cl_uint *size, void *host_ptr, cl_mem *buff_ptr );
void FC_FUNC_(ocl_pin_read_write_buffer_async,OCL_PIN_READ_WRITE_BUFFER_ASYNC)(bigdft_context *context, bigdft_command_queue *command_queue, cl_uint *size, void *host_ptr, cl_mem *buff_ptr );
void FC_FUNC_(ocl_pin_write_buffer_async,OCL_PIN_WRITE_BUFFER_ASYNC)(bigdft_context *context, bigdft_command_queue *command_queue, cl_uint *size, void *host_ptr, cl_mem *buff_ptr );

#endif
