!!subroutine gauss_to_ISF(hgrid,factor,gau_cen,gau_a,n_gau,&!no err, errsuc
!!     nmax,n_left,n_right,cISF,err_norm,&                      !no err_wav. nmax instead of n_intvx
!!     ww,nwork,periodic)                         !added work arrays ww with dimension nwork
!!  use module_base
!!  implicit none
!!  logical, intent(in) :: periodic
!!  integer, intent(in) :: n_gau,nmax,nwork
!!  real(gp), intent(in) :: hgrid,factor,gau_cen,gau_a
!!  real(wp), dimension(0:nwork,2), intent(inout) :: ww 
!!  integer, intent(out) :: n_left,n_right
!!  real(gp), intent(out) :: err_norm
!!  real(wp), dimension(0:nmax), intent(out) :: cISF
!!  !local variables
!!  integer :: rightx,leftx,right_t,i0,i,k,length,j
!!  real(gp) :: a,z0,h,theor_norm2,x,r,coeff,r2,error,fac
!!  real(dp) :: cn2,tt
!!  real(wp) :: func
!!  integer, dimension(0:4) :: lefts,rights
!!  !include the convolutions filters
!!  include 'recs16.inc' !< MAGIC FILTER  
!!  include 'intots.inc' !< HERE WE KEEP THE ANALYTICAL NORMS OF GAUSSIANS
!!  include 'sym_16.inc' !< WAVELET FILTERS

!!  !rescale the parameters so that hgrid goes to 1.d0  
!!  a=gau_a/hgrid

!!  i0=nint(gau_cen/hgrid) ! the array is centered at i0
!!  z0=gau_cen/hgrid-real(i0,gp)
!!  h=.125_gp*.5_gp

!!  !calculate the array sizes;
!!  !at level 0, positions shifted by i0 
!!  right_t= ceiling(15.d0*a)

!!  ! initialise array
!!  cISF=0.0_gp


!!  if (periodic) then
!!     !we expand the whole Gaussian in scfunctions and later fold one of its tails periodically
!!     !we limit however the folding to one cell on each side (it can be eliminated)
!!     !!     lefts( 0)=max(i0-right_t,-nmax)
!!     !!     rights(0)=min(i0+right_t,2*nmax)
!!
!!     lefts( 0)=i0-right_t
!!     rights(0)=i0+right_t
!!
!!
!!     call gauss_to_scf()
!!
!!     ! special for periodic case:
!!     call fold_tail
!!  else
!!     ! non-periodic: the Gaussian is bounded by the cell borders
!!     lefts( 0)=max(i0-right_t,   0)
!!     rights(0)=min(i0+right_t,nmax/2) !nmax is even
!!
!!     call gauss_to_scf
!!
!!     ! non-periodic: no tails to fold
!!     do i=0,length-1
!!        cISF(i+n_left)=ww(i       ,2) !n_left..n_right <->    0  ..  length-1
!!     end do
!!  endif
!!
!!  !calculate the (relative) error
!!  cn2=0.0_dp
!!  do i=0,length
!!     tt=real(ww(i,2),dp)
!!     cn2=cn2+tt**2
!!  end do
!!
!!  theor_norm2=valints(n_gau)*a**(2*n_gau+1)
!!
!!  error=sqrt(abs(1.0_gp-real(cn2,gp)/theor_norm2))
!!
!!  !write(*,*)'error, non scaled:',error
!!  !
!!  !RESCALE BACK THE COEFFICIENTS AND THE ERROR
!!  fac= hgrid**n_gau*sqrt(hgrid)*factor
!!  cISF=real(fac,wp)*cISF
!!  err_norm=error*fac

!!contains

!!  !> Once the bounds LEFTS(0) and RIGHTS(0) of the expansion coefficient array
!!  !! are fixed, we get the expansion coefficients in the usual way:
!!  !! get them on the finest grid by quadrature
!!  !! then forward transform to get the coeffs on the coarser grid.
!!  !! All this is done assuming nonperiodic boundary conditions
!!  !! but will also work in the periodic case if the tails are folded
!!  subroutine gauss_to_scf
!!
!!    do k=1,4
!!       rights(k)=2*rights(k-1)+m
!!       lefts( k)=2*lefts( k-1)-m
!!    enddo
!!
!!    leftx = lefts(4)-n
!!    rightx=rights(4)+n  
!!
!!    !do not do anything if the gaussian is too extended
!!    if (rightx-leftx > nwork) then
!!       STOP 'gaustoisf'
!!       return
!!    end if
!!
!!    n_left=lefts(1)-n
!!    n_right=rights(1)+n
!!    length=n_right-n_left+1
!!
!!    print *,'nleft,nright',n_left,n_right
!!
!!
!!    !calculate the expansion coefficients at level 4, positions shifted by 16*i0 
!!
!!    !corrected for avoiding 0**0 problem
!!    if (n_gau == 0) then
!!       do i=leftx,rightx
!!          x=real(i-i0*16,gp)*h
!!          r=x-z0
!!          r2=r/a
!!          r2=r2*r2
!!          r2=0.5_gp*r2
!!          func=real(dexp(-real(r2,kind=8)),wp)
!!          ww(i-leftx,1)=func
!!       enddo
!!    else
!!       do i=leftx,rightx
!!          x=real(i-i0*16,gp)*h
!!          r=x-z0
!!          coeff=r**n_gau
!!          r2=r/a
!!          r2=r2*r2
!!          r2=0.5_gp*r2
!!          func=real(dexp(-real(r2,kind=8)),wp)
!!          func=real(coeff,wp)*func
!!          ww(i-leftx,1)=func
!!       enddo
!!    end if
!!
!!    call apply_w(ww(:,1),ww(:,2),&
!!         leftx   ,rightx   ,lefts(4),rights(4),h)
!!
!!    call forward_c(ww(:,2),ww(:,1),&
!!         lefts(4),rights(4),lefts(3),rights(3)) 
!!    call forward_c(ww(:,1),ww(:,2),&
!!         lefts(3),rights(3),lefts(2),rights(2)) 
!!    call forward_c(ww(:,2),ww(:,1),&
!!         lefts(2),rights(2),lefts(1),rights(1)) 
!!    !here inverse magic filter can be applied
!!    call apply_inverse_w(ww(:,1),ww(:,2),&
!!         lefts(1),rights(1),lefts(1)-n,rights(1)+n,h)
!!
!!
!!    !call forward(  ww(:,1),ww(:,2),&
!!    !     lefts(1),rights(1),lefts(0),rights(0)) 
!!
!!
!!  END SUBROUTINE gauss_to_scf
!!
!!
!!  !> One of the tails of the Gaussian is folded periodically
!!  !! We assume that the situation when we need to fold both tails
!!  !! will never arise
!!  subroutine fold_tail
!!
!!    !modification of the calculation.
!!    !at this stage the values of c are fixed to zero
!!
!!    do i=n_left,n_right
!!       j=modulo(i,nmax+1)
!!       cISF(j)=cISF(j)+ww(i-n_left       ,2)
!!    end do
!!
!!  END SUBROUTINE fold_tail
!!end subroutine gauss_to_ISF

subroutine gauss_c_to_daub_k(hgrid,kval,ncplx,gau_bf,ncs_s,factor , &
     gau_cen,gau_a, n_gau,&!no err, errsuc
     nmax,n_left,n_right,c,& 
     ww,nwork,periodic, hcutoff)      !added work arrays ww with dimension nwork
  use module_precisions
  implicit none
  logical, intent(in) :: periodic
  integer, intent(in) :: n_gau,nmax,nwork,ncs_s,ncplx
  real(gp), intent(in) :: hgrid,factor,gau_cen,gau_a,gau_bf
  real(wp), dimension(0:nwork,2,ncs_s, ncplx), intent(inout) :: ww 
  integer, intent(out) :: n_left,n_right
  real(wp), dimension(  ncs_s,ncplx,0:nmax,2), intent(out) :: c
  real(gp)  hcutoff

  !local variables
!  real(gp), parameter :: pi=3.141592653589793_gp
  integer :: rightx,leftx,right_t,i0,i,k,length,j,ics, icplx
  real(gp) :: a,z0,h,x,r,coeff,r2,fac
  real(wp) :: func,cval,sval
  integer, dimension(0:8) :: lefts,rights
  integer :: nrefinement, nforwards, ifwdtarget , ifwdsource, iswap
  real(gp) gau_kval, kval
  real(gp) cutoff, pishift

  !include the convolutions filters
  include 'recs16.inc'! MAGIC FILTER  
  !include 'intots.inc'! HERE WE KEEP THE ANALYTICAL NORMS OF GAUSSIANS
  include 'sym_16.inc'! WAVELET FILTERS

  !rescale the parameters so that hgrid goes to 1.d0  
  a=gau_a/hgrid
  gau_kval=gau_bf*hgrid*hgrid

  i0=nint(gau_cen/hgrid) ! the array is centered at i0

  z0=gau_cen/hgrid-real(i0,gp)
  cutoff= hcutoff /hgrid

  nrefinement=64
  nforwards=6

  h = (16 * .125_gp*.5_gp)/ nrefinement

  !calculate the array sizes;
  !at level 0, positions shifted by i0 
  right_t= ceiling(15.d0*a)

  !print *,'a,right_t',a,right_t,gau_a,hgrid

  !to rescale back the coefficients
  fac = hgrid**n_gau*sqrt(hgrid)*factor


  !initialise array
  c=0.0_gp

  if (periodic) then
     !we expand the whole Gaussian in scfunctions and later fold one of its tails periodically
     !we limit however the folding to one cell on each side 
     !!(commented out)
     !!     lefts( 0)=max(i0-right_t,-nmax)
     !!     rights(0)=min(i0+right_t,2*nmax)

     lefts( 0)=i0-right_t
     rights(0)=i0+right_t
     
     call gauss_c_to_scf()
     
     ! special for periodic case:
     call fold_tail
  else
     ! non-periodic: the Gaussian is bounded by the cell borders
     lefts( 0)=max(i0-right_t,   0)
     rights(0)=min(i0+right_t,nmax)

     call gauss_c_to_scf
     
     !loop for each complex component
     do icplx=1,ncplx
        do ics=1,ncs_s
           ! non-periodic: no tails to fold
           do i=0,length-1
              c( ics,icplx,i+n_left,1)=fac*ww(i       ,2,ics, icplx)
              c( ics,icplx,i+n_left,2)=fac*ww(i+length,2,ics, icplx) 
           end do
        end do
     end do
  endif


contains

  subroutine gauss_c_to_scf
    use numerics
    ! Once the bounds LEFTS(0) and RIGHTS(0) of the expansion coefficient array
    ! are fixed, we get the expansion coefficients in the usual way:
    ! get them on the finest grid by quadrature
    ! then forward transform to get the coeffs on the coarser grid.
    ! All this is done assuming nonperiodic boundary conditions
    ! but will also work in the periodic case if the tails are folded
    n_left=lefts(0)
    n_right=rights(0)
    length=n_right-n_left+1

    !print *,'nleft,nright',n_left,n_right

    do k=1,nforwards
       rights(k)=2*rights(k-1)+m
       lefts( k)=2*lefts( k-1)-m
    enddo

    leftx = lefts(nforwards)-n
    rightx=rights(nforwards)+n  

    !stop the code if the gaussian is too extended
    if (rightx-leftx > nwork) then
       STOP 'gaustodaub'
       return
    end if

    !loop for each complex component
    do icplx=1,ncplx
       pishift=-(icplx-1)*pi/2.0_gp
       do ics=1,ncs_s
          !calculate the expansion coefficients at level 4, positions shifted by 16*i0 
          if( mod(nforwards,2)==0) then
             ifwdtarget=1
             ifwdsource=2
          else
             ifwdtarget=2
             ifwdsource=1
          endif


          if (ics == 1) then
             if (n_gau == 0) then
                do i=leftx,rightx
                   x=real(i-i0*nrefinement,gp)*h
                   sval=real(cos(kval*x+pishift))
                   r=x-z0
                   r2=r
                   r2=r2*r2
                   cval=real(cos(gau_kval*r2),wp)
                   r2=0.5_gp*r2/a/a
                   func=safe_exp(-r2)
                   if(abs(r)>cutoff) func=0
                   ww(i-leftx,ifwdtarget ,ics, icplx)=func*cval*sval
                enddo
             else
                do i=leftx,rightx
                   x=real(i-i0*nrefinement,gp)*h
                   sval=real(cos(kval*x+pishift))
                   r=x-z0
                   coeff=r**n_gau
                   r2=r
                   r2=r2*r2
                   cval=real(cos(gau_kval*r2),wp)
                   r2=0.5_gp*r2/a/a
                   func=safe_exp(-r2)
                   func=real(coeff,wp)*func
                   if(abs(r)>cutoff) func=0
                   ww(i-leftx,ifwdtarget,ics, icplx)=func*cval*sval
                enddo
             end if
          else if (ics == 2) then
             if (n_gau == 0) then
                do i=leftx,rightx
                   x=real(i-i0*nrefinement,gp)*h
                   sval=real(cos(kval*x+pishift))
                   r=x-z0
                   r2=r
                   r2=r2*r2
                   cval=real(sin(gau_kval*r2),wp)
                   r2=0.5_gp*r2/a/a
                   func=safe_exp(-r2)
                   if(abs(r)>cutoff) func=0
                   ww(i-leftx,ifwdtarget,ics, icplx)=func*cval*sval
                enddo
             else
                do i=leftx,rightx
                   x=real(i-i0*nrefinement,gp)*h
                   sval=real(cos(kval*x+pishift))
                   r=x-z0
                   coeff=r**n_gau
                   r2=r
                   r2=r2*r2
                   cval=real(sin(gau_kval*r2),wp)
                   r2=0.5_gp*r2/a/a
                   func=safe_exp(-r2)
                   func=real(coeff,wp)*func
                   if(abs(r)>cutoff) func=0
                   ww(i-leftx,ifwdtarget,ics, icplx)=func*cval*sval
                enddo
             end if
          end if

          !print *,'here',gau_a,gau_cen,n_gau

          iswap=ifwdsource
          ifwdsource=ifwdtarget
          ifwdtarget=iswap

          call apply_w(ww(0,ifwdsource ,ics,icplx),ww(0,ifwdtarget ,ics,icplx),&
               leftx   ,rightx   ,lefts( nforwards),rights(nforwards  ),h)

          do i=nforwards,2,-1

             iswap=ifwdsource
             ifwdsource=ifwdtarget
             ifwdtarget=iswap

             call forward_c(ww(0,ifwdsource ,ics, icplx),ww(0, ifwdtarget,ics, icplx),&
                  lefts( i),rights( i),lefts(i-1),rights(i-1)) 

          enddo

          iswap=ifwdsource
          ifwdsource=ifwdtarget
          ifwdtarget=iswap

          if( ifwdsource .ne. 1) then
             STOP ' ifwdsource .ne. 1  '
          endif

          call forward(  ww(0,1,ics, icplx),ww(0,2,ics, icplx),&
               lefts(1),rights(1),lefts(0),rights(0)) 

       end do
    end do

  END SUBROUTINE gauss_c_to_scf

  subroutine fold_tail
    ! One of the tails of the Gaussian is folded periodically
    ! We assume that the situation when we need to fold both tails
    ! will never arise
    !implicit none


    !modification of the calculation.
    !at this stage the values of c are fixed to zero
    !print *,'ncs_s',ncs_s,n_left,n_right,nwork,length
    do icplx=1,ncplx
       do ics=1,ncs_s
          do i=n_left,n_right
             j=modulo(i,nmax+1)
             c(ics, icplx, j,1)=c(ics,icplx, j,1)+ww(i-n_left       ,2,ics, icplx)
             c(ics, icplx, j,2)=c(ics,icplx,j,2)+ww(i-n_left+length,2,ics, icplx)
          end do
       end do
    end do


    do icplx=1,ncplx
       do ics=1,ncs_s
          do j=0,nmax
             c(ics,icplx, j,1)=fac*c(ics, icplx , j, 1 )
             c(ics,icplx, j,2)=fac*c(ics, icplx , j, 2 )
          enddo
       enddo
    end do
    
  END SUBROUTINE fold_tail


END SUBROUTINE gauss_c_to_daub_k

!> APPLYING THE MAGIC FILTER ("SHRINK") 
subroutine apply_w(cx,c,leftx,rightx,left,right,h)
  use module_precisions
  implicit none
  integer, intent(in) :: leftx,rightx,left,right
  real(gp), intent(in) :: h
  real(wp), dimension(leftx:rightx), intent(in) :: cx
  real(wp), dimension(left:right), intent(out) :: c
  !local variables
  include 'recs16.inc'
  integer :: i,j
  real(wp) :: sqh,ci

  sqh=real(sqrt(h),wp)

 !$omp parallel do schedule(static) default(shared) private(i,ci,j)
  do i=left,right
     ci=0.0_wp
     do j=-n,n
        ci=ci+cx(i+j)*w(j)         
     enddo
     c(i)=ci*sqh
  enddo
 !$omp end parallel do

END SUBROUTINE apply_w

!> APPLYING THE INVERSE MAGIC FILTER ("GROW") 
!!subroutine apply_inverse_w(cx,c,leftx,rightx,left,right,h)
!!  use module_base
!!  implicit none
!!  integer, intent(in) :: leftx,rightx,left,right
!!  real(gp), intent(in) :: h
!!  real(wp), dimension(leftx:rightx), intent(in) :: cx
!!  real(wp), dimension(left:right), intent(out) :: c
!!  !local variables
!!  include 'recs16.inc'
!!  integer :: i,j
!!  real(wp) :: sqh,ci
!!
!!  sqh=real(sqrt(h),wp)
!!
!!  do i=left,right
!!     ci=0.0_wp
!!     do j=-n,n
!!        ci=ci+cx(i+j)*w(-j) !transposed MF         
!!     enddo
!!     c(i)=ci*sqh
!!  enddo
!!  
!!END SUBROUTINE apply_inverse_w


!> FORWARD WAVELET TRANSFORM WITHOUT WAVELETS ("SHRINK")
subroutine forward_c(c,c_1,left,right,left_1,right_1)
  use module_precisions
  implicit none
  integer, intent(in) :: left,right,left_1,right_1
  real(wp), dimension(left:right), intent(in) :: c
  real(wp), dimension(left_1:right_1), intent(out) :: c_1
  !local variables
  integer :: i,i2,j
  real(wp) :: ci
  include 'sym_16.inc'

  ! get the coarse scfunctions and wavelets
  !$omp parallel do schedule(static) default(shared) private(i,i2,ci,j)
  do i=left_1,right_1
     i2=2*i
     ci=0.0_wp
     do j=-m,m
        ci=ci+cht(j)*c(j+i2)
     enddo
     c_1(i)=ci
  enddo
  !$end parallel do

END SUBROUTINE forward_c


!>  CONVENTIONAL FORWARD WAVELET TRANSFORM ("SHRINK")
subroutine forward(c,cd_1,left,right,left_1,right_1)
  use module_precisions
  implicit none
  integer, intent(in) :: left,right,left_1,right_1
  real(wp), dimension(left:right), intent(in) :: c
  real(wp), dimension(left_1:right_1,2), intent(out) :: cd_1
  !local variables
  integer :: i,i2,j
  real(wp) :: ci,di
  include 'sym_16.inc'

  ! get the coarse scfunctions and wavelets
  !$omp parallel do schedule(static) default(shared) private(i,i2,ci,di,j)
  do i=left_1,right_1
     i2=2*i
     ci=0.d0
     di=0.d0
     do j=-m,m
        ci=ci+cht(j)*c(j+i2)
        di=di+cgt(j)*c(j+i2)
     enddo
     cd_1(i,1)=ci
     cd_1(i,2)=di
  enddo
  !$omp end parallel do

END SUBROUTINE forward


subroutine gaussians_c_to_wavelets_orb(ncplx,lr,hx,hy,hz,kx,ky,kz,G,wfn_gau,psi, cutoff)
  use module_precisions
  use module_types
  use gaussians
  use locregs
  use at_domain, only: domain_periodic_dims
  use module_bigdft_arrays
  implicit none
  integer, intent(in) :: ncplx
  real(gp), intent(in) :: hx,hy,hz,kx,ky,kz
  type(locreg_descriptors), intent(in) :: lr
  type(gaussian_basis_c), intent(in) :: G
  real(wp), dimension(G%ncoeff), intent(in) :: wfn_gau
  real(wp), dimension(array_dim(lr)*ncplx), intent(out) :: psi
  real(gp) cutoff

  !local variables
  character(len=*), parameter :: subname='gaussians_to_wavelets_orb'
  integer, parameter :: nterm_max=3,maxsizeKB=2048,nw=65536
  logical :: perx,pery,perz
  integer :: ishell,iexpo,icoeff,iat,isat,ng,l,m,i,nterm,ig
!! integer :: i_stat,i_all,
  integer :: nterms_max,nterms,iscoeff,iterm,n_gau,ml1,mu1,ml2,mu2,ml3,mu3
  real(gp) :: rx,ry,rz,gau_a, gau_bf
  integer, dimension(nterm_max) :: lx,ly,lz
  real(gp), dimension(nterm_max) :: fac_arr
  real(wp), allocatable, dimension(:,:,:, :) :: work
  real(wp), allocatable, dimension(:,  :,:,:,:) :: wx,wy,wz
  real(wp), allocatable, dimension(:,:) :: cossinfacts
  integer :: ncplxC
  logical, dimension(3) :: peri

  ncplxC=2

  ! if ( ncplx.ne.1) then
  !    stop ' ncplx must be 1 in  actual version of  gaussians_c_to_wavelets_orb'
  ! end if

  !calculate nterms_max:
  !allows only maxsizeKB per one-dimensional array
  !(for a grid of dimension 100 nterms_max=655)
  !but with at least ngx*nterm_max ~= 100 elements
  nterms_max=max(maxsizeKB*1024/(2*ncplxC*(maxval(lr%mesh_coarse%ndims)-1)),100)

  work = f_malloc((/ 0.to.nw, 1.to.2, 1.to.2, 1.to.ncplx /),id='work')
  wx = f_malloc((/ 1.to.ncplxC, 1.to.ncplx, 0.to.lr%mesh_coarse%ndims(1)-1, 1.to.2, 1.to.nterms_max /),id='wx')
  wy = f_malloc((/ 1.to.ncplxC, 1.to.ncplx, 0.to.lr%mesh_coarse%ndims(2)-1, 1.to.2, 1.to.nterms_max /),id='wy')
  wz = f_malloc((/ 1.to.ncplxC, 1.to.ncplx, 0.to.lr%mesh_coarse%ndims(3)-1, 1.to.2, 1.to.nterms_max /),id='wz')
  cossinfacts = f_malloc((/ 1.to.2, 1.to.nterms_max /),id='cossinfacts')


  !conditions for periodicity in the three directions
!!$  perx=(lr%geocode /= 'F')
!!$  pery=(lr%geocode == 'P')
!!$  perz=(lr%geocode /= 'F')
  peri=domain_periodic_dims(lr%mesh%dom)
  perx=peri(1)
  pery=peri(2)
  perz=peri(3)

  !initialize the wavefunction
  call f_zero(psi)

  !calculate the number of terms for this orbital
  nterms=0
  !loop over the atoms
  ishell=0
  iexpo=1
  icoeff=1
  iscoeff=1
  iterm=1
  do iat=1,G%nat

     rx=G%rxyz(1,iat)
     ry=G%rxyz(2,iat)
     rz=G%rxyz(3,iat)
     !loop over the number of shells of the atom type
     do isat=1,G%nshell(iat)

        ishell=ishell+1
        !the degree of contraction of the basis function
        !is the same as the ng value of the createAtomicOrbitals routine
        ng=G%ndoc(ishell)

        !angular momentum of the basis set(shifted for compatibility with BigDFT routines
        l=abs(G%nam(ishell)  )
        !print *,iproc,iat,ishell,G%nam(ishell),G%nshell(iat)
        !multiply the values of the gaussian contraction times the orbital coefficient

        do m=1,2*l-1
           call calc_coeff_inguess(l,m,nterm_max,nterm,lx,ly,lz,fac_arr)
           !control whether the basis element may be
           !contribute to some of the orbital of the processor

           if (wfn_gau(icoeff) /= 0.0_wp) then
              if (nterms + nterm*ng > nterms_max) then
                 call lr_accumulate(lr, ncplx,cossinfacts , nterms,wx,wy,wz,psi)
                 iterm=1
                 nterms=0
              end if
              !assign the arrays
              !make sure that the coefficients returned by
              !gauss_to_daub are zero outside [ml:mr]
              do ig=1,ng

                 gau_a= sqrt( 1.0_gp/REAL(2.0_gp*G%expof(iexpo+ig-1))   )
                 gau_bf = AIMAG ( G%expof(iexpo+ig-1) )

                 do i=1,nterm

                    n_gau=lx(i)

                    call gauss_c_to_daub_k(hx,kx,ncplx,gau_bf ,ncplxC,fac_arr(i), &
                         rx,gau_a,  n_gau,&
                         lr%mesh_coarse%ndims(1)-1,ml1,mu1,&
                         wx(1,1,0,1,iterm),work,nw,perx, cutoff)

                    n_gau=ly(i)
                    !print *,'y',ml2,mu2,lr%d%n2
                    call gauss_c_to_daub_k(hy,ky,ncplx,gau_bf,ncplxC,wfn_gau(icoeff), &
                         ry,gau_a,n_gau,&
                         lr%mesh_coarse%ndims(2)-1,ml2,mu2,&
                         wy(1,1,0,1,iterm),work,nw,pery, cutoff)
                    n_gau=lz(i)
                    !print *,'z',ml3,mu3,lr%d%n3
                    call gauss_c_to_daub_k(hz,kz,ncplx,gau_bf,ncplxC,  1.0_wp,  &
                         rz,gau_a,n_gau,&
                         lr%mesh_coarse%ndims(3)-1,ml3,mu3,&
                         wz(1,1,0,1,iterm),work,nw,perz, cutoff)

                    cossinfacts(1,iterm)= REAL( G%psiat(iexpo+ig-1))
                    cossinfacts(2,iterm)= AIMAG(G%psiat(iexpo+ig-1))

                    iterm=iterm+1
                 end do
              end do


              nterms=nterms+nterm*ng
           end if
           icoeff=icoeff+1
        end do
        iexpo=iexpo+ng
     end do
  end do

  call gaudim_check(iexpo,icoeff,ishell,G%nexpo,G%ncoeff,G%nshltot)

  !accumulate wavefuncton

  call lr_accumulate(lr, ncplx,  cossinfacts    ,nterms,wx,wy,wz,psi)

!psi=1.d0

  call f_free(wx)
  call f_free(wy)
  call f_free(wz)
  call f_free(cossinfacts)
  call f_free(work)

END SUBROUTINE gaussians_c_to_wavelets_orb
