'''This is the configuration file for the BigDFT installer
for use on the summer cluster at CEA Grenoble. 
Before using this, you should load the following modules:
module load gcc/9.3.0
module load impi/20
module load mkl/20
module load python/anaconda-3.8
'''

conditions.add("testing")
makeargs = '-j1'


def fflags():
    '''
    Defines the flags that will be sent to the fortran compiler.
    '''
    return "-I${MKLROOT}/include -O2 -fopenmp -g"

# List the module the this rcfile will build
modules = ['spred', ]


def env_configuration():
    '''
    Species the configure line for various packages.
    '''
    from os import getcwd, path
    env = {}
    env["FC"] = "mpif90"
    env["CC"] = "mpicc"
    env["CXX"] = "mpicxx"
    env["FCFLAGS"] = fflags()
    env["CFLAGS"] = "-O2 -fpic"
    env["CXXFLAGS"] = "-std=c++11"
    linkline = " -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_gf_lp64.a ${MKLROOT}/lib/intel64/libmkl_intel_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -liomp5 -lpthread -lm -ldl"
    env["--with-ext-linalg"] = linkline
    env["--with-pyyaml-path"] = path.join(getcwd(), "install", "lib", "python3.6", "site-packages")
    #env["--with-pyyaml-path"] = "%(prefix)s/lib/python3.6/site-packages"

    return " ".join(['"' + x + '=' + y + '"' for x, y in env.items()])

def cmake_configuration():
    from os import getcwd, path

    cmake_flags = {}
    cmake_flags["CMAKE_Fortran_FLAGS_RELEASE"] = fflags()
    cmake_flags["CMAKE_Fortran_FLAGS"] = fflags()
    cmake_flags["CMAKE_C_FLAGS"] = fflags()
    cmake_flags["CMAKE_CXX_FLAGS"] = fflags()
    cmake_flags["CMAKE_Fortran_COMPILER"] = "gfortran"
    cmake_flags["CMAKE_C_COMPILER"] = "gcc"
    cmake_flags["CMAKE_CXX_COMPILER"] = "g++"
    cmake_flags["USE_MPIH"] = "ON"
    cmake_flags["CMAKE_PREFIX_PATH"] = path.join(getcwd(), "install")
    cmake_flags["CMAKE_BINARY_DIR"] = "/home/sd271316/files/cmake-3.2.2"

    return " ".join(['-D' + x + '="' + y + '"' for x, y in cmake_flags.items()])

# For NTPoly
module_cmakeargs.update({
    'openbabel': cmake_configuration(),
    'ntpoly': cmake_configuration()
})

module_autogenargs.update({

    'libyaml': env_configuration(),

    'futile': env_configuration(),

    'psolver': env_configuration(),

    'chess': env_configuration(),

    'libxc': env_configuration(),

    'libABINIT': env_configuration(),

    'GaIn': env_configuration(),

    'bigdft': env_configuration(),

    'spred': env_configuration(),

    'atlab': env_configuration(),

    'liborbs': env_configuration(),

})

