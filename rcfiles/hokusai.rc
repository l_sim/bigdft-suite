'''
This is the configuration file for the BigDFT installer
for use on the Hokusai Big Wave computer at RIKEN.

First load the required modules:
  module load cmake/3.19.3
  module load intel
'''

# Handle the maximum parallelism on the front end nodes
from socket import gethostname
if "hokusai" in gethostname():
    makeargs = '-j1'

# List the module the this rcfile will build
modules = ['spred', ]

def env_configuration():
    '''
    Species the configure line for various autotools packages.
    '''
    env = {}
    env["FC"] = "mpiifort"
    env["CC"] = "icc"
    env["CXX"] = "icpc"
    env["FCFLAGS"] = "-O2 -qopenmp"
    env["FCFLAGS"] += ' -I"${MKLROOT}/include"'
    env["CXXFLAGS"] = "-std=c++11"
    env["LIBS"] = "-lstdc++"
    env["--with-ext-linalg"] = "-L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 " \
                               "-lmkl_intel_thread -lmkl_core -liomp5 " \
                               " -lpthread -lm -ldl"

    return " ".join(['"' + x + '=' + y + '"' for x, y in env.items()])

def ntpoly_configuration():
    ''' 
    For NTPoly we need to specify the cmake options.
    ''' 
    from os import getcwd, path

    cmake_flags = {}
    cmake_flags["CMAKE_Fortran_FLAGS_RELEASE"] = "-O2 -qopenmp"
    cmake_flags["CMAKE_Fortran_COMPILER"] = "mpiifort"
    cmake_flags["CMAKE_C_COMPILER"] = "mpiicc"
    cmake_flags["CMAKE_CXX_COMPILER"] = "mpiicpc"
    cmake_flags["CMAKE_PREFIX_PATH"] = path.join(getcwd(), "install")

    return " ".join(['-D' + x + '="' + y + '"' for x, y in cmake_flags.items()])

autogenargs = env_configuration()
module_cmakeargs.update({
    'ntpoly': ntpoly_configuration()
})

