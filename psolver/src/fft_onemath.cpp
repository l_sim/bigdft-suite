#include <oneapi/mkl.hpp>
using mkl_descriptor_real = oneapi::mkl::dft::descriptor<oneapi::mkl::dft::precision::DOUBLE, oneapi::mkl::dft::domain::REAL>;
using mkl_descriptor_complex = oneapi::mkl::dft::descriptor<oneapi::mkl::dft::precision::DOUBLE, oneapi::mkl::dft::domain::COMPLEX>;
using oneapi::mkl::dft::compute_backward;
using oneapi::mkl::dft::compute_forward;


// //put all the functions as 'extern "C" here'
extern "C"
{
    void dpcpp_fft_destroy_c2c(mkl_descriptor_complex **plan);
    void dpcpp_fft_destroy_r2c(mkl_descriptor_real **plan);
    void dpcpp_3d_psolver_plangeneral(const int N[3], Complex *const d_data,
                                      Complex *const d_data2, double *const __restrict__ d_kernel,
                                      const int geo[3], const double scal);
}
// /************ 3D Poisson Solver for general boundary *************/

/// @brief Generate plans for fft.
/// @param N
/// @param plan Array of 5 pointers to plans.
/// @param switch_alg
/// @param geo
void dpcpp_3d_psolver_general_plan(const int N[3],
                                   void **plan, int *const __restrict__ switch_alg,
                                   const int geo[3])
{
    const int NX = N[0];
    const int NY = N[1];
    const int NZ = N[2];

    // const int geo1 = geo[0];
    const int geo2 = geo[1];
    const int geo3 = geo[2];

    // int n1d[3]= {1, 1, 1};

    // const int xsize = NX/2 + geo1 * NX/2;
    const int ysize = NY / 2 + geo2 * NY / 2;
    const int zsize = NZ / 2 + geo3 * NZ / 2;

    plan[0] = new mkl_descriptor_real(NX);
    ((mkl_descriptor_real *)plan[0])->set_value(oneapi::mkl::dft::config_param::NUMBER_OF_TRANSFORMS, ysize * zsize);
    ((mkl_descriptor_real *)plan[0])->set_value(oneapi::mkl::dft::config_param::PLACEMENT, DFTI_CONFIG_VALUE::DFTI_NOT_INPLACE);
    std::int64_t stride[2] = {0, 1};
    ((mkl_descriptor_real *)plan[0])->set_value(oneapi::mkl::dft::config_param::INPUT_STRIDES, stride);
    ((mkl_descriptor_real *)plan[0])->set_value(oneapi::mkl::dft::config_param::OUTPUT_STRIDES, stride);
    ((mkl_descriptor_real *)plan[0])->set_value(oneapi::mkl::dft::config_param::FWD_DISTANCE, NX);
    ((mkl_descriptor_real *)plan[0])->set_value(oneapi::mkl::dft::config_param::BWD_DISTANCE, NX / 2 + 1);
    //((mkl_descriptor_real*)plan[0])->set_value(oneapi::mkl::dft::config_param::BACKWARD_SCALE,  (1.0/NX));
    ((mkl_descriptor_real *)plan[0])->commit(*Q);

    /// same as plan[0], dont use it
    // plan[1] = new mkl_descriptor_real(NX);
    // ((mkl_descriptor_real*)plan[1])->set_value(oneapi::mkl::dft::config_param::NUMBER_OF_TRANSFORMS, ysize*zsize);
    // ((mkl_descriptor_real*)plan[1])->set_value(oneapi::mkl::dft::config_param::PLACEMENT, DFTI_CONFIG_VALUE::DFTI_NOT_INPLACE);
    // std::int64_t stride[2] = {0, 1};
    // ((mkl_descriptor_real*)plan[1])->set_value(oneapi::mkl::dft::config_param::INPUT_STRIDES, stride);
    // ((mkl_descriptor_real*)plan[1])->set_value(oneapi::mkl::dft::config_param::OUTPUT_STRIDES, stride);
    // ((mkl_descriptor_real*)plan[1])->set_value(oneapi::mkl::dft::config_param::FWD_DISTANCE, NX);
    // ((mkl_descriptor_real*)plan[1])->set_value(oneapi::mkl::dft::config_param::BWD_DISTANCE, NX/2 + 1);
    // ((mkl_descriptor_real*)plan[1])->commit(*Q);

    // ///ATTENTION: mis-using 2nd spot (plan[1]) for 3d ffts.
    // plan[1] = new mkl_descriptor_real({NZ, NY, NX});
    // ((mkl_descriptor_real*)plan[1])->set_value(oneapi::mkl::dft::config_param::PLACEMENT, DFTI_CONFIG_VALUE::DFTI_NOT_INPLACE);
    // std::int64_t stridefwd_3d[4] = {0, NX*NY, NX, 1};
    // ((mkl_descriptor_real*)plan[1])->set_value(oneapi::mkl::dft::config_param::INPUT_STRIDES, stridefwd_3d);
    // std::int64_t stridebwd_3d[4] = {0, (NX/2+1)*(NY/2+1), (NX/2+1), 1};
    // ((mkl_descriptor_real*)plan[1])->set_value(oneapi::mkl::dft::config_param::OUTPUT_STRIDES, stridebwd_3d);
    // std::int64_t dim_fwd[3] = {NZ, NY, NX};
    // ((mkl_descriptor_real*)plan[1])->set_value(oneapi::mkl::dft::config_param::FWD_DISTANCE, dim_fwd);
    // std::int64_t dim_bwd[3] = {NZ/2+1, NY/2+1, NX/2 + 1};
    // ((mkl_descriptor_real*)plan[1])->set_value(oneapi::mkl::dft::config_param::BWD_DISTANCE, dim_bwd);
    // ((mkl_descriptor_real*)plan[1])->commit(*Q);

    plan[2] = new mkl_descriptor_complex(NY);
    ((mkl_descriptor_complex *)plan[2])->set_value(oneapi::mkl::dft::config_param::NUMBER_OF_TRANSFORMS, (NX / 2 + 1) * zsize);
    ((mkl_descriptor_complex *)plan[2])->set_value(oneapi::mkl::dft::config_param::PLACEMENT, DFTI_CONFIG_VALUE::DFTI_NOT_INPLACE);
    ((mkl_descriptor_complex *)plan[2])->set_value(oneapi::mkl::dft::config_param::FWD_DISTANCE, NY);
    ((mkl_descriptor_complex *)plan[2])->set_value(oneapi::mkl::dft::config_param::BWD_DISTANCE, NY);
    ((mkl_descriptor_complex *)plan[2])->commit(*Q);

    plan[3] = new mkl_descriptor_complex(NZ);
    ((mkl_descriptor_complex *)plan[3])->set_value(oneapi::mkl::dft::config_param::NUMBER_OF_TRANSFORMS, (NX / 2 + 1) * NY);
    ((mkl_descriptor_complex *)plan[3])->set_value(oneapi::mkl::dft::config_param::PLACEMENT, DFTI_CONFIG_VALUE::DFTI_NOT_INPLACE);
    ((mkl_descriptor_complex *)plan[3])->set_value(oneapi::mkl::dft::config_param::FWD_DISTANCE, NZ);
    ((mkl_descriptor_complex *)plan[3])->set_value(oneapi::mkl::dft::config_param::BWD_DISTANCE, NZ);
    ((mkl_descriptor_complex *)plan[3])->commit(*Q);

    // Was commented in original code but plan[4] is used below
    //  n1d[0] = NZ;
    //  int inembed[1];
    //  int onembed[1];
    //  inembed[0] = 1;
    //  onembed[0] = 1;
    //  plan[4] = dpct::fft::fft_engine::create(Q, 1, n1d, inembed, NY, 1, onembed, NZ, 1,
    //      Transform, NY); //CB: not sure this works.
    //  //end of the code part which was commented
    plan[4] = nullptr; // commented for now but set to 0 as in the original cuda version. Certain code paths will crash

    *switch_alg = 0;
}


/// @brief
/// @param N
/// @param plan Array of 5 pointers to plans.
/// @param d_data
/// @param d_data2
/// @param d_kernel
/// @param switch_alg
/// @param geo
/// @param scal 1/(NX*NY*NZ) required to ensure backw transform is inverse of forward
void dpcpp_3d_psolver_general(const int N[3], void **plan,
                              Complex *const d_data, Complex *const d_data2,
                              double const *const __restrict__ d_kernel, const int switch_alg,
                              const int geo[3], const double scal)
{
    const int NX = N[0];
    const int NY = N[1];
    const int NZ = N[2];

    if (NX > 1024)
        throw std::invalid_argument("NX > 1024 exceeds max wg size in dpcpp_3d_psolver_general. Report Bug to developers.");

    // const double scal = *scal_p;

    const int geo1 = geo[0];
    const int geo2 = geo[1];
    const int geo3 = geo[2];

    const int ysize = NY / 2 + geo2 * NY / 2;
    const int zsize = NZ / 2 + geo3 * NZ / 2;

    // transpose kernel parameters
    sycl::range<3> grid(1, (ysize * zsize + TILE_DIM - 1) / TILE_DIM,
                        (NX / 2 + 1 + TILE_DIM - 1) / TILE_DIM);
    sycl::range<3> threads(1, TILE_DIM, TILE_DIM);

    // spread kernel parameters
    sycl::range<3> nblocks(1, ysize, zsize);

    Complex *dst = d_data;
    Complex *src = d_data2;

    try
    {
        // X transform
        if (geo1 == 0)
        {
            src = d_data;
            dst = d_data2;

            Q->parallel_for(sycl::nd_range<3>(nblocks * sycl::range<3>(1, 1, NX),
                                              sycl::range<3>(1, 1, NX)),
                            [=](sycl::nd_item<3> item_ct1)
                            {
                                dpcpp_spread((double *)src, NX / 2, (double *)dst, NX, item_ct1);
                            });
        }

        // x transpose
        compute_forward(*((mkl_descriptor_real *)plan[0]), (double *)dst, (double *)src);

        if (geo2 == 0)
        {

            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(TILE_DIM, TILE_DIM), cgh);
                cgh.parallel_for<class psolver_general_gpu_transpose_spread_1>(sycl::nd_range<3>(grid * threads, threads),
                [=](sycl::nd_item<3> item_ct1)//[[intel::reqd_sub_group_size(32)]] 
                {
                    dpcpp_transpose_spread(src, dst, NX / 2 + 1, ysize * zsize, NY / 2,
                        item_ct1, tile_acc_ct1);
                }); });
        }
        else
        {

            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(8, 9), cgh);

                cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                [=](sycl::nd_item<3> item_ct1) 
                {
                    dpcpp_transpose(src, dst, NX / 2 + 1, ysize * zsize,
                        item_ct1, tile_acc_ct1);
                }); });
            // oneapi::mkl::blas::row_major::omatcopy(*Q, oneapi::mkl::transpose::T, ysize * zsize, NX / 2 + 1, std::complex<double>(1.0, 0.0), (std::complex<double>*)src, NX / 2 + 1, (std::complex<double>*)dst, ysize * zsize);
        }

        // Y transform
        compute_forward(*((mkl_descriptor_complex *)plan[2]), (std::complex<double> *)dst, (std::complex<double> *)src);

        // Z transform, on entire cube
        if (!switch_alg)
        {
            grid[2] = (NY + TILE_DIM - 1) / TILE_DIM;
            grid[1] = ((NX / 2 + 1) * zsize + TILE_DIM - 1) / TILE_DIM;

            if (geo3 == 0)
            {
                Q->submit([&](sycl::handler &cgh)
                          {
                    sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(TILE_DIM, TILE_DIM), cgh);
                    cgh.parallel_for<class psolver_general_gpu_transpose_spread_2>(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1)//[[intel::reqd_sub_group_size(32)]] 
                    {
                        dpcpp_transpose_spread(src, dst, NY, (NX / 2 + 1) * NZ / 2,
                            NZ / 2, item_ct1, tile_acc_ct1);
                    }); });
            }
            else
            {
                Q->submit([&](sycl::handler &cgh)
                          {
                    sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(8, 9), cgh);

                    cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                        dpcpp_transpose(src, dst, NY, (NX / 2 + 1) * NZ,
                            item_ct1, tile_acc_ct1);
                    }); });
            }

            // z transpose
            compute_forward(*((mkl_descriptor_complex *)plan[3]), (std::complex<double> *)dst, (std::complex<double> *)src);
        }
        else
        {
            if (geo3 == 0)
            {
                nblocks[2] = zsize;
                nblocks[1] = NX;
                Q->parallel_for<class psolver_general_gpu_spread_y>(sycl::nd_range<3>(nblocks * sycl::range<3>(1, 1, NY),
                                                                                      sycl::range<3>(1, 1, NY)),
                                                                    [=](sycl::nd_item<3> item_ct1)
                                                                    {
                                                                        dpcpp_spread_y(src, dst, item_ct1);
                                                                    });
            }

            for (int k = 0; k < NX; ++k)
            {
                // TransformExec(plan[4], dst, src, dpct::fft::fft_direction::forward);
                //((dpct::fft::fft_engine*)plan[4])->compute<sycl::double2, sycl::double2>(dst, src, dpct::fft::fft_direction::forward);
                throw std::logic_error("This code path leads to a fft plan which is currently not developed.");
                src += NY * NZ;
                dst += NY * NZ;
            }

            src -= NX * NY * NZ;
            dst -= NX * NY * NZ;
        }

        // multiply with kernel
        Q->parallel_for<class psolver_general_gpu_multiply_kernel_linear>(NY * NZ * (NX / 2 + 1),
                                                                          [=](auto item_ct1)
                                                                          {
                                                                              dpcpp_multiply_kernel_linear((NX / 2 + 1) * NY * NZ, src, d_kernel, scal, item_ct1);
                                                                          });

        // inverse transform

        // Z transform, on entire cube
        if (!switch_alg)
        {
            // z transpose
            compute_backward(*((mkl_descriptor_complex *)plan[3]), (std::complex<double> *)src, (std::complex<double> *)dst);

            grid[2] = (zsize * (NX / 2 + 1) + TILE_DIM - 1) / TILE_DIM;
            grid[1] = (NY + TILE_DIM - 1) / TILE_DIM;
            if (geo3 == 0)
            {
                // Q->parallel_for(NY*NZ*NX,
                // [=](auto item_ct1)
                // {
                //     int id = item_ct1.get_id();
                //     dst[id] = id+1;
                // });
                // std::cout << "before inv spread" << std::endl;
                // plotDeviceVector(dst, NX, NY, NZ);
                Q->submit([&](sycl::handler &cgh)
                          {
                    sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(TILE_DIM, TILE_DIM), cgh);
                    cgh.parallel_for<class psolver_general_gpu_transpose_spread_i_1>(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                        dpcpp_transpose_spread_i(dst, src, NZ / 2 * (NX / 2 + 1), NY,
                            NZ / 2, item_ct1, tile_acc_ct1);
                    }); });
                // std::cout << "after inv spread" << std::endl;
                // plotDeviceVector(src, NX, NY, NZ);
            }
            else
            {
                Q->submit([&](sycl::handler &cgh)
                          {
                    sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(8, 9), cgh);

                    cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                        dpcpp_transpose(dst, src, NZ * (NX / 2 + 1), NY,
                            item_ct1, tile_acc_ct1);
                    }); });
            }
        }
        else
        {
            for (int k = 0; k < NX; ++k)
            {
                // TransformExec(plan[4], src, dst, dpct::fft::fft_direction::backward);
                //((dpct::fft::fft_engine*)plan[4])->compute<sycl::double2, sycl::double2>(src, dst, dpct::fft::fft_direction::backward);
                throw std::logic_error("This code path leads to fft plan which is currently not developed.");
                src += NY * NZ;
                dst += NY * NZ;
            }

            src -= NX * NY * NZ;
            dst -= NX * NY * NZ;

            if (geo3 == 0)
            {
                Q->parallel_for<class psolver_general_gpu_spread_y_i>(sycl::nd_range<3>(nblocks * sycl::range<3>(1, 1, NY),
                                                                                        sycl::range<3>(1, 1, NY)),
                                                                      [=](sycl::nd_item<3> item_ct1)
                                                                      {
                                                                          dpcpp_spread_y_i(dst, src, item_ct1);
                                                                      });
            }
        }

        // y transform
        compute_backward(*((mkl_descriptor_complex *)plan[2]), (std::complex<double> *)src, (std::complex<double> *)dst);

        grid[2] = (ysize * zsize + TILE_DIM - 1) / TILE_DIM;
        grid[1] = (NX / 2 + 1 + TILE_DIM - 1) / TILE_DIM;

        if (geo2 == 0)
        {
            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(TILE_DIM, TILE_DIM), cgh);
                cgh.parallel_for<class psolver_general_gpu_transpose_spread_i_2>(sycl::nd_range<3>(grid * threads, threads),
                [=](sycl::nd_item<3> item_ct1) 
                {
                    dpcpp_transpose_spread_i(dst, src, ysize * zsize,
                        NX / 2 + 1, NY / 2, item_ct1, tile_acc_ct1);
                }); });
        }
        else
        {
            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(8, 9), cgh);

                cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                        dpcpp_transpose(dst, src, ysize * zsize, NX / 2 + 1,
                            item_ct1, tile_acc_ct1);
                    }); });
        }

        // X transform
        compute_backward(*((mkl_descriptor_real *)plan[0]), (double *)src, (double *)dst);
        // std::cout << "After 2nd transform" << std::endl;
        // plotDeviceVector(dst, NX, NY, NZ);

        nblocks[2] = zsize;
        nblocks[1] = ysize;
        if (geo1 == 0)
        {
            Q->parallel_for<class psolver_general_gpu_spread_i>(sycl::nd_range<3>(nblocks * sycl::range<3>(1, 1, NX / 2),
                                                                                  sycl::range<3>(1, 1, NX / 2)),
                                                                [=](sycl::nd_item<3> item_ct1)
                                                                {
                                                                    dpcpp_spread_i((double *)dst, NX / 2, (double *)src, NX, item_ct1);
                                                                });
        }
    }
    catch (std::exception &e)
    {
        std::cout << "ERROR: in dpcpp_3d_psolver_general\n";
        std::cout << e.what() << std::endl;
        exit(1);
    }
}


void dpcpp_3d_psolver_plangeneral(const int N[3], Complex *const d_data,
                                  Complex *const d_data2, double *const __restrict__ d_kernel,
                                  const int geo[3], const double scal)
{
    dpct::fft::fft_engine *plan;

    const int NX = N[0];
    const int NY = N[1];
    const int NZ = N[2];

    // const double scal = *scal_p;

    const int geo1 = geo[0];
    const int geo2 = geo[1];
    const int geo3 = geo[2];

    const int ysize = NY / 2 + geo2 * NY / 2;
    const int zsize = NZ / 2 + geo3 * NZ / 2;

    // transpose kernel parameters
    sycl::range<3> grid(1, (ysize * zsize + TILE_DIM - 1) / TILE_DIM,
                        (NX / 2 + 1 + TILE_DIM - 1) / TILE_DIM);
    sycl::range<3> threads(1, TILE_DIM, TILE_DIM);

    // spread kernel parameters
    sycl::range<3> nblocks(1, ysize, zsize);

    Complex *dst = d_data;
    Complex *src = d_data2;

    int n1d[3] = {1, 1, 1};

    n1d[0] = NX;

    try
    {

        plan = dpct::fft::fft_engine::create(Q, 1, n1d, NULL, 1, NX, NULL, 1, NX,
                                             dpct::fft::fft_type::real_double_to_complex_double, ysize * zsize);
        plan->set_queue(Q); // necessary? CB: do not think so

        // X transform
        if (geo1 == 0)
        {
            src = d_data;
            dst = d_data2;
            /*
            DPCT1049:25: The work-group size passed to the SYCL kernel may exceed
            the limit. To get the device limit, query
            info::device::max_work_group_size. Adjust the work-group size if needed.
            */
            Q->parallel_for(sycl::nd_range<3>(nblocks * sycl::range<3>(1, 1, NX),
                                              sycl::range<3>(1, 1, NX)),
                            [=](sycl::nd_item<3> item_ct1)
                            {
                                dpcpp_spread((double *)src, NX / 2, (double *)dst, NX, item_ct1);
                            });
        }

        plan->compute<double, sycl::double2>((double *)dst, src,
                                             dpct::fft::fft_direction::forward);

        if (geo2 == 0)
        {
            /*
            DPCT1049:26: The work-group size passed to the SYCL kernel may exceed
            the limit. To get the device limit, query
            info::device::max_work_group_size. Adjust the work-group size if needed.
            */
            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(TILE_DIM, TILE_DIM), cgh);

                cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                        dpcpp_transpose_spread(src, dst, NX / 2 + 1, ysize * zsize, NY / 2, 
                            item_ct1, tile_acc_ct1);
                    }); });
        }
        else
        {
            /*
            DPCT1049:27: The work-group size passed to the SYCL kernel may exceed
            the limit. To get the device limit, query
            info::device::max_work_group_size. Adjust the work-group size if needed.
            */
            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(8, 9), cgh);

                cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                        dpcpp_transpose(src, dst, NX / 2 + 1, ysize * zsize,
                                item_ct1, tile_acc_ct1);
                    }); });
        }

        dpct::fft::fft_engine::destroy(plan);

        n1d[0] = NY;

        plan = dpct::fft::fft_engine::create(Q, 1, n1d, NULL, 1, NY, NULL, 1,
                                             NY, Transform, (NX / 2 + 1) * zsize);
        plan->set_queue(Q);

        // Y transform
        plan->compute<sycl::double2, sycl::double2>(dst, src, dpct::fft::fft_direction::forward);

        // Z transform, on entire cube
        grid[2] = (NY + TILE_DIM - 1) / TILE_DIM;
        grid[1] = ((NX / 2 + 1) * zsize + TILE_DIM - 1) / TILE_DIM;

        if (geo3 == 0)
        {
            /*
            DPCT1049:28: The work-group size passed to the SYCL kernel may exceed
            the limit. To get the device limit, query
            info::device::max_work_group_size. Adjust the work-group size if needed.
            */
            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(TILE_DIM, TILE_DIM), cgh);

                cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                        dpcpp_transpose_spread(src, dst, NY, (NX / 2 + 1) * NZ / 2, NZ / 2,
                            item_ct1, tile_acc_ct1);
                    }); });
        }
        else
        {
            /*
            DPCT1049:29: The work-group size passed to the SYCL kernel may exceed
            the limit. To get the device limit, query
            info::device::max_work_group_size. Adjust the work-group size if needed.
            */
            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(8, 9), cgh);

                cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                        dpcpp_transpose(src, dst, NY, (NX / 2 + 1) * NZ,
                            item_ct1, tile_acc_ct1);
                    }); });
        }

        dpct::fft::fft_engine::destroy(plan);
        n1d[0] = NZ;

        plan = dpct::fft::fft_engine::create(Q, 1, n1d, NULL, 1, NZ, NULL, 1, NZ, Transform,
                                             (NX / 2 + 1) * NY);
        plan->set_queue(Q);

        plan->compute<sycl::double2, sycl::double2>(dst, src, dpct::fft::fft_direction::forward);

        Q->parallel_for(NY * NZ * (NX / 2 + 1),
                        [=](auto item_ct1)
                        {
                            dpcpp_multiply_kernel_linear((NX / 2 + 1) * NY * NZ, src, d_kernel, scal, item_ct1);
                        });
        // inverse transform

        // Z transform, on entire cube
        plan->compute<sycl::double2, sycl::double2>(src, dst, dpct::fft::fft_direction::backward);

        grid[2] = (zsize * (NX / 2 + 1) + TILE_DIM - 1) / TILE_DIM;
        grid[1] = (NY + TILE_DIM - 1) / TILE_DIM;

        if (geo3 == 0)
        {
            /*
            DPCT1049:30: The work-group size passed to the SYCL kernel may exceed
            the limit. To get the device limit, query
            info::device::max_work_group_size. Adjust the work-group size if needed.
            */
            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(8, 9), cgh);

                cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                        dpcpp_transpose_spread_i(dst, src, NZ / 2 * (NX / 2 + 1), NY,
                            NZ / 2, item_ct1, tile_acc_ct1);
                    }); });
        }
        else
        {
            /*
            DPCT1049:31: The work-group size passed to the SYCL kernel may exceed
            the limit. To get the device limit, query
            info::device::max_work_group_size. Adjust the work-group size if needed.
            */
            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(8, 9), cgh);

                cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                        dpcpp_transpose(dst, src, NZ * (NX / 2 + 1), NY,
                            item_ct1, tile_acc_ct1);
                    }); });
        }

        // Y transform
        dpct::fft::fft_engine::destroy(plan);
        n1d[0] = NY;

        plan = dpct::fft::fft_engine::create(Q, 1, n1d, NULL, 1, NY, NULL, 1, NY, Transform,
                                             (NX / 2 + 1) * zsize);
        plan->set_queue(Q);

        plan->compute<sycl::double2, sycl::double2>(src, dst, dpct::fft::fft_direction::backward);

        grid[2] = (ysize * zsize + TILE_DIM - 1) / TILE_DIM;
        grid[1] = (NX / 2 + 1 + TILE_DIM - 1) / TILE_DIM;

        if (geo2 == 0)
        {
            /*
            DPCT1049:33: The work-group size passed to the SYCL kernel may exceed
            the limit. To get the device limit, query
            info::device::max_work_group_size. Adjust the work-group size if needed.
            */
            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(8, 9), cgh);

                cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                    dpcpp_transpose_spread_i(dst, src, ysize * zsize,
                                        NX / 2 + 1, NY / 2,
                                        item_ct1, tile_acc_ct1);
                    }); });
        }
        else
        {
            /*
            DPCT1049:32: The work-group size passed to the SYCL kernel may exceed
            the limit. To get the device limit, query
            info::device::max_work_group_size. Adjust the work-group size if needed.
            */
            Q->submit([&](sycl::handler &cgh)
                      {
                sycl::local_accessor<Complex, 2> tile_acc_ct1(sycl::range<2>(8, 9), cgh);

                cgh.parallel_for(sycl::nd_range<3>(grid * threads, threads),
                    [=](sycl::nd_item<3> item_ct1) 
                    {
                        dpcpp_transpose(dst, src, ysize * zsize, NX / 2 + 1,
                            item_ct1, tile_acc_ct1);
                    }); });
        }

        // X transform
        dpct::fft::fft_engine::destroy(plan);
        n1d[0] = NX;

        plan = dpct::fft::fft_engine::create(Q, 1, n1d, NULL, 1, NX, NULL, 1, NX,
                                             dpct::fft::fft_type::complex_double_to_real_double, ysize * zsize);
        plan->set_queue(Q);

        plan->compute<sycl::double2, double>(src, (double *)dst, dpct::fft::fft_direction::backward);

        nblocks[2] = zsize;
        nblocks[1] = ysize;
        if (geo1 == 0)
        {
            /*
            DPCT1049:34: The work-group size passed to the SYCL kernel may exceed
            the limit. To get the device limit, query
            info::device::max_work_group_size. Adjust the work-group size if needed.
            */
            Q->parallel_for(sycl::nd_range<3>(nblocks * sycl::range<3>(1, 1, NX / 2), sycl::range<3>(1, 1, NX / 2)),
                            [=](sycl::nd_item<3> item_ct1)
                            {
                                dpcpp_spread_i((double *)dst, NX / 2, (double *)src, NX, item_ct1);
                            });
        }

        dpct::fft::fft_engine::destroy(plan);
    }
    catch (std::exception &e)
    {
        std::cout << "ERROR: in dpcpp_3d_psolver_plangeneral\n";
        std::cout << e.what() << std::endl;
        exit(1);
    }
}


/// Function equivalent to cufftdestroy
/// Note, takes a single plan!
void dpcpp_fft_destroy_c2c(mkl_descriptor_complex **plan)
{
    dpcpp_fft_destroy<mkl_descriptor_complex>(plan);
}

void dpcpp_fft_destroy_r2c(mkl_descriptor_real **plan)
{
    dpcpp_fft_destroy<mkl_descriptor_real>(plan);
}
