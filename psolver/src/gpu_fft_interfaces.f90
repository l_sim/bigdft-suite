!> @file
!!  In this file, we define fft interface functions for gpu fft 
!!  operations which choose the correct function to call at runtime based 
!!  on an input parameter (igpu), where igpu == 1 is CUDA, and igpu == 2 is sycl

!! @author
!!    Copyright (C) 2002-2017 BigDFT group  (LG)<br/>
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    Christoph Bauinger



module gpu_fft_interfaces
    use f_precisions, only: f_address, f_double
    use dictionaries, only: f_err_throw
    use iso_c_binding
    use gpu_utils_interfaces
    use time_profiling, only: f_timing
    use dynamic_memory, only: f_routine, f_release_routine
    use PSbase, only: TCAT_PSOLV_COMPUT
    implicit none
    private

    public :: pad_data_interface, unpad_data_interface
    public :: finalize_reduction_kernel_interface
    public :: gpu_3d_psolver_plangeneral_interface, gpu_3d_psolver_general_interface
    public :: gpucreatestream_interface, gpudestroystream_interface
    public :: gpucreateblashandle_interface, gpudestroyblashandle_interface
    public :: destroyfftplans_interface
    public :: gpu_estimate_memory_needs_interface
    public :: gpu_pre_computation_interface, gpu_post_computation_interface
    public :: gpu_accumulate_eexctX_interface
    public :: gpu_3d_psolver_create_plan_interface
    public :: first_reduction_kernel_interface, second_reduction_kernel_interface, third_reduction_kernel_interface



contains
    !> GPU interface to the pad_data function
    subroutine pad_data_interface(igpu, h_data, d_data, m1, m2, m3, md1, md2, md3)
        implicit none
        integer, intent(in) :: igpu, m1, m2, m3, md1, md2, md3
        integer(f_address), intent(in) :: h_data, d_data

        interface
            subroutine dpcpp_pad_data(h_data, d_data, m1, m2, m3, md1, md2, md3) bind(C, name='dpcpp_pad_data')
                use iso_c_binding, only: c_int
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: m1, m2, m3, md1, md2, md3
                integer(f_address), intent(in), value :: h_data, d_data
            end subroutine dpcpp_pad_data
        end interface


        if (igpu == 1) then !cuda case
            call pad_data(h_data, d_data, m1, m2, m3, md1, md2, md3)
        else if (igpu == 2 .or. igpu == 3) then !sycl offload case
            call dpcpp_pad_data(h_data, d_data, m1, m2, m3, md1, md2, md3)
        else
            call f_err_throw('pad_data_interface called with invalid value of igpu')
        end if
    end subroutine pad_data_interface


    !> GPU interface to the unpad_data function
    subroutine unpad_data_interface(igpu, d_data1, d_data2, m1, m2, m3, md1, md2, md3)
        implicit none
        integer, intent(in) :: igpu, m1, m2, m3, md1, md2, md3
        integer(f_address), intent(in) :: d_data1, d_data2

        interface
            subroutine dpcpp_unpad_data(d_data1, d_data2, m1, m2, m3, md1, md2, md3) bind(C, name='dpcpp_unpad_data')
                use iso_c_binding, only: c_int
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: m1, m2, m3, md1, md2, md3
                integer(f_address), intent(in), value :: d_data1, d_data2
            end subroutine dpcpp_unpad_data
        end interface


        if (igpu == 1) then !cuda case
            call unpad_data(d_data1, d_data2, m1, m2, m3, md1, md2, md3)
        else if (igpu == 2 .or. igpu == 3) then !sycl offload case
            call dpcpp_unpad_data(d_data1, d_data2, m1, m2, m3, md1, md2, md3)
        else
            call f_err_throw('unpad_data_interface called with invalid value of igpu')
        end if
    end subroutine unpad_data_interface


    !> GPU interface to the finalize_reduction_kernel function
    subroutine finalize_reduction_kernel_interface(igpu,sumpion,n1,n23,m1,m23,&
            zf_GPU,rho_GPU,pot_ion_GPU,d_odata,oncard_result,result,retrieve)
        implicit none
        integer, intent(in) :: igpu,n1,n23,m1,m23,retrieve
        logical, intent(in) :: sumpion
        integer(f_address), intent(in) :: zf_GPU,rho_GPU,pot_ion_GPU,d_odata,oncard_result
        real(f_double), intent(inout) :: result

        interface
            subroutine dpcpp_finalize_reduction_kernel(sumpion,n1,n23,m1,m23,zf_GPU,rho_GPU,&
                    pot_ion_GPU,d_odata,oncard_result,result,retrieve) bind(C, name='dpcpp_finalize_reduction_kernel')
                use iso_c_binding, only: c_int, c_double, c_bool
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: n1,n23,m1,m23,retrieve
                logical(kind=4), intent(in), value :: sumpion
                integer(f_address), intent(in), value :: zf_GPU,rho_GPU,pot_ion_GPU,d_odata,oncard_result
                real(kind=c_double), intent(inout) :: result
            end subroutine dpcpp_finalize_reduction_kernel
        end interface


        if (igpu == 1) then !cuda case
            call finalize_reduction_kernel(sumpion,n1,n23,m1,m23,zf_GPU,rho_GPU,&
                pot_ion_GPU,d_odata,oncard_result,result,retrieve)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_finalize_reduction_kernel(sumpion,n1,n23,m1,m23,zf_GPU,rho_GPU,&
                pot_ion_GPU,d_odata,oncard_result,result,retrieve)
        else
            call f_err_throw('finalize_reduction_kernel_interface called with invalid value of igpu')
        end if
    end subroutine finalize_reduction_kernel_interface


    !> GPU interface for the 3d psolver 
    subroutine gpu_3d_psolver_plangeneral_interface(igpu, N, d_data, d_data2,&
            d_kernel, geo, scal_p)
        implicit none
        integer, intent(in) :: igpu
        integer, dimension(3), intent(in) :: N, geo
        integer(f_address) :: d_data, d_data2, d_kernel
        real(f_double) :: scal_p

        interface
            subroutine dpcpp_3d_psolver_general_noplan_dbfft(N, d_data, d_data2, d_kernel, &
                    geo, scal_p) bind(C, name='dpcpp_3d_psolver_general_noplan_dbfft')
                use iso_c_binding, only: c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), dimension(3), intent(in) :: N, geo
                integer(f_address), intent(in), value :: d_data, d_data2, d_kernel
                real(kind=c_double), value :: scal_p
            end subroutine dpcpp_3d_psolver_general_noplan_dbfft
        end interface


        if (igpu == 1) then !cuda case
            call cuda_3d_psolver_plangeneral(N, d_data, d_data2, d_kernel, geo, scal_p)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            !call dpcpp_3d_psolver_plangeneral(N, d_data, d_data2, d_kernel, geo, scal_p)
            call dpcpp_3d_psolver_general_noplan_dbfft(N, d_data, d_data2, d_kernel, geo, scal_p)
        else
            call f_err_throw('gpu_3d_psolver_plangeneral called with invalid value of igpu')
        end if
    end subroutine gpu_3d_psolver_plangeneral_interface


    !> GPU interface for the 3d psolver 
    subroutine gpu_3d_psolver_general_interface(igpu, N, plan, d_data, d_data2,&
        d_kernel, switch_alg, geo, scal_p)
        implicit none
        integer, intent(in) :: igpu, switch_alg
        integer, dimension(3), intent(in) :: N, geo
        integer(kind=c_intptr_t), dimension(5), intent(in) :: plan 
        integer(f_address) :: d_data, d_data2, d_kernel
        real(f_double) :: scal_p

        interface
            subroutine dpcpp_3d_psolver_general(N, plan, d_data, d_data2, d_kernel, &
                    switch_alg, geo, scal_p) bind(C, name='dpcpp_3d_psolver_general')
                use iso_c_binding, only: c_int, c_double, c_intptr_t
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: switch_alg
                integer(kind=c_int), dimension(3), intent(in) :: N, geo
                integer(kind=c_intptr_t), dimension(5), intent(in) :: plan 
                integer(f_address), intent(in), value :: d_data, d_data2, d_kernel
                real(kind=c_double), intent(in), value :: scal_p
            end subroutine dpcpp_3d_psolver_general
        end interface

        interface
            subroutine dpcpp_3d_psolver_general_dbfft(N, plan, d_data, d_data2, d_kernel, &
                    switch_alg, geo, scal_p) bind(C, name='dpcpp_3d_psolver_general_dbfft')
                use iso_c_binding, only: c_int, c_double, c_intptr_t
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: switch_alg
                integer(kind=c_int), dimension(3), intent(in) :: N, geo
                integer(kind=c_intptr_t), dimension(5), intent(in) :: plan 
                integer(f_address), intent(in), value :: d_data, d_data2, d_kernel
                real(kind=c_double), intent(in), value :: scal_p
            end subroutine dpcpp_3d_psolver_general_dbfft
        end interface

        
        call f_routine(id='gpu_3d_psolver_general_interface')
        call f_timing(TCAT_PSOLV_COMPUT,'ON')

        if (igpu == 1) then !cuda case
            call cuda_3d_psolver_general(N, plan, d_data, d_data2, d_kernel, &
                switch_alg, geo, scal_p)
        else if (igpu == 2) then !sycl gpu case
            !call dpcpp_3d_psolver_general(N, plan, d_data, d_data2, d_kernel, &
            !    switch_alg, geo, scal_p)
            call dpcpp_3d_psolver_general_dbfft(N, plan, d_data, d_data2, d_kernel, &
                switch_alg, geo, scal_p)
            call synchronize_interface(igpu) !required for the dbfft version. Not entirely clear why.
        else if (igpu == 3) then !sycl cpu case
            !call dpcpp_3d_psolver_general(N, plan, d_data, d_data2, d_kernel, &
            !    switch_alg, geo, scal_p)
            call dpcpp_3d_psolver_general_dbfft(N, plan, d_data, d_data2, d_kernel, &
                switch_alg, geo, scal_p)
        else
            call f_err_throw('gpu_3d_psolver_general called with invalid value of igpu')
        end if

        call f_timing(TCAT_PSOLV_COMPUT,'OF')
        call f_release_routine()
    end subroutine gpu_3d_psolver_general_interface


    !> Generate cuda stream. Do nothing in OMP case and maybe a queue? in sycl
    subroutine gpucreatestream_interface(igpu)
        implicit none
        integer, intent(in) :: igpu
        integer :: i_stat

        interface
            subroutine create_queue() bind(c, name='create_queue')
                implicit none
            end subroutine create_queue
        end interface


        i_stat = 0

        if (igpu == 1) then !cuda case
            call cudacreatestream(i_stat)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            !do nothing since there are no streams.
            !call create_queue()
        else
            call f_err_throw('gpucreatestream_interface called with invalid value of igpu')
        end if

        if (i_stat /= 0) call f_err_throw('Error in gpucreatestream_interface')
    end subroutine gpucreatestream_interface
    
    
    !> Destroy cuda stream. Do nothing in OMP case and maybe? destroy queue in sycl
    subroutine gpudestroystream_interface(igpu)
        implicit none
        integer, intent(in) :: igpu
        integer :: i_stat

        interface
            subroutine destroy_queue() bind(c, name='destroy_queue')
                implicit none
            end subroutine destroy_queue
        end interface


        i_stat = 0

        if (igpu == 1) then !cuda case
            call cudadestroystream(i_stat)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call destroy_queue()
        else
            call f_err_throw('gpudestroystream_interface called with invalid value of igpu')
        end if

        if (i_stat /= 0) call f_err_throw('Error in gpudestroystream_interface')
    end subroutine gpudestroystream_interface


    !> create blas handle. Cublas in cuda case, mkl blas in sycl and omp case
    subroutine gpucreateblashandle_interface(igpu)
        implicit none
        integer, intent(in) :: igpu


        if (igpu == 1) then !cuda case
            call cudacreatecublashandle()
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            !Nothing to be done here since we do not have cublas handle 
            !in sycl version
            !print *, "*Calling gpucreateblashandle_interface"
        else
            call f_err_throw('gpucreateblashandle_interface called with invalid value of igpu')
        end if
    end subroutine gpucreateblashandle_interface 
    
    
    !> destroy the blas handle
    subroutine gpudestroyblashandle_interface(igpu)
        implicit none
        integer, intent(in) :: igpu


        if (igpu == 1) then !cuda case
            call cudadestroycublashandle()
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            !Nothing to be done here since sycl has no cublas handles
            !print *, "*Calling gpudestroyblashandle_interface"
        else
            call f_err_throw('gpudestroyblashandle_interface called with invalid value of igpu')
        end if
    end subroutine gpudestroyblashandle_interface


    !> interface to destroy fft plans
    !> Note that this takes a single pointer to a single plan
    subroutine destroyfftplans_interface(igpu, plans)
        implicit none
        integer, intent(in) :: igpu
        integer(kind=c_intptr_t), dimension(5), intent(inout) :: plans

        interface
            subroutine dpcpp_fft_destroy_c2c(plan) bind(C, name='dpcpp_fft_destroy_c2c')
                use iso_c_binding, only: c_intptr_t
                implicit none
                integer(kind=c_intptr_t), intent(inout) :: plan
            end subroutine dpcpp_fft_destroy_c2c
        end interface

        interface
            subroutine dpcpp_fft_destroy_r2c(plan) bind(C, name='dpcpp_fft_destroy_r2c')
                use iso_c_binding, only: c_intptr_t
                implicit none
                integer(kind=c_intptr_t), intent(inout) :: plan
            end subroutine dpcpp_fft_destroy_r2c
        end interface

        interface
            subroutine dpcpp_fft_destroy_dbfft(plans) bind(C, name='dpcpp_fft_destroy_dbfft')
                use iso_c_binding, only: c_intptr_t
                implicit none
                integer(kind=c_intptr_t), dimension(5), intent(inout) :: plans
            end subroutine dpcpp_fft_destroy_dbfft
        end interface


        if (igpu == 1) then !cuda case
            if (plans(1) /= 0) call cufftdestroy_ptr(plans(1))
            if (plans(2) /= 0) call cufftdestroy_ptr(plans(2))
            if (plans(3) /= 0) call cufftdestroy_ptr(plans(3))
            if (plans(4) /= 0) call cufftdestroy_ptr(plans(4))
            if (plans(5) /= 0) call cufftdestroy_ptr(plans(5))
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            !Note that the type of plan is implicitly defined in 
            !dpcppfft.cpp dpcpp_3d_psolver_general_plan function
            !Note that the two different destroy functions
            !are simply for the sake of ease of readability and to have the simplest
            !possible interface between C++ and Fortran
            ! call dpcpp_fft_destroy_r2c(plans(1))
            ! call dpcpp_fft_destroy_c2c(plans(3))
            ! call dpcpp_fft_destroy_c2c(plans(4))
            call dpcpp_fft_destroy_dbfft(plans);
        else
            call f_err_throw('destroyfftplans_interface called with invalid value of igpu')
        end if
    end subroutine destroyfftplans_interface


    !> interface function for memory needs estimation
    subroutine gpu_estimate_memory_needs_interface(igpu, iproc, n, geo, plansSize, &
            maxPlanSize, freeGPUSize, totalGPUSize)
        !use onemklfft, only : onemkl_estimate_memory_needs
        use iso_c_binding, only : c_size_t

        implicit none
        integer, intent(in) :: igpu, iproc
        integer,dimension(3), intent(in) :: n, geo
        integer(kind=c_size_t), intent(inout) :: maxPlanSize, freeGPUSize, totalGPUSize
        integer(kind=8), intent(inout) :: plansSize

        interface
            subroutine dpcpp_estimate_memory_needs(iproc, n, geo, plansSize,&
                    maxPlanSize, freeGPUSize, totalGPUSize) bind(C, name='dpcpp_estimate_memory_needs')
                use iso_c_binding, only: c_size_t, c_int
                implicit none
                integer(kind=c_int), intent(in), value :: iproc
                integer(kind=c_int),dimension(3), intent(in) :: n, geo
                integer(kind=c_size_t), intent(inout) :: plansSize, maxPlanSize, &
                    freeGPUSize, totalGPUSize
            end subroutine dpcpp_estimate_memory_needs
        end interface


        if (igpu == 1) then !cuda case
            call cuda_estimate_memory_needs_cu(iproc, n, geo, plansSize, maxPlanSize, &
                freeGPUSize, totalGPUSize)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_estimate_memory_needs(iproc, n, geo, plansSize, maxPlanSize, &
                freeGPUSize, totalGPUSize)
            !call onemkl_estimate_memory_needs(iproc, n, geo, plansSize, maxPlanSize, freeGPUSize, totalGPUSize)
        else
            call f_err_throw('gpu_estimate_memory_needs_interface called with invalid value of igpu')
        end if
    end subroutine gpu_estimate_memory_needs_interface


    !> interface function for the gpu precomputation
    subroutine gpu_pre_computation_interface(igpu, NX_p, NY_p, NZ_p, rho_GPU, &
            data1_GPU, shift1, data2_GPU, shift2, hfac)
        implicit none
        integer, intent(in) :: igpu, NX_p, NY_p, NZ_p, shift1, shift2
        integer(f_address) :: rho_GPU
        type(c_ptr) :: data1_GPU, data2_GPU
        real(f_double) :: hfac

        interface
            subroutine dpcpp_pre_computation(NX_p, NY_p, NZ_p, rho_GPU, &
                    data1_GPU, shift1, data2_GPU, shift2, hfac) bind(C, name='dpcpp_pre_computation')
                use iso_c_binding, only: c_ptr, c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: NX_p, NY_p, NZ_p, shift1, shift2
                integer(f_address), value :: rho_GPU
                type(c_ptr), value :: data1_GPU, data2_GPU
                real(kind=c_double), value :: hfac
            end subroutine dpcpp_pre_computation
        end interface

        if (igpu == 1) then !cuda case
            call gpu_pre_computation(NX_p, NY_p, NZ_p, rho_GPU, data1_GPU, shift1, &
                data2_GPU, shift2, hfac)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_pre_computation(NX_p, NY_p, NZ_p, rho_GPU, data1_GPU, shift1, &
                data2_GPU, shift2, hfac)
        else
            call f_err_throw('gpu_pre_computation_interface called with invalid value of igpu')
        end if
    end subroutine gpu_pre_computation_interface


    !> interface function for the gpu precomputation
    subroutine gpu_post_computation_interface(igpu, NX_p, NY_p, NZ_p, rho_GPU, &
            data1_GPU, shift1, data2_GPU, shift2, hfac)
        implicit none
        integer, intent(in) :: igpu, NX_p, NY_p, NZ_p, shift1, shift2
        integer(f_address) :: rho_GPU
        type(c_ptr) :: data1_GPU, data2_GPU
        real(f_double) :: hfac

        interface
            subroutine dpcpp_post_computation(NX_p, NY_p, NZ_p, rho_GPU, data1_GPU, &
                    shift1, data2_GPU, shift2, hfac) bind(C, name='dpcpp_post_computation')
                use iso_c_binding, only: c_ptr, c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: NX_p, NY_p, NZ_p, shift1, shift2
                integer(f_address), value :: rho_GPU
                type(c_ptr), value :: data1_GPU, data2_GPU
                real(kind=c_double), intent(in), value :: hfac
            end subroutine dpcpp_post_computation
        end interface


        if (igpu == 1) then !cuda case
            call gpu_post_computation(NX_p, NY_p, NZ_p, rho_GPU, data1_GPU, &
                shift1, data2_GPU, shift2, hfac)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_post_computation(NX_p, NY_p, NZ_p, rho_GPU, data1_GPU, &
                shift1, data2_GPU, shift2, hfac)
        else
            call f_err_throw('gpu_post_computation_interface called with invalid value of igpu')
        end if
    end subroutine gpu_post_computation_interface


    !> interface for the gpu accumulate eexctX function
    subroutine gpu_accumulate_eexctX_interface(igpu, ehart_GPU, eexctX_GPU, hfac)
        implicit none
        integer, intent(in) :: igpu
        integer(f_address), intent(in) :: ehart_GPU, eexctX_GPU
        real(f_double) :: hfac

        interface
            subroutine dpcpp_accumulate_eexctx(ehart_GPU, eexctX_GPU, &
                    hfac) bind(C, name='dpcpp_accumulate_eexctx')
                use iso_c_binding, only: c_double
                use f_precisions, only: f_address
                implicit none
                integer(f_address), intent(in), value :: ehart_GPU, eexctX_GPU
                real(kind=c_double), intent(in), value :: hfac
            end subroutine dpcpp_accumulate_eexctx
        end interface


        if (igpu == 1) then !cuda case
            call gpu_accumulate_eexctX(ehart_GPU, eexctX_GPU, hfac)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_accumulate_eexctx(ehart_GPU, eexctX_GPU, hfac)
        else
            call f_err_throw('gpu_accumulate_eexctX_interface invalid value of igpu')
        end if
    end subroutine gpu_accumulate_eexctX_interface


    !> interface for the 3d plan generation
    subroutine gpu_3d_psolver_create_plan_interface(igpu, N, plan, switch_alg, geo)
        implicit none
        integer, intent(in) :: igpu
        integer, intent(inout) :: switch_alg
        integer, dimension(3), intent(in) :: N, geo
        integer(kind=c_intptr_t), dimension(5), intent(inout) :: plan

        interface
            subroutine dpcpp_3d_psolver_general_plan(N, plan, switch_alg, &
                    geo) bind(C, name='dpcpp_3d_psolver_general_plan')
                use iso_c_binding, only: c_int, c_intptr_t
                implicit none
                integer(kind=c_int), intent(inout) :: switch_alg
                integer(kind=c_int), dimension(3), intent(in) :: N, geo
                integer(kind=c_intptr_t), dimension(5), intent(inout) :: plan
            end subroutine dpcpp_3d_psolver_general_plan
        end interface

        interface
            subroutine dpcpp_3d_psolver_generate_dbfftplans(N, plan, switch_alg, &
                    geo) bind(C, name='dpcpp_3d_psolver_generate_dbfftplans')
                use iso_c_binding, only: c_int, c_intptr_t
                implicit none
                integer(kind=c_int), intent(inout) :: switch_alg
                integer(kind=c_int), dimension(3), intent(in) :: N, geo
                integer(kind=c_intptr_t), dimension(5), intent(inout) :: plan
            end subroutine dpcpp_3d_psolver_generate_dbfftplans
        end interface


        if (igpu == 1) then !cuda case
            call cuda_3d_psolver_general_plan(N, plan, switch_alg, geo)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            !call dpcpp_3d_psolver_general_plan(N, plan, switch_alg, geo)
            call dpcpp_3d_psolver_generate_dbfftplans(N, plan, switch_alg, geo)
        else
            call f_err_throw('gpu_3d_psolver_create_plan_interface with invalid value of igpu')
        end if
    end subroutine gpu_3d_psolver_create_plan_interface


    !> interface for first reduction kernel
    subroutine first_reduction_kernel_interface(igpu,n1,n23,p,q,r,x,z,corr,oneoeps,&
            alpha,beta,beta0,kappa,d_odata,result)
        implicit none
        integer, intent(in) :: igpu, n1, n23
        integer(f_address) :: p, q, r, x, z, corr, oneoeps, alpha, beta, beta0, kappa, d_odata
        real(f_double), intent(out) :: result

        interface
            subroutine dpcpp_first_reduction_kernel(n1,n23,p,q,r,x,z,corr,oneoeps,alpha,beta,&
                    beta0,kappa,d_odata,result) bind(C, name='dpcpp_first_reduction_kernel')
                use iso_c_binding, only: c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: n1, n23
                integer(f_address), value :: p, q, r, x, z, corr, oneoeps, alpha, &
                    beta, beta0, kappa, d_odata
                real(kind=c_double), intent(out) :: result
            end subroutine dpcpp_first_reduction_kernel
        end interface


        if (igpu == 1) then !cuda case
            call first_reduction_kernel(n1,n23,p,q,r,x,z,corr,oneoeps,alpha,beta,&
                beta0,kappa,d_odata,result)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_first_reduction_kernel(n1,n23,p,q,r,x,z,corr,oneoeps,alpha,beta,&
                beta0,kappa,d_odata,result)
        else
            call f_err_throw('first_reduction_kernel_interface called with invalid value of igpu')
        end if
    end subroutine first_reduction_kernel_interface


    !> interface for first reduction kernel
    subroutine second_reduction_kernel_interface(igpu,n1,n23,p,q,r,x,z,corr,oneoeps,&
            alpha,beta,beta0,kappa,d_odata,result)
        implicit none
        integer, intent(in) :: igpu, n1, n23
        integer(f_address) :: p, q, r, x, z, corr, oneoeps, alpha, beta, beta0, kappa, d_odata
        real(f_double), intent(out) :: result

        interface
            subroutine dpcpp_second_reduction_kernel(n1,n23,p,q,r,x,z,corr,oneoeps,alpha,beta,&
                beta0,kappa,d_odata,result) bind(C, name='dpcpp_second_reduction_kernel')
                use iso_c_binding, only: c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: n1, n23
                integer(f_address), value :: p, q, r, x, z, corr, oneoeps, alpha, beta, beta0, kappa, d_odata
                real(kind=c_double), intent(out) :: result
            end subroutine dpcpp_second_reduction_kernel
        end interface


        if (igpu == 1) then !cuda case
            call second_reduction_kernel(n1,n23,p,q,r,x,z,corr,oneoeps,alpha,beta,&
                beta0,kappa,d_odata,result)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_second_reduction_kernel(n1,n23,p,q,r,x,z,corr,oneoeps,alpha,beta,&
                beta0,kappa,d_odata,result)
        else
            call f_err_throw('second_reduction_kernel_interface called with invalid value of igpu')
        end if
    end subroutine second_reduction_kernel_interface


    !> interface for first reduction kernel
    subroutine third_reduction_kernel_interface(igpu,n1,n23,p,q,r,x,z,corr,oneoeps,&
            alpha,beta,beta0,kappa,d_odata,result)
        implicit none
        integer, intent(in) :: igpu, n1, n23
        integer(f_address) :: p, q, r, x, z, corr, oneoeps, alpha, beta, beta0, kappa, d_odata
        real(f_double), intent(out) :: result

        interface
            subroutine dpcpp_third_reduction_kernel(n1,n23,p,q,r,x,z,corr,oneoeps,alpha,beta,&
                beta0,kappa,d_odata,result) bind(C, name='dpcpp_third_reduction_kernel')
                use iso_c_binding, only: c_int, c_double
                use f_precisions, only: f_address
                implicit none
                integer(kind=c_int), intent(in), value :: n1, n23
                integer(f_address), value :: p, q, r, x, z, corr, oneoeps, alpha, beta, beta0, kappa, d_odata
                real(kind=c_double), intent(out) :: result
            end subroutine dpcpp_third_reduction_kernel
        end interface


        if (igpu == 1) then !cuda case
            call third_reduction_kernel(n1,n23,p,q,r,x,z,corr,oneoeps,alpha,beta,&
                beta0,kappa,d_odata,result)
        else if (igpu == 2 .or. igpu == 3) then !sycl case
            call dpcpp_third_reduction_kernel(n1,n23,p,q,r,x,z,corr,oneoeps,alpha,beta,&
                beta0,kappa,d_odata,result)
        else
            call f_err_throw('third_reduction_kernel_interface called with invalid value of igpu')
        end if
    end subroutine third_reduction_kernel_interface


end module gpu_fft_interfaces