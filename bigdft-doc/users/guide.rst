User Guide
==========

In this page you may found different tutorials and exercises that will help you
in the usage of BigDFT. The BigDFT code is a standard executable that can
be run from the command line by passing an input file. However, the preferred
way to drive BigDFT calculations is from its high level PyBigDFT interface.
PyBigDFT will allow you to construct the system you wish to study, perform
calculations with the BigDFT code, and extract scientific data from the
results.

Jupyter notebooks are interactive lab notebooks that can run python code.
Each of the tutorials presented here is built as a jupyter notebook. We
encourage you to test out these notebooks on your own computer and to
build on top of them by copying one from the ``bigdft-doc`` folder of the code.

Environment Variables
---------------------

Before running the code, you will want to configure your system path to
include the bigdft executable and libraries. This can be accomplished with:

.. code:: bash

    source install/bin/bigdftvars.sh

Before running the code, you might want to manually set a few more variables
manually. The variable ``BIGDFT_MPIRUN`` is the mpirun command you wish to
use (i.e. ``mpirun -np 2``). ``OMP_NUM_THREADS`` should also be set if you
wish to enable multithreading.
Notice that in the case of a execution triggered from a Jupyter notebook
the above command should be issued before the opening of the notebook session.

BigDFT Training Lessons
-----------------------
One way to start is to go through the lessons we prepared for CCPBioSim. These
lessons are available in colab notebook form:

 1. Go here with your google account: https://colab.research.google.com/
 2. File-> Open Notebook - > GitHub
 3. `BigDFT-group/bigdft-school`
 4. Start from the Notebook `0.Installation.ipynb`

Tutorials
---------

.. toctree::
   :maxdepth: 1
   /school/QuickStart.ipynb

This is the older set of tutorials.

Building Systems Programmatically
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

 .. toctree::
   :maxdepth: 1

   /tutorials/Systems.ipynb
   /tutorials/IO.ipynb
   /tutorials/Rototranslations.ipynb

Running The Code
^^^^^^^^^^^^^^^^

 .. toctree::
   :maxdepth: 1

   /tutorials/N2.ipynb
   /tutorials/N2-solution.ipynb
   /tutorials/CH4.ipynb
   /tutorials/Datasets.ipynb
   /tutorials/SolidState.ipynb
   /tutorials/CH4_aiida.ipynb

Examining and postprocessing the output
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

 .. toctree::
   :maxdepth: 1

   /tutorials/Logfile-basics.ipynb
   /tutorials/DoS-Manipulation.ipynb
   /tutorials/PDoS.ipynb
   /tutorials/Tight-Binding.ipynb

Interoperability With Other Codes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

 .. toctree::
   :maxdepth: 1

   /tutorials/Interoperability-Visualization.ipynb
   /tutorials/Interoperability-Simulation.ipynb



Lessons and Workflows
---------------------

Last we share some in depth lessons which put BigDFT into practice.

 .. toctree::
   :maxdepth: 1

   /lessons/GeometryOptimization.ipynb
   /lessons/MolecularDynamics.ipynb
   /lessons/ComplexityReduction.ipynb
   /lessons/Gaussian.ipynb
   /lessons/MachineLearning.ipynb
